#!/bin/bash

if test "$1" == ""
then
    echo "Syntax error: $0 <listname>"
    exit 1
fi

listname="$1"
webpage="https://sympa.inria.fr/sympa/arc/$listname"

function check()
{
    wget -q -O /tmp/$$.html $webpage/index.html
    x=$(grep ERREUR /tmp/$$.html)
    if test -n "$x"
    then
	echo "Error. List $listname invalid (URL $webpage/index.html not found)"
	exit 1
    fi
}

function getNextMonth()
{
    year=$(echo $1 | cut -f1 -d'-')
    year1=$(echo $year + 1 | bc -l)
    month=$(echo $1 | cut -f2 -d'-')
    if test "$month" == "01" ; then echo "$year-02" ; fi
    if test "$month" == "02" ; then echo "$year-03" ; fi
    if test "$month" == "03" ; then echo "$year-04" ; fi
    if test "$month" == "04" ; then echo "$year-05" ; fi
    if test "$month" == "05" ; then echo "$year-06" ; fi
    if test "$month" == "06" ; then echo "$year-07" ; fi
    if test "$month" == "07" ; then echo "$year-08" ; fi
    if test "$month" == "08" ; then echo "$year-09" ; fi
    if test "$month" == "09" ; then echo "$year-10" ; fi
    if test "$month" == "10" ; then echo "$year-11" ; fi
    if test "$month" == "11" ; then echo "$year-12" ; fi
    if test "$month" == "12" ; then echo "$year1-01" ; fi
}

function buildMessageList()
{
    yearMonth="2010-01"
    if test -f $list
    then
	yearMonth=$(tail -1 $list | awk -F'/' '{print $7}')
	f=$(mktemp)
	grep -v $yearMonth $list > $f
	mv $f $list
    fi

    output=$(mktemp -d)
    endOfTime=$(LC_ALL=C date "+%Y-%m" --date='next Month')

    begin=0
    end=99999

    while true
    do
	echo Testing $yearMonth
	mkdir $output/$yearMonth
	for n in $(seq -w $begin $end)
	do
	    wget -q -P $output/$yearMonth $webpage/$yearMonth/msg$n.html
	    x1=$(grep month_not_found $output/$yearMonth/msg$n.html)
	    x2=$(grep arc_not_found $output/$yearMonth/msg$n.html)
	    if test -n "$x1" -o -n "$x2"
	    then
		rm -f $output/$yearMonth/msg$n.html
		if test $n -gt 50
		then
		    break
		fi
	    else
		title=$(grep --binary-files=text "<title>" $output/$yearMonth/msg$n.html | sed -e 's/<title> '$listname' - //' -e 's/ - arc <\/title>//' -e 's/\[.tarpu\-devel\]//' -e 's/\[Morse\-devel\]//' -e 's/\[.tarpu\-announce\]//'  | tr '\t' ' ' | tr -s ' ')
		echo "$webpage/$yearMonth/msg$n.html&$title" | tee -a $list
	    fi
	done
	yearMonth=$(getNextMonth $yearMonth)
	if test "$yearMonth" == "$endOfTime"
	then
	    break
	fi
    done
}

function buildWebPage()
{
    count_log=$(mktemp)
    title_list=$(mktemp)
    titles_list=$(mktemp -d)
    declare -A titles
    titles=()
    count=1

    cat $list | while read m
    do
	url=$(echo $m | awk -F'&' '{print $1}')
	title=$(echo $m | awk -F'&' '{for(i=2;i<=NF;i++) print $i}' | tr '\012' ' ' | sed 's/^Re://')
	if test "$title" != "<title> "
	then
	    if test "${titles[$title]}" == ""
	    then
		#echo new title "'"$title"'" count $count
		titles[$title]=$count

		(
		    echo "## $title"
		) > $titles_list/$count.md

		echo $count > $count_log
		count=$(( count + 1 ))
	    fi
	    echo "- <$url>" >> $titles_list/${titles["$title"]}.md
	fi
    done

    count=$(cat $count_log)
    for c in $(seq $count -1 1)
    do
	cat $titles_list/$c.md
    done > $archive_contents

    #    for title in "${!titles[@]}"
    #    do
    #	echo "title <"$title">"
    #    done
    #    for t in "${titles[@]}"
    #    do
    #	echo "title <" $t "> <" ${titles["$t"]} ">"
    #    done
}

function buildFinalPage()
{
    echo "---"
    echo "layout: default"
    echo "title: Getting Help. Local archive for list ${listname}"
    echo "permalink: help/archive_${listname}.html"
    echo "---"
    echo "# StarPU: Getting Help. Local archive for list $listname"
    echo
    cat $archive_contents
    rm -f $archive_contents
}

check

list=./archives/$listname/message_list
archive_contents=./archives/$listname/archive_contents_${listname}.md
archive=./archives/$listname/archive_${listname}.md
mkdir -p ./archives/$listname

buildMessageList
buildWebPage
buildFinalPage > $archive

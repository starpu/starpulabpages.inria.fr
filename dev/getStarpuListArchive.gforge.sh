#!/bin/bash

if test "$1" == ""
then
    echo "Syntax error: $0 <listname>"
    exit 1
fi

begin=0
end=999999
web="https://lists.gforge.inria.fr/pipermail"
listname="$1"
webpage="${web}/${listname}"

wget -q -O /tmp/$$.html $webpage/index.html
if test ! -s /tmp/$$.html
then
    echo "Error. List $listname invalid (URL $webpage/index.html not found)"
    exit 1
fi

function getNextMonth()
{
    year=$(echo $1 | cut -f1 -d'-')
    year1=$(echo $year + 1 | bc -l)
    month=$(echo $1 | cut -f2 -d'-')
    if test "$month" == "January" ; then echo "$year-February" ; fi
    if test "$month" == "February" ; then echo "$year-March" ; fi
    if test "$month" == "March" ; then echo "$year-April" ; fi
    if test "$month" == "April" ; then echo "$year-May" ; fi
    if test "$month" == "May" ; then echo "$year-June" ; fi
    if test "$month" == "June" ; then echo "$year-July" ; fi
    if test "$month" == "July" ; then echo "$year-August" ; fi
    if test "$month" == "August" ; then echo "$year-September" ; fi
    if test "$month" == "September" ; then echo "$year-October" ; fi
    if test "$month" == "October" ; then echo "$year-November" ; fi
    if test "$month" == "November" ; then echo "$year-December" ; fi
    if test "$month" == "December" ; then echo "$year1-January" ; fi
}

list=./message_list
title_list=./title_list
archive_contents=./archive_contents_${listname}.html
archive=./archive_${listname}.html
titles_list=./titles_list

function buildMessageList()
{
    output=$(mktemp -d)
    rm -f $list
    rm -f $title_list
    yearMonth="2010-March"
    endOfTime=$(LC_ALL=C date "+%Y-%B" --date='next Month')

    for n in $(seq -w $begin $end)
    do
	#echo "Testing message $n"
	while true
	do
	    #echo "getting $webpage/$yearMonth/$n.html"
	    wget -q -P $output $webpage/$yearMonth/$n.html
	    if test ! -f $output/$n.html
	    then
		yearMonth=$(getNextMonth $yearMonth)
		if test "$yearMonth" == "$endOfTime"
		then
		    break
		fi
	    else
		echo "$webpage/$yearMonth/$n.html" | tee -a $list
		grep --binary-files=text "<TITLE>" $output/$n.html | sed 's/<TITLE>//' | sed 's/\[Starpu\-devel\]//' | sed 's/\[Morse\-devel\]//' | sed 's/\[Starpu\-announce\]//' | tr '\t' ' ' | tr -s ' ' >> $title_list
	    fi
	    if test -f $output/$n.html
	    then
		break
	    fi
	done
	if test "$yearMonth" == "$endOfTime"
	then
	    break
	fi
    done

    rm -rf $output/*
}

function buildWebPage()
{
    mkdir -p $titles_list
    rm -f $titles_list/*
    currentTitle=""
    declare -A titles
    titles=()
    count=1
    countDown=$(cat $title_list | sort | uniq | wc -l)
    for m in $(tac $list)
    do
	wget -q -O xxx.html $m
	title=$(grep --binary-files=text "<TITLE>" xxx.html | sed 's/<TITLE>//' | sed 's/\[Starpu\-devel\]//' | sed 's/\[Morse\-devel\]//' | sed 's/\[Starpu\-announce\]//' | tr '\t' ' ' | tr -s ' ')
	if test "${titles[$title]}" == ""
	then
	    #echo new title $title
	    titles[$title]=$count

	    (
		echo "<div class=\"section\" id=\"title$countDown\">"
		echo "<h4>$title</h4>"
		echo "<ul>"
	    ) > $titles_list/$count.html

	    count=$(( count + 1 ))
	    countDown=$(( countDown - 1 ))
	fi
	echo "<li><a href=\"$m\">$m</a></li>" >> $titles_list/${titles["$title"]}.html
    done
    rm -f xxx.html
    count=$(( count - 1 ))
    for c in $(seq 1 $count)
    do
	cat $titles_list/$c.html
	echo "</ul>"
	echo "</div>"
    done
    #    for title in "${!titles[@]}"
    #    do
    #	echo "title <"$title">"
    #    done
    #    for t in "${titles[@]}"
    #    do
    #	echo "title <" $t "> <" ${titles["$t"]} ">"
    #    done
}

function buildFinalPage()
{
    date=$(date "+%Y/%m/%d")
    cat ./head.html
    cat $archive_contents
    echo "<div class=\"section bot\">"
    echo "<p class=\"updated\">"
    echo "Last updated on $date."
    echo "</p>"
    echo "</body>"
    echo "</html>"
}

function clean()
{
    rm -f $list
    rm -f $title_list
    rm -rf $titles_list
    rm -f $archive_contents
}

buildMessageList
buildWebPage > $archive_contents
buildFinalPage > $archive
clean

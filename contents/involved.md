---
layout: default
title: Contributing
permalink: involved.html
full-width: True
---

# Contributing

## Introduction
For a small issue, you may just want to email [starpu-devel@inria.fr](mailto:starpu-devel@inria.fr)

For a merge request or for contributing in a specific branch, you can
submit it on <https://gitlab.inria.fr/>.  Inria members can
login directly with their Inria login in the iLDAP tab on this [page](https://gitlab.inria.fr/users/sign_in).
External users need to request an external account from an Inria
member (email [starpu-devel@inria.fr](mailto:starpu-devel@inria.fr)). When the account is
created, you can login at the Standard tab on this [page](https://gitlab.inria.fr/users/sign_in).

Merge requests can also be submitted on github, on this [page](https://github.com/starpu-runtime/starpu/pulls)

## Submit an issue
You can create a new issue on this [page](https://gitlab.inria.fr/starpu/starpu/-/issues). Please give as many details
as possible (feature, fix, ...), possibly a patch. We will either
directly fix the issue in the master branch, or create a dedicated
branch for you to work on, and eventually perform a merge request.

StarPU also has a repository on github, you can also submit issues on
this [page](https://github.com/starpu-runtime/starpu/issues).

## Contributing through a merge request
### Create a fork

First you need to fork the repository into your own account by
clicking on <https://gitlab.inria.fr/starpu/starpu/-/forks/new>

Then, clone the repository on your laptop:

``` sh
git clone git@gitlab.inria.fr:username/starpu.git
```

Once this is done, you can setup the StarPU repository as the upstream
of your clone to simplify the update of your fork repository.

``` sh
git remote add upstream git@gitlab.inria.fr:starpu/starpu.git
```

Note: If you do not have an Inria gitlab account, you can fork from
the github mirror at <https://github.com/starpu-runtime/starpu/>, clone
`git@github.com:username/starpu.git` and add
`git@github.com:starpu-runtime/starpu.git` as upstream.

To update your fork with the upstream StarPU's state:
``` sh
git pull upstream master
git push -u origin master
```

Create a /Feature/ branch in your fork. To add a new feature, fix a
bug, and so on, you need to create a new branch from the last state of
the master branch.

``` sh
git branch feature
git checkout feature
```

Apply your modifications in that /Feature/ branch. Then, you need to
push this branch on your online repository.

``` sh
git push origin feature
```

### Merge request

Once your branch is online, on the gitlab or github interface, go to the
`repository/branches` webpage, click on the `merge request` button
next to your branch.

If the pull request is made to fix an issue, please name the branch
`issueXX`, so it is automatically linked to the issue. In addition,
please add `fix issue #xx` in the comment of the merge request to
automatically close the issue when the PR is merged.

Before merging your branch, you will probably need to rebase the
master onto it. We actually recommand to rebase the master in your
branch very regularly, so as to avoid any potential conflicts.

To do that, just go at the HEAD of your /Feature/ branch and rebase:

``` sh
git checkout feature
git rebase master
```

Then force to push on your origin

``` sh
git push --force origin feature
```

## Configure a runner to test your branch

To be effectively merged, your branch must be tested through the
`gitlab-ci` mechanism. This will be done automatically if you create a
merge request on gitlab. Otherwise, you need to follow the
instructions below.

To execute the tests, you should define a gitlab runner, running
either on your laptop or any other remote machine. StarPU provides a
Docker image `registry.gitlab.inria.fr/starpu/docker/ci` whose
recipe is defined at <https://gitlab.inria.fr/starpu/starpu-docker>. This
project shows a minimal [https://gitlab.inria.fr/starpu/starpu-docker/-/blob/main/.gitlab-ci.yml](pipeline) running this docker image to test StarPU.

To register a compatible runner, the requirements on the system are :

- OS must be Linux

- Docker must be installed, e.g.

``` sh
sudo apt-get update && sudo apt-get install -y curl
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo apt install -y software-properties-common
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
sudo apt-get update
sudo apt install -y docker-ce
sudo usermod -aG docker ${USER}
newgrp docker
```

- Register your runner

  Please read first the Gitlab documentation for general information about runners registration.

  Three steps are required:
    - install the gitlab-runner program
      ``` sh
      # install gitlab-runner
      sudo wget -O /usr/local/bin/gitlab-runner https://gitlab-ci-multi-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-ci-multi-runner-linux-amd64
      sudo chmod +x /usr/local/bin/gitlab-runner
      sudo useradd --comment 'GitLab Runner' --create-home gitlab-runner --shell /bin/bash
      ```
    - register your runner to your project (your fork of StarPU)
      ``` sh
      # register runner to https://gitlab.inria.fr/
      sudo gitlab-runner register # see just after for an example
      ```
    - start gitlab-runner as a service
      ``` sh
      # install and run as a service
      sudo gitlab-runner install --user=gitlab-runner --working-directory=/home/gitlab-runner
      sudo gitlab-runner start
      ```

Example of registering sequence:

``` sh
sudo gitlab-runner register
Please enter the gitlab-ci coordinator URL (e.g. https://gitlab.com/):
https://gitlab.inria.fr/
Please enter the gitlab-ci token for this runner:
# copy/paste the project's secret token here
# Setup the URL and the token found in the gitlab web interface (Settings -> CI/CD -> Runners -> Specific runners -> Set up a specific runner manually).
Please enter the gitlab-ci description for this runner:
[ubuntu1604]:
Please enter the gitlab-ci tags for this runner (comma separated):
linux, ubuntu
Whether to run untagged builds [true/false]:
[false]: true
Whether to lock Runner to current project [true/false]:
[false]:
Registering runner... succeeded                     runner=4jknGvoz
Please enter the executor: shell, ssh, docker+machine, docker-ssh+machine, kubernetes, docker, parallels, virtualbox, docker-ssh:
docker
Please enter the default Docker image (e.g. ruby:2.1):
ubuntu
Runner registered successfully. Feel free to start it, but if it's running already the config should be automatically reloaded!
```

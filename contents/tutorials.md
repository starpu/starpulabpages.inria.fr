---
layout: default
title: Tutorials
permalink: tutorials.html
full-width: True
---

# Tutorials

- You can start by reading the chapter
  [Applications](https://files.inria.fr/starpu/doc/html_web_applications/)
  of the StarPU documentation. That will show you how to add StarPU in
  an existing application.

- A full docker tutorial is available [here](/tutorials/docker).

- The EXA2PRO tutorial was recorded, it is [available on
youtube](https://www.youtube.com/watch?v=R43W4pQb9tE) and you
can also look at the [tutorial material](./tutorials/2021-02-EoCoE/)
and [slides](./tutorials/2021-02-EoCoE/21-02-24-eocoe.pdf).

<center> <iframe width="420" height="315" src="https://www.youtube.com/embed/R43W4pQb9tE" frameborder="0" allowfullscreen></iframe> </center>

## Past tutorials
- May 2024 : [Saclay](/tutorials/2024-05-Saclay/)
- November 2022 : [TREX Workshop (CALMIP)](/tutorials/2022-11-CALMIP/)
- March 2022 : [ETP4HPC Webinar](/tutorials/2022-03-ETP4HPC/etp4hpc_oaumage_task_based_performance_portability_in_hpc.pdf)
- February 2021 : [EoCoE](/tutorials/2021-02-EoCoE/)
- February 2021 : [Grafmm working group tutorial](/tutorials/2021-02-Grafmm-Tutorial_StarPU.pdf)
- November 2019 : [Inria automn school "High Performance Numerical Simulation](/tutorials/2019-11-HPNS-Inria/)
- May 2019 : [EXA2PRO Tutorial meeting](/tutorials/2019-05-EXA2PRO/)
- July 2018 : [HPCS](/tutorials/2018-07-HPCS/2018-07-HPCS-Tutorial-StarPU.pdf)
- July 2018 : [ComPAS](/tutorials/2018-07-ComPAS/)
- July 2017 : [JLESC tutorial: Urbana Champaign](/tutorials/2017-07-JLESC/17-07-20-JLESC.pdf)
- June 2016 : [PATC Training](/tutorials/2016-06-PATC/)
- June 2015 : [PATC Training](/tutorials/2015-06-PATC/)
- January 2015 : [HetComp Tutorial (StarPU part), HiPEAC 2015 Conference in Amsterdam.](/tutorials/2015-01-HiPEAC/hipeac_tutorial_hetcomp_starpu_2015.pdf)
- May 2014  : [PATC Training](/tutorials/2014-05-PATC/)
- May 2013 : [ComplexHCC Spring School](/tutorials/2013-ComplexHPC/)
- January 2013 : [ComPAS conference in Grenoble (in French)](/tutorials/2013-01-ComPAS/)
- October 2012 : [HPC-GA](/tutorials/2012-HPC-GA/)
- May 2011 : [ComplexHCC Spring School](/tutorials/2011-05-ComplexHPC/)
- July 2010 : [SAAHPC](/tutorials/2010-07-SAAHPC/saahpc.pdf)


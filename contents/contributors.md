---
layout: default
title: Contributors
permalink: contributors.html
full-width: True
---

# Partners and Contributors

[![Université de Bordeaux](images/Univ_Bx.png)](http://cpu.labex.u-bordeaux.fr/)
[![Inria](images/Inria_logo.png)](https://www.inria.fr/)
[![CNRS](images/CNRS_logo2.png)](https://www.cnrs.fr/)

## List of contributors

### Local team
- [Aumage Olivier (lead)](http://people.bordeaux.inria.fr/olivier.aumage/)
- [Counilh Marie-Christine](http://dept-info.labri.fr/~counilh/)
- D'aviau de Piolant Albert
- [Furmento Nathalie (technical lead)](http://www.labri.fr/perso/furmento/)
- [Guermouche Amina](https://guermouche.wp.imtbs-tsp.eu/)
- Lion Romain
- Morin Thomas
- [Namyst Raymond](http://raymond.namyst.emi.u-bordeaux.fr/)
- Tayeb Hayfa
- [Thibault Samuel (lead)](http://dept-info.labri.fr/~thibault/)
- [Wacrenier Pierre-André](http://dept-info.labri.fr/~wacren/)

### Collaborators
- [Agullo Emmanuel](https://team.inria.fr/hiepacs/team-members/emmanuel-agullo/)
- Beaumont Olivier
- Bramas Berenger
- [Buttari Alfredo](http://buttari.perso.enseeiht.fr/index.html)
- Coti Camille
- [Denis Alexandre](http://people.bordeaux.inria.fr/adenis/)
- [Eyraud-Dubois Lionel](https://www.labri.fr/perso/eyraud/)
- [Faverge Mathieu](https://mfaverge.vvv.enseirb-matmeca.fr/)
- Fuentes Mathis
- Jego Antoine
- [Guermouche Abdou](https://www.labri.fr/perso/guermouc/)
- Kuhn Mathieu
- Lacoste Xavier
- Lisito Alycia
- Nesi Lucas Leandro
- Pruvost Florent
- Schnorr Lucas Mello
- [Swartvagher Philippe](https://ph-sw.fr/)

### Alumni
- Archipoff Simon
- Augonnet Cédric
- [Barthou Denis](http://www.labri.fr/perso/barthou/)
- Beauchamp Guillaume
- Cassagne Adrien
- Clet-Ortega Jérôme
- Cojean Terry
- Collin Nicolas
- Courtés Ludovic
- Danjean Vincent
- Daoudi Idriss
- Flint Clément
- Gonthier Maxime
- Guilbaud Adrien
- He Kun
- Henri Sylvain
- Hugo Andra
- Hunout Léo
- Juhoor Mehdi
- Juven Alexis
- Keryell-Even Maël
- Khorsi Yanis
- Kumar Suraj
- Lambert Thibaut
- Leria Erwan
- Lizé Benoît
- Lucas Gwenolé
- Makni Mariem
- Nakov Stojce
- Pablo Joris
- Paillat Ludovic
- Pasqualinotto Damien
- Pinto Vinicius Garcia
- Pitoiset Samuel
- Point Gérald
- Quôc-Dinh Nguyen
- Roelandt Cyril
- Sakka Chiheb
- Salingue Corentin
- Sergent Marc
- Simonet Anthony
- Song Maxime
- Stanisic Luka
- Subervie Bérangère
- Tagliaro Bastien
- Tessier François
- Videau Brice
- Villeveygoux Léo
- Virouleau Philippe


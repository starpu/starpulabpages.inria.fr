---
layout: default
title: News
permalink: software.html
full-width: True
---

# Software

Some software is known for being able to use StarPU to tackle heterogeneous
architectures, here is a non-exhaustive list (feel free to ask to be added to the
list!):
- [AL4SAN](https://github.com/ecrc/al4san), dense linear algebra library
- [Chameleon](https://project.inria.fr/chameleon/), dense linear algebra library
- [Exa2pro](http://exa2pro.eu), Enhancing Programmability and boosting Performance Portability for Exascale Computing Systems
- [ExaGeoStat](http://github.com/ecrc/exageostat), Machine learning framework for Climate/Weather prediction applications
- [FLUSEPA](https://hal.inria.fr/hal-01507613), Navier-Stokes Solver for Unsteady Problems with Bodies in Relative Motion
- [HiCMA](http://github.com/ecrc/hicma), Low-rank general linear algebra library
- hmat, hierarchical matrix C/C++ library
- [HPSM](https://github.com/danidomenico/hpsm), a C++ API for parallel loops programs supporting muti-CPUs and multi-GPUs
- [K'Star](http://kstar.gitlabpages.inria.fr/), OpenMP 4 - compatible interface on top of StarPU.
- [KSVD](http://github.com/ecrc/ksvd), dense SVD on distributed-memory manycore systems
- [MAGMA](http://icl.cs.utk.edu/magma/), dense linear algebra library, starting from version 1.1
- [MaPHyS](https://gitlab.inria.fr/solverstack/maphys), Massively Parallel Hybrid Solver
- [MASA-StarPU](https://inria.hal.science/hal-02914793/file/lopes_rafael_paper25_sbacpad2020.pdf), Parallel Sequence Comparison
- [MOAO](http://github.com/ecrc/moao), HPC framework for computational astronomy, servicing the European Extremely Large Telescope and the Japanese Subaru Telescope
- [NNTile](https://github.com/skolai/nntile), a framework for training large neural networks.
- [PaStiX](http://pastix.gforge.inria.fr/), sparse linear algebra library, starting from version 5.2.1
- [PEPPHER](https://inria.hal.science/hal-00648480/PDF/micro2011.pdf), Performance Portability and Programmability for Heterogeneous Many-core Architectures
- [QDWH](http://github.com/ecrc/qdwh), QR-based Dynamically Weighted Halley
- [QMCkl](https://github.com/TREX-CoE/qmckl), Quantum Monte Carlo Kernel Library
- [qr_mumps](https://qr_mumps.gitlab.io/), sparse linear algebra library
- [ScalFMM](https://solverstack.gitlabpages.inria.fr/ScalFMM/index.html), N-body interaction simulation using the Fast Multipole Method.
- [SCHNAPS](https://tel.archives-ouvertes.fr/tel-01410049/), Solver for Conservative Hyperbolic Non-linear systems Applied to PlasmaS.
- [SignalPU](https://hal.archives-ouvertes.fr/hal-01086246), a Dataflow-Graph-specific programming model.
- [SkePU](http://www.ida.liu.se/~chrke/skepu/), a skeleton programming framework.
- [StarNEig](https://github.com/NLAFET/StarNEig/), a dense nonsymmetric (generalized) eigenvalue solving library.
- [STARS-H](http://github.com/ecrc/stars-h), HPC low-rank matrix market
- [TBFMM](https://gitlab.inria.fr/bramas/tbfmm), Fast Multipole Method.
- [XcalableMP](http://www.xcalablemp.org/), Directive-based language eXtension for Scalable and performance-aware Parallel Programming

You can find [here](./publications.html#PublicationsOnApplications) the list of publications related to applications using StarPU.

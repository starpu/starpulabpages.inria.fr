---
layout: default
title: Timeline
permalink: timeline.html
full-width: True
---

<script type="text/javascript" src="https://unpkg.com/vis-timeline@latest/standalone/umd/vis-timeline-graph2d.min.js"></script>
<link href="https://unpkg.com/vis-timeline@latest/styles/vis-timeline-graph2d.min.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="https://sed-bso.gitlabpages.inria.fr/projets-dev/timeline.js"></script>

<style>
body,
/* alternating column backgrounds */
.vis-time-axis .vis-grid.vis-odd {
	background: #f5f5f5;
}
.vis-item .vis-item-overflow {
	overflow: visible;
}
.vis-item {
	color: black;
	background-color: greenyellow;
	border-color: greenyellow;
}
.vis-item.leader {
	color: black;
	background-color: gold;
	border-color: gold;
}
.vis-item.funding {
	color: black;
	background-color: pink;
	border-color: pink;
}
</style>

<div id="visualization"></div>
<script type="text/javascript">
loadTimeline("timeline.json", "StarPU");
</script>

<!--
    IPL HPC - BigData (dates ???)
    Projet Région Aquitaine / CEA: 2013 - 2016
    FP7 MCA HPC-GA: 2012 - 2014

    PRACE 5IP: 2017 - 2019
    H2020 EXA2PRO: 2018 - 2021
    EuroHPC MICROCARD: 2021 - 2025
  -->

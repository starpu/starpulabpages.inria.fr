---
layout: default
full-width: True
---

# StarPU: A Unified Runtime System for Heterogeneous Multicore Architectures

**StarPU** is a task programming library for hybrid architectures

- The application provides algorithms and constraints
  - CPU/GPU implementations of tasks
  - A graph of tasks, using either StarPU's rich **C/C++/Fortran/Python API**, or **OpenMP pragmas**.
- **StarPU internally deals with the following aspects**:
  - Task dependencies
  - Optimized heterogeneous scheduling
  - Optimized data transfers and replication between main memory and discrete memories
  - Optimized cluster communications
  - Fully asynchronous execution without spurious waits

**Rather than handling low-level issues, programmers can concentrate on algorithmic aspects!**

A video recording (26') of [presentation at the XDC2014
conference](http://www.x.org/wiki/Events/XDC2014/XDC2014ThibaultStarPU/)
gives an overview of StarPU ([slides](http://www.x.org/wiki/Events/XDC2014/XDC2014ThibaultStarPU/xdc_starpu.pdf))

<center> <iframe width="420" height="315" src="https://www.youtube.com/embed/frsWSqb8UJU" frameborder="0" allowfullscreen></iframe> </center>

- The latest tutorial material for StarPU is composed of two parts:
   - [General presentation](./tutorials/2021-02-EoCoE/21-02-22-eocoe.pdf)
   - [The tutorial itself](./tutorials/2021-02-EoCoE/21-02-24-eocoe.pdf)
   - [A recording is also available](https://www.youtube.com/watch?v=R43W4pQb9tE)
- A [set of slides](./slides.pdf) is also available to get an overview of StarPU.

The main development repository with issues and merge request is hosted on Inria's GitLab, but a GitHub mirror is also available.

<a class="btn btn-success btn-lg" href="download.html" role="button">
  <i class="fas fa-download fa-lg" aria-hidden="true">&nbsp;</i>Download page
</a>

<a class="btn btn-primary btn-lg" href="https://gitlab.inria.fr/starpu/starpu" role="button" target="blank">
  <i class="fab fa-gitlab fa-lg" aria-hidden="true">&nbsp;</i>Source code on GitLab
</a>
<a class="btn btn-primary btn-lg" href="https://github.com/starpu-runtime/starpu" role="button" target="blank">
  <i class="fab fa-github fa-lg" aria-hidden="true">&nbsp;</i>GitHub mirror
</a>

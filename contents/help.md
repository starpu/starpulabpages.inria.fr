---
layout: default
title: Help
permalink: help.html
full-width: True
---

# Help
For any questions regarding StarPU, you can:
- Read the latest release [documentation](https://files.inria.fr/starpu/doc/html/), and more specifically its [FAQ](https://files.inria.fr/starpu/doc/html/FrequentlyAskedQuestions.html),
- Read the latest master [documentation](https://files.inria.fr/starpu/testing/master/doc/html/), and more specifically its [FAQ](https://files.inria.fr/starpu/testing/master/doc/html/FrequentlyAskedQuestions.html),
- Read the [tutorials](./tutorials.html),
- Browse the [archive](https://sympa.inria.fr/sympa/arc/starpu-announce/) of the StarPU announcement mailing list (archive list also available [locally](./help/archive_starpu-announce.html)),

- Browse the [archive](https://sympa.inria.fr/sympa/arc/starpu-devel/) of the StarPU developers mailing list (archive list also available [locally](./help/archive_starpu-devel.html)),
- Contact the StarPU developers mailing list, [starpu-devel@inria.fr](mailto:starpu-devel@inria.fr?subject=StarPU).
- You can also contact the devel team on
  [github](https://github.com/starpu-runtime/starpu/discussions) or [discord](https://discord.gg/ERq6hvY)

- To submit an issue, if you have an account on <https://gitlab.inria.fr>, you can
  go to <https://gitlab.inria.fr/starpu/starpu/-/issues>. StarPU also has a repository on github,
  you can also submit issues on <https://github.com/starpu-runtime/starpu/issues>.
  Without an account, send an email to [starpu-devel@inria.fr](mailto:starpu-devel@inria.fr?subject=StarPU).
  More informations are available [here](./involved.html).

---
layout: default
title: Download
permalink: download.html
full-width: True
---

# Download

<a class="btn btn-success btn-lg" href="http://files.inria.fr/starpu" role="button"> <i class="fas fa-download fa-lg" aria-hidden="true">&nbsp;</i>Release download page</a>
<a class="btn btn-success btn-lg" href="https://gitlab.inria.fr/starpu/starpu/-/releases" role="button"> <i class="fas fa-download fa-lg" aria-hidden="true">&nbsp;</i>Gitlab release download page</a>


<a class="btn btn-primary btn-lg" href="https://gitlab.inria.fr/starpu/starpu" role="button" target="blank"><i class="fab fa-gitlab fa-lg" aria-hidden="true">&nbsp;</i>Source code on GitLab</a>
<a class="btn btn-primary btn-lg" href="https://github.com/starpu-runtime/starpu" role="button" target="blank"><i class="fab fa-github fa-lg" aria-hidden="true">&nbsp;</i>GitHub mirror</a>


- The development tree of StarPU is freely available under the LGPL license. Some releases are available under the BSD license.

- Get the latest branch nightly snapshot (along with documentation and coverage report for Unix)
    - master : [unix](http://files.inria.fr/starpu/testing/master/) / [windows](https://ci.inria.fr/starpu/job/windows/job/master/)
    - 1.4 : [unix](http://files.inria.fr/starpu/testing/starpu-1.4/) / [windows](https://ci.inria.fr/starpu/job/windows/job/starpu-1.4/)
    - 1.3 : [unix](http://files.inria.fr/starpu/testing/starpu-1.3/) / [windows](https://ci.inria.fr/starpu/job/windows/job/starpu-1.3/)
    - 1.2 : [unix](http://files.inria.fr/starpu/testing/starpu-1.2/) / [windows](https://ci.inria.fr/starpu/job/windows/job/starpu-1.2/)

- Get the latest packages:
    - [Guix](https://gitlab.inria.fr/guix-hpc/guix-hpc/-/blob/master/inria/storm.scm) ([Guix HPC project](https://gitlab.inria.fr/guix-hpc/guix-hpc))
    - [spack](https://github.com/spack/spack/)
    - [Brew](https://gitlab.inria.fr/solverstack/brew-repo/-/blob/master/starpu.rb) ([solverstack project](https://gitlab.inria.fr/solverstack/brew-repo))

- Check the Continuous Integration processes:
    - [sonarqube inria server](https://sonarqube.inria.fr/sonarqube/projects?search=starpu&sort=-analysis_date)
    - [GUIX bot](https://guix.bordeaux.inria.fr/search?query=starpu)
    - [SIMGRID CI](https://github.com/simgrid/simgrid/actions/workflows/ci-starpu.yml)
    - [StarPU gitlab ci](https://gitlab.inria.fr/starpu/starpu/-/pipeline_schedules)
    - [StarPU Github ci](https://github.com/starpu-runtime/starpu/actions)
    - [StarPU packages](https://gitlab.inria.fr/starpu/starpu-packages/-/pipeline_schedules)

---
layout: default
title: News
permalink: news.html
full-width: True
---

# News

Get the latest StarPU news by subscribing to the [starpu-announce mailing list](https://sympa.inria.fr/sympa/info/starpu-announce)
## 2024
- [2024-05-28] [*The release 1.4.7 is now available!*](https://files.inria.fr/starpu/index.html#1.4)
  The 1.4 release series brings among other functionalities a Python
  interface for StarPU, a driver for HIP-based GPUs, the possibility
  to store performance model files in multiple directories, an OpenMP
  LLVM support, a new sched_tasks.rec trace file which monitors task
  scheduling push/pop actions.
- [2024-05-15] [*The release 1.4.6 is now available!*](https://files.inria.fr/starpu/index.html#1.4)
  The 1.4 release series brings among other functionalities a Python
  interface for StarPU, a driver for HIP-based GPUs, the possibility
  to store performance model files in multiple directories, an OpenMP
  LLVM support, a new sched_tasks.rec trace file which monitors task
  scheduling push/pop actions.
- [2024-04-19] [*The release 1.4.5 is now available!*](https://files.inria.fr/starpu/index.html#1.4)
  The 1.4 release series brings among other functionalities a Python
  interface for StarPU, a driver for HIP-based GPUs, the possibility
  to store performance model files in multiple directories, an OpenMP
  LLVM support, a new sched_tasks.rec trace file which monitors task
  scheduling push/pop actions.
- [2024-02-02] [*The release 1.4.4 is now available!*](https://files.inria.fr/starpu/index.html#1.4)
  The 1.4 release series brings among other functionalities a Python
  interface for StarPU, a driver for HIP-based GPUs, the possibility
  to store performance model files in multiple directories, an OpenMP
  LLVM support, a new sched_tasks.rec trace file which monitors task
  scheduling push/pop actions.
- [2024-01-29] [*The release 1.4.3 is now available!*](https://files.inria.fr/starpu/index.html#1.4)
  The 1.4 release series brings among other functionalities a Python
  interface for StarPU, a driver for HIP-based GPUs, the possibility
  to store performance model files in multiple directories, an OpenMP
  LLVM support, a new sched_tasks.rec trace file which monitors task
  scheduling push/pop actions.
- [2024-01-12] StarPU is now directly available on the Jean Zay
  cluster. See [this page](http://www.idris.fr/jean-zay/cpu/jean-zay-cpu-starpu.html) to find out how to use it.
## 2023
- [2023-11-23] [*The release 1.4.2 is now available!*](https://files.inria.fr/starpu/index.html#1.4)
  The 1.4 release series brings among other functionalities a Python
  interface for StarPU, a driver for HIP-based GPUs, the possibility
  to store performance model files in multiple directories, an OpenMP
  LLVM support, a new sched_tasks.rec trace file which monitors task
  scheduling push/pop actions.
- [2023-06-05] [*The release 1.3.11 of StarPU is now available!*](https://files.inria.fr/starpu/index.html#1.3)
  The 1.3 release serie brings among other functionalities a MPI
  master-slave support, a tool to replay execution through SimGrid, a
  HDF5 implementation of the Out-of-core, a new implementation of
  StarPU-MPI on top of NewMadeleine, implicit support for asynchronous
  partition planning, a resource management module to share processor
  cores and accelerator devices with other parallel runtime systems, ...
- [2023-05-24] [*The release 1.4.1 is now available!*](https://files.inria.fr/starpu/index.html#1.4)
  The 1.4 release series brings among other functionalities a Python
  interface for StarPU, a driver for HIP-based GPUs, the possibility
  to store performance model files in multiple directories, an OpenMP
  LLVM support, a new sched_tasks.rec trace file which monitors task
  scheduling push/pop actions.
- [2023-03-29] [*The release 1.4.0 is now available!*](https://files.inria.fr/starpu/index.html#1.4)
  The 1.4 release series brings among other functionalities a Python
  interface for StarPU, a driver for HIP-based GPUs, the possibility
  to store performance model files in multiple directories, an OpenMP
  LLVM support, a new sched_tasks.rec trace file which monitors task
  scheduling push/pop actions.
## 2022
- [2022-16-11] [*The release 1.3.10 of StarPU is now available!*](https://files.inria.fr/starpu/index.html#1.3)
  The 1.3 release serie brings among other functionalities a MPI
  master-slave support, a tool to replay execution through SimGrid, a
  HDF5 implementation of the Out-of-core, a new implementation of
  StarPU-MPI on top of NewMadeleine, implicit support for asynchronous
  partition planning, a resource management module to share processor
  cores and accelerator devices with other parallel runtime systems, ...
- [2022-07-01] StarPU is now installed on the [IDRIS](http://www.idris.fr/) [Jean Zay](http://www.idris.fr/jean-zay/)
  supercomputer
- [2022-05-02] StarPU is now available in [2012-10-01](https://github.com/spack/spack/)
## 2021
-  A ETP4HPC White Paper 'Task-Based Performance
  Portability in HPC' has been published and can be read [here](https://www.etp4hpc.eu/white-papers.html#taskbased) or
  directly downloaded [here](https://www.etp4hpc.eu/pujades/files/ETP4HPC_WP_Task-based-PP_FINAL.pdf).
- [2021-10-21] [*The release 1.3.9 of StarPU is now available!*](https://files.inria.fr/starpu/index.html#1.3)
  The 1.3 release serie brings among other functionalities a MPI
  master-slave support, a tool to replay execution through SimGrid, a
  HDF5 implementation of the Out-of-core, a new implementation of
  StarPU-MPI on top of NewMadeleine, implicit support for asynchronous
  partition planning, a resource management module to share processor
  cores and accelerator devices with other parallel runtime systems, ...
- [2021-05-17] [*The release 1.3.8 of StarPU is now available!*](https://files.inria.fr/starpu/index.html#1.3)
  The 1.3 release serie brings among other functionalities a MPI
  master-slave support, a tool to replay execution through SimGrid, a
  HDF5 implementation of the Out-of-core, a new implementation of
  StarPU-MPI on top of NewMadeleine, implicit support for asynchronous
  partition planning, a resource management module to share processor
  cores and accelerator devices with other parallel runtime systems, ...
## 2020
- [2020-10-13] [*The release 1.3.7 of StarPU is now available!*](https://files.inria.fr/starpu/index.html#1.3)
  The 1.3 release serie brings among other functionalities a MPI
  master-slave support, a tool to replay execution through SimGrid, a
  HDF5 implementation of the Out-of-core, a new implementation of
  StarPU-MPI on top of NewMadeleine, implicit support for asynchronous
  partition planning, a resource management module to share processor
  cores and accelerator devices with other parallel runtime systems, ...
- [2020-08-31] [*The release 1.3.5 of StarPU is now available!*](https://files.inria.fr/starpu/index.html#1.3)
  The 1.3 release serie brings among other functionalities a MPI
  master-slave support, a tool to replay execution through SimGrid, a
  HDF5 implementation of the Out-of-core, a new implementation of
  StarPU-MPI on top of NewMadeleine, implicit support for asynchronous
  partition planning, a resource management module to share processor
  cores and accelerator devices with other parallel runtime systems, ...
- [2020-06-12] [*The release 1.2.10 of StarPU is now available!*](https://files.inria.fr/starpu/index.html#1.2)
  The 1.2 release serie notably brings an out-of-core support, a MIC
  Xeon Phi support, an OpenMP runtime support, and a new internal
  communication system for MPI.
- [2020-06-11] [*The release 1.3.4 of StarPU is now available!*](https://files.inria.fr/starpu/index.html#1.3)
  The 1.3 release serie brings among other functionalities a MPI
  master-slave support, a tool to replay execution through SimGrid, a
  HDF5 implementation of the Out-of-core, a new implementation of
  StarPU-MPI on top of NewMadeleine, implicit support for asynchronous
  partition planning, a resource management module to share processor
  cores and accelerator devices with other parallel runtime systems, ...
- [2020-01-27] [*The release 1.2.9 of StarPU is now available!*](https://files.inria.fr/starpu/index.html#1.2).
  The 1.2 release serie notably brings an out-of-core support, a MIC
  Xeon Phi support, an OpenMP runtime support, and a new internal
  communication system for MPI.
## 2019
- [2019-11-01] A [StarPU tutorial](/tutorials/2019-11-HPNS-Inria/) will be given as part of the Inria
  automn school "High Performance Numerical Simulation".
- [2019-10-24] [*The release 1.3.3 of StarPU is now available!*](https://files.inria.fr/starpu/index.html#1.3)
  The 1.3 release serie brings among other functionalities a MPI
  master-slave support, a tool to replay execution through SimGrid, a
  HDF5 implementation of the Out-of-core, a new implementation of
  StarPU-MPI on top of NewMadeleine, implicit support for asynchronous
  partition planning, a resource management module to share processor
  cores and accelerator devices with other parallel runtime systems, ...
- [2019-06-14] [*The release 1.3.2 of StarPU is now available!*](https://files.inria.fr/starpu/index.html#1.3)
  The 1.3 release serie brings among other functionalities a MPI
  master-slave support, a tool to replay execution through SimGrid, a
  HDF5 implementation of the Out-of-core, a new implementation of
  StarPU-MPI on top of NewMadeleine, implicit support for asynchronous
  partition planning, a resource management module to share processor
  cores and accelerator devices with other parallel runtime systems, ...
- [2019-05-17] [*The v1.1.8 release of StarPU is now available!*](https://files.inria.fr/starpu/index.html#1.1).
  This release notably brings the concept of scheduling contexts which
  allows to separate computation resources. This is really intented to
  be the last release for the branch 1.1.
- [2019-04-02] [*The release 1.3.1 of StarPU is now available!*](https://files.inria.fr/starpu/index.html#1.3)
  The 1.3 release serie brings among other functionalities a MPI
  master-slave support, a tool to replay execution through SimGrid, a
  HDF5 implementation of the Out-of-core, a new implementation of
  StarPU-MPI on top of NewMadeleine, implicit support for asynchronous
  partition planning, a resource management module to share processor
  cores and accelerator devices with other parallel runtime systems, ...
- [2019-03-21] [*The release 1.3.0 of StarPU is now available!*](https://files.inria.fr/starpu/index.html#1.3)
  The 1.3 release serie brings among other functionalities a MPI
  master-slave support, a tool to replay execution through SimGrid, a
  HDF5 implementation of the Out-of-core, a new implementation of
  StarPU-MPI on top of NewMadeleine, implicit support for asynchronous
  partition planning, a resource management module to share processor
  cores and accelerator devices with other parallel runtime systems, ...
- [2019-02-27] [*The 1.2.8 release of StarPU is now available!*](https://files.inria.fr/starpu/index.html#1.2).
  The 1.2 release serie notably brings an out-of-core support, a MIC Xeon
  Phi support, an OpenMP runtime support, and a new internal
  communication system for MPI.
  (The release 1.2.7 is broken and should not be used)
## 2018
- [2018-09-21] [*The 1.2.6 release of StarPU is now available!*](https://files.inria.fr/starpu/index.html#1.2).
  The 1.2 release serie notably brings an out-of-core support, a MIC Xeon
  Phi support, an OpenMP runtime support, and a new internal
  communication system for MPI.
- [2018-08-16] [*The release 1.2.5 of StarPU is now available!*](https://files.inria.fr/starpu/index.html#1.2).
  The 1.2 release serie notably brings an out-of-core support, a MIC Xeon
  Phi support, an OpenMP runtime support, and a new internal
  communication system for MPI.
- [2018-04-12] [*The release 1.2.4 of StarPU is now available!*](https://files.inria.fr/starpu/index.html#1.2).
  The 1.2 release serie notably brings an out-of-core support, a MIC Xeon
  Phi support, an OpenMP runtime support, and a new internal
  communication system for MPI.
- [2018-03-01] A [tutorial](https://events.prace-ri.eu/event/681/) "Runtime systems for heterogeneous platform
  programming" will be given at the Maison de la Simulation in
  June 2018.
## 2017
- [2017-11-09] [*The release 1.2.3 of StarPU is now available!*](https://files.inria.fr/starpu/index.html#1.2).
  The 1.2 release serie notably brings an out-of-core
  support, a MIC Xeon Phi support, an OpenMP runtime
  support, and a new internal communication system
  for MPI.
- [2017-05-02] StarPU is part of the HPCLib project that aims at
  performing static analysis of the Inria Bordeaux Sud-Ouest solver
  stack. See the [StarPU page](https://sonarqube.inria.fr/sonarqube/dashboard?id=storm%3Astarpu%3Arelease%3Av1.2) on the sonarqube server of INRIA.
- [2017-05-11] [*The release 1.2.2 of StarPU is now available!*](https://files.inria.fr/starpu/index.html#1.2).
  The 1.2 release serie notably brings an out-of-core support, a
  MIC Xeon Phi support, an OpenMP runtime support, and a
  new internal communication system for MPI.
- [2017-04-01] A [tutorial](https://events.prace-ri.eu/event/618/) "Runtime systems for heterogeneous platform
  programming" will be given at the Maison de la Simulation in May 2017.
- [2017-03-10] [*The v1.1.7 release of StarPU is now available!*](https://files.inria.fr/starpu/index.html#1.1).
  This release notably brings the concept of scheduling contexts which
  allows to separate computation resources. This is intented to be the
  last release for the branch 1.1.
- [2017-03-06] [*The 1.2.1 release of StarPU is now available!*](https://files.inria.fr/starpu/index.html#1.2).
  This release notably brings an out-of-core support, a MIC Xeon Phi
  support, an OpenMP runtime support, and a new internal communication
  system for MPI.
## 2016
- [2016-08-25] [*The v1.1.6 release of StarPU is now available!*](https://files.inria.fr/starpu/index.html#1.1).
  This release notably brings the concept of scheduling
  contexts which allows to separate computation
  resources.
- [2016-08-25] [*The 1.2.0 release of StarPU is now available!*](https://files.inria.fr/starpu/index.html#1.2).
  This release notably brings an out-of-core support, a MIC Xeon
  Phi support, an OpenMP runtime support, and a new internal
  communication system for MPI.
- [2016-03-01] Engineer job offer at Inria: more details on the job
  and on how to apply are available [here](../internships/hibox.html)
## 2015
- [2015-09-09] [*The v1.1.5 release of StarPU is now available!*](https://files.inria.fr/starpu/index.html#1.1).
  This release notably brings the concept of
  scheduling contexts which allows to separate computation
  resources.
- [2015-04-01] A [tutorial](https://events.prace-ri.eu/event/339/) on runtime systems including StarPU will be
  given at INRIA Bordeaux in June 2015.
- [2015-03-11] [*The v1.1.4 release of StarPU is now available!*](https://files.inria.fr/starpu/index.html#1.1).
  This release notably brings the concept of
  scheduling contexts which allows to separate computation
  resources.
## 2014
- [2014-09-12] [*The v1.1.3 release of StarPU is now available!*](https://files.inria.fr/starpu/index.html#1.1).
  This release notably brings the concept of
  scheduling contexts which allows to separate computation
  resources.
- [2014-06-03] [*The v1.1.2 release of StarPU is now available!*](https://files.inria.fr/starpu/index.html#1.1).
  This release notably brings the concept of
  scheduling contexts which allows to separate computation
  resources.
- [2014-05-01] Open [*Engineer Position*](https://www.inria.fr/en/institute/recruitment/offers/young-graduate-engineers-research-and-development/%28view%29/details.html?id=PNGFK026203F3VBQB6G68LOE1&LOV5=4510&ContractType=4545&LG=EN&Resultsperpage=20&nPostingID=8751&nPostingTargetID=14612&option=52&sort=DESC&nDepartmentID=10).
- [2014-04-14] [*The v1.1.1 release of StarPU is now available!*](https://files.inria.fr/starpu/index.html#1.1).
  This release notably brings the concept of
  scheduling contexts which allows to separate computation
  resources.
## 2013
- [2013-12-18] [*The v1.1.0 release of StarPU is now available!*](https://files.inria.fr/starpu/index.html#1.1).
  This release notably brings the concept of
  scheduling contexts which allows to separate computation
  resources.
- [2013-05-02] Engineer position open at Inria (Team Runtime + Team
  Moais): [More details here](https://www.inria.fr/institut/recrutement-metiers/offres/ingenieurs-confirmes-specialistes/%28view%29/details.html?id=PGTFK026203F3VBQB6G68LONZ&LOV5=4510&ContractType=7549&LG=FR&Resultsperpage=20&nPostingID=7746&nPostingTargetID=13293&option=52&sort=DESC&nDepartmentID=10).
- [2013-04-10] StarPU is now featured in the [Tools and Libraries
  section of AMD's heterogeneous computing application showcase](http://developer.amd.com/community/application-showcase/tools-and-libraries/).
- [2013-04-01] Read the new report [C Language Extensions for Hybrid
  CPU/GPU Programming with StarPU](http://hal.inria.fr/hal-00807033/en) to know everything you always wanted
  to know about the C Languages Extensions for StarPU.
- [2013-02-15] [*The v1.0.5 release of StarPU is now available!*](https://files.inria.fr/starpu/index.html#1.0).
  This release mainly brings bug fixes.
- [2013-01-25] A [tutorial](../tutorials/201301-ComPAS/) on StarPU was given at the [ComPAS
  conference](http://compas2013.inrialpes.fr/).
## 2012
- [2012-11-01] StarPU at SuperComputing'12: A StarPU poster is on
  display on the Inria booth, Feel free to come & have a chat, at
  booth #1209!
- [2012-10-15] [*The v1.0.4 release of StarPU is now available!*](https://files.inria.fr/starpu/index.html#1.0).
  This release mainly brings bug fixes.
- [2012-09-25] StarPU was presented at the conference [EuroMPI](http://www.par.univie.ac.at/conference/eurompi2012/)
- [2012-09-17]  [*The v1.0.3 release of StarPU is now available!*](https://files.inria.fr/starpu/index.html#1.0).
  This release mainly brings bug fixes.
- [2012-08-10] [*The v1.0.2 release of StarPU is now available!*](https://files.inria.fr/starpu/index.html#1.0).
  This release notably fixes CPU/GPU binding.
- [2012-07-25] StarPU was presented at the [GNU Tools Cauldron 2012](http://gcc.gnu.org/wiki/cauldron2012#StarPU.27s_C_Extensions_for_Hybrid_CPU.2BAC8-GPU_Task_Programming.2C_or.2C_An_Experience_in_Turning_a_Clumsy_API_Into_Language_Extensions)
- [2012-05-25]  [*The v1.0.1 release of StarPU is now available!*](https://files.inria.fr/starpu/index.html#1.0).
  This release mainly brings bug fixes.
- [2012-03-28]: [*The v1.0.0 release of StarPU is now available!*](https://files.inria.fr/starpu/index.html#1.0).
  This release provides notably a GCC plugin to extend the C interface
  with pragmas which allows to easily define codelets and issue tasks,
  and a new multi-format interface which permits to use different
  binary formats on CPUs & GPUs.
See the announcements on [LWN](http://lwn.net/Articles/489337/), [the GCC mailing list](http://article.gmane.org/gmane.comp.gcc.devel/125551), and [Phoronix](http://www.phoronix.com/scan.php?page=news_item&px=MTA4MDI).
## 2011
- [2011-05-13] [New release](https://files.inria.fr/starpu/index.html#0.)
  This release provides a reduction mode, an external API for
  schedulers, theoretical bounds, power-based optimization, parallel
  tasks, an MPI DSM, profiling interfaces, an initial support for
  CUDA4 (GPU-GPU transfers), improved documentation and of course
  various fixes.
## 2010
- [2010-09-10] Discover how we ported the MAGMA and the PLASMA
  libraries on top of StarPU in collaboration with ICL/UTK in [*this
  Lapack Working Note*](http://www.netlib.org/lapack/lawnspdf/lawn230.pdf).
- [2010-08-25] [New release](https://files.inria.fr/starpu/index.html#0.)
  This release provides support for task-based dependencies, implicit
  data-based dependencies (RAW/WAR/WAW), profiling feedback, an MPI
  layer, OpenCL and Windows support, as well as an API naming revamp.
- [2012-07-25] StarPU was presented during a tutorial entitled
  "Accelerating Linear Algebra on Heterogeneous Architectures of
  Multicore and GPUs using MAGMA and the DPLASMA and StarPU Scheduler"
  at SAAHPC in Knoxville, TN (USA) [(slides)](/tutorials/2010-07-SAAHPC/saahpc.pdf)
- [2012-05-25] Want to get an overview of StarPU ? Check out our
  [latest research report](http://hal.archives-ouvertes.fr/inria-00467677)!
## 2009
- [2009-10-01] [*StarPU 0.2.901 (0.3-rc1) is now available !*](https://files.inria.fr/starpu/index.html#0.)
  This release adds support for asynchronous GPUs and heterogeneous
  multi-GPU platforms as well as many other improvements.
- [2009-06-01] NVIDIA granted the StarPU team with a professor
  partnership and donated several high-end CUDA-capable cards.

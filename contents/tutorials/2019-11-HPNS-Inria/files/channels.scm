(list (channel
        (name 'guix-hpc)
        (url "https://gitlab.inria.fr/guix-hpc/guix-hpc.git")
        (commit
          "446507e4ee8ec9ca6335679c8bb96bfb7d929538"))
      (channel
        (name 'guix-hpc-non-free)
        (url "https://gitlab.inria.fr/guix-hpc/guix-hpc-non-free.git")
        (commit
          "e058192f39e427c9fac8c31f9fcb27b0f671e43f"))
      (channel
        (name 'guix)
        (url "https://git.savannah.gnu.org/git/guix.git")
        (commit
          "bbad38f4d8e6b6ecc15c476b973094cdf96cdeae")))

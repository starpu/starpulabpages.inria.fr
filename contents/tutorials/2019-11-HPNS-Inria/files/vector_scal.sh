#!/bin/bash

. ./init.sh

make vector_scal_task_insert
STARPU_WORKER_STATS=1 ./vector_scal_task_insert

# to force the implementation on a GPU device, by default, it will enable CUDA
STARPU_WORKER_STATS=1 STARPU_NCPU=0 ./vector_scal_task_insert

# to force the implementation on a OpenCL device
STARPU_WORKER_STATS=1  STARPU_NCPU=0 STARPU_NCUDA=0 ./vector_scal_task_insert

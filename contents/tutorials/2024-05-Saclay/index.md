---
layout: default
title: Atelier StarPU, Saclay, 28 mai 2024
permalink: tutorials/2024-05-Saclay/index.html
full-width: True
---

# Atelier StarPU, Saclay, 28 mai 2024

The [general presentation slides are available as PDF](./24-05-28-saclay.pdf).

The [tutorial slides are available as PDF](./01_talk_saclay.pdf).

The [C version of the tutorial practice session is available as webpage](./C.html)

The [Fortran version of the tutorial practice session is available as webpage](./fortran.html)

## Contact
For any questions regarding StarPU, please contact the StarPU developers mailing list [starpu-devel@inria.fr](mailto:starpu-devel@inria.fr?subject=StarPU)


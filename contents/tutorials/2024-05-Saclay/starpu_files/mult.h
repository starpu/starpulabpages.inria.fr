#include <starpu.h>

extern unsigned nslicesx;
extern unsigned nslicesy;
extern unsigned xdim;
extern unsigned ydim;
extern unsigned zdim;

extern struct starpu_codelet mult_cl;

void parse_args(int argc, char **argv);

void init_problem_data(float **A_r, float **B_r, float **C_r);

void partition_mult_data(float *A, float *B, float *C,
	starpu_data_handle_t *A_handle_r, starpu_data_handle_t *B_handle_r, starpu_data_handle_t *C_handle_r);


int launch_tasks(starpu_data_handle_t A_handle, starpu_data_handle_t B_handle, starpu_data_handle_t C_handle);

void unpartition_mult_data(starpu_data_handle_t A_handle, starpu_data_handle_t B_handle, starpu_data_handle_t C_handle);

! StarPU --- Runtime system for heterogeneous multicore architectures.
!
! Copyright (C) 2022 Université de Bordeaux, CNRS (LaBRI UMR 5800), Inria
!
! StarPU is free software; you can redistribute it and/or modify
! it under the terms of the GNU Lesser General Public License as published by
! the Free Software Foundation; either version 2.1 of the License, or (at
! your option) any later version.
!
! StarPU is distributed in the hope that it will be useful, but
! WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
!
! See the GNU Lesser General Public License in COPYING.LGPL for more details.
!
! The codelet is passed 3 matrices, the "buffer" union-type field gives a
! description of the layout of those 3 matrices in the local memory (ie. RAM
! in the case of CPU, GPU frame buffer in the case of GPU etc.). Since we have
! registered data with the "matrix" data interface, we use the matrix macros.
!
module nf_mult_cl
contains
!--------------------------------------------------------------!
! 'cl_mult' codelet routine
recursive subroutine cl_cpu_mult_func (buffers, cl_args) bind(C)
        use iso_c_binding       ! C interfacing module
        use fstarpu_mod         ! StarPU interfacing module
        implicit none

        type(c_ptr), value, intent(in) :: buffers, cl_args ! cl_args is unused
        real, dimension(:,:), pointer :: ma, mb, mc
        integer :: ld_ma, nx_ma, ny_ma, ld_mb, nx_mb, ny_mb, ld_mc, nx_mc, ny_mc
        integer :: i, j, k
        real :: sum

        ld_ma = fstarpu_matrix_get_ld(buffers, 0)
        nx_ma = fstarpu_matrix_get_nx(buffers, 0)
        ny_ma = fstarpu_matrix_get_ny(buffers, 0)
        !write(*,*) "ld_ma = ", ld_ma, ", nx_ma = ", nx_ma, ", ny_ma = ", ny_ma

        call c_f_pointer(fstarpu_matrix_get_ptr(buffers, 0), ma, shape=[ld_ma,ny_ma])
        !write(*,*) "ma"
        !do i=1,nx_ma
        !do j=1,ny_ma
        !        write(*,*) i,j,ma(i,j)
        !end do
        !write(*,*) '-'
        !end do

        ld_mb = fstarpu_matrix_get_ld(buffers, 1)
        nx_mb = fstarpu_matrix_get_nx(buffers, 1)
        ny_mb = fstarpu_matrix_get_ny(buffers, 1)
        !write(*,*) "ld_mb = ", ld_mb, ", nx_mb = ", nx_mb, ", ny_mb = ", ny_mb

        call c_f_pointer(fstarpu_matrix_get_ptr(buffers, 1), mb, shape=[ld_mb,ny_mb])
        !write(*,*) "mb"
        !do i=1,nx_mb
        !do j=1,ny_mb
        !        write(*,*) i,j,mb(i,j)
        !end do
        !write(*,*) '-'
        !end do

        ld_mc = fstarpu_matrix_get_ld(buffers, 2)
        nx_mc = fstarpu_matrix_get_nx(buffers, 2)
        ny_mc = fstarpu_matrix_get_ny(buffers, 2)
        !write(*,*) "ld_mc = ", ld_mc, ", nx_mc = ", nx_mc, ", ny_mc = ", ny_mc

        call c_f_pointer(fstarpu_matrix_get_ptr(buffers, 2), mc, shape=[ld_mc,ny_mc])
        !write(*,*) "mc"
        !do i=1,nx_mc
        !do j=1,ny_mc
        !        write(*,*) i,j,mc(i,j)
        !end do
        !write(*,*) '-'
        !end do

        do i=1, ny_mc
                do j=1, nx_mc
                        sum = 0
                        do k=1, nx_ma
                                sum = sum + ma(i, k)*mb(k, j)
                        end do
                        mc(i, j) = sum
                end do
        end do

end subroutine cl_cpu_mult_func

end module nf_mult_cl

module nf_mult_task
contains
!--------------------------------------------------------------!
subroutine init_problem_data(ma, mb, mc, X, Y, Z) bind(C)
        use iso_c_binding       ! C interfacing module
        real, dimension(:,:), allocatable, target, intent(inout) :: ma, mb, mc
        integer(c_int), intent(in) :: X, Y, Z
        integer, target :: i, j
        real :: rand_v

        allocate(ma(Z,Y))
        allocate(mb(X,Z))
        allocate(mc(X,Y))

        call random_seed()
        do j=1,Y
                do i=1,Z
                        call random_number(rand_v)
                        ma(i,j) = REAL(rand_v)
                end do
        end do

        do j=1,Z
                do i=1,X
                        call random_number(rand_v)
                        mb(i,j) = REAL(rand_v)
                end do
        end do

        do j=1,Y
                do i=1,X
                        mc(i,j) = REAL(0)
                end do
        end do

end subroutine init_problem_data

!--------------------------------------------------------------!
subroutine partition_mult_data(ma, mb, mc, ma_handle, mb_handle, mc_handle, X, Y, Z, X_parts, Y_parts) bind(C)
        use iso_c_binding, only: c_ptr, c_int       ! C interfacing module
        use fstarpu_mod         ! StarPU interfacing module

        real, dimension(:,:), allocatable, target, intent(inout) :: ma, mb, mc
        integer(c_int), intent(in) :: X, Y, Z
        integer(c_int), intent(in) :: X_parts, Y_parts

        type(c_ptr) , intent(inout) :: ma_handle, mb_handle, mc_handle   ! pointer for the matrix data handle
        type(c_ptr) :: filter_horiz
        type(c_ptr) :: filter_vert

        
        ! register 'ma', a matrix of real elements
        call fstarpu_matrix_data_register(ma_handle, 0, c_loc(ma), Y, Y, Z, c_sizeof(ma(1,1)))

        ! register 'mb', a matrix of real elements
        call fstarpu_matrix_data_register(mb_handle, 0, c_loc(mb), Z, Z, X, c_sizeof(mb(1,1)))

        ! register 'mc', a matrix of real elements
        call fstarpu_matrix_data_register(mc_handle, 0, c_loc(mc), Y, Y, X, c_sizeof(mc(1,1)))

        ! allocate partitioning filters
        filter_horiz = fstarpu_df_alloc_matrix_filter_block()
        call fstarpu_data_filter_set_nchildren(filter_horiz, Y_parts)

        filter_vert = fstarpu_df_alloc_matrix_filter_vertical_block()
        call fstarpu_data_filter_set_nchildren(filter_vert, X_parts)

        ! apply partitioning
        call fstarpu_data_partition (ma_handle,filter_horiz)
        call fstarpu_data_partition (mb_handle,filter_vert)

        ! fstarpu_data_map_filters is a variable-arity function, the first argument
        ! is the handle of the data to partition, the second argument is the
        ! number of filters to apply recursively. Filters are applied in the
        ! same order as the arguments.
        ! This would be equivalent to fstarpu_data_partition(mc_handle, filter_vert) and
        ! then applying horiz on each sub-data (ie. each column of C)
        call fstarpu_data_map_filters(mc_handle, 2, (/ filter_horiz, filter_vert /))

        ! free data filter structures
        call fstarpu_data_filter_free(filter_horiz)
        call fstarpu_data_filter_free(filter_vert)

end subroutine partition_mult_data

!--------------------------------------------------------------!
subroutine launch_tasks(cl_mult, ma_handle, mb_handle, mc_handle, X_parts, Y_parts, X, Y, Z) bind(C)
        use iso_c_binding, only: c_ptr, c_int       ! C interfacing module
        use fstarpu_mod         ! StarPU interfacing module

        type(c_ptr), intent(inout):: cl_mult   ! a pointer for the codelet structure
        type(c_ptr), intent(inout) :: ma_handle, mb_handle, mc_handle   ! pointer for the matrix data handle
        type(c_ptr) :: sub_handleA, sub_handleB, sub_handleC  ! pointer for the sub-data handle
        integer(c_int), target :: taskx, tasky
        integer(c_int), intent(in) :: X_parts, Y_parts
        integer(c_int) :: X, Y, Z
        real(KIND=C_DOUBLE), target :: flops

!C[taskx, tasky] = A[tasky] B[taskx] 
!              |---|---|---|---|
!              |   | * |   |   | B
!              |---|---|---|---|
!                    X 
!     |----|   |---|---|---|---|
!     |****| Y |   |***|   |   |
!     |****|   |   |***|   |   |
!     |----|   |---|---|---|---|
!     |    |   |   |   |   |   |
!     |    |   |   |   |   |   |
!     |----|   |---|---|---|---|
!       A              C
!
        
        do taskx=0, X_parts-1
        do tasky=0, Y_parts-1
                ! get partitioned tile
                ! there was a single filter applied to matrices A
                ! (respectively B) so we grab the handle to the chunk
                ! identified by "tasky" (respectively "taskx). The "1"
                ! tells StarPU that there is a single argument to the
                ! variable-arity function starpu_data_get_sub_data
                sub_handleA = fstarpu_data_get_sub_data (ma_handle, 1, (/tasky/))
                sub_handleB = fstarpu_data_get_sub_data (mb_handle, 1, (/taskx/))
                ! 2 filters were applied on matrix C, so we give
                ! fstarpu_data_get_sub_data 2 arguments. The order of the arguments
                ! must match the order in which the filters were
                ! applied.
                ! NB: fstarpu_data_get_sub_data(mc_handle, 1, (/k/)) would have returned
                ! a handle to the column number k of matrix C.
                ! NB2: fstarpu_data_get_sub_data(mc_handle, 2, (/taskx, tasky/)) is
                ! equivalent to
                ! fstarpu_data_get_sub_data(fstarpu_data_get_sub_data(mc_handle, 1, (/taskx/)), 1, (/tasky/))
                sub_handleC = fstarpu_data_get_sub_data (mc_handle, 2, (/taskx, tasky/))

                flops = 2 * (X / X_parts) * (Y / Y_parts) * Z

                call fstarpu_task_insert((/ cl_mult, &
                        FSTARPU_R, sub_handleA, &
                        FSTARPU_R, sub_handleB, &
                        FSTARPU_W, sub_handleC, &
                        FSTARPU_FLOPS, c_loc(flops), &
                        C_NULL_PTR /))
        end do
        end do
end subroutine launch_tasks

!--------------------------------------------------------------!
subroutine parse_args(X, Y, Z, X_parts, Y_parts) bind(C)
        use iso_c_binding, only: c_int
        integer :: i, j
        integer :: n, m
        character(len=64) :: arg
        character(len=16) :: para

        integer(c_int), intent(inout) :: X, Y, Z
        integer(c_int), intent(inout) :: X_parts, Y_parts

        n=iargc() !number of parameters
        do i = 1, n
                call getarg(i,arg)
                read(arg,'(A16)')para
                !print *, para
                if(para == "-nslices") then
                        j=i+1
                        call getarg(j,arg)
                        read(arg,'(I16)') m
                        X_parts = m
                        Y_parts = m
                else if (para == "--nslicesx") then
                        j=i+1
                        call getarg(j,arg)
                        read(arg,'(I16)') m
                        X_parts = m
                else if (para == "--nslicesy") then
                        j=i+1
                        call getarg(j,arg)
                        read(arg,'(I16)') m
                        Y_parts = m
                else if (para == "-x") then
                        j=i+1
                        call getarg(j,arg)
                        read(arg,'(I16)') m
                        X = m
                else if (para == "-y") then
                        j=i+1
                        call getarg(j,arg)
                        read(arg,'(I16)') m
                        Y = m
                else if (para == "-z") then
                        j=i+1
                        call getarg(j,arg)
                        read(arg,'(I16)') m
                        Z = m
                else if (para == "-xy") then
                        j=i+1
                        call getarg(j,arg)
                        read(arg,'(I16)') m
                        X = m
                        Y = m
                else if (para == "-size") then
                        j=i+1
                        call getarg(j,arg)
                        read(arg,'(I16)') m
                        X = m
                        Y = m
                        Z = m
                else if (para == "-help" .or. para == "--help" .or. para == "-h") then
                        call getarg(0,arg)
                        read(arg,'(A16)')para
                        write(*,*) "Usage:",para,"[-nslices n] [-nslicesx x] [-nslicesy y] [-x x] [-y y]&
                         [-xy n] [-z z] [-size size]"
                        write(*,*) "Currently selected is:",Z,"x",Y,"*",X,"x",Z,X_parts,"x",Y_parts,"blocks",niter,"iterations"      
                end if
        end do
          
end subroutine parse_args

end module nf_mult_task

! StarPU --- Runtime system for heterogeneous multicore architectures.
!
! Copyright (C) 2022 Université de Bordeaux, CNRS (LaBRI UMR 5800), Inria
!
! StarPU is free software; you can redistribute it and/or modify
! it under the terms of the GNU Lesser General Public License as published by
! the Free Software Foundation; either version 2.1 of the License, or (at
! your option) any later version.
!
! StarPU is distributed in the hope that it will be useful, but
! WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
!
! See the GNU Lesser General Public License in COPYING.LGPL for more details.
!
module nf_vector_increment_cl
contains
recursive subroutine cl_cpu_func_increment (buffers, cl_args) bind(C)
        use iso_c_binding       ! C interfacing module
        use fstarpu_mod         ! StarPU interfacing module
        implicit none

        type(c_ptr), value, intent(in) :: buffers, cl_args ! cl_args is unused
        integer(c_int), pointer :: x

        call c_f_pointer(fstarpu_variable_get_ptr(buffers, 0), x)
        
        x=x+1

end subroutine cl_cpu_func_increment

end module nf_vector_increment_cl

program nf_ring_async_implicit
        use iso_c_binding       ! C interfacing module
        use fstarpu_mod         ! StarPU interfacing module
        use fstarpu_mpi_mod     ! StarPU MPI interfacing module
        use nf_vector_increment_cl
        implicit none

        integer(c_int) :: ret
        integer(c_int) :: world
        integer(c_int) :: rank
        integer(c_int) :: size
        integer(c_int), target :: token = 42
        integer(c_int) :: nloops = 32
        integer(c_int) :: loop, last_loop, last_rank
        integer(c_int64_t) :: tag
        integer(c_int) :: src,dst
        type(c_ptr) :: st

        type(c_ptr) :: increment_cl   ! a pointer for the codelet structure
        type(c_ptr) :: token_handle    ! a pointer for the data handle
        

        ! initialize StarPU
        ret = fstarpu_init(C_NULL_PTR)

        ret = fstarpu_mpi_init(1)

        world = fstarpu_mpi_world_comm()
        rank = fstarpu_mpi_world_rank()
        size = fstarpu_mpi_world_size()
        
        if (size < 2) then
			if (rank == 0) then
				print *, "We need at least 2 processes."
			end if

			call fstarpu_shutdown()
            ret = fstarpu_mpi_shutdown()
            stop 77
		end if

        ! allocate an empty codelet structure
        increment_cl = fstarpu_codelet_allocate()

        ! set the codelet name
        call fstarpu_codelet_set_name(increment_cl, C_CHAR_"increment_codelet"//C_NULL_CHAR)

        ! add a CPU implementation function to the codelet
        call fstarpu_codelet_add_cpu_func(increment_cl, C_FUNLOC(cl_cpu_func_increment))

        ! add a Read-Write mode data buffer to the codelet
        call fstarpu_codelet_add_buffer(increment_cl, FSTARPU_RW)

        ! register 'token'
        call fstarpu_variable_data_register(token_handle, 0, c_loc(token), c_sizeof(token))

        last_loop = nloops
        last_rank = size-1

        st = fstarpu_mpi_status_alloc()

        do loop = 1,  nloops

			tag = loop*size + rank

			if ((loop == 1) .and. (rank == 0)) then
				token = 0
				write(*,*) "Start with token value ", token
			else
				src = mod((rank+size-1),size)
				ret = fstarpu_mpi_recv(token_handle, src, tag, world, st)
			end if

			! submit a task with codelet increment_cl, and vector 'vector'
	        !
	        call fstarpu_task_insert((/ increment_cl, &
	                FSTARPU_RW, token_handle, &
	                C_NULL_PTR /))

			if ((loop == last_loop) .and. (rank == last_rank)) then
				call fstarpu_data_acquire(token_handle, FSTARPU_R)
				write(*,*) "Finished : token value ", token
				call fstarpu_data_release(token_handle)
			else
				dst = mod((rank+1),size)
				ret = fstarpu_mpi_send(token_handle, dst, tag+1, world)
			end if

		end do


        ! wait for task completion
        call fstarpu_task_wait_for_all()

        call fstarpu_mpi_status_free(st)

        ! unregister 'vector'
        call fstarpu_data_unregister(token_handle)

        ! free codelet structure
        call fstarpu_codelet_free(increment_cl)

        ! shut MPI down
        ret = fstarpu_mpi_shutdown()

        ! shut StarPU down
        call fstarpu_shutdown()

        if (rank == last_rank) then
        write(*,*) "[", rank, "] token=", token, "==", nloops, "*", size, "?"
			if (token /= nloops*size) then
				stop 1
			end if
		end if


end program nf_ring_async_implicit

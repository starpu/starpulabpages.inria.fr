---
layout: default
title: StarPU Tutorial - ComPAS - 2013 - Utilisation de PlaFRIM/DiHPES
permalink: tutorials/2013-01-ComPAS/plafrim.html
full-width: True
---

# StarPU Tutorial - ComPAS - 2013 - Utilisation de PlaFRIM/DiHPES

## Connection à la plateforme

Démarrez votre ordinateur à partir de la clef USB fournie. Une
connection va s'ouvrir, cliquer sur le gestionnaire de fenêtres (2ème
icone en partant de la gauche dans la barre de menus en bas de
l'écran), et ouvrir `home-rw`.

Le script `ssh_plafrim` permet de se connecter à
la plateforme. La passphrase de l'utilisateur "compas_user_XX" (XX
ayant une valeur de 01 à 20) est la chaine "compas_user_XX".

```sh
$ cd /media/home-rw/
/media/home-rw/$ ./ssh_plafrim
Enter passphrase for key 'compas_user_XX': compas_user_XX
*****
***** PlaFRIM - Machine de formation
*****

compas2013-XX@formation:~>
```

## Compilation et exécution des applications

La compilation et l'exécution d'applications StarPU doit se faire sur
les noeuds de calcul mirage et non sur la machine frontale. On utilise
pour cela le gestionnaire de ressources torque. Voici rapidement les
commandes que vous allez besoin lors de ce tutoriel.

- Connaitre l'état des noeuds. La commande `pbsnodes` permet
  d'interroger le gestionnaire de ressource pour connaitre l'état des
  noeuds.
  ```sh
  $ pbsnodes
  ```
- Soumettre un job de manière différé:
  ```sh
  $ qsub script.pbs
  ```
  Ceci soumet un noeud sur une seule machine. Pour réserver plusieurs machines:
  ```sh
  $ qsub -l nodes=2 script.pbs
  ```
  Par défaut seul un coeur de la machine est réservé, plusieurs
  soumissions peuvent donc s'exécuter en même temps sur la machine.
  Pour réserver la totalité d'une ou plusieurs machines:
  ```sh
  $ qsub -l nodes=2:ppn=8 script.pbs
  ```

StarPU est déjà compilé sur mirage, ainsi que les applications des 2
exercices. Chaque répertoire `exercices/partieX` contient un
fichier README expliquant comment lancer les applications. Des
exemples de fichier script à donner à la commande `qsub` sont
disponible dans le répertoire `qsub` de votre répertoire
principal.

## Utiliser la version de StarPU présente sur la clef USB

Les utilitaires StarPU, ainsi que des exemples d'application sont
disponibles sur la clef USB. Tapez `starpu-<TAB>;` pour
voir la liste des utilitaires, les exemples sont disponibles dans le
répertoire `/usr/lib/starpu/examples`.

Les fichiers de développement pour StarPU sont également disponibles
(voir `dpkg -L libstarpu-dev`)


---
layout: default
title: StarPU Tutorial - ComPAS - 2013
permalink: tutorials/2013-01-ComPAS/index.html
full-width: True
---

# StarPU Tutorial - ComPAS - 2013

## Introduction
 [Conférence ComPAS](http://compas2013.inrialpes.fr/) à Grenoble, Janvier 2013.

## Résumé
Les GPGPUs sont de plus en plus utilisés pour paralléliser massivement
des codes de calcul. Même si, sur des problèmes précis, on arrive à
obtenir de très bonnes performances, il reste difficile de programmer
efficacement de façon transparente ces nouveaux supports d’exécution
parallèle.

Ce tutoriel, organisé sur une journée, présentera d’abord
l’architecture et les bases théoriques permettant d’obtenir de bonnes
performances sur les GPGPU. Puis, deux environnements de programmation
(StarPU et XKaapi) pour utiliser ce matériel de manière efficace et
relativement transparente seront présentés plus en détail.

Le tutorial commencera par une présentation succincte des
architectures matérielles ciblées (GPU) et leurs environnements de
programmation dédiés (Cuda/OpenCL). La connaissance préalable de ces
outils est vivement conseillée pour suivre ce tutorial.

Puis, le tutorial introduira des notions plus théoriques.  Programmer
des applications parallèles revient à sélectionner quelles tâches
doivent être allouées aux GPUs et à déterminer l’ordre d’exécution.
Nous rappellerons les fondements utiles de la théorie de
l’ordonnancement pour concevoir et analyser des politiques et nous
détaillerons les différentes solutions envisageables pour ordonnancer
sur des supports mixtes.

Le reste du tutorial, plus pratique, sera l’occasion de découvrir et
de prendre en main les environnements StarPU et XKaapi.  Ces
environnements sont là pour prendre en charge et simplifier des
parties purement techniques (chargement de noyaux, transferts de
données) mais surtout pour faire de l’ordonnancement de tâches entre
CPU et GPU. Ainsi, l’application peut espérer un placement de ses
tâches relativement performant quelque soit le nombre de GPU et CPU à
sa disposition. L’ordonnancement sera réalisé en prenant en charge de
manière transparente les mouvements et la cohérence de données entre
la mémoire des différentes unités de calcul.

StarPU et XKaapi utilisent tous les deux des tâches avec dépendances.
Pour chaque tâche, une ou plusieurs implémentations CPU et/ou GPU
peuvent être définies. Ces environnements sont proches mais se
distinguent par les caractéristiques de leur ordonnanceur et par leur
interface de programmation.

StarPU obtient la description du graphe de tâche de l’application en
utilisant les directives fournies par l’extension GCC, ou directement
l’API bas-niveau C. Il est également possible d’exécuter le graphe de
tâches de manière distribuée sur différents noeuds de calcul en
définissant une distribution des données. Ce tutorial présentera plus
en détail les directives de programmation fournies par l’extension
GCC, ainsi que l’intégration du modèle de tâches de StarPU avec MPI.

XKaapi obtient la description du graphe de tâche de l’application de
manière dynamique grâce à des directives de compilation ou à
l’utilisation d’une API C/C++/Fortran.  Pour illustrer le vol de
taches à grain fin et le placement sur les processeurs de calcul
(CPU/GPU) réalisé par XKaapi, nous utiliserons dans ce tutoriel
l’interface C++ pour l’implémentation d’algorithmes d’algèbre
linéaire.

## Supports et Exercices

Les supports du tutoriel sont disponible au
format [pdf](./starpu-20130115.pdf).

Durant la durée du tutoriel, des comptes
sur [PlaFRIM/DiHPES](https://www.plafrim.fr/)
seront mis à disposition des personnes présentes. Deux ensembles
d'exercices sont disponibles:
- Des applications de base avec un exemple utilisant cuda et OpenCL:
  [zip](./starpu-20130115-partie1.zip)
- Une même application écrite en utilisant l'API classique de
  StarPU, l'API distribuée au dessus de MPI, et les directives de
  programmation: [zip](./starpu-20130115-partie2.zip)

Pour utiliser la plateforme PlaFRIM/DiHPES, les instructions sont [ici](./plafrim.html).

Pour utiliser une autre machine, il sera nécessaire d'y installer
StarPU. Pour ceci, vous pouvez récupérer la dernière version
officielle de StarPU [ici](https://files.inria.fr/starpu/).

StarPU se compile et s'installe à la manière classique des autotools:
configure, make et make install. Pour utiliser les directives de
programmation, vous aurez besoin d'une version récente du compilateur
gcc. En appelant configure, vous verrez à la fin un résumé, des
options présentes, comme par exemple:

```
StarPU Extensions:
       MPI enabled:                                 yes
       MPI test suite:                              yes
       FFT Support:                                 yes
       GCC plug-in:                                 yes
       GCC plug-in test suite (requires GNU Guile): yes
```

Les fichiers zip des exercices fournis avec ce tutoriel contiennent un
fichier README expliquant comment compiler et exécuter les exemples.

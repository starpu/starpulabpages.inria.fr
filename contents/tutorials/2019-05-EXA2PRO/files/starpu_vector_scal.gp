#!/usr/bin/gnuplot -persist

set term postscript eps enhanced color
set output "starpu_vector_scal.eps"
set title "Model for codelet vector-scal"
set xlabel "Total data size"
set ylabel "Time (ms)"

set key top left
set logscale x
set logscale y

set xrange [1:10**9]

plot	0.001 * 0.000062 * x ** 0.950389 + 0.001 * 18.876390 title "Non-Linear Regression cuda0_impl0 (Comb0)",\
	0.001 * 0.000014 * x ** 1.051224 + 0.001 * 17.841970 title "Non-Linear Regression cuda1_impl0 (Comb1)",\
	0.001 * 0.000043 * x ** 0.974107 + 0.001 * 18.069800 title "Non-Linear Regression cuda2_impl0 (Comb2)",\
	0.001 * 0.001169 * x ** 0.999555 + 0.001 * 0.378427 title "Non-Linear Regression cpu0_impl0 (Comb3)",\
	"starpu_vector_scal_avg.data" using 1:2:3 with errorlines title "Average cuda0-impl0 (Comb0)",\
	"starpu_vector_scal_avg.data" using 1:4:5 with errorlines title "Average cuda1-impl0 (Comb1)",\
	"starpu_vector_scal_avg.data" using 1:6:7 with errorlines title "Average cuda2-impl0 (Comb2)",\
	"starpu_vector_scal_avg.data" using 1:8:9 with errorlines title "Average cpu0-impl0 (Comb3)"

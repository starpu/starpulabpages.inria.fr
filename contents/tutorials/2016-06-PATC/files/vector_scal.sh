#!/bin/bash
# @ class            = clgpu
# @ job_name = job_vector_scal
# @ total_tasks = 10
# @ node = 1
# @ wall_clock_limit = 00:10:00
# @ output = $(HOME)/starpu/$(job_name).$(jobid).out
# @ error = $(HOME)/starpu/$(job_name).$(jobid).err
# @ job_type = mpich
# @ queue

source /gpfslocal/pub/training/runtime_june2016/starpu_env.sh

make vector_scal_task_insert
STARPU_WORKER_STATS=1 ./vector_scal_task_insert

# to force the implementation on a GPU device, by default, it will enable CUDA
STARPU_WORKER_STATS=1 STARPU_NCPUS=0 ./vector_scal_task_insert

# to force the implementation on a OpenCL device
STARPU_WORKER_STATS=1  STARPU_NCPUS=0 STARPU_NCUDA=0 ./vector_scal_task_insert

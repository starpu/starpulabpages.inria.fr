#!/bin/bash
# @ class            = clgpu
# @ job_name = job_gemm
# @ total_tasks = 10
# @ node = 1
# @ wall_clock_limit = 00:10:00
# @ output = $(HOME)/starpu/$(job_name).$(jobid).out
# @ error = $(HOME)/starpu/$(job_name).$(jobid).err
# @ job_type = mpich
# @ queue

source /gpfslocal/pub/training/runtime_june2016/starpu_env.sh

make gemm/sgemm
STARPU_WORKER_STATS=1 ./gemm/sgemm

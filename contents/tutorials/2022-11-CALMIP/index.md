---
layout: default
title: TREX Workshop at CALMIP, StarPU Tutorial, November, 22nd 2022
permalink: tutorials/2022-11-CALMIP/index.html
full-width: True
---

# TREX Workshop at CALMIP: StarPU Tutorial, November, 22nd 2022

![Overview](./CALMIP.svg)

This tutorial is part of the [TREX Workshop at CALMIP](https://www.calmip.univ-toulouse.fr/evenements-formations-formation/formations-calmip) taking place on November, 22nd 2022.

The [general presentation slides are available as PDF](./22-11-22-calmip.pdf).

The [tutorial slides are available as PDF](./01_talk_calmip.pdf).

The [C version of the tutorial practice session is available as webpage](./C.html)

The [Fortran version of the tutorial practice session is available as webpage](./fortran.html)

## Contact
For any questions regarding StarPU, please contact the StarPU developers mailing list [starpu-devel@inria.fr](mailto:starpu-devel@inria.fr?subject=StarPU)


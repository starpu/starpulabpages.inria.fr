# StarPU --- Runtime system for heterogeneous multicore architectures.
#
# Copyright (C) 2022  Université de Bordeaux, CNRS (LaBRI UMR 5800), Inria
#
# StarPU is free software; you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation; either version 2.1 of the License, or (at
# your option) any later version.
#
# StarPU is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
#
# See the GNU Lesser General Public License in COPYING.LGPL for more details.
#
PROG = nf_sgemm nf_dgemm

STARPU_VERSION=1.3
FSTARPU_MOD = $(shell pkg-config --cflags-only-I starpu-$(STARPU_VERSION)|sed -e 's/^\([^ ]*starpu\/$(STARPU_VERSION)\).*$$/\1/;s/^.* //;s/^-I//')/fstarpu_mod.f90

SRCSF = nf_xgemm.f90

FC = gfortran
CC = gcc
NVCC = nvcc

CFLAGS = -g $(shell pkg-config --cflags starpu-$(STARPU_VERSION))
NVCCFLAGS = -g $(shell pkg-config --cflags starpu-$(STARPU_VERSION)) -std=c++11
FCFLAGS = -J. -g -cpp
LDLIBS =  $(shell pkg-config --libs starpu-$(STARPU_VERSION))
#LDLIBS += -llapack -lblas
LDLIBS += -lmkl_intel_lp64 -lmkl_sequential -lmkl_core

# Automatically enable CUDA / OpenCL
STARPU_CONFIG=$(shell pkg-config --variable=includedir starpu-$(STARPU_VERSION))/starpu/$(STARPU_VERSION)/starpu_config.h
ifneq ($(shell grep "USE_CUDA 1" $(STARPU_CONFIG)),)
USE_CUDA=1
endif

OBJS = fstarpu_mod.o $(SRCSF:%.f90=%.o)

.phony: all clean
all: $(PROG)

ifeq ($(USE_CUDA),1)
FCFLAGS+=-DSTARPU_USE_CUDA=1
OBJS += nf_xgemm_cuda.o
nf_sgemm.o: nf_xgemm.o nf_xgemm_cuda.o
nf_dgemm.o: nf_xgemm.o nf_xgemm_cuda.o
$(PROG): LDLIBS+=-L$(CUDA_PATH)/lib64 -lcublas -lcudart -lcuda -lstdc++ -lcublasLt
endif

$(PROG): %: %.o $(OBJS)
	$(FC) $(LDFLAGS) -o $@ $^ $(LDLIBS)

%.o: %.cu
	$(NVCC) $(NVCCFLAGS) -c -o $@ $<

fstarpu_mod.o: $(FSTARPU_MOD)
	$(FC) $(FCFLAGS) -c -o $@ $<

%.o: %.f90
	$(FC) $(FCFLAGS) -c -o $@ $<

clean:
	rm -fv *.o *.mod $(PROG)

CFLAGS+=-DSTARPU_OPENBLAS=1
nf_sgemm.o: nf_xgemm.o fstarpu_mod.o
nf_dgemm.o: nf_xgemm.o fstarpu_mod.o
nf_xgemm.o: fstarpu_mod.o

! StarPU --- Runtime system for heterogeneous multicore architectures.
!
! Copyright (C) 2022 Université de Bordeaux, CNRS (LaBRI UMR 5800), Inria
!
! StarPU is free software; you can redistribute it and/or modify
! it under the terms of the GNU Lesser General Public License as published by
! the Free Software Foundation; either version 2.1 of the License, or (at
! your option) any later version.
!
! StarPU is distributed in the hope that it will be useful, but
! WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
!
! See the GNU Lesser General Public License in COPYING.LGPL for more details.
!

module nf_xgemm_cl_cuda
implicit none

interface 
        ! void kernel_wrapper_sgemm_(char transa, char transb, int m, int n,
        ! int k, float alpha, const float *A, int lda,
        ! const float *B, int ldb, float beta, float *C, int ldc)
        subroutine kernel_wrapper_sgemm(cta, ctb, m, n, k,&
           alpha, A, lda, B, ldb, beta, c, ldc) bind(C,name='kernel_wrapper_sgemm_')
           use iso_c_binding
           character(1,c_char),value :: cta, ctb
           integer(c_int),value :: m,n,k,lda,ldb,ldc
           real,value :: alpha,beta
           real, dimension(lda,*) :: A
           real, dimension(ldb,*) :: B
           real, dimension(ldc,*) :: C
          end subroutine kernel_wrapper_sgemm

          !  void kernel_wrapper_dgemm_(char transa, char transb, int m, int n,
          ! int k, double alpha, const double *A, int lda,
          ! const double *B, int ldb, double beta, double *C, int ldc)
        subroutine kernel_wrapper_dgemm(cta, ctb, m, n, k,&
           alpha, A, lda, B, ldb, beta, c, ldc) bind(C,name='kernel_wrapper_dgemm_')
           use iso_c_binding
           character(1,c_char),value :: cta, ctb
           integer(c_int),value :: m,n,k,lda,ldb,ldc
           real(8),value :: alpha,beta
           real(8), dimension(lda,*) :: A
           real(8), dimension(ldb,*) :: B
           real(8), dimension(ldc,*) :: C
          end subroutine kernel_wrapper_dgemm
end interface

end module nf_xgemm_cl_cuda

module nf_xgemm_cl

        interface mult_kernel_common
                module procedure mult_kernel_sgemm
                module procedure mult_kernel_dgemm
        end interface

contains
subroutine mult_kernel_sgemm (buffers, type, kind)
        use iso_c_binding      ! C interfacing module
        use fstarpu_mod         ! StarPU interfacing module
        use nf_xgemm_cl_cuda

        type(c_ptr), value, intent(in) :: buffers
        character(len=*), intent(in) :: type
        real, intent(in) :: kind
        real, dimension(:,:), pointer :: ma, mb, mc
        integer(c_int) :: ld_ma, nx_ma, ny_ma, ld_mb, nx_mb, ny_mb, ld_mc, nx_mc, ny_mc

        ld_ma = fstarpu_matrix_get_ld(buffers, 0)
        nx_ma = fstarpu_matrix_get_nx(buffers, 0)
        ny_ma = fstarpu_matrix_get_ny(buffers, 0)

        call c_f_pointer(fstarpu_matrix_get_ptr(buffers, 0), ma, shape=[ld_ma,ny_ma])

        ld_mb = fstarpu_matrix_get_ld(buffers, 1)
        nx_mb = fstarpu_matrix_get_nx(buffers, 1)
        ny_mb = fstarpu_matrix_get_ny(buffers, 1)

        call c_f_pointer(fstarpu_matrix_get_ptr(buffers, 1), mb, shape=[ld_mb,ny_mb])

        ld_mc = fstarpu_matrix_get_ld(buffers, 2)
        nx_mc = fstarpu_matrix_get_nx(buffers, 2)
        ny_mc = fstarpu_matrix_get_ny(buffers, 2)

        call c_f_pointer(fstarpu_matrix_get_ptr(buffers, 2), mc, shape=[ld_mc,ny_mc])

        if(type == "CPU") then
                call sgemm('N', 'N', nx_mc, ny_mc, ny_ma, REAL(1.0), ma, ld_ma, mb, ld_mb, REAL(0.0), mc, ld_mc)
#ifdef STARPU_USE_CUDA
        else
                call kernel_wrapper_sgemm('N', 'N', nx_mc, ny_mc, ny_ma, REAL(1.0), ma, ld_ma, mb, ld_mb, REAL(0.0), mc, ld_mc)
#endif
        end if

end subroutine mult_kernel_sgemm

subroutine mult_kernel_dgemm (buffers, type, kind)
        use iso_c_binding      ! C interfacing module
        use fstarpu_mod         ! StarPU interfacing module
        use nf_xgemm_cl_cuda

        type(c_ptr), value, intent(in) :: buffers
        character(len=*), intent(in) :: type
        real(8), intent(in) :: kind
        real(8), dimension(:,:), pointer :: ma, mb, mc
        integer(c_int) :: ld_ma, nx_ma, ny_ma, ld_mb, nx_mb, ny_mb, ld_mc, nx_mc, ny_mc


        ld_ma = fstarpu_matrix_get_ld(buffers, 0)
        nx_ma = fstarpu_matrix_get_nx(buffers, 0)
        ny_ma = fstarpu_matrix_get_ny(buffers, 0)

        call c_f_pointer(fstarpu_matrix_get_ptr(buffers, 0), ma, shape=[ld_ma,ny_ma])

        ld_mb = fstarpu_matrix_get_ld(buffers, 1)
        nx_mb = fstarpu_matrix_get_nx(buffers, 1)
        ny_mb = fstarpu_matrix_get_ny(buffers, 1)

        call c_f_pointer(fstarpu_matrix_get_ptr(buffers, 1), mb, shape=[ld_mb,ny_mb])

        ld_mc = fstarpu_matrix_get_ld(buffers, 2)
        nx_mc = fstarpu_matrix_get_nx(buffers, 2)
        ny_mc = fstarpu_matrix_get_ny(buffers, 2)

        call c_f_pointer(fstarpu_matrix_get_ptr(buffers, 2), mc, shape=[ld_mc,ny_mc])

        if(type == "CPU") then
                call dgemm('N', 'N', nx_mc, ny_mc, ny_ma, DBLE(1.0), ma, ld_ma, mb, ld_mb, DBLE(0.0), mc, ld_mc)
#ifdef STARPU_USE_CUDA
        else
                call kernel_wrapper_dgemm('N', 'N', nx_mc, ny_mc, ny_ma, DBLE(1.0), ma, ld_ma, mb, ld_mb, DBLE(0.0), mc, ld_mc)
#endif
        end if


end subroutine mult_kernel_dgemm

recursive subroutine nf_cpu_cl_mult(buffer, cl_args) bind(C)
        use iso_c_binding      ! C interfacing module
        use fstarpu_mod         ! StarPU interfacing module

        type(c_ptr), value, intent(in) :: buffer, cl_args 
        integer(c_int), target :: kind
        real :: s_value
        real(8) :: b_value

        call fstarpu_unpack_arg(cl_args, (/ c_loc(kind) /))

        if(kind == 4) then
                call mult_kernel_common(buffer, "CPU", s_value)
        else if(kind == 8) then
                call mult_kernel_common(buffer, "CPU", b_value)
        end if

end subroutine nf_cpu_cl_mult

#ifdef STARPU_USE_CUDA
subroutine nf_cuda_cl_mult(buffer, cl_args) bind(C)
        use iso_c_binding      ! C interfacing module
        use fstarpu_mod         ! StarPU interfacing module

        type(c_ptr), value, intent(in) :: buffer, cl_args 
        integer(c_int), target :: kind
        real :: s_value
        real(8) :: b_value

        call fstarpu_unpack_arg(cl_args, (/ c_loc(kind) /))

        if(kind == 4) then
                call mult_kernel_common(buffer, "CUDA", s_value)
        else if(kind == 8) then
                call mult_kernel_common(buffer, "CUDA", b_value)
        end if

end subroutine nf_cuda_cl_mult
#endif

end module nf_xgemm_cl
!--------------------------------------------------------------!

module nf_xgemm_func

        interface check_output
                module procedure check_output_sgemm
                module procedure check_output_dgemm
        end interface

        interface init_problem_data
                module procedure init_problem_data_sgemm
                module procedure init_problem_data_dgemm
        end interface

        interface partition_xgemm_data
                module procedure partition_sgemm_data
                module procedure partition_dgemm_data
        end interface

contains
subroutine check_output_sgemm(ma, mb, mc, X, Y, Z) bind(C)
        use iso_c_binding       ! C interfacing module
        real, dimension(:,:), allocatable, target, intent(inout) :: ma, mb, mc
        integer(c_int), intent(in) :: X, Y, Z

        real :: err
        integer(c_int) :: max

        call sgemm('N', 'N', Y, X, Z, REAL(-1.0), ma, Y, mb, Z, REAL(1.0), mc, Y);

        err = sasum(X*Y, mc, 1);

        if (err < X*Y*0.001) then
                write (*,*) "Results are OK"
        else
                max = isamax(X*Y, mc, 1)

                write(*,*) "There were errors ... err = ", err
                
        end if

end subroutine check_output_sgemm

subroutine check_output_dgemm(ma, mb, mc, X, Y, Z) bind(C)
        use iso_c_binding       ! C interfacing module
        real(8), dimension(:,:), allocatable, target, intent(inout) :: ma, mb, mc
        integer(c_int), intent(in) :: X, Y, Z

        real :: err
        integer(c_int) :: max

        call dgemm('N', 'N', Y, X, Z, DBLE(-1.0), ma, Y, mb, Z, DBLE(1.0), mc, Y);

        err = dasum(X*Y, mc, 1);

        if (err < X*Y*0.001) then
                write (*,*) "Results are OK"
        else
                max = idamax(X*Y, mc, 1)

                write(*,*) "There were errors ... err = ", err
                
        end if

end subroutine check_output_dgemm
!--------------------------------------------------------------!
subroutine init_problem_data_sgemm(ma, mb, mc, X, Y, Z) bind(C)
        use iso_c_binding       ! C interfacing module
        real, dimension(:,:), allocatable, target, intent(inout) :: ma, mb, mc
        integer(c_int), intent(in) :: X, Y, Z
        integer, target :: i, j
        real :: rand_v

        allocate(ma(Z,Y))
        allocate(mb(X,Z))
        allocate(mc(X,Y))

        call random_seed()
        do j=1,Y
                do i=1,Z
                        call random_number(rand_v)
                        ma(i,j) = REAL(rand_v)
                end do
        end do

        do j=1,Z
                do i=1,X
                        call random_number(rand_v)
                        mb(i,j) = REAL(rand_v)
                end do
        end do

        do j=1,Y
                do i=1,X
                        mc(i,j) = REAL(0)
                end do
        end do

end subroutine init_problem_data_sgemm

subroutine init_problem_data_dgemm(ma, mb, mc, X, Y, Z) bind(C)
        use iso_c_binding       ! C interfacing module
        real(8), dimension(:,:), allocatable, target, intent(inout) :: ma, mb, mc
        integer(c_int), intent(in) :: X, Y, Z
        integer, target :: i, j
        real(8) :: rand_v

        allocate(ma(Z,Y))
        allocate(mb(X,Z))
        allocate(mc(X,Y))

        call random_seed()
        do j=1,Y
                do i=1,Z
                        call random_number(rand_v)
                        ma(i,j) = DBLE(rand_v)
                end do
        end do

        do j=1,Z
                do i=1,X
                        call random_number(rand_v)
                        mb(i,j) = DBLE(rand_v)
                end do
        end do

        do j=1,Y
                do i=1,X
                        mc(i,j) = DBLE(0)
                end do
        end do

end subroutine init_problem_data_dgemm

!--------------------------------------------------------------!
subroutine partition_sgemm_data(ma, mb, mc, ma_handle, mb_handle, mc_handle, &
                X, Y, Z, X_parts, Y_parts) bind(C)
        use iso_c_binding, only: c_ptr, c_int       ! C interfacing module
        use fstarpu_mod         ! StarPU interfacing module

        real, dimension(:,:), allocatable, target, intent(inout) :: ma, mb, mc
        integer(c_int), intent(in) :: X, Y, Z
        integer(c_int), intent(in) :: X_parts, Y_parts

        type(c_ptr) , intent(inout) :: ma_handle, mb_handle, mc_handle   ! pointer for the matrix data handle
        type(c_ptr) :: filter_horiz
        type(c_ptr) :: filter_vert
        
        ! register 'ma', a matrix of real elements
        call fstarpu_matrix_data_register(ma_handle, 0, c_loc(ma), Y, Y, Z, c_sizeof(ma(1,1)))

        ! register 'mb', a matrix of real elements
        call fstarpu_matrix_data_register(mb_handle, 0, c_loc(mb), Z, Z, X, c_sizeof(mb(1,1)))

        ! register 'mc', a matrix of real elements
        call fstarpu_matrix_data_register(mc_handle, 0, c_loc(mc), Y, Y, X, c_sizeof(mc(1,1)))

        ! allocate partitioning filters
        filter_horiz = fstarpu_df_alloc_matrix_filter_block()
        call fstarpu_data_filter_set_nchildren(filter_horiz, Y_parts)

        filter_vert = fstarpu_df_alloc_matrix_filter_vertical_block()
        call fstarpu_data_filter_set_nchildren(filter_vert, X_parts)

        ! apply partitioning
        call fstarpu_data_partition (ma_handle,filter_horiz)
        call fstarpu_data_partition (mb_handle,filter_vert)

        ! fstarpu_data_map_filters is a variable-arity function, the first argument
        ! is the handle of the data to partition, the second argument is the
        ! number of filters to apply recursively. Filters are applied in the
        ! same order as the arguments.
        ! This would be equivalent to fstarpu_data_partition(mc_handle, filter_vert) and
        ! then applying horiz on each sub-data (ie. each column of C)
        call fstarpu_data_map_filters(mc_handle, 2, (/ filter_horiz, filter_vert /))

        ! free data filter structures
        call fstarpu_data_filter_free(filter_horiz)
        call fstarpu_data_filter_free(filter_vert)

end subroutine partition_sgemm_data

subroutine partition_dgemm_data(ma, mb, mc, ma_handle, mb_handle, mc_handle, &
                X, Y, Z, X_parts, Y_parts) bind(C)
        use iso_c_binding, only: c_ptr, c_int       ! C interfacing module
        use fstarpu_mod         ! StarPU interfacing module

        real(8), dimension(:,:), allocatable, target, intent(inout) :: ma, mb, mc
        integer(c_int), intent(in) :: X, Y, Z
        integer(c_int), intent(in) :: X_parts, Y_parts

        type(c_ptr) , intent(inout) :: ma_handle, mb_handle, mc_handle   ! pointer for the matrix data handle
        type(c_ptr) :: filter_horiz
        type(c_ptr) :: filter_vert
        
        ! register 'ma', a matrix of real elements
        call fstarpu_matrix_data_register(ma_handle, 0, c_loc(ma), Y, Y, Z, c_sizeof(ma(1,1)))

        ! register 'mb', a matrix of real elements
        call fstarpu_matrix_data_register(mb_handle, 0, c_loc(mb), Z, Z, X, c_sizeof(mb(1,1)))

        ! register 'mc', a matrix of real elements
        call fstarpu_matrix_data_register(mc_handle, 0, c_loc(mc), Y, Y, X, c_sizeof(mc(1,1)))

        ! allocate partitioning filters
        filter_horiz = fstarpu_df_alloc_matrix_filter_block()
        call fstarpu_data_filter_set_nchildren(filter_horiz, Y_parts)

        filter_vert = fstarpu_df_alloc_matrix_filter_vertical_block()
        call fstarpu_data_filter_set_nchildren(filter_vert, X_parts)

        ! apply partitioning
        call fstarpu_data_partition (ma_handle,filter_horiz)
        call fstarpu_data_partition (mb_handle,filter_vert)

        ! fstarpu_data_map_filters is a variable-arity function, the first argument
        ! is the handle of the data to partition, the second argument is the
        ! number of filters to apply recursively. Filters are applied in the
        ! same order as the arguments.
        ! This would be equivalent to fstarpu_data_partition(mc_handle, filter_vert) and
        ! then applying horiz on each sub-data (ie. each column of C)
        call fstarpu_data_map_filters(mc_handle, 2, (/ filter_horiz, filter_vert /))

        ! free data filter structures
        call fstarpu_data_filter_free(filter_horiz)
        call fstarpu_data_filter_free(filter_vert)

end subroutine partition_dgemm_data

!--------------------------------------------------------------!
subroutine launch_tasks(cl_xgemm, ma_handle, mb_handle, mc_handle, X_parts, Y_parts, kind) bind(C)
        use iso_c_binding, only: c_ptr, c_int       ! C interfacing module
        use fstarpu_mod         ! StarPU interfacing module

        type(c_ptr), intent(inout):: cl_xgemm   ! a pointer for the codelet structure
        type(c_ptr), intent(inout) :: ma_handle, mb_handle, mc_handle   ! pointer for the matrix data handle
        type(c_ptr) :: sub_handleA, sub_handleB, sub_handleC  ! pointer for the sub-data handle
        integer, target :: taskx, tasky
        integer(c_int), intent(in) :: X_parts, Y_parts
        integer(c_int), target, intent(in) :: kind
        
        do taskx=0, X_parts-1
        do tasky=0, Y_parts-1
                ! get partitioned tile
                sub_handleA = fstarpu_data_get_sub_data (ma_handle, 1, (/tasky/))
                sub_handleB = fstarpu_data_get_sub_data (mb_handle, 1, (/taskx/))
                sub_handleC = fstarpu_data_get_sub_data (mc_handle, 2, (/taskx, tasky/))

                call fstarpu_task_insert((/ cl_xgemm, &
                        FSTARPU_R, sub_handleA, &
                        FSTARPU_R, sub_handleB, &
                        FSTARPU_W, sub_handleC, &
                        FSTARPU_VALUE, c_loc(kind), FSTARPU_SZ_C_INT, &
                        C_NULL_PTR /))
        end do
        end do
end subroutine launch_tasks

end module nf_xgemm_func
!--------------------------------------------------------------!

module nf_xgemm
contains
subroutine nf_xgemm_common(kind) bind(C)
        use iso_c_binding, only: c_ptr, c_int, c_funptr     ! C interfacing module
        use fstarpu_mod         ! StarPU interfacing module
        use nf_xgemm_cl
        use nf_xgemm_func

        real, dimension(:,:), allocatable :: s_ma, s_mb, s_mc
        real(8), dimension(:,:), allocatable :: d_ma, d_mb, d_mc

        integer(c_int) :: X = 1024
        integer(c_int) :: Y = 1024
        integer(c_int) :: Z = 1024
        integer(c_int) :: X_parts = 4
        integer(c_int) :: Y_parts = 4

        integer(c_int) :: niter = 10
        integer(c_int), target :: i, j, iter, taskx, tasky
        integer(c_int), intent(in) :: kind
        integer(c_int), target :: r = 1

        type(c_ptr) :: perfmodel_xgemm   ! a pointer for the perfmodel structure
        type(c_ptr) :: cl_xgemm   ! a pointer for the codelet structure
        type(c_ptr) :: ma_handle, mb_handle, mc_handle   ! pointer for the matrix data handle
        type(c_ptr) :: sub_handleA, sub_handleB, sub_handleC  ! pointer for the sub-data handle
        integer(c_int) :: err   ! return status for fstarpu_init

        integer(c_int) :: check = 0

        real flops

        real(c_double) :: start_time ! start clock in usec
        real(c_double) :: end_time   ! end clock in usec

        integer :: start_count, end_count, count_rate, count_max

        call parse_args(X, Y, Z, X_parts, Y_parts, niter, check)
        
        if(kind == 4) then
        call init_problem_data(s_ma, s_mb, s_mc, X, Y, Z)
        else if (kind == 8) then
        call init_problem_data(d_ma, d_mb, d_mc, X, Y, Z)
        end if

        ! initialize StarPU
        err = fstarpu_init(C_NULL_PTR)

        ! allocate an empty perfmodel structure
        perfmodel_xgemm = fstarpu_perfmodel_allocate()

        ! set the perfmodel symbol
        call fstarpu_perfmodel_set_symbol(perfmodel_xgemm, C_CHAR_"nf_xgemm_perf"//C_NULL_CHAR)

        ! set the perfmodel type
        call fstarpu_perfmodel_set_type(perfmodel_xgemm, FSTARPU_HISTORY_BASED)

        ! allocate an empty codelet structure
        cl_xgemm = fstarpu_codelet_allocate()

        ! set the codelet perfmodel
        call fstarpu_codelet_set_model(cl_xgemm, perfmodel_xgemm)

        ! set the codelet name
        call fstarpu_codelet_set_name(cl_xgemm, C_CHAR_"xgemm_codelet"//C_NULL_CHAR)

        ! add a CPU implementation function to the codelet
        call fstarpu_codelet_add_cpu_func(cl_xgemm, C_FUNLOC(nf_cpu_cl_mult))

#ifdef STARPU_SIMGRID
        !CUDA pseudo-implementation of the codelet
        call fstarpu_codelet_add_cuda_func(cl_xgemm, C_FUNLOC(c_loc(r)))
#elif defined (STARPU_USE_CUDA)
        ! add a CUDA implementation function to the codelet
        call fstarpu_codelet_add_cuda_func(cl_xgemm, C_FUNLOC(nf_cuda_cl_mult))
#endif

        ! add Read mode data buffer to the codelet
        call fstarpu_codelet_add_buffer(cl_xgemm, FSTARPU_R)
        ! add Read mode data buffer to the codelet
        call fstarpu_codelet_add_buffer(cl_xgemm, FSTARPU_R)
        ! add Write mode data buffer to the codelet
        call fstarpu_codelet_add_buffer(cl_xgemm, FSTARPU_W)

        if(kind == 4) then
                call partition_xgemm_data(s_ma, s_mb, s_mc, ma_handle, mb_handle, mc_handle, &
                                X, Y, Z, X_parts, Y_parts)
        else if(kind == 8) then
                call partition_xgemm_data(d_ma, d_mb, d_mc, ma_handle, mb_handle, mc_handle, &
                                X, Y, Z, X_parts, Y_parts)
        end if

        ! collect the start clock time
        ! Will be available in starpu-1.4
        ! start_time = fstarpu_timing_now()
        call system_clock(start_count, count_rate, count_max)

        do iter=1, niter
                call launch_tasks(cl_xgemm, ma_handle, mb_handle, mc_handle, X_parts, Y_parts, kind)

                ! wait for task completion
                call fstarpu_task_wait_for_all()
        end do

        ! Will be available in starpu-1.4
        ! end_time = fstarpu_timing_now()
        call system_clock(end_count, count_rate, count_max)

        flops = 2 * real(niter) * real(X) * real(Y) * real(Z)

        ! print '(a, es 10.3, a)', 'Time:', (end_time - start_time) / 1000, ' ms'
        print '(a, es 10.3, a)', 'Time:', real(end_count - start_count) / count_rate * 1000, ' ms'
        ! print '(a, es 10.3)', 'GFlop/s:', flops/(end_time - start_time)/1000
        print '(a, es 10.3)', 'GFlop/s:', flops/(real(end_count - start_count)/count_rate)/1000000000

        ! unpartition
        call fstarpu_data_unpartition(ma_handle, 0)
        call fstarpu_data_unpartition(mb_handle, 0)
        call fstarpu_data_unpartition(mc_handle, 0)

        ! unregister
        call fstarpu_data_unregister(ma_handle)
        call fstarpu_data_unregister(mb_handle)
        call fstarpu_data_unregister(mc_handle)

#ifndef STARPU_SIMGRID
        if(check == 1 .and. kind == 4) then
                call check_output(s_ma, s_mb, s_mc, X, Y, Z)
        else if(check == 1 .and. kind == 8) then
                call check_output(d_ma, d_mb, d_mc, X, Y, Z)
        end if
#endif

        ! free codelet structure
        call fstarpu_codelet_free(cl_xgemm)

        ! shut StarPU down
        call fstarpu_shutdown()

        ! free perfmodel structure (must be called after fstarpu_shutdown)
        !call fstarpu_perfmodel_free(perfmodel_xgemm)

        if(kind == 4) then
                deallocate(s_ma)
                deallocate(s_mb)
                deallocate(s_mc)
        else if(kind == 8) then
                deallocate(d_ma)
                deallocate(d_mb)
                deallocate(d_mc)
        end if

end subroutine nf_xgemm_common

subroutine parse_args(X, Y, Z, X_parts, Y_parts, niter, check) bind(C)
        use iso_c_binding, only: c_int
        integer :: i, j
        integer :: n, m
        character(len=64) :: arg
        character(len=16) :: para

        integer(c_int), intent(inout) :: X, Y, Z
        integer(c_int), intent(inout) :: X_parts, Y_parts
        integer(c_int), intent(inout) :: check

        n=iargc() !number of parameters
        do i = 1, n
                call getarg(i,arg)
                read(arg,'(A16)')para
                !print *, para
                if(para == "-nblocks") then
                        j=i+1
                        call getarg(j,arg)
                        read(arg,'(I16)') m
                        X_parts = m
                        Y_parts = m
                else if (para == "-nblocksx") then
                        j=i+1
                        call getarg(j,arg)
                        read(arg,'(I16)') m
                        X_parts = m
                else if (para == "-nblocksy") then
                        j=i+1
                        call getarg(j,arg)
                        read(arg,'(I16)') m
                        Y_parts = m
                else if (para == "-x") then
                        j=i+1
                        call getarg(j,arg)
                        read(arg,'(I16)') m
                        X = m
                else if (para == "-y") then
                        j=i+1
                        call getarg(j,arg)
                        read(arg,'(I16)') m
                        Y = m
                else if (para == "-z") then
                        j=i+1
                        call getarg(j,arg)
                        read(arg,'(I16)') m
                        Z = m
                else if (para == "-xy") then
                        j=i+1
                        call getarg(j,arg)
                        read(arg,'(I16)') m
                        X = m
                        Y = m
                else if (para == "-iter") then
                        j=i+1
                        call getarg(j,arg)
                        read(arg,'(I16)') m
                        niter = m
                else if (para == "-check") then
                        check = 1
                else if (para == "-help" .or. para == "--help" .or. para == "-h") then
                        call getarg(0,arg)
                        read(arg,'(A16)')para
                        write(*,*) "Usage:",para,"[-nblocks n] [-nblocksx x] [-nblocksy y] [-x x] [-y y]&
                         [-xy n] [-z z] [-iter iter] [-check]"
                        write(*,*) "Currently selected is:",Z,"x",Y,"*",X,"x",Z,X_parts,"x",Y_parts,"blocks",niter,"iterations"    
                end if
        end do
          
end subroutine parse_args
end module nf_xgemm

---
layout: default
title: StarPU Publications
permalink: publications/index.html
full-width: True
---

## StarPU Publications
### Selection by year 


<table align="center" cellpadding="4" cellspacing="2">
<tr align="left" valign="top">
<td><a href="./Year/2024.html">2024</a></td>
<td><a href="./Year/2023.html">2023</a></td>
<td><a href="./Year/2022.html">2022</a></td>
<td><a href="./Year/2021.html">2021</a></td>
<td><a href="./Year/2020.html">2020</a></td>
<td><a href="./Year/2019.html">2019</a></td>
<td><a href="./Year/2018.html">2018</a></td>
<td><a href="./Year/2017.html">2017</a></td>
<td><a href="./Year/2016.html">2016</a></td>
</tr>
<tr align="left" valign="top">
<td><a href="./Year/2015.html">2015</a></td>
<td><a href="./Year/2014.html">2014</a></td>
<td><a href="./Year/2013.html">2013</a></td>
<td><a href="./Year/2012.html">2012</a></td>
<td><a href="./Year/2011.html">2011</a></td>
<td><a href="./Year/2010.html">2010</a></td>
<td><a href="./Year/2009.html">2009</a></td>
<td><a href="./Year/2008.html">2008</a></td>
<td></td>
</tr>
</table>

### Selection by keyword 
<table align="center" cellpadding="3" cellspacing="1">
<tr align="left" valign="top">
<td><a href="./Keyword/GENERAL-PRESENTATIONS.html">General Presentations</a></td>
<td></td>
<td></td>
</tr>
<tr align="left" valign="top">
<td><a href="./Keyword/ON-APPLICATIONS.html">On Applications</a></td>
<td><a href="./Keyword/ON-COMPOSABILITY.html">On Composability</a></td>
<td><a href="./Keyword/ON-MEMORY-CONTROL.html">On Memory Control</a></td>
</tr>
<tr align="left" valign="top">
<td><a href="./Keyword/ON-MPI-SUPPORT.html">On MPI Support</a></td>
<td><a href="./Keyword/ON-OPENMP-SUPPORT-ON-TOP-OF-STARPU.html">On OpenMP Support on top of StarPU</a></td>
<td><a href="./Keyword/ON-PARALLEL-TASKS.html">On Parallel Tasks</a></td>
</tr>
<tr align="left" valign="top">
<td><a href="./Keyword/ON-PERFORMANCE-MODEL-TUNING.html">On Performance Model Tuning</a></td>
<td><a href="./Keyword/ON-PERFORMANCE-VISUALIZATION.html">On Performance Visualization</a></td>
<td><a href="./Keyword/ON-RECURSIVE-TASKS.html">On Recursive Tasks</a></td>
</tr>
<tr align="left" valign="top">
<td><a href="./Keyword/ON-SCHEDULING.html">On Scheduling</a></td>
<td><a href="./Keyword/ON-THE-C-EXTENSIONS.html">On The C Extensions</a></td>
<td><a href="./Keyword/ON-THE-CELL-SUPPORT.html">On The Cell Support</a></td>
</tr>
<tr align="left" valign="top">
<td><a href="./Keyword/ON-THE-SIMULATION-SUPPORT-THROUGH-SIMGRID.html">On The Simulation Support through SimGrid</a></td>
<td></td>
<td></td>
</tr>
<tr align="left" valign="top">
<td><a href="./Keyword/PAPERS-RELATED-TO-STARPU.html">Papers related to StarPU</a></td>
<td></td>
<td></td>
</tr>
</table>


### Complete bibliography 


<table align="center" cellpadding="4" cellspacing="2">
<tr align="left" valign="top">
<td><a href="./Biblio/StarPU.html">Complete bibliography as a single HTML page</a>
</td>
</tr>
<tr align="left" valign="top">
<td><a href="./Biblio/StarPU.bib">Complete bibliography as a single BIBTEX file</a>
</td>
</tr>
</table>

 



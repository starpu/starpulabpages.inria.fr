---
layout: default
title: All publications sorted by year
permalink: publications/Biblio/StarPU.html
full-width: True
---

## All publications sorted by year
### 2024 


- <a name="beaumont:hal-04690154"></a>
Olivier Beaumont, Jean-François David, Lionel Eyraud-Dubois,  and Samuel Thibault.
**Exploiting Processor Heterogeneity to Improve Throughput and Reduce Latency for Deep Neural Network Inference**.
In *SBAC-PAD 2024 - IEEE 36th International Symposium on Computer Architecture and High Performance Computing*, Hilo, Hawaii, United States, November 2024.
<a title="Web link" href="https://hal.science/hal-04690154" target="_blank" onclick="return trackOutboundLink('https://hal.science/hal-04690154');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://hal.science/hal-04690154v1/file/sbac_pad%20%281%29.pdf" target="_blank" onclick="return trackOutboundLink('https://hal.science/hal-04690154v1/file/sbac_pad%20%281%29.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[bibtex-key = beaumont:hal-04690154]




- <a name="beaumont:hal-04668550"></a>
Olivier Beaumont, Jean-François David, Lionel Eyraud-Dubois,  and Samuel Thibault.
**StarONNX : Un ordonanceur dynamique pour une inférence rapide et à haut débit sur des ressources hétérogènes**.
In *Compas 2024 - Conférence francophone d'informatique en Parallélisme, Architecture et Système*, Nantes, France, July 2024.
<a title="Web link" href="https://inria.hal.science/hal-04668550" target="_blank" onclick="return trackOutboundLink('https://inria.hal.science/hal-04668550');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://inria.hal.science/hal-04668550/file/staronnx_compas2024.pdf" target="_blank" onclick="return trackOutboundLink('https://inria.hal.science/hal-04668550/file/staronnx_compas2024.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[bibtex-key = beaumont:hal-04668550]




- <a name="beaumont:hal-04646530"></a>
Olivier Beaumont, Jean-François David, Lionel Eyraud-Dubois,  and Samuel Thibault.
**StarONNX: a Dynamic Scheduler for Low Latency and High Throughput Inference on Heterogeneous Resources**.
In *HeteroPar 2024 - 22ND INTERNATIONAL WORKSHOP Algorithms, Models and Tools for Parallel Computing on Heterogeneous Platforms*, HeteroPar'24 Proceedings, Madrid, Spain, August 2024.
EuroPar'24.
Note: (collocated with Euro-Par 2024).
<a title="Web link" href="https://inria.hal.science/hal-04646530" target="_blank" onclick="return trackOutboundLink('https://inria.hal.science/hal-04646530');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://inria.hal.science/hal-04646530/file/heteropar.pdf" target="_blank" onclick="return trackOutboundLink('https://inria.hal.science/hal-04646530/file/heteropar.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[bibtex-key = beaumont:hal-04646530]




- <a name="furmento:hal-04548787"></a>
Nathalie Furmento, Abdou Guermouche, Gwenolé Lucas, Thomas Morin, Samuel Thibault,  and Pierre-André Wacrenier.
**Optimizing Parallel System Efficiency: Dynamic Task Graph Adaptation with Recursive Tasks**.
In *WAMTA 2024 - Workshop on Asynchronous Many-Task Systems and Applications 2024*, Knoxville, United States, February 2024.
https://wamta24.icl.utk.edu/.
<a title="Web link" href="https://inria.hal.science/hal-04548787" target="_blank" onclick="return trackOutboundLink('https://inria.hal.science/hal-04548787');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://inria.hal.science/hal-04548787/file/wamta24.pdf" target="_blank" onclick="return trackOutboundLink('https://inria.hal.science/hal-04548787/file/wamta24.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[bibtex-key = furmento:hal-04548787]




- <a name="gonthier:hal-04500281"></a>
Maxime Gonthier, Elisabeth Larsson, Loris Marchal, Carl Nettelblad,  and Samuel Thibault.
**Data-Driven Locality-Aware Batch Scheduling**.
In *APDCM 2024 - 26th Workshop on Advances in Parallel and Distributed Computational Models*, San Francisco, United States, May 2024.
38th IEEE International Parallel and Distributed Processing Symposium.
<a title="Web link" href="https://inria.hal.science/hal-04500281" target="_blank" onclick="return trackOutboundLink('https://inria.hal.science/hal-04500281');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://inria.hal.science/hal-04500281/file/Data-Driven%20Locality-Aware%20Batch%20Scheduling.pdf" target="_blank" onclick="return trackOutboundLink('https://inria.hal.science/hal-04500281/file/Data-Driven%20Locality-Aware%20Batch%20Scheduling.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[bibtex-key = gonthier:hal-04500281]




- <a name="pinto2024performance"></a>
Vinicius Garcia Pinto, João VF Lima, Vanderlei Munhoz, Daniel Cordeiro, Emilio Francesquini,  and Márcio Castro.
**Performance Evaluation of Dense Linear Algebra Kernels using Chameleon and StarPU on AWS**.
In *Simpósio em Sistemas Computacionais de Alto Desempenho (SSCAD)*, pages 300-311, 2024.
SBC.
[bibtex-key = pinto2024performance]




### 2023 


- <a name="gonthier:tel-04260094"></a>
Maxime Gonthier.
**Scheduling Under Memory Constraint in Task-based Runtime Systems**.
Ph.D Thesis, Ecole normale supérieure de lyon - ENS LYON, September 2023.
<a title="Web link" href="https://theses.hal.science/tel-04260094" target="_blank" onclick="return trackOutboundLink('https://theses.hal.science/tel-04260094');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://theses.hal.science/tel-04260094/file/GONTHIER_Maxime_2023ENSL0061.pdf" target="_blank" onclick="return trackOutboundLink('https://theses.hal.science/tel-04260094/file/GONTHIER_Maxime_2023ENSL0061.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[bibtex-key = gonthier:tel-04260094]




- <a name="lucas:tel-04316145"></a>
Gwenolé Lucas.
**On the Use of Hierarchical Task for Heterogeneous Architectures**.
Ph.D Thesis, Université de Bordeaux, October 2023.
<a title="Web link" href="https://theses.hal.science/tel-04316145" target="_blank" onclick="return trackOutboundLink('https://theses.hal.science/tel-04316145');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://theses.hal.science/tel-04316145/file/LUCAS_GWENOLE_2023.pdf" target="_blank" onclick="return trackOutboundLink('https://theses.hal.science/tel-04316145/file/LUCAS_GWENOLE_2023.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[bibtex-key = lucas:tel-04316145]




- <a name="agullo:hal-03936659"></a>
Emmanuel Agullo, Alfredo Buttari, Abdou Guermouche, Julien Herrmann,  and Antoine Jego.
**Task-based parallel programming for scalable matrix product algorithms**.
*ACM Transactions on Mathematical Software*, 2023.
<a title="Web link" href="https://hal.science/hal-03936659" target="_blank" onclick="return trackOutboundLink('https://hal.science/hal-03936659');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://hal.science/hal-03936659v2/file/journal-nocopy.pdf" target="_blank" onclick="return trackOutboundLink('https://hal.science/hal-03936659v2/file/journal-nocopy.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[doi:<a href="http://dx.doi.org/10.1145/3583560">10.1145/3583560</a>]
[bibtex-key = agullo:hal-03936659]




- <a name="denis:hal-04236246"></a>
Alexandre Denis, Emmanuel Jeannot, Philippe Swartvagher,  and Samuel Thibault.
**Tracing task-based runtime systems: Feedbacks from the StarPU case**.
*Concurrency and Computation: Practice and Experience*, pp 24, October 2023.
<a title="Web link" href="https://inria.hal.science/hal-04236246" target="_blank" onclick="return trackOutboundLink('https://inria.hal.science/hal-04236246');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://inria.hal.science/hal-04236246/file/article.pdf" target="_blank" onclick="return trackOutboundLink('https://inria.hal.science/hal-04236246/file/article.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[doi:<a href="http://dx.doi.org/10.1002/cpe.7920">10.1002/cpe.7920</a>]
[bibtex-key = denis:hal-04236246]




- <a name="faverge:hal-04088833"></a>
Mathieu Faverge, Nathalie Furmento, Abdou Guermouche, Gwenolé Lucas, Raymond Namyst, Samuel Thibault,  and Pierre-André Wacrenier.
**Programming Heterogeneous Architectures Using Hierarchical Tasks**.
*Concurrency and Computation: Practice and Experience*, 2023.
<a title="Web link" href="https://hal.science/hal-04088833" target="_blank" onclick="return trackOutboundLink('https://hal.science/hal-04088833');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://hal.science/hal-04088833v1/file/CCPEpaper.pdf" target="_blank" onclick="return trackOutboundLink('https://hal.science/hal-04088833v1/file/CCPEpaper.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[doi:<a href="http://dx.doi.org/10.1002/cpe.7811">10.1002/cpe.7811</a>]
[bibtex-key = faverge:hal-04088833]




- <a name="gonthier:hal-03623220"></a>
Maxime Gonthier, Loris Marchal,  and Samuel Thibault.
**Taming data locality for task scheduling under memory constraint in runtime systems**.
*Future Generation Computer Systems*, 2023.
<a title="Web link" href="https://hal.inria.fr/hal-03623220" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-03623220');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://hal.inria.fr/hal-03623220v2/file/fgcs-paper-reviewers-round2-unmarked-submitted.pdf" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-03623220v2/file/fgcs-paper-reviewers-round2-unmarked-submitted.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[doi:<a href="http://dx.doi.org/10.1016/j.future.2023.01.024">10.1016/j.future.2023.01.024</a>]
[bibtex-key = gonthier:hal-03623220]




- <a name="leandronesi:hal-04005071"></a>
Lucas Leandro Nesi, Vinicius Garcia Pinto, Lucas Mello Schnorr,  and Arnaud Legrand.
**Summarizing task-based applications behavior over many nodes through progression clustering**.
In *PDP 2023 - 31st Euromicro International Conference on Parallel, Distributed, and Network-Based Processing*, Naples, Italy, pages 1-8, March 2023.
<a title="Web link" href="https://hal.science/hal-04005071" target="_blank" onclick="return trackOutboundLink('https://hal.science/hal-04005071');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://hal.science/hal-04005071/file/PDP.pdf" target="_blank" onclick="return trackOutboundLink('https://hal.science/hal-04005071/file/PDP.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[doi:<a href="http://dx.doi.org/10.1109/PDP59025.2023.00014">10.1109/PDP59025.2023.00014</a>]
[bibtex-key = leandronesi:hal-04005071]




- <a name="thibault:hal-04115280"></a>
Samuel Thibault.
**Vector operations, tiled operations, distributed execution, task graphs, ... What next ?**.
In *15th Joint Laboratory for Extreme Scale Computing (JLESC) Workshop*, Talence, France, March 2023.
<a title="Web link" href="https://inria.hal.science/hal-04115280" target="_blank" onclick="return trackOutboundLink('https://inria.hal.science/hal-04115280');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://inria.hal.science/hal-04115280/file/23-03-21-jlesc.pdf" target="_blank" onclick="return trackOutboundLink('https://inria.hal.science/hal-04115280/file/23-03-21-jlesc.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[bibtex-key = thibault:hal-04115280]




- <a name="gonthier:hal-04090595"></a>
Maxime Gonthier, Loris Marchal,  and Samuel Thibault.
**Memory-Aware Scheduling Of Tasks Sharing Data On Multiple GPUs**.
ISC 2023 - ISC High Performance 2023, May 2023.
Note: Poster.
<a title="Web link" href="https://inria.hal.science/hal-04090595" target="_blank" onclick="return trackOutboundLink('https://inria.hal.science/hal-04090595');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://inria.hal.science/hal-04090595/file/Poster.pdf" target="_blank" onclick="return trackOutboundLink('https://inria.hal.science/hal-04090595/file/Poster.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[bibtex-key = gonthier:hal-04090595]




- <a name="gonthier:hal-04090612"></a>
Maxime Gonthier, Loris Marchal,  and Samuel Thibault.
**Memory-Aware Scheduling of Tasks Sharing Data on Multiple GPUs**.
JLESC 2023 -15th JLESC Workshop, March 2023.
<a title="Web link" href="https://inria.hal.science/hal-04090612" target="_blank" onclick="return trackOutboundLink('https://inria.hal.science/hal-04090612');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://inria.hal.science/hal-04090612/file/jlesc%20%281%29.pdf" target="_blank" onclick="return trackOutboundLink('https://inria.hal.science/hal-04090612/file/jlesc%20%281%29.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[bibtex-key = gonthier:hal-04090612]




### 2022 


- <a name="lion:tel-04213186"></a>
Romain Lion.
**Réplication de données pour la tolérance aux pannes dans un support d'exécution distribué à base de tâches**.
Ph.D Thesis, Université de Bordeaux, December 2022.
<a title="Web link" href="https://theses.hal.science/tel-04213186" target="_blank" onclick="return trackOutboundLink('https://theses.hal.science/tel-04213186');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://theses.hal.science/tel-04213186/file/LION_ROMAIN_2022.pdf" target="_blank" onclick="return trackOutboundLink('https://theses.hal.science/tel-04213186/file/LION_ROMAIN_2022.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[bibtex-key = lion:tel-04213186]




- <a name="swartvagher:tel-03989856"></a>
Philippe Swartvagher.
**On the Interactions between HPC Task-based Runtime Systems and Communication Libraries**.
Ph.D Thesis, Université de Bordeaux, November 2022.
<a title="Web link" href="https://theses.hal.science/tel-03989856" target="_blank" onclick="return trackOutboundLink('https://theses.hal.science/tel-03989856');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://theses.hal.science/tel-03989856/file/SWARTVAGHER_PHILIPPE__2022.pdf" target="_blank" onclick="return trackOutboundLink('https://theses.hal.science/tel-03989856/file/SWARTVAGHER_PHILIPPE__2022.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[bibtex-key = swartvagher:tel-03989856]




- <a name="miletto:hal-03298021"></a>
Marcelo Cogo Miletto, Lucas Leandro Nesi, Lucas Mello Schnorr,  and Arnaud Legrand.
**Performance Analysis of Irregular Task-Based Applications on Hybrid Platforms: Structure Matters**.
*Future Generation Computer Systems*, 135, October 2022.
<a title="Web link" href="https://inria.hal.science/hal-03298021" target="_blank" onclick="return trackOutboundLink('https://inria.hal.science/hal-03298021');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://inria.hal.science/hal-03298021v2/file/elsevier.pdf" target="_blank" onclick="return trackOutboundLink('https://inria.hal.science/hal-03298021v2/file/elsevier.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[doi:<a href="http://dx.doi.org/10.1016/j.future.2022.05.013">10.1016/j.future.2022.05.013</a>]
[bibtex-key = miletto:hal-03298021]




- <a name="faverge:hal-03789625"></a>
Mathieu Faverge, Nathalie Furmento, Abdou Guermouche, Gwenolé Lucas, Raymond Namyst, Samuel Thibault,  and Pierre-André Wacrenier.
**Programming Heterogeneous Architectures Using Hierarchical Tasks**.
In *HeteroPar 2022*, Glasgow, United Kingdom, pages 12, August 2022.
<a title="Web link" href="https://hal.science/hal-03789625" target="_blank" onclick="return trackOutboundLink('https://hal.science/hal-03789625');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://hal.science/hal-03789625/file/paper%20%281%29.pdf" target="_blank" onclick="return trackOutboundLink('https://hal.science/hal-03789625/file/paper%20%281%29.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[doi:<a href="http://dx.doi.org/10.1007/978-3-031-31209-0_7">10.1007/978-3-031-31209-0_7</a>]
[bibtex-key = faverge:hal-03789625]




- <a name="faverge:hal-03773486"></a>
Mathieu Faverge, Nathalie Furmento, Abdou Guermouche, Gwenolé Lucas, Samuel Thibault,  and Pierre-André Wacrenier.
**Programmation des architectures hétérogènes à l'aide de tâches hiérarchiques**.
In *COMPAS 2022 - Conférence francophone d'informatique en Parallélisme, Architecture et Système*, Amiens, France, July 2022.
<a title="Web link" href="https://hal.inria.fr/hal-03773486" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-03773486');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://hal.inria.fr/hal-03773486/file/ComPAS2022_paper_10.pdf" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-03773486/file/ComPAS2022_paper_10.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[bibtex-key = faverge:hal-03773486]




- <a name="gonthier:hal-03552243"></a>
Maxime Gonthier, Samuel Thibault,  and Loris Marchal.
**Memory-Aware Scheduling of Tasks Sharing Data on Multiple GPUs with Dynamic Runtime Systems**.
In *IPDPS 2022 - 36th IEEE International Parallel & Distributed Processing Symposium*, Lyon, France, May 2022.
IEEE.
<a title="Web link" href="https://hal.inria.fr/hal-03552243" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-03552243');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://hal.inria.fr/hal-03552243/file/IPDPS-camera-ready.pdf" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-03552243/file/IPDPS-camera-ready.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[doi:<a href="http://dx.doi.org/10.1109/IPDPS53621.2022.00073">10.1109/IPDPS53621.2022.00073</a>]
[bibtex-key = gonthier:hal-03552243]




- <a name="agullo:hal-03588491"></a>
Emmanuel Agullo, Alfredo Buttari, Abdou Guermouche, Julien Herrmann,  and Antoine Jego.
**Task-Based Parallel Programming for Scalable Algorithms: application to Matrix Multiplication**.
Research Report 9461, Inria Bordeaux - Sud-Ouest, February 2022.
<a title="Web link" href="https://hal.inria.fr/hal-03588491" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-03588491');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://hal.inria.fr/hal-03588491/file/RR-9461.pdf" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-03588491/file/RR-9461.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[bibtex-key = agullo:hal-03588491]




- <a name="agullo:hal-03773985"></a>
Emmanuel Agullo, Olivier Coulaud, Alexandre Denis, Mathieu Faverge, Alain A. Franc, Jean-Marc Frigerio, Nathalie Furmento, Samuel Thibault, Adrien Guilbaud, Emmanuel Jeannot, Romain Peressoni,  and Florent Pruvost.
**Task-based randomized singular value decomposition and multidimensional scaling**.
Research Report 9482, Inria Bordeaux - Sud Ouest ; Inrae - BioGeCo, September 2022.
<a title="Web link" href="https://hal.inria.fr/hal-03773985" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-03773985');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://hal.inria.fr/hal-03773985/file/RR-9482.pdf" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-03773985/file/RR-9482.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[bibtex-key = agullo:hal-03773985]




- <a name="faverge:hal-03609275"></a>
Mathieu Faverge, Nathalie Furmento, Gwenolé Lucas, Abdou Guermouche, Raymond Namyst, Samuel Thibault,  and Pierre-André Wacrenier.
**Programming Heterogeneous Architectures Using Hierarchical Tasks**.
Research Report RR-9466, Inria Bordeaux Sud-Ouest, March 2022.
<a title="Web link" href="https://hal.inria.fr/hal-03609275" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-03609275');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://hal.inria.fr/hal-03609275/file/RR-9466.pdf" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-03609275/file/RR-9466.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[bibtex-key = faverge:hal-03609275]




### 2021 


- <a name="agullo:hal-03348787"></a>
Emmanuel Agullo, Mirco Altenbernd, Hartwig Anzt, Leonardo Bautista-Gomez, Tommaso Benacchio, Luca Bonaventura, Hans-Joachim Bungartz, Sanjay Chatterjee, Florina M Ciorba, Nathan Debardeleben, Daniel Drzisga, Sebastian Eibl, Christian Engelmann, Wilfried N Gansterer, Luc Giraud, Dominik Göddeke, Marco Heisig, Fabienne Jézéquel, Nils Kohl, Sherry Xiaoye, Romain Lion, Miriam Mehl, Paul Mycek, Michael Obersteiner, Enrique S Quintana-Ortì, Francesco Rizzi, Ulrich Rüde, Martin Schulz, Fred Fung, Robert Speck, Linda Stals, Keita Teranishi, Samuel Thibault, Dominik Thönnes, Andreas Wagner,  and Barbara Wohlmuth.
**Resiliency in numerical algorithm design for extreme scale simulations**.
*International Journal of High Performance Computing Applications*, September 2021.
<a title="Web link" href="https://hal.inria.fr/hal-03348787" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-03348787');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://hal.inria.fr/hal-03348787/file/2010.13342.pdf" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-03348787/file/2010.13342.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[doi:<a href="http://dx.doi.org/10.1177/10943420211055188">10.1177/10943420211055188</a>]
[bibtex-key = agullo:hal-03348787]




- <a name="papadopoulos:hal-03318644"></a>
Lazaros Papadopoulos, Dimitrios Soudris, Christoph Kessler, August Ernstsson, Johan Ahlqvist, Nikos Vasilas, Athanasios I Papadopoulos, Panos Seferlis, Charles Prouveur, Matthieu Haefele, Samuel Thibault, Athanasios Salamanis, Theodoros Ioakimidis,  and Dionysios Kehagias.
**EXA2PRO: A Framework for High Development Productivity on Heterogeneous Computing Systems**.
*IEEE Transactions on Parallel and Distributed Systems*, August 2021.
<a title="Web link" href="https://hal.inria.fr/hal-03318644" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-03318644');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://hal.inria.fr/hal-03318644/file/EXA2PRO___TPDS.pdf" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-03318644/file/EXA2PRO___TPDS.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[doi:<a href="http://dx.doi.org/10.1109/TPDS.2021.3104257">10.1109/TPDS.2021.3104257</a>]
[bibtex-key = papadopoulos:hal-03318644]




- <a name="denis:hal-03290121"></a>
Alexandre Denis, Emmanuel Jeannot,  and Philippe Swartvagher.
**Interferences between Communications and Computations in Distributed HPC Systems**.
In *ICPP 2021 - 50th International Conference on Parallel Processing*, Chicago / Virtual, United States, pages 11, August 2021.
<a title="Web link" href="https://hal.inria.fr/hal-03290121" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-03290121');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://hal.inria.fr/hal-03290121/file/rr.pdf" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-03290121/file/rr.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[doi:<a href="http://dx.doi.org/10.1145/3472456.3473516">10.1145/3472456.3473516</a>]
[bibtex-key = denis:hal-03290121]




- <a name="gonthier:hal-03290998"></a>
Maxime Gonthier, Loris Marchal,  and Samuel Thibault.
**Locality-Aware Scheduling of Independent Tasks for Runtime Systems**.
In *COLOC: 5th workshop on data locality - 7th International European Conference on Parallel and Distributed Computing Workshops*, Lisbon, Portugal, August 2021.
<a title="Web link" href="https://hal.archives-ouvertes.fr/hal-03290998" target="_blank" onclick="return trackOutboundLink('https://hal.archives-ouvertes.fr/hal-03290998');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://hal.archives-ouvertes.fr/hal-03290998/file/coloc-cameraready-submitted.pdf" target="_blank" onclick="return trackOutboundLink('https://hal.archives-ouvertes.fr/hal-03290998/file/coloc-cameraready-submitted.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[doi:<a href="http://dx.doi.org/10.1007/978-3-031-06156-1_1">10.1007/978-3-031-06156-1_1</a>]
[bibtex-key = gonthier:hal-03290998]




- <a name="pintohwc2021"></a>
Vinicius Garcia Pinto, Lucas Leandro Nesi, Marcelo Cogo Miletto,  and Lucas Mello Schnorr.
**Providing In-depth Performance Analysis for Heterogeneous Task-based Applications with StarVZ**.
In *2021 IEEE International Parallel and Distributed Processing Symposium (IPDPS)*, May 2021.
<a title="Web link" href="https://drive.google.com/file/d/18IYdLW9eiSDzwMM3RJV0Flp83OcBWp5R/view?usp=sharing" target="_blank" onclick="return trackOutboundLink('https://drive.google.com/file/d/18IYdLW9eiSDzwMM3RJV0Flp83OcBWp5R/view?usp=sharing');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
[doi:<a href="http://dx.doi.org/10.1109/IPDPSW52791.2021.00013">10.1109/IPDPSW52791.2021.00013</a>]
[bibtex-key = pintohwc2021]




- <a name="gonthier:hal-03144290"></a>
Maxime Gonthier, Loris Marchal,  and Samuel Thibault.
**Locality-Aware Scheduling of Independant Tasks for Runtime Systems**.
Research Report RR-9394, Inria, 2021.
<a title="Web link" href="https://hal.inria.fr/hal-03144290" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-03144290');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://hal.inria.fr/hal-03144290v5/file/RR_coloc2021-submitted.pdf" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-03144290v5/file/RR_coloc2021-submitted.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[bibtex-key = gonthier:hal-03144290]




### 2020 


- <a name="daoudi:hal-02933803"></a>
Idriss Daoudi, Philippe Virouleau, Thierry Gautier, Samuel Thibault,  and Olivier Aumage.
**sOMP: Simulating OpenMP Task-Based Applications with NUMA Effects**.
In *IWOMP 2020 - 16th International Workshop on OpenMP*, volume 12295 of *LNCS*, Austin, USA, September 2020.
Springer.
<a title="Web link" href="https://hal.inria.fr/hal-02933803" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-02933803');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://hal.inria.fr/hal-02933803/file/p05_daoudi.pdf" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-02933803/file/p05_daoudi.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[doi:<a href="http://dx.doi.org/10.1007/978-3-030-58144-2_13">10.1007/978-3-030-58144-2_13</a>]
[bibtex-key = daoudi:hal-02933803]




- <a name="denis:hal-02872765"></a>
Alexandre Denis, Emmanuel Jeannot, Philippe Swartvagher,  and Samuel Thibault.
**Using Dynamic Broadcasts to improve Task-Based Runtime Performances**.
In *Euro-Par - 26th International European Conference on Parallel and Distributed Computing*, Warsaw, Poland, August 2020.
Rzadca and Malawski, Springer.
<a title="Web link" href="https://hal.inria.fr/hal-02872765" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-02872765');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://hal.inria.fr/hal-02872765/file/dynamic_broadcasts.pdf" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-02872765/file/dynamic_broadcasts.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[doi:<a href="http://dx.doi.org/10.1007/978-3-030-57675-2_28">10.1007/978-3-030-57675-2_28</a>]
[bibtex-key = denis:hal-02872765]




- <a name="lion:hal-02970529"></a>
Romain Lion and Samuel Thibault.
**From tasks graphs to asynchronous distributed checkpointing with local restart**.
In *2020 IEEE/ACM 10th Workshop on Fault Tolerance for HPC at eXtreme Scale (FTXS)*, Atlanta, USA, November 2020.
<a title="Web link" href="https://hal.archives-ouvertes.fr/hal-02970529" target="_blank" onclick="return trackOutboundLink('https://hal.archives-ouvertes.fr/hal-02970529');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://hal.archives-ouvertes.fr/hal-02970529/file/2020001221.pdf" target="_blank" onclick="return trackOutboundLink('https://hal.archives-ouvertes.fr/hal-02970529/file/2020001221.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[doi:<a href="http://dx.doi.org/10.1109/FTXS51974.2020.00009">10.1109/FTXS51974.2020.00009</a>]
[bibtex-key = lion:hal-02970529]




- <a name="lopes:hal-02914793"></a>
Rafael Alvares da Silva Lopes, Samuel Thibault,  and Alba Cristina Magalhães Alves de Melo.
**MASA-StarPU: Parallel Sequence Comparison with Multiple Scheduling Policies and Pruning**.
In *SBAC-PAD 2020 - IEEE 32nd International Symposium on Computer Architecture and High Performance Computing*, Porto, Portugal, September 2020.
<a title="Web link" href="https://hal.inria.fr/hal-02914793" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-02914793');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://hal.inria.fr/hal-02914793/file/lopes_rafael_paper25_sbacpad2020.pdf" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-02914793/file/lopes_rafael_paper25_sbacpad2020.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[doi:<a href="http://dx.doi.org/10.1109/SBAC-PAD49847.2020.00039">10.1109/SBAC-PAD49847.2020.00039</a>]
[bibtex-key = lopes:hal-02914793]




- <a name="10.5555/3433701.3433783"></a>
Elliott Slaughter, Wei Wu, Yuankun Fu, Legend Brandenburg, Nicolai Garcia, Wilhem Kautz, Emily Marx, Kaleb S. Morris, Qinglei Cao, George Bosilca, Seema Mirchandaney, Wonchan Lee, Sean Treichler, Patrick McCormick,  and Alex Aiken.
**Task Bench: A Parameterized Benchmark for Evaluating Parallel Runtime Performance**.
In *Proceedings of the International Conference for High Performance Computing, Networking, Storage and Analysis*, SC '20, 2020.
IEEE Press.
**ISBN:** 9781728199986.
<a title="Web link" href="https://dl.acm.org/doi/10.5555/3433701.3433783" target="_blank" onclick="return trackOutboundLink('https://dl.acm.org/doi/10.5555/3433701.3433783');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://dl.acm.org/doi/pdf/10.5555/3433701.3433783" target="_blank" onclick="return trackOutboundLink('https://dl.acm.org/doi/pdf/10.5555/3433701.3433783');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[doi:<a href="http://dx.doi.org/10.5555/3433701.3433783">10.5555/3433701.3433783</a>]
<center>
<table border=1 align=center width=80%>
<tr>
Abstract: <td>
We present Task Bench, a parameterized benchmark designed to explore the performance of distributed programming systems under a variety of application scenarios. Task Bench dramatically lowers the barrier to benchmarking and comparing multiple programming systems by making the implementation for a given system orthogonal to the benchmarks themselves: every benchmark constructed with Task Bench runs on every Task Bench implementation. Furthermore, Task Bench's parameterization enables a wide variety of benchmark scenarios that distill the key characteristics of larger applications.To assess the effectiveness and overheads of the tested systems, we introduce a novel metric, minimum effective task granularity (METG). We conduct a comprehensive study with 15 programming systems on up to 256 Haswell nodes of the Cori supercomputer. Running at scale, 100Î¼s-long tasks are the finest granularity that any system runs efficiently with current technologies. We also study each system's scalability, ability to hide communication and mitigate load imbalance.</td>

</tr></table></center>
[bibtex-key = 10.5555/3433701.3433783]




- <a name="thibault:hal-02943753"></a>
Samuel Thibault, Luka Stanisic,  and Arnaud Legrand.
**Faithful Performance Prediction of a Dynamic Task-based Runtime System, an Opportunity for Task Graph Scheduling**.
In *SIAM Conference on Parallel Processing for Scientific Computing (SIAM PP 2020)*, Seattle, USA, February 2020.
<a title="Web link" href="https://hal.inria.fr/hal-02943753" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-02943753');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://hal.inria.fr/hal-02943753/file/20-02-15-siampp-seattle.pdf" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-02943753/file/20-02-15-siampp-seattle.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[bibtex-key = thibault:hal-02943753]




- <a name="soni:hal-02985721"></a>
Georgios Tzanos, Vineet Soni, Charles Prouveur, Matthieu Haefele, Stavroula Zouzoula, Lazaros Papadopoulos, Samuel Thibault, Nicolas Vandenbergen, Dirk Pleiter,  and Dimitrios Soudris.
**Applying StarPU runtime system to scientific applications: Experiences and lessons learned**.
In *Parallel Optimization using/for Multi and Many-core High Performance Computing (POMCO)*, Barcelona, Spain, December 2020.
<a title="Web link" href="https://hal.inria.fr/hal-02985721" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-02985721');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://hal.inria.fr/hal-02985721/file/POMCO2020-camera-ready.pdf" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-02985721/file/POMCO2020-camera-ready.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[bibtex-key = soni:hal-02985721]




### 2019 


- <a name="bramas:hal-02120736"></a>
Bérenger Bramas.
**Impact study of data locality on task-based applications through the Heteroprio scheduler**.
*PeerJ Computer Science*, May 2019.
<a title="Web link" href="https://hal.inria.fr/hal-02120736" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-02120736');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://hal.inria.fr/hal-02120736/file/peerj-cs-190.pdf" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-02120736/file/peerj-cs-190.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[doi:<a href="http://dx.doi.org/10.7717/peerj-cs.190">10.7717/peerj-cs.190</a>]
[bibtex-key = bramas:hal-02120736]




- <a name="alonazi:hal-02403109"></a>
A. AlOnazi, H. Ltaief, D. Keyes, I. Said,  and Samuel Thibault.
**Asynchronous Task-Based Execution of the Reverse Time Migration for the Oil and Gas Industry**.
In *2019 IEEE International Conference on Cluster Computing (CLUSTER)*, Albuquerque, USA, pages 1-11, September 2019.
IEEE.
<a title="Web link" href="https://hal.inria.fr/hal-02403109" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-02403109');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://hal.inria.fr/hal-02403109/file/2019_tbrtm_cluster.pdf" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-02403109/file/2019_tbrtm_cluster.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[doi:<a href="http://dx.doi.org/10.1109/CLUSTER.2019.8891054">10.1109/CLUSTER.2019.8891054</a>]
[bibtex-key = alonazi:hal-02403109]




- <a name="leandronesi:hal-02275363"></a>
Lucas Leandro Nesi, Samuel Thibault, Luka Stanisic,  and Lucas Mello Schnorr.
**Visual Performance Analysis of Memory Behavior in a Task-Based Runtime on Hybrid Platforms**.
In *2019 19th IEEE/ACM International Symposium on Cluster, Cloud and Grid Computing (CCGRID)*, Larnaca, Cyprus, pages 142-151, May 2019.
IEEE.
<a title="Web link" href="https://hal.inria.fr/hal-02275363" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-02275363');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://hal.inria.fr/hal-02275363/file/CCGRID_camera_ready.pdf" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-02275363/file/CCGRID_camera_ready.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[doi:<a href="http://dx.doi.org/10.1109/CCGRID.2019.00025">10.1109/CCGRID.2019.00025</a>]
[bibtex-key = leandronesi:hal-02275363]




- <a name="lion:hal-02296118"></a>
Romain Lion.
**Tolérance aux pannes dans l'exécution distribuée de graphes de tâches**.
In *Conférence d'informatique en Parallélisme, Architecture et Système*, Anglet, France, June 2019.
<a title="Web link" href="https://hal.inria.fr/hal-02296118" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-02296118');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://hal.inria.fr/hal-02296118/file/Compas_Romain_LION_submitted_final.pdf" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-02296118/file/Compas_Romain_LION_submitted_final.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[bibtex-key = lion:hal-02296118]




- <a name="alias:hal-02421327"></a>
Christophe Alias, Samuel Thibault,  and Laure Gonnord.
**A Compiler Algorithm to Guide Runtime Scheduling**.
Research Report RR-9315, INRIA Grenoble ; INRIA Bordeaux, December 2019.
<a title="Web link" href="https://hal.inria.fr/hal-02421327" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-02421327');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://hal.inria.fr/hal-02421327/file/RR-9315.pdf" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-02421327/file/RR-9315.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[bibtex-key = alias:hal-02421327]




### 2018 


- <a name="cojean:tel-01816341"></a>
Terry Cojean.
**Programmation of heterogeneous architectures using moldable tasks**.
Ph.D Thesis, Université de Bordeaux, March 2018.
<a title="Web link" href="https://tel.archives-ouvertes.fr/tel-01816341" target="_blank" onclick="return trackOutboundLink('https://tel.archives-ouvertes.fr/tel-01816341');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://tel.archives-ouvertes.fr/tel-01816341/file/COJEAN_TERRY_2018.pdf" target="_blank" onclick="return trackOutboundLink('https://tel.archives-ouvertes.fr/tel-01816341/file/COJEAN_TERRY_2018.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[bibtex-key = cojean:tel-01816341]




- <a name="thibault:tel-01959127"></a>
Samuel Thibault.
**On Runtime Systems for Task-based Programming on Heterogeneous Platforms**.
Habilitation à diriger des recherches, Université de Bordeaux, December 2018.
<a title="Web link" href="https://hal.inria.fr/tel-01959127" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/tel-01959127');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://hal.inria.fr/tel-01959127/file/hdr.pdf" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/tel-01959127/file/hdr.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
<a title="BIB" href="https://inria.hal.science/tel-01959127v1/bibtex" target="_blank" onclick="return trackOutboundLink('https://inria.hal.science/tel-01959127v1/bibtex');"><i class="fa fa-book" aria-hidden="true"></i></a>
[bibtex-key = thibault:tel-01959127]




- <a name="refId0"></a>
Mohamed Essadki, Jonathan Jung, Adam Larat, Milan Pelletier,  and Vincent Perrier.
**A Task-Driven Implementation of a Simple Numerical Solver for Hyperbolic Conservation Laws**.
*ESAIM: ProcS*, 63:228-247, 2018.
<a title="Web link" href="https://doi.org/10.1051/proc/201863228" target="_blank" onclick="return trackOutboundLink('https://doi.org/10.1051/proc/201863228');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://www.esaim-proc.org/articles/proc/pdf/2018/03/proc_esaim2018_228.pdf" target="_blank" onclick="return trackOutboundLink('https://www.esaim-proc.org/articles/proc/pdf/2018/03/proc_esaim2018_228.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[doi:<a href="http://dx.doi.org/10.1051/proc/201863228">10.1051/proc/201863228</a>]
[bibtex-key = refId0]




- <a name="garciapinto:hal-01616632"></a>
Vinicius Garcia Pinto, Lucas Mello Schnorr, Luka Stanisic, Arnaud Legrand, Samuel Thibault,  and Vincent Danjean.
**A Visual Performance Analysis Framework for Task-based Parallel Applications running on Hybrid Clusters**.
*CCPE - Concurrency and Computation: Practice and Experience*, 30, April 2018.
<a title="Web link" href="https://hal.inria.fr/hal-01616632" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-01616632');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://hal.inria.fr/hal-01616632/file/CCPE_article_submitted_2018_02_06.pdf" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-01616632/file/CCPE_article_submitted_2018_02_06.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[doi:<a href="http://dx.doi.org/10.1002/cpe.4472">10.1002/cpe.4472</a>]
[bibtex-key = garciapinto:hal-01616632]




- <a name="thoman2018taxonomy"></a>
Peter Thoman, Kiril Dichev, Thomas Heller, Roman Iakymchuk, Xavier Aguilar, Khalid Hasanov, Philipp Gschwandtner, Pierre Lemarinier, Stefano Markidis, Herbert Jordan,  and others.
**A taxonomy of task-based parallel programming technologies for high-performance computing**.
*The Journal of Supercomputing*, 74(4):1422-1434, 2018.
<a title="Web link" href="https://link.springer.com/article/10.1007%2Fs11227-018-2238-4" target="_blank" onclick="return trackOutboundLink('https://link.springer.com/article/10.1007%2Fs11227-018-2238-4');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://link.springer.com/content/pdf/10.1007/s11227-018-2238-4.pdf" target="_blank" onclick="return trackOutboundLink('https://link.springer.com/content/pdf/10.1007/s11227-018-2238-4.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[doi:<a href="http://dx.doi.org/10.1007/s11227-018-2238-4">10.1007/s11227-018-2238-4</a>]
[bibtex-key = thoman2018taxonomy]




- <a name="pinto:hal-01842038"></a>
Vinicius Garcia Pinto, Lucas Mello Schnorr, Arnaud Legrand, Samuel Thibault, Luka Stanisic,  and Vincent Danjean.
**Detecção de Anomalias de Desempenho em Aplicações de Alto Desempenho baseadas em Tarefas em Clusters Hìbridos**.
In *WPerformance - 17o Workshop em Desempenho de Sistemas Computacionais e de Comunicação*, Natal, Brazil, July 2018.
<a title="Web link" href="https://hal.inria.fr/hal-01842038" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-01842038');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://hal.inria.fr/hal-01842038/file/181587_1.pdf" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-01842038/file/181587_1.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[bibtex-key = pinto:hal-01842038]




- <a name="soudris:hal-03273509"></a>
Dimitrios Soudris, Lazaros Papadopoulos, Christoph W Kessler, Dionysios D Kehagias, Athanasios Papadopoulos, Panos Seferlis, Alexander Chatzigeorgiou, Apostolos Ampatzoglou, Samuel Thibault, Raymond Namyst, Dirk Pleiter, Georgi Gaydadjiev, Tobias Becker,  and Matthieu Haefele.
**EXA2PRO programming environment**.
In *SAMOS XVIII: Architectures, Modeling, and Simulation*, Pythagorion, Greece, pages 202-209, July 2018.
ACM.
<a title="Web link" href="https://hal.inria.fr/hal-03273509" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-03273509');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://hal.inria.fr/hal-03273509/file/3229631.3239369.pdf" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-03273509/file/3229631.3239369.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[doi:<a href="http://dx.doi.org/10.1145/3229631.3239369">10.1145/3229631.3239369</a>]
[bibtex-key = soudris:hal-03273509]




### 2017 


- <a name="chevalier:hal-01718280"></a>
Arthur Chevalier.
**Critical resources management and scheduling under StarPU**.
Master Thesis, Université de Bordeaux, September 2017.
<a title="Web link" href="https://hal.inria.fr/hal-01718280" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-01718280');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://hal.inria.fr/hal-01718280/file/Memoire.pdf" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-01718280/file/Memoire.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[bibtex-key = chevalier:hal-01718280]




- <a name="kumar:tel-01538516"></a>
Suraj Kumar.
**Scheduling of Dense Linear Algebra Kernels on Heterogeneous Resources**.
Ph.D Thesis, Université de Bordeaux, April 2017.
<a title="Web link" href="https://tel.archives-ouvertes.fr/tel-01538516" target="_blank" onclick="return trackOutboundLink('https://tel.archives-ouvertes.fr/tel-01538516');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://tel.archives-ouvertes.fr/tel-01538516/file/KUMAR_SURAL_2017.pdf" target="_blank" onclick="return trackOutboundLink('https://tel.archives-ouvertes.fr/tel-01538516/file/KUMAR_SURAL_2017.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[bibtex-key = kumar:tel-01538516]




- <a name="agullo:hal-01517153"></a>
Emmanuel Agullo, Olivier Aumage, Berenger Bramas, Olivier Coulaud,  and Samuel Pitoiset.
**Bridging the gap between OpenMP and task-based runtime systems for the fast multipole method**.
*IEEE Transactions on Parallel and Distributed Systems*, April 2017.
<a title="Web link" href="https://hal.inria.fr/hal-01517153" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-01517153');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://hal.inria.fr/hal-01517153/file/tpds_kstar_scalfmm_print.pdf" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-01517153/file/tpds_kstar_scalfmm_print.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[doi:<a href="http://dx.doi.org/10.1109/TPDS.2017.2697857">10.1109/TPDS.2017.2697857</a>]
[bibtex-key = agullo:hal-01517153]




- <a name="agullo:hal-01618526"></a>
Emmanuel Agullo, Olivier Aumage, Mathieu Faverge, Nathalie Furmento, Florent Pruvost, Marc Sergent,  and Samuel Thibault.
**Achieving High Performance on Supercomputers with a Sequential Task-based Programming Model**.
*TPDS - IEEE Transactions on Parallel and Distributed Systems*, December 2017.
<a title="Web link" href="https://hal.inria.fr/hal-01618526" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-01618526');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://hal.inria.fr/hal-01618526/file/tpds14.pdf" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-01618526/file/tpds14.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[doi:<a href="http://dx.doi.org/10.1109/TPDS.2017.2766064">10.1109/TPDS.2017.2766064</a>]
[bibtex-key = agullo:hal-01618526]




- <a name="couteyencarpaye:hal-01507613"></a>
Jean-Marie Couteyen Carpaye, Jean Roman,  and Pierre Brenner.
**Design and Analysis of a Task-based Parallelization over a Runtime System of an Explicit Finite-Volume CFD Code with Adaptive Time Stepping**.
*International Journal of Computational Science and Engineering*, pp 1 - 22, 2017.
<a title="Web link" href="https://hal.inria.fr/hal-01507613" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-01507613');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://hal.inria.fr/hal-01507613/file/flusepa-task-hal-inria-preprint.pdf" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-01507613/file/flusepa-task-hal-inria-preprint.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[doi:<a href="http://dx.doi.org/10.1016/j.jocs.2017.03.008">10.1016/j.jocs.2017.03.008</a>]
[bibtex-key = couteyencarpaye:hal-01507613]




- <a name="7973751"></a>
Olivier Aumage, Julien Bigot, HÃ©lÃ¨ne Coullon, Christian PÃ©rez,  and JÃ©rÃ´me Richard.
**Combining Both a Component Model and a Task-Based Model for HPC Applications: A Feasibility Study on GYSELA**.
In *2017 17th IEEE/ACM International Symposium on Cluster, Cloud and Grid Computing (CCGRID)*, pages 635-644, 2017.
[doi:<a href="http://dx.doi.org/10.1109/CCGRID.2017.88">10.1109/CCGRID.2017.88</a>]
[bibtex-key = 7973751]




- <a name="beaumont:hal-01386174"></a>
Olivier Beaumont, Lionel Eyraud-Dubois,  and Suraj Kumar.
**Approximation Proofs of a Fast and Efficient List Scheduling Algorithm for Task-Based Runtime Systems on Multicores and GPUs**.
In *2017 IEEE International Parallel and Distributed Processing Symposium (IPDPS)*, pages 768-777, May 2017.
<a title="Web link" href="https://hal.inria.fr/hal-01386174" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-01386174');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://hal.inria.fr/hal-01386174/file/heteroPrioApproxProofsRR.pdf" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-01386174/file/heteroPrioApproxProofsRR.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[doi:<a href="http://dx.doi.org/10.1109/IPDPS.2017.71">10.1109/IPDPS.2017.71</a>]
[bibtex-key = beaumont:hal-01386174]




- <a name="agullo:hal-01474556"></a>
Emmanuel Agullo, Bérenger Bramas, Olivier Coulaud, Luka Stanisic,  and Samuel Thibault.
**Modeling Irregular Kernels of Task-based codes: Illustration with the Fast Multipole Method**.
Research Report RR-9036, INRIA Bordeaux, February 2017.
<a title="Web link" href="https://hal.inria.fr/hal-01474556" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-01474556');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://hal.inria.fr/hal-01474556/file/rapport.pdf" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-01474556/file/rapport.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[bibtex-key = agullo:hal-01474556]




- <a name="agullo:hal-01473475"></a>
Emmanuel Agullo, Alfredo Buttari, Mikko Byckling, Abdou Guermouche,  and Ian Masliah.
**Achieving high-performance with a sparse direct solver on Intel KNL**.
Research Report RR-9035, Inria Bordeaux Sud-Ouest ; CNRS-IRIT ; Intel corporation ; Université Bordeaux, February 2017.
<a title="Web link" href="https://hal.inria.fr/hal-01473475" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-01473475');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://hal.inria.fr/hal-01473475/file/RR-9035.pdf" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-01473475/file/RR-9035.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[bibtex-key = agullo:hal-01473475]




### 2016 


- <a name="sergent:tel-01483666"></a>
Marc Sergent.
**Scalability of a task-based runtime system for dense linear algebra applications**.
Ph.D Thesis, Université de Bordeaux, December 2016.
<a title="Web link" href="https://tel.archives-ouvertes.fr/tel-01483666" target="_blank" onclick="return trackOutboundLink('https://tel.archives-ouvertes.fr/tel-01483666');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://tel.archives-ouvertes.fr/tel-01483666/file/SERGENT_MARC_2016.pdf" target="_blank" onclick="return trackOutboundLink('https://tel.archives-ouvertes.fr/tel-01483666/file/SERGENT_MARC_2016.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[bibtex-key = sergent:tel-01483666]




- <a name="cojean:hal-01409965"></a>
Terry Cojean, Abdou Guermouche, Andra Hugo, Raymond Namyst,  and Pierre-André Wacrenier.
**Resource aggregation for task-based Cholesky Factorization on top of modern architectures**.
*Parallel Computing*, 83:73-92, November 2016.
Note: This paper is submitted for review to the Parallel Computing special issue for HCW and HeteroPar 16 workshops.
<a title="Web link" href="https://hal.inria.fr/hal-01409965" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-01409965');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://hal.inria.fr/hal-01409965/file/submission.pdf" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-01409965/file/submission.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[doi:<a href="http://dx.doi.org/10.1016/j.parco.2018.10.007">10.1016/j.parco.2018.10.007</a>]
[bibtex-key = cojean:hal-01409965]




- <a name="agullo:hal-01223573"></a>
Emmanuel Agullo, Olivier Beaumont, Lionel Eyraud-Dubois,  and Suraj Kumar.
**Are Static Schedules so Bad ? A Case Study on Cholesky Factorization**.
In *Proceedings of the 30th IEEE International Parallel & Distributed Processing Symposium, IPDPS'16*, Chicago, IL, USA, May 2016.
IEEE.
<a title="Web link" href="https://hal.inria.fr/hal-01223573" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-01223573');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://hal.inria.fr/hal-01223573/file/heteroprioCameraReady-ieeeCompatiable.pdf" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-01223573/file/heteroprioCameraReady-ieeeCompatiable.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[doi:<a href="http://dx.doi.org/10.1109/IPDPS.2016.90">10.1109/IPDPS.2016.90</a>]
[bibtex-key = agullo:hal-01223573]




- <a name="balin2016fast"></a>
Nolwenn Balin, Guillaume Sylvand,  and Jérôme Robert.
**Fast methods applied to BEM solvers for acoustic propagation problems**.
In *22nd AIAA/CEAS Aeroacoustics Conference*, pages 2712, 2016.
[doi:<a href="http://dx.doi.org/10.2514/6.2016-2712">10.2514/6.2016-2712</a>]
[bibtex-key = balin2016fast]




- <a name="beaumont:hal-01361992"></a>
Olivier Beaumont, Terry Cojean, Lionel Eyraud-Dubois, Abdou Guermouche,  and Suraj Kumar.
**Scheduling of Linear Algebra Kernels on Multiple Heterogeneous Resources**.
In *International Conference on High Performance Computing, Data, and Analytics (HiPC)*, Hyderabad, India, December 2016.
<a title="Web link" href="https://hal.inria.fr/hal-01361992" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-01361992');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://hal.inria.fr/hal-01361992v2/document" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-01361992v2/document');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[doi:<a href="http://dx.doi.org/10.1109/HiPC.2016.045">10.1109/HiPC.2016.045</a>]
[bibtex-key = beaumont:hal-01361992]




- <a name="cojean:hal-01502749"></a>
Terry Cojean.
**Exploiting Two-Level Parallelism by Aggregating Computing Resources in Task-Based Applications Over Accelerator-Based Machines**.
In *SIAM Conference on Parallel Processing for Scientific Computing (SIAM PP 2016)*, Paris, France, April 2016.
<a title="Web link" href="https://inria.hal.science/hal-01502749" target="_blank" onclick="return trackOutboundLink('https://inria.hal.science/hal-01502749');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
[bibtex-key = cojean:hal-01502749]




- <a name="cojean:hal-01410103"></a>
Terry Cojean.
**The StarPU Runtime System at Exascale ?**.
In *RESPA workshop at SC16*, Salt Lake City, Utah, United States, November 2016.
<a title="Web link" href="https://inria.hal.science/hal-01410103" target="_blank" onclick="return trackOutboundLink('https://inria.hal.science/hal-01410103');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
[bibtex-key = cojean:hal-01410103]




- <a name="cojean:hal-01181135"></a>
Terry Cojean, Abdou Guermouche, Andra Hugo, Raymond Namyst,  and Pierre-André Wacrenier.
**Resource aggregation for task-based Cholesky Factorization on top of heterogeneous machines**.
In *HeteroPar'2016 workshop of Euro-Par*, Grenoble, France, August 2016.
<a title="Web link" href="https://hal.inria.fr/hal-01181135" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-01181135');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://hal.inria.fr/hal-01181135/file/papier%20%281%29.pdf" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-01181135/file/papier%20%281%29.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[bibtex-key = cojean:hal-01181135]




- <a name="cojean:hal-01355385"></a>
Terry Cojean, Abdou Guermouche, Andra-Ecaterina Hugo, Raymond Namyst,  and Pierre-André Wacrenier.
**Resource aggregation in task-based applications over accelerator-based multicore machines**.
In *HeteroPar'2016 worshop of Euro-Par*, Grenoble, France, August 2016.
<a title="Web link" href="https://inria.hal.science/hal-01355385" target="_blank" onclick="return trackOutboundLink('https://inria.hal.science/hal-01355385');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
[bibtex-key = cojean:hal-01355385]




- <a name="garciapinto:hal-01353962"></a>
Vinicius Garcia Pinto, Luka Stanisic, Arnaud Legrand, Lucas Mello Schnorr, Samuel Thibault,  and Vincent Danjean.
**Analyzing Dynamic Task-Based Applications on Hybrid Platforms: An Agile Scripting Approach**.
In *VPA - 3rd Workshop on Visual Performance Analysis*, Salt Lake City, USA, November 2016.
Note: Held in conjunction with SC16.
<a title="Web link" href="https://hal.inria.fr/hal-01353962" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-01353962');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://hal.inria.fr/hal-01353962v2/document" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-01353962v2/document');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[doi:<a href="http://dx.doi.org/10.1109/VPA.2016.008">10.1109/VPA.2016.008</a>]
[bibtex-key = garciapinto:hal-01353962]




- <a name="JaBlHU2016a"></a>
Johan Janzén, David Black-Schaffer,  and Andra Hugo.
**Partitioning GPUs for Improved Scalability**.
In *IEEE 28th International Symposium on Computer Architecture and High Performance Computing (SBAC-PAD)*, October 2016.
<a title="Web link" href="http://ieeexplore.ieee.org/abstract/document/7789322/" target="_blank" onclick="return trackOutboundLink('http://ieeexplore.ieee.org/abstract/document/7789322/');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
[doi:<a href="http://dx.doi.org/10.1109/SBAC-PAD.2016.14">10.1109/SBAC-PAD.2016.14</a>]
[bibtex-key = JaBlHU2016a]




- <a name="sergent:hal-01284004"></a>
Marc Sergent, David Goudin, Samuel Thibault,  and Olivier Aumage.
**Controlling the Memory Subscription of Distributed Applications with a Task-Based Runtime System**.
In *HIPS - 21st International Workshop on High-Level Parallel Programming Models and Supportive Environments*, Chicago, USA, May 2016.
<a title="Web link" href="https://hal.inria.fr/hal-01284004" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-01284004');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://hal.inria.fr/hal-01284004/file/PID4127657.pdf" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-01284004/file/PID4127657.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[doi:<a href="http://dx.doi.org/10.1109/IPDPSW.2016.105">10.1109/IPDPSW.2016.105</a>]
[bibtex-key = sergent:hal-01284004]




- <a name="agullo:hal-01372022"></a>
Emmanuel Agullo, Olivier Aumage, Berenger Bramas, Olivier Coulaud,  and Samuel Pitoiset.
**Bridging the gap between OpenMP 4.0 and native runtime systems for the fast multipole method**.
Research Report RR-8953, Inria, March 2016.
<a title="Web link" href="https://hal.inria.fr/hal-01372022" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-01372022');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://hal.inria.fr/hal-01372022/file/RR-8953.pdf" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-01372022/file/RR-8953.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[bibtex-key = agullo:hal-01372022]




- <a name="agullo:hal-01387482"></a>
Emmanuel Agullo, Bérenger Bramas, Olivier Coulaud, Martin Khannouz,  and Luka Stanisic.
**Task-based fast multipole method for clusters of multicore processors**.
Research Report RR-8970, Inria Bordeaux Sud-Ouest, October 2016.
<a title="Web link" href="https://hal.inria.fr/hal-01387482" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-01387482');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://hal.inria.fr/hal-01387482/file/report-8970.pdf" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-01387482/file/report-8970.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[bibtex-key = agullo:hal-01387482]




- <a name="agullo:hal-01316982"></a>
Emmanuel Agullo, Luc Giraud, Abdou Guermouche, Stojce Nakov,  and Jean Roman.
**Task-based Conjugate Gradient: from multi-GPU towards heterogeneous architectures**.
Research Report 8912, Inria Bordeaux Sud-Ouest, May 2016.
<a title="Web link" href="https://hal.inria.fr/hal-01316982" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-01316982');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://hal.inria.fr/hal-01316982/file/RR-8912.pdf" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-01316982/file/RR-8912.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[bibtex-key = agullo:hal-01316982]




### 2015 


- <a name="rossignon:tel-01230876"></a>
Corentin Rossignon.
**A fine grain model programming for parallelization of sparse linear solver**.
Ph.D Thesis, Université de Bordeaux, July 2015.
<a title="Web link" href="https://tel.archives-ouvertes.fr/tel-01230876" target="_blank" onclick="return trackOutboundLink('https://tel.archives-ouvertes.fr/tel-01230876');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://tel.archives-ouvertes.fr/tel-01230876/file/ROSSIGNON_CORENTIN_2015.pdf" target="_blank" onclick="return trackOutboundLink('https://tel.archives-ouvertes.fr/tel-01230876/file/ROSSIGNON_CORENTIN_2015.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[bibtex-key = rossignon:tel-01230876]




- <a name="stanisic:hal-01147997"></a>
Luka Stanisic, Samuel Thibault, Arnaud Legrand, Brice Videau,  and Jean-François Méhaut.
**Faithful Performance Prediction of a Dynamic Task-Based Runtime System for Heterogeneous Multi-Core Architectures**.
*CCPE - Concurrency and Computation: Practice and Experience*, pp 16, May 2015.
<a title="Web link" href="https://hal.inria.fr/hal-01147997" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-01147997');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://hal.inria.fr/hal-01147997/file/CCPE14_article.pdf" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-01147997/file/CCPE14_article.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[doi:<a href="http://dx.doi.org/10.1002/cpe.3555">10.1002/cpe.3555</a>]
[bibtex-key = stanisic:hal-01147997]




- <a name="agullo:hal-01120507"></a>
Emmanuel Agullo, Olivier Beaumont, Lionel Eyraud-Dubois, Julien Herrmann, Suraj Kumar, Loris Marchal,  and Samuel Thibault.
**Bridging the Gap between Performance and Bounds of Cholesky Factorization on Heterogeneous Platforms**.
In *HCW'2015 - Heterogeneity in Computing Workshop of IPDPS*, Hyderabad, India, May 2015.
<a title="Web link" href="https://hal.inria.fr/hal-01120507" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-01120507');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://hal.inria.fr/hal-01120507/document" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-01120507/document');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[doi:<a href="http://dx.doi.org/10.1109/IPDPSW.2015.35">10.1109/IPDPSW.2015.35</a>]
[bibtex-key = agullo:hal-01120507]




- <a name="MaMiDuAuThiAoNa15"></a>
Vìctor Martìnez, David Michéa, Fabrice Dupros, Olivier Aumage, Samuel Thibault, Hideo Aochi,  and Philippe Olivier Alexandre Navaux.
**Towards seismic wave modeling on heterogeneous many-core architectures using task-based runtime system**.
In *SBAC-PAD - 27th International Symposium on Computer Architecture and High Performance Computing*, Florianopolis, Brazil, October 2015.
<a title="Web link" href="https://hal.inria.fr/hal-01182746" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-01182746');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://hal.inria.fr/hal-01182746/file/sbac2015_soumission.pdf" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-01182746/file/sbac2015_soumission.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[doi:<a href="http://dx.doi.org/10.1109/SBAC-PAD.2015.33">10.1109/SBAC-PAD.2015.33</a>]
[bibtex-key = MaMiDuAuThiAoNa15]




- <a name="stanisic:hal-01180272"></a>
Luka Stanisic, Emmanuel Agullo, Alfredo Buttari, Abdou Guermouche, Arnaud Legrand, Florent Lopez,  and Brice Videau.
**Fast and Accurate Simulation of Multithreaded Sparse Linear Algebra Solvers**.
In *The 21st IEEE International Conference on Parallel and Distributed Systems*, Melbourne, Australia, December 2015.
<a title="Web link" href="https://hal.inria.fr/hal-01180272" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-01180272');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://hal.inria.fr/hal-01180272/file/QRMSTARSG_article.pdf" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-01180272/file/QRMSTARSG_article.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[doi:<a href="http://dx.doi.org/10.1109/ICPADS.2015.67">10.1109/ICPADS.2015.67</a>]
[bibtex-key = stanisic:hal-01180272]




### 2014 


- <a name="hugo:tel-01162975"></a>
Andra-Ecaterina Hugo.
**Composability of parallel codes on heterogeneous architectures**.
Ph.D Thesis, Université de Bordeaux, December 2014.
<a title="Web link" href="https://tel.archives-ouvertes.fr/tel-01162975" target="_blank" onclick="return trackOutboundLink('https://tel.archives-ouvertes.fr/tel-01162975');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://tel.archives-ouvertes.fr/tel-01162975/file/HUGO_ANDRA_2014.pdf" target="_blank" onclick="return trackOutboundLink('https://tel.archives-ouvertes.fr/tel-01162975/file/HUGO_ANDRA_2014.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[bibtex-key = hugo:tel-01162975]




- <a name="agullo:hal-00911856"></a>
Emmanuel Agullo, Bérenger Bramas, Olivier Coulaud, Eric Darve, Matthias Messner,  and Toru Takahashi.
**Task-Based FMM for Multicore Architectures**.
*SIAM Journal on Scientific Computing*, 36(1):66-93, 2014.
<a title="Web link" href="https://hal.inria.fr/hal-00911856" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-00911856');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://hal.inria.fr/hal-00911856/file/sisc-cpu.pdf" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-00911856/file/sisc-cpu.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[doi:<a href="http://dx.doi.org/10.1137/130915662">10.1137/130915662</a>]
[bibtex-key = agullo:hal-00911856]




- <a name="hugo:hal-01101045"></a>
Andra Hugo, Abdou Guermouche, Pierre-André Wacrenier,  and Raymond Namyst.
**Composing multiple StarPU applications over heterogeneous machines: A supervised approach**.
*International Journal of High Performance Computing Applications*, 28:285 - 300, February 2014.
<a title="Web link" href="https://inria.hal.science/hal-01101045" target="_blank" onclick="return trackOutboundLink('https://inria.hal.science/hal-01101045');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://inria.hal.science/hal-01101045/file/article.pdf" target="_blank" onclick="return trackOutboundLink('https://inria.hal.science/hal-01101045/file/article.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[doi:<a href="http://dx.doi.org/10.1177/1094342014527575">10.1177/1094342014527575</a>]
[bibtex-key = hugo:hal-01101045]




- <a name="agullo:hal-01283949"></a>
Emmanuel Agullo, Olivier Aumage, Mathieu Faverge, Nathalie Furmento, Florent Pruvost, Marc Sergent,  and Samuel Thibault.
**Harnessing clusters of hybrid nodes with a sequential task-based programming model**.
In *8th International Workshop on Parallel Matrix Algorithms and Applications*, July 2014.
<a title="Web link" href="https://hal.inria.fr/hal-01283949" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-01283949');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://hal.inria.fr/hal-01283949/file/pmaa14.pdf" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-01283949/file/pmaa14.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[bibtex-key = agullo:hal-01283949]




- <a name="sylvain:hal-01005765"></a>
Sylvain Henry, Alexandre Denis, Denis Barthou, Marie-Christine Counilh,  and Raymond Namyst.
**Toward OpenCL Automatic Multi-Device Support**.
In Fernando Silva, Ines Dutra,  and Vitor Santos Costa, editors, *Euro-Par 2014*, Porto, Portugal, August 2014.
Springer.
<a title="Web link" href="http://hal.inria.fr/hal-01005765" target="_blank" onclick="return trackOutboundLink('http://hal.inria.fr/hal-01005765');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="http://hal.inria.fr/hal-01005765/PDF/final.pdf" target="_blank" onclick="return trackOutboundLink('http://hal.inria.fr/hal-01005765/PDF/final.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[doi:<a href="http://dx.doi.org/10.1007/978-3-319-09873-9_65">10.1007/978-3-319-09873-9_65</a>]
[bibtex-key = sylvain:hal-01005765]




- <a name="hugo:hal-01101054"></a>
Andra Hugo, Abdou Guermouche, Pierre.-André Wacrenier,  and Raymond Namyst.
**A runtime approach to dynamic resource allocation for sparse direct solvers**.
In *43rd International Conference on Parallel Processing*, Minneapolis, United States, September 2014.
<a title="Web link" href="https://inria.hal.science/hal-01101054" target="_blank" onclick="return trackOutboundLink('https://inria.hal.science/hal-01101054');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://inria.hal.science/hal-01101054/file/AHugo.pdf" target="_blank" onclick="return trackOutboundLink('https://inria.hal.science/hal-01101054/file/AHugo.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[doi:<a href="http://dx.doi.org/10.1109/ICPP.2014.57">10.1109/ICPP.2014.57</a>]
[bibtex-key = hugo:hal-01101054]




- <a name="lacoste:hal-00987094"></a>
Xavier Lacoste, Mathieu Faverge, Pierre Ramet, Samuel Thibault,  and George Bosilca.
**Taking advantage of hybrid systems for sparse direct solvers via task-based runtimes**.
In *HCW'2014 - Heterogeneity in Computing Workshop of IPDPS*, Phoenix, USA, May 2014.
IEEE.
Note: RR-8446.
<a title="Web link" href="http://hal.inria.fr/hal-00987094" target="_blank" onclick="return trackOutboundLink('http://hal.inria.fr/hal-00987094');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="http://hal.inria.fr/hal-00987094/PDF/sparsegpus.pdf" target="_blank" onclick="return trackOutboundLink('http://hal.inria.fr/hal-00987094/PDF/sparsegpus.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[doi:<a href="http://dx.doi.org/10.1109/IPDPSW.2014.9">10.1109/IPDPSW.2014.9</a>]
[bibtex-key = lacoste:hal-00987094]




- <a name="6937016"></a>
I. D. Mironescu and L. Vintan.
**Coloured Petri Net modelling of task scheduling on a heterogeneous computational node**.
In *2014 IEEE 10th International Conference on Intelligent Computer Communication and Processing (ICCP)*, pages 323-330, 2014.
<a title="Web link" href="https://ieeexplore.ieee.org/document/6937016" target="_blank" onclick="return trackOutboundLink('https://ieeexplore.ieee.org/document/6937016');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://www.researchgate.net/profile/Lucian_Vintan/publication/264744234_Coloured_Petri_Net_Modelling_of_Task_Scheduling_on_a_Heterogeneous_Computational_Node/links/53edbae80cf26b9b7dc5f79e/Coloured-Petri-Net-Modelling-of-Task-Scheduling-on-a-Heterogeneous-Computational-Node.pdf" target="_blank" onclick="return trackOutboundLink('https://www.researchgate.net/profile/Lucian_Vintan/publication/264744234_Coloured_Petri_Net_Modelling_of_Task_Scheduling_on_a_Heterogeneous_Computational_Node/links/53edbae80cf26b9b7dc5f79e/Coloured-Petri-Net-Modelling-of-Task-Scheduling-on-a-Heterogeneous-Computational-Node.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[doi:<a href="http://dx.doi.org/10.1109/ICCP.2014.6937016">10.1109/ICCP.2014.6937016</a>]
[bibtex-key = 6937016]




- <a name="sergent:hal-00978364"></a>
Marc Sergent and Simon Archipoff.
**Modulariser les ordonnanceurs de tâches : une approche structurelle**.
In *Compas'2014*, Neuchâtel, Suisse, April 2014.
<a title="Web link" href="http://hal.inria.fr/hal-00978364" target="_blank" onclick="return trackOutboundLink('http://hal.inria.fr/hal-00978364');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="http://hal.inria.fr/hal-00978364/PDF/ordonnanceurs_modulaires.pdf" target="_blank" onclick="return trackOutboundLink('http://hal.inria.fr/hal-00978364/PDF/ordonnanceurs_modulaires.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[bibtex-key = sergent:hal-00978364]




- <a name="stanisic:hal-01011633"></a>
Luka Stanisic, Samuel Thibault, Arnaud Legrand, Brice Videau,  and Jean-François Méhaut.
**Modeling and Simulation of a Dynamic Task-Based Runtime System for Heterogeneous Multi-Core Architectures**.
In *Euro-Par - 20th International Conference on Parallel Processing*, Porto, Portugal, August 2014.
Springer-Verlag.
<a title="Web link" href="http://hal.inria.fr/hal-01011633" target="_blank" onclick="return trackOutboundLink('http://hal.inria.fr/hal-01011633');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="http://hal.inria.fr/hal-01011633/PDF/StarPUSG_article.pdf" target="_blank" onclick="return trackOutboundLink('http://hal.inria.fr/hal-01011633/PDF/StarPUSG_article.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[doi:<a href="http://dx.doi.org/10.1007/978-3-319-09873-9_5">10.1007/978-3-319-09873-9_5</a>]
[bibtex-key = stanisic:hal-01011633]




- <a name="virouleau:hal-01081974"></a>
Philippe Virouleau, Pierrick Brunet, François Broquedis, Nathalie Furmento, Samuel Thibault, Olivier Aumage,  and Thierry Gautier.
**Evaluation of OpenMP Dependent Tasks with the KASTORS Benchmark Suite**.
In *IWOMP2014 - 10th International Workshop on OpenMP*, Salvador, Brazil, pages 16 - 29, September 2014.
Springer.
<a title="Web link" href="https://hal.inria.fr/hal-01081974" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-01081974');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://hal.inria.fr/hal-01081974/document" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-01081974/document');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[doi:<a href="http://dx.doi.org/10.1007/978-3-319-11454-5_2">10.1007/978-3-319-11454-5_2</a>]
[bibtex-key = virouleau:hal-01081974]




- <a name="agullo:hal-00974674"></a>
Emmanuel Agullo, Berenger Bramas, Olivier Coulaud, Eric Darve, Matthias Messner,  and Toru Takahashi.
**Task-based FMM for heterogeneous architectures**.
Research Report RR-8513, Inria Bordeaux - Sud-Ouest, April 2014.
<a title="Web link" href="https://hal.inria.fr/hal-00974674" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-00974674');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://hal.inria.fr/hal-00974674/file/RR-8513.pdf" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-00974674/file/RR-8513.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[bibtex-key = agullo:hal-00974674]




- <a name="augonnet:hal-00992208"></a>
Cédric Augonnet, Olivier Aumage, Nathalie Furmento, Samuel Thibault,  and Raymond Namyst.
**StarPU-MPI: Task Programming over Clusters of Machines Enhanced with Accelerators**.
Research Report RR-8538, INRIA, May 2014.
<a title="Web link" href="http://hal.inria.fr/hal-00992208" target="_blank" onclick="return trackOutboundLink('http://hal.inria.fr/hal-00992208');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="http://hal.inria.fr/hal-00992208/PDF/RR-8538.pdf" target="_blank" onclick="return trackOutboundLink('http://hal.inria.fr/hal-00992208/PDF/RR-8538.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[bibtex-key = augonnet:hal-00992208]




- <a name="lacoste:hal-00925017"></a>
Xavier Lacoste, Mathieu Faverge, Pierre Ramet, Samuel Thibault,  and George Bosilca.
**Taking advantage of hybrid systems for sparse direct solvers via task-based runtimes**.
Research Report RR-8446, INRIA, January 2014.
<a title="Web link" href="http://hal.inria.fr/hal-00925017" target="_blank" onclick="return trackOutboundLink('http://hal.inria.fr/hal-00925017');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="http://hal.inria.fr/hal-00925017/PDF/RR-8446.pdf" target="_blank" onclick="return trackOutboundLink('http://hal.inria.fr/hal-00925017/PDF/RR-8446.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[bibtex-key = lacoste:hal-00925017]




- <a name="stanisic:hal-00966862"></a>
Luka Stanisic, Samuel Thibault, Arnaud Legrand, Brice Videau,  and Jean-François Méhaut.
**Modeling and Simulation of a Dynamic Task-Based Runtime System for Heterogeneous Multi-Core Architectures**.
Research Report RR-8509, INRIA, March 2014.
<a title="Web link" href="https://inria.hal.science/hal-00966862" target="_blank" onclick="return trackOutboundLink('https://inria.hal.science/hal-00966862');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://inria.hal.science/hal-00966862/file/RR-8509.pdf" target="_blank" onclick="return trackOutboundLink('https://inria.hal.science/hal-00966862/file/RR-8509.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[bibtex-key = stanisic:hal-00966862]




- <a name="sergent:hal-00978602"></a>
Emmanuel Agullo, Olivier Aumage, Mathieu Faverge, Nathalie Furmento, Florent Pruvost, Marc Sergent,  and Samuel Thibault.
**Overview of Distributed Linear Algebra on Hybrid Nodes over the StarPU Runtime**.
SIAM Conference on Parallel Processing for Scientific Computing, February 2014.
<a title="Web link" href="http://hal.inria.fr/hal-00978602" target="_blank" onclick="return trackOutboundLink('http://hal.inria.fr/hal-00978602');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="http://hal.inria.fr/hal-00978602/PDF/siampp14.pdf" target="_blank" onclick="return trackOutboundLink('http://hal.inria.fr/hal-00978602/PDF/siampp14.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[bibtex-key = sergent:hal-00978602]




### 2013 


- <a name="Bor13Thesis"></a>
Cyril Bordage.
**Ordonnancement dynamique, adapté aux architectures hétérogènes, de la méthode multipôle pour les équations de Maxwell, en électromagnétisme**.
Ph.D Thesis, Université de Bordeaux, December 2013.
<a title="Web link" href="https://www.theses.fr/2013BOR15026" target="_blank" onclick="return trackOutboundLink('https://www.theses.fr/2013BOR15026');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://www.theses.fr/2013BOR15026/document" target="_blank" onclick="return trackOutboundLink('https://www.theses.fr/2013BOR15026/document');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[bibtex-key = Bor13Thesis]




- <a name="Hen13Thesis"></a>
Sylvain Henry.
**Modèles de programmation et supports exécutifs pour architectures hétérogènes**.
Ph.D Thesis, Université de Bordeaux, November 2013.
<a title="Web link" href="http://tel.archives-ouvertes.fr/tel-00948309" target="_blank" onclick="return trackOutboundLink('http://tel.archives-ouvertes.fr/tel-00948309');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="http://tel.archives-ouvertes.fr/tel-00948309/document" target="_blank" onclick="return trackOutboundLink('http://tel.archives-ouvertes.fr/tel-00948309/document');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[bibtex-key = Hen13Thesis]




- <a name="hen13fhpc"></a>
Sylvain Henry.
**ViperVM: a Runtime System for Parallel Functional High-Performance Computing on Heterogeneous Architectures**.
In *2nd Workshop on Functional High-Performance Computing (FHPC'13)*, Boston, USA, September 2013.
<a title="Web link" href="http://hal.inria.fr/hal-00851122" target="_blank" onclick="return trackOutboundLink('http://hal.inria.fr/hal-00851122');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="http://hal.inria.fr/hal-00851122/PDF/fhpc13.pdf" target="_blank" onclick="return trackOutboundLink('http://hal.inria.fr/hal-00851122/PDF/fhpc13.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[doi:<a href="http://dx.doi.org/10.1145/2502323.2502329">10.1145/2502323.2502329</a>]
[bibtex-key = hen13fhpc]




- <a name="AH13Renpar"></a>
Andra Hugo.
**Le problème de la composition parallèle : une approche supervisée**.
In *21èmes Rencontres Francophones du Parallélisme (RenPar'21)*, Grenoble, France, January 2013.
<a title="Web link" href="http://hal.inria.fr/hal-00773610" target="_blank" onclick="return trackOutboundLink('http://hal.inria.fr/hal-00773610');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="http://hal.inria.fr/hal-00773610/document" target="_blank" onclick="return trackOutboundLink('http://hal.inria.fr/hal-00773610/document');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[bibtex-key = AH13Renpar]




- <a name="hugo:hal-00824514"></a>
Andra Hugo, Abdou Guermouche, Raymond Namyst,  and Pierre-André Wacrenier.
**Composing multiple StarPU applications over heterogeneous machines: a supervised approach**.
In *Third International Workshop on Accelerators and Hybrid Exascale Systems*, Boston, USA, May 2013.
<a title="Web link" href="http://hal.inria.fr/hal-00824514" target="_blank" onclick="return trackOutboundLink('http://hal.inria.fr/hal-00824514');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="http://hal.inria.fr/hal-00824514/document" target="_blank" onclick="return trackOutboundLink('http://hal.inria.fr/hal-00824514/document');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[doi:<a href="http://dx.doi.org/10.1177/1094342014527575">10.1177/1094342014527575</a>]
[bibtex-key = hugo:hal-00824514]




- <a name="odajima:hal-00920915"></a>
Tetsuya Odajima, Taisuke Boku, Mitsuhisa Sato, Toshihiro Hanawa, Yuetsu Kodama, Raymond Namyst, Samuel Thibault,  and Olivier Aumage.
**Adaptive Task Size Control on High Level Programming for GPU/CPU Work Sharing**.
In *ICA3PP-2013 - The 13th International Conference on Algorithms and Architectures for Parallel Processing*, Vietri sul Mare, Italy, December 2013.
<a title="Web link" href="http://hal.inria.fr/hal-00920915" target="_blank" onclick="return trackOutboundLink('http://hal.inria.fr/hal-00920915');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="http://hal.inria.fr/hal-00920915/PDF/ADPC2013-117.pdf" target="_blank" onclick="return trackOutboundLink('http://hal.inria.fr/hal-00920915/PDF/ADPC2013-117.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[doi:<a href="http://dx.doi.org/10.1007/978-3-319-03889-6_7">10.1007/978-3-319-03889-6_7</a>]
[bibtex-key = odajima:hal-00920915]




- <a name="ohshima:hal-00926144"></a>
Satoshi Ohshima, Satoshi Katagiri, Kengo Nakajima, Samuel Thibault,  and Raymond Namyst.
**Implementation of FEM Application on GPU with StarPU**.
In *SIAM CSE13 - SIAM Conference on Computational Science and Engineering 2013*, Boston, USA, February 2013.
SIAM.
<a title="Web link" href="http://hal.inria.fr/hal-00926144" target="_blank" onclick="return trackOutboundLink('http://hal.inria.fr/hal-00926144');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
[bibtex-key = ohshima:hal-00926144]




- <a name="Ros13Renpar"></a>
Corentin Rossignon.
**Optimisation du produit matrice-vecteur creux sur architecture GPU pour un simulateur de reservoir**.
In *21èmes Rencontres Francophones du Parallélisme (RenPar'21)*, Grenoble, France, January 2013.
<a title="Web link" href="http://hal.inria.fr/hal-00773571" target="_blank" onclick="return trackOutboundLink('http://hal.inria.fr/hal-00773571');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="http://hal.inria.fr/hal-00773571/document" target="_blank" onclick="return trackOutboundLink('http://hal.inria.fr/hal-00773571/document');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[bibtex-key = Ros13Renpar]




- <a name="rossignon:hal-00858350"></a>
Corentin Rossignon, Pascal Hénon, Olivier Aumage,  and Samuel Thibault.
**A NUMA-aware fine grain parallelization framework for multi-core architecture**.
In *PDSEC - 14th IEEE International Workshop on Parallel and Distributed Scientific and Engineering Computing - 2013*, Boston, USA, May 2013.
<a title="Web link" href="http://hal.inria.fr/hal-00858350" target="_blank" onclick="return trackOutboundLink('http://hal.inria.fr/hal-00858350');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="http://hal.inria.fr/hal-00858350/PDF/taggre_pdsec_2013.pdf" target="_blank" onclick="return trackOutboundLink('http://hal.inria.fr/hal-00858350/PDF/taggre_pdsec_2013.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[doi:<a href="http://dx.doi.org/10.1109/IPDPSW.2013.204">10.1109/IPDPSW.2013.204</a>]
<center>
<table border=1 align=center width=80%>
<tr>
Abstract: <td>
In this paper, we present some solutions to handle to problems commonly encountered when dealing with fine grain parallelization on multi-core architecture: expressing algorithm using a task grain size suitable for the hardware and minimizing the time penalty due to Non Uniform Memory Accesses. To evaluate the benefit of our work we present some experiments on the fine grain parallelization of an iterative solver for spare linear system with some comparisons with the Intel TBB approach.</td>

</tr></table></center>
[bibtex-key = rossignon:hal-00858350]




- <a name="LC13Report"></a>
Ludovic Courtès.
**C Language Extensions for Hybrid CPU/GPU Programming with StarPU**.
Research Report RR-8278, INRIA, April 2013.
<a title="Web link" href="http://hal.inria.fr/hal-00807033" target="_blank" onclick="return trackOutboundLink('http://hal.inria.fr/hal-00807033');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="http://hal.inria.fr/hal-00807033/PDF/RR-8278.pdf" target="_blank" onclick="return trackOutboundLink('http://hal.inria.fr/hal-00807033/PDF/RR-8278.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[bibtex-key = LC13Report]




- <a name="henry:hal-00853423"></a>
Sylvain Henry, Denis Barthou, Alexandre Denis, Raymond Namyst,  and Marie-Christine Counilh.
**SOCL: An OpenCL Implementation with Automatic Multi-Device Adaptation Support**.
Research Report RR-8346, INRIA, August 2013.
<a title="Web link" href="https://inria.hal.science/hal-00853423" target="_blank" onclick="return trackOutboundLink('https://inria.hal.science/hal-00853423');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://inria.hal.science/hal-00853423/file/RR-8346.pdf" target="_blank" onclick="return trackOutboundLink('https://inria.hal.science/hal-00853423/file/RR-8346.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[bibtex-key = henry:hal-00853423]




### 2012 


- <a name="HenDenBar2012TSI"></a>
Sylvain Henry, Alexandre Denis,  and Denis Barthou.
**Programmation unifiée multi-accélérateur OpenCL**.
*Techniques et Sciences Informatiques*, (8-9-10):1233-1249, 2012.
<a title="Web link" href="http://hal.inria.fr/hal-00772742" target="_blank" onclick="return trackOutboundLink('http://hal.inria.fr/hal-00772742');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="http://hal.inria.fr/hal-00772742/document" target="_blank" onclick="return trackOutboundLink('http://hal.inria.fr/hal-00772742/document');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[bibtex-key = HenDenBar2012TSI]




- <a name="MahManAugThi12TSI"></a>
Sidi Ahmed Mahmoudi, Pierre Manneback, Cédric Augonnet,  and Samuel Thibault.
**Traitements d'Images sur Architectures Parallèles et Hétérogènes**.
*Technique et Science Informatiques*, 31(8-10):1183-1203, 2012.
<a title="Web link" href="http://hal.inria.fr/hal-00714858/" target="_blank" onclick="return trackOutboundLink('http://hal.inria.fr/hal-00714858/');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://hal.inria.fr/hal-00714858/document" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-00714858/document');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[doi:<a href="http://dx.doi.org/10.3166/tsi.31.1183-1203">10.3166/tsi.31.1183-1203</a>]
[bibtex-key = MahManAugThi12TSI]




- <a name="AugAumFurNamThi2012EuroMPI"></a>
Cédric Augonnet, Olivier Aumage, Nathalie Furmento, Raymond Namyst,  and Samuel Thibault.
**StarPU-MPI: Task Programming over Clusters of Machines Enhanced with Accelerators**.
In Siegfried Benkner Jesper Larsson Träff and Jack Dongarra, editors, *EuroMPI 2012*, volume 7490 of *LNCS*, September 2012.
Springer.
Note: Poster Session.
<a title="Web link" href="http://hal.inria.fr/hal-00725477" target="_blank" onclick="return trackOutboundLink('http://hal.inria.fr/hal-00725477');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="http://hal.inria.fr/hal-00725477/document" target="_blank" onclick="return trackOutboundLink('http://hal.inria.fr/hal-00725477/document');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[bibtex-key = AugAumFurNamThi2012EuroMPI]




- <a name="BenkBajMarSanNamThiEuroPar2012"></a>
Siegfried Benkner, Enes Bajrovic, Erich Marth, Martin Sandrieser, Raymond Namyst,  and Samuel Thibault.
**High-Level Support for Pipeline Parallelism on Many-Core Architectures**.
In *Euro-Par - 18th International Conference on Parallel Processing*, Rhodes Island, Greece, August 2012.
<a title="Web link" href="http://hal.inria.fr/hal-00697020" target="_blank" onclick="return trackOutboundLink('http://hal.inria.fr/hal-00697020');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="http://hal.inria.fr/hal-00697020/PDF/europar2012-submitted.pdf" target="_blank" onclick="return trackOutboundLink('http://hal.inria.fr/hal-00697020/PDF/europar2012-submitted.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[doi:<a href="http://dx.doi.org/10.1007/978-3-642-32820-6_61">10.1007/978-3-642-32820-6_61</a>]
[bibtex-key = BenkBajMarSanNamThiEuroPar2012]




- <a name="bordage:hal-00773114"></a>
Cyril Bordage.
**Parallelization on Heterogeneous Multicore and Multi-GPU Systems of the Fast Multipole Method for the Helmholtz Equation Using a Runtime System**.
In *ADVCIMP12*, ADVCOMP 2012, The Sixth International Conference on Advanced Engineering Computing and Applications in Sciences, Barcelone, Spain, pages 90-95, September 2012.
IARIA.
<a title="Web link" href="https://inria.hal.science/hal-00773114" target="_blank" onclick="return trackOutboundLink('https://inria.hal.science/hal-00773114');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
[bibtex-key = bordage:hal-00773114]




- <a name="kessler:hal-00776610"></a>
Christoph Kessler, Usman Dastgeer, Samuel Thibault, Raymond Namyst, Andrew Richards, Uwe Dolinsky, Siegfried Benkner, Jesper Larsson Träff,  and Sabri Pllana.
**Programmability and Performance Portability Aspects of Heterogeneous Multi-/Manycore Systems**.
In *DATE - Design, Automation and Test in Europe*, Dresden, Deutschland, March 2012.
**ISBN:** 978-3-9810801-8-6.
<a title="Web link" href="http://hal.inria.fr/hal-00776610" target="_blank" onclick="return trackOutboundLink('http://hal.inria.fr/hal-00776610');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="http://hal.inria.fr/hal-00776610/PDF/date12-paper.pdf" target="_blank" onclick="return trackOutboundLink('http://hal.inria.fr/hal-00776610/PDF/date12-paper.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[doi:<a href="http://dx.doi.org/10.1109/DATE.2012.6176582">10.1109/DATE.2012.6176582</a>]
[bibtex-key = kessler:hal-00776610]




### 2011 


- <a name="Aug11Thesis"></a>
Cédric Augonnet.
**Scheduling Tasks over Multicore machines enhanced with Accelerators: a Runtime System's Perspective**.
Ph.D Thesis, Université de Bordeaux, December 2011.
<a title="Web link" href="http://tel.archives-ouvertes.fr/tel-00777154" target="_blank" onclick="return trackOutboundLink('http://tel.archives-ouvertes.fr/tel-00777154');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="http://tel.archives-ouvertes.fr/tel-00777154/document" target="_blank" onclick="return trackOutboundLink('http://tel.archives-ouvertes.fr/tel-00777154/document');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[bibtex-key = Aug11Thesis]




- <a name="AH11Master"></a>
Andra Hugo.
**Composabilité de codes parallèles sur architectures hétérogènes**.
Master Thesis, Université de Bordeaux, June 2011.
<a title="Web link" href="http://hal.inria.fr/inria-00619654/en/" target="_blank" onclick="return trackOutboundLink('http://hal.inria.fr/inria-00619654/en/');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="http://hal.inria.fr/inria-00619654/document" target="_blank" onclick="return trackOutboundLink('http://hal.inria.fr/inria-00619654/document');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[bibtex-key = AH11Master]




- <a name="AugThiNamWac11CCPE"></a>
Cédric Augonnet, Samuel Thibault, Raymond Namyst,  and Pierre-André Wacrenier.
**StarPU: A Unified Platform for Task Scheduling on Heterogeneous Multicore Architectures**.
*CCPE - Concurrency and Computation: Practice and Experience, Special Issue: Euro-Par 2009*, 23:187-198, February 2011.
<a title="Web link" href="http://hal.inria.fr/inria-00550877" target="_blank" onclick="return trackOutboundLink('http://hal.inria.fr/inria-00550877');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="http://hal.inria.fr/inria-00550877/document" target="_blank" onclick="return trackOutboundLink('http://hal.inria.fr/inria-00550877/document');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[doi:<a href="http://dx.doi.org/10.1002/cpe.1631">10.1002/cpe.1631</a>]
[bibtex-key = AugThiNamWac11CCPE]




- <a name="BenPllTraTsiDolAugBacKesMolOsi11IEEEMicro"></a>
Siegfried Benkner, Sabri Pllana, Jesper Larsson Träff, Philippas Tsigas, Uwe Dolinsky, Cédric Augonnet, Beverly Bachmayer, Christoph Kessler, David Moloney,  and Vitaly Osipov.
**PEPPHER: Efficient and Productive Usage of Hybrid Computing Systems**.
*IEEE Micro*, 31(5):28-41, September 2011.
**ISSN:** 0272-1732.
<a title="Web link" href="http://hal.inria.fr/hal-00648480" target="_blank" onclick="return trackOutboundLink('http://hal.inria.fr/hal-00648480');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="http://hal.inria.fr/hal-00648480/document" target="_blank" onclick="return trackOutboundLink('http://hal.inria.fr/hal-00648480/document');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[doi:<a href="http://dx.doi.org/10.1109/MM.2011.67">10.1109/MM.2011.67</a>]
[bibtex-key = BenPllTraTsiDolAugBacKesMolOsi11IEEEMicro]




- <a name="AguAugDonFavLanLtaTomAICCSA11"></a>
Emmanuel Agullo, Cédric Augonnet, Jack Dongarra, Mathieu Faverge, Julien Langou, Hatem Ltaief,  and Stanimire Tomov.
**LU factorization for accelerator-based systems**.
In *9th ACS/IEEE International Conference on Computer Systems and Applications (AICCSA 11)*, Sharm El-Sheikh, Egypt, June 2011.
<a title="Web link" href="http://hal.inria.fr/hal-00654193" target="_blank" onclick="return trackOutboundLink('http://hal.inria.fr/hal-00654193');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="http://hal.inria.fr/hal-00654193/document" target="_blank" onclick="return trackOutboundLink('http://hal.inria.fr/hal-00654193/document');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[doi:<a href="http://dx.doi.org/10.1109/AICCSA.2011.6126599">10.1109/AICCSA.2011.6126599</a>]
[bibtex-key = AguAugDonFavLanLtaTomAICCSA11]




- <a name="AguAugDonFavLtaThiTom11IPDPS"></a>
Emmanuel Agullo, Cédric Augonnet, Jack Dongarra, Mathieu Faverge, Hatem Ltaief, Samuel Thibault,  and Stanimire Tomov.
**QR Factorization on a Multicore Node Enhanced with Multiple GPU Accelerators**.
In *25th IEEE International Parallel & Distributed Processing Symposium (IEEE IPDPS 2011)*, Anchorage, Alaska, USA, May 2011.
<a title="Web link" href="http://hal.inria.fr/inria-00547614" target="_blank" onclick="return trackOutboundLink('http://hal.inria.fr/inria-00547614');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="http://hal.inria.fr/inria-00547614/document" target="_blank" onclick="return trackOutboundLink('http://hal.inria.fr/inria-00547614/document');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[doi:<a href="http://dx.doi.org/10.1109/IPDPS.2011.90">10.1109/IPDPS.2011.90</a>]
[bibtex-key = AguAugDonFavLtaThiTom11IPDPS]




- <a name="benkner:hal-00661320"></a>
Siegfried Benkner, Sabri Pllana, Jesper Larsson Träff, Philippas Tsigas, Andrew Richards, Raymond Namyst, Beverly Bachmayer, Christoph Kessler, David Moloney,  and Peter Sanders.
**The PEPPHER Approach to Programmability and Performance Portability for Heterogeneous many-core Architectures**.
In *ParCo*, Ghent, Belgium, August 2011.
<a title="Web link" href="https://inria.hal.science/hal-00661320" target="_blank" onclick="return trackOutboundLink('https://inria.hal.science/hal-00661320');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://inria.hal.science/hal-00661320/file/peppher_parco_2011.pdf" target="_blank" onclick="return trackOutboundLink('https://inria.hal.science/hal-00661320/file/peppher_parco_2011.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[bibtex-key = benkner:hal-00661320]




- <a name="DasKesThi11ParCo"></a>
Usman Dastgeer, Christoph Kessler,  and Samuel Thibault.
**Flexible runtime support for efficient skeleton programming on hybrid systems**.
In *ParCo - Proceedings of the International Conference on Parallel Computing*, volume 22 of *Advances of Parallel Computing*, Gent, Belgium, pages 159-166, August 2011.
<a title="Web link" href="http://hal.inria.fr/inria-00606200/" target="_blank" onclick="return trackOutboundLink('http://hal.inria.fr/inria-00606200/');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.699.6216&rep=rep1&type=pdf" target="_blank" onclick="return trackOutboundLink('http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.699.6216&rep=rep1&type=pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[doi:<a href="http://dx.doi.org/10.3233/978-1-61499-041-3-159">10.3233/978-1-61499-041-3-159</a>]
[bibtex-key = DasKesThi11ParCo]




- <a name="Hen11Renpar"></a>
Sylvain Henry.
**Programmation multi-accélérateurs unifiée en OpenCL**.
In *20èmes Rencontres Francophones du Parallélisme (RenPar'20)*, Saint Malo, France, May 2011.
<a title="Web link" href="http://hal.archives-ouvertes.fr/hal-00643257" target="_blank" onclick="return trackOutboundLink('http://hal.archives-ouvertes.fr/hal-00643257');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="http://hal.archives-ouvertes.fr/hal-00643257/document" target="_blank" onclick="return trackOutboundLink('http://hal.archives-ouvertes.fr/hal-00643257/document');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[bibtex-key = Hen11Renpar]




- <a name="MahManAugThi11Renpar20"></a>
Sidi Ahmed Mahmoudi, Pierre Manneback, Cédric Augonnet,  and Samuel Thibault.
**Détection optimale des coins et contours dans des bases d'images volumineuses sur architectures multicoeurs hétérogènes**.
In *RenPar'20 - 20èmes Rencontres Francophones du Parallélisme*, Saint-Malo, France, May 2011.
<a title="Web link" href="http://hal.inria.fr/inria-00606195" target="_blank" onclick="return trackOutboundLink('http://hal.inria.fr/inria-00606195');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://hal.inria.fr/inria-00606195/document" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/inria-00606195/document');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[bibtex-key = MahManAugThi11Renpar20]




### 2010 


- <a name="AguAugDonLtaNamThiTomGPUgems"></a>
Emmanuel Agullo, Cédric Augonnet, Jack Dongarra, Hatem Ltaief, Raymond Namyst, Samuel Thibault,  and Stanimire Tomov.
**A Hybridization Methodology for High-Performance Linear Algebra Software for GPUs**.
In Wen-mei W. Hwu, editor, *GPU Computing Gems*, volume 2.
Morgan Kaufmann, September 2010.
<a title="Web link" href="http://hal.inria.fr/inria-00547847" target="_blank" onclick="return trackOutboundLink('http://hal.inria.fr/inria-00547847');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="http://hal.inria.fr/inria-00547847/document" target="_blank" onclick="return trackOutboundLink('http://hal.inria.fr/inria-00547847/document');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[doi:<a href="http://dx.doi.org/10.1016/B978-0-12-385963-1.00034-4">10.1016/B978-0-12-385963-1.00034-4</a>]
[bibtex-key = AguAugDonLtaNamThiTomGPUgems]




- <a name="AguAugDonLtaNamRomThiTom10SAAHPC"></a>
Emmanuel Agullo, Cédric Augonnet, Jack Dongarra, Hatem Ltaief, Raymond Namyst, Jean Roman, Samuel Thibault,  and Stanimire Tomov.
**Dynamically scheduled Cholesky factorization on multicore architectures with GPU accelerators**.
In *SAAHPC - Symposium on Application Accelerators in High Performance Computing*, Knoxville, USA, July 2010.
<a title="Web link" href="http://hal.inria.fr/inria-00547616" target="_blank" onclick="return trackOutboundLink('http://hal.inria.fr/inria-00547616');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="http://hal.inria.fr/inria-00547616/document" target="_blank" onclick="return trackOutboundLink('http://hal.inria.fr/inria-00547616/document');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[bibtex-key = AguAugDonLtaNamRomThiTom10SAAHPC]




- <a name="AugCleThiNam10ICPADS"></a>
Cédric Augonnet, Jérôme Clet-Ortega, Samuel Thibault,  and Raymond Namyst.
**Data-Aware Task Scheduling on Multi-Accelerator based Platforms**.
In *The 16th International Conference on Parallel and Distributed Systems (ICPADS)*, Shanghai, China, December 2010.
<a title="Web link" href="http://hal.inria.fr/inria-00523937" target="_blank" onclick="return trackOutboundLink('http://hal.inria.fr/inria-00523937');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="http://hal.inria.fr/inria-00523937/document" target="_blank" onclick="return trackOutboundLink('http://hal.inria.fr/inria-00523937/document');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[doi:<a href="http://dx.doi.org/10.1109/ICPADS.2010.129">10.1109/ICPADS.2010.129</a>]
[bibtex-key = AugCleThiNam10ICPADS]




- <a name="AugThiNamWac10RR7240"></a>
Cédric Augonnet, Samuel Thibault,  and Raymond Namyst.
**StarPU: a Runtime System for Scheduling Tasks over Accelerator-Based Multicore Machines**.
Research Report RR-7240, INRIA, March 2010.
<a title="Web link" href="http://hal.inria.fr/inria-00467677" target="_blank" onclick="return trackOutboundLink('http://hal.inria.fr/inria-00467677');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="http://hal.inria.fr/inria-00467677/document" target="_blank" onclick="return trackOutboundLink('http://hal.inria.fr/inria-00467677/document');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[bibtex-key = AugThiNamWac10RR7240]




### 2009 


- <a name="Aug09Renpar19"></a>
Cédric Augonnet.
**StarPU: un support exécutif unifié pour les architectures multicoeurs hétérogènes**.
In *19èmes Rencontres Francophones du Parallélisme*, Toulouse, France, September 2009.
Note: Best Paper Award.
<a title="Web link" href="http://hal.inria.fr/inria-00411581" target="_blank" onclick="return trackOutboundLink('http://hal.inria.fr/inria-00411581');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="http://hal.inria.fr/inria-00411581/document" target="_blank" onclick="return trackOutboundLink('http://hal.inria.fr/inria-00411581/document');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[bibtex-key = Aug09Renpar19]




- <a name="AugThiNam09HPPC"></a>
Cédric Augonnet, Samuel Thibault,  and Raymond Namyst.
**Automatic Calibration of Performance Models on Heterogeneous Multicore Architectures**.
In *HPPC - Proceedings of the International Euro-Par Workshops, Highly Parallel Processing on a Chip*, volume 6043 of *LNCS*, Delft, The Netherlands, pages 56-65, August 2009.
Springer.
<a title="Web link" href="http://hal.inria.fr/inria-00421333" target="_blank" onclick="return trackOutboundLink('http://hal.inria.fr/inria-00421333');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="http://hal.inria.fr/inria-00421333/document" target="_blank" onclick="return trackOutboundLink('http://hal.inria.fr/inria-00421333/document');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[doi:<a href="http://dx.doi.org/10.1007/978-3-642-14122-5_9">10.1007/978-3-642-14122-5_9</a>]
[bibtex-key = AugThiNam09HPPC]




- <a name="AugThiNamNij09Samos"></a>
Cédric Augonnet, Samuel Thibault, Raymond Namyst,  and Maik Nijhuis.
**Exploiting the Cell/BE architecture with the StarPU unified runtime system**.
In *SAMOS Workshop - International Workshop on Systems, Architectures, Modeling, and Simulation*, volume 5657 of *LNCS*, Samos, Greece, July 2009.
<a title="Web link" href="http://hal.inria.fr/inria-00378705" target="_blank" onclick="return trackOutboundLink('http://hal.inria.fr/inria-00378705');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="http://hal.inria.fr/inria-00378705/document" target="_blank" onclick="return trackOutboundLink('http://hal.inria.fr/inria-00378705/document');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[doi:<a href="http://dx.doi.org/10.1007/978-3-642-03138-0_36">10.1007/978-3-642-03138-0_36</a>]
[bibtex-key = AugThiNamNij09Samos]




- <a name="AugThiNamWac09Europar"></a>
Cédric Augonnet, Samuel Thibault, Raymond Namyst,  and Pierre-André Wacrenier.
**StarPU: A Unified Platform for Task Scheduling on Heterogeneous Multicore Architectures**.
In *Euro-Par - 15th International Conference on Parallel Processing*, volume 5704 of *LNCS*, Delft, The Netherlands, pages 863-874, August 2009.
Springer.
<a title="Web link" href="http://hal.inria.fr/inria-00384363" target="_blank" onclick="return trackOutboundLink('http://hal.inria.fr/inria-00384363');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="http://hal.inria.fr/inria-00384363/document" target="_blank" onclick="return trackOutboundLink('http://hal.inria.fr/inria-00384363/document');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[doi:<a href="http://dx.doi.org/10.1007/978-3-642-03869-3_80">10.1007/978-3-642-03869-3_80</a>]
[bibtex-key = AugThiNamWac09Europar]




### 2008 


- <a name="Aug08Master"></a>
Cédric Augonnet.
**Vers des supports d'exécution capables d'exploiter les machines multicoeurs hétérogènes**.
Master Thesis, Université de Bordeaux, June 2008.
<a title="Web link" href="http://hal.inria.fr/inria-00289361" target="_blank" onclick="return trackOutboundLink('http://hal.inria.fr/inria-00289361');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="http://hal.inria.fr/inria-00289361/document" target="_blank" onclick="return trackOutboundLink('http://hal.inria.fr/inria-00289361/document');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[bibtex-key = Aug08Master]




- <a name="AugNam08HPPC"></a>
Cédric Augonnet and Raymond Namyst.
**A unified runtime system for heterogeneous multicore architectures**.
In *Proceedings of the International Euro-Par Workshops 2008, HPPC'08*, volume 5415 of *LNCS*, Las Palmas de Gran Canaria, Spain, pages 174-183, August 2008.
Springer.
**ISBN:** 978-3-642-00954-9.
<a title="Web link" href="http://hal.inria.fr/inria-00326917" target="_blank" onclick="return trackOutboundLink('http://hal.inria.fr/inria-00326917');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="http://hal.inria.fr/inria-00326917/document" target="_blank" onclick="return trackOutboundLink('http://hal.inria.fr/inria-00326917/document');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[doi:<a href="http://dx.doi.org/10.1007/978-3-642-00955-6_22">10.1007/978-3-642-00955-6_22</a>]
[bibtex-key = AugNam08HPPC]







---
layout: default
title: Publications of year 2018
permalink: publications/Year/2018.html
full-width: True
---

## Publications of year 2018
### Thesis 


- <a name="cojean:tel-01816341"></a>
Terry Cojean.
**Programmation of heterogeneous architectures using moldable tasks**.
Ph.D Thesis, Université de Bordeaux, March 2018.
<a title="Web link" href="https://tel.archives-ouvertes.fr/tel-01816341" target="_blank" onclick="return trackOutboundLink('https://tel.archives-ouvertes.fr/tel-01816341');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://tel.archives-ouvertes.fr/tel-01816341/file/COJEAN_TERRY_2018.pdf" target="_blank" onclick="return trackOutboundLink('https://tel.archives-ouvertes.fr/tel-01816341/file/COJEAN_TERRY_2018.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>




- <a name="thibault:tel-01959127"></a>
Samuel Thibault.
**On Runtime Systems for Task-based Programming on Heterogeneous Platforms**.
Habilitation à diriger des recherches, Université de Bordeaux, December 2018.
<a title="Web link" href="https://hal.inria.fr/tel-01959127" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/tel-01959127');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://hal.inria.fr/tel-01959127/file/hdr.pdf" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/tel-01959127/file/hdr.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
<a title="BIB" href="https://inria.hal.science/tel-01959127v1/bibtex" target="_blank" onclick="return trackOutboundLink('https://inria.hal.science/tel-01959127v1/bibtex');"><i class="fa fa-book" aria-hidden="true"></i></a>




### Articles in journal, book chapters 


- <a name="refId0"></a>
Mohamed Essadki, Jonathan Jung, Adam Larat, Milan Pelletier,  and Vincent Perrier.
**A Task-Driven Implementation of a Simple Numerical Solver for Hyperbolic Conservation Laws**.
*ESAIM: ProcS*, 63:228-247, 2018.
<a title="Web link" href="https://doi.org/10.1051/proc/201863228" target="_blank" onclick="return trackOutboundLink('https://doi.org/10.1051/proc/201863228');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://www.esaim-proc.org/articles/proc/pdf/2018/03/proc_esaim2018_228.pdf" target="_blank" onclick="return trackOutboundLink('https://www.esaim-proc.org/articles/proc/pdf/2018/03/proc_esaim2018_228.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[doi:<a href="http://dx.doi.org/10.1051/proc/201863228">10.1051/proc/201863228</a>]




- <a name="garciapinto:hal-01616632"></a>
Vinicius Garcia Pinto, Lucas Mello Schnorr, Luka Stanisic, Arnaud Legrand, Samuel Thibault,  and Vincent Danjean.
**A Visual Performance Analysis Framework for Task-based Parallel Applications running on Hybrid Clusters**.
*CCPE - Concurrency and Computation: Practice and Experience*, 30, April 2018.
<a title="Web link" href="https://hal.inria.fr/hal-01616632" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-01616632');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://hal.inria.fr/hal-01616632/file/CCPE_article_submitted_2018_02_06.pdf" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-01616632/file/CCPE_article_submitted_2018_02_06.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[doi:<a href="http://dx.doi.org/10.1002/cpe.4472">10.1002/cpe.4472</a>]




- <a name="thoman2018taxonomy"></a>
Peter Thoman, Kiril Dichev, Thomas Heller, Roman Iakymchuk, Xavier Aguilar, Khalid Hasanov, Philipp Gschwandtner, Pierre Lemarinier, Stefano Markidis, Herbert Jordan,  and others.
**A taxonomy of task-based parallel programming technologies for high-performance computing**.
*The Journal of Supercomputing*, 74(4):1422-1434, 2018.
<a title="Web link" href="https://link.springer.com/article/10.1007%2Fs11227-018-2238-4" target="_blank" onclick="return trackOutboundLink('https://link.springer.com/article/10.1007%2Fs11227-018-2238-4');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://link.springer.com/content/pdf/10.1007/s11227-018-2238-4.pdf" target="_blank" onclick="return trackOutboundLink('https://link.springer.com/content/pdf/10.1007/s11227-018-2238-4.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[doi:<a href="http://dx.doi.org/10.1007/s11227-018-2238-4">10.1007/s11227-018-2238-4</a>]




### Conference articles 


- <a name="pinto:hal-01842038"></a>
Vinicius Garcia Pinto, Lucas Mello Schnorr, Arnaud Legrand, Samuel Thibault, Luka Stanisic,  and Vincent Danjean.
**Detecção de Anomalias de Desempenho em Aplicações de Alto Desempenho baseadas em Tarefas em Clusters Hìbridos**.
In *WPerformance - 17o Workshop em Desempenho de Sistemas Computacionais e de Comunicação*, Natal, Brazil, July 2018.
<a title="Web link" href="https://hal.inria.fr/hal-01842038" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-01842038');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://hal.inria.fr/hal-01842038/file/181587_1.pdf" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-01842038/file/181587_1.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>




- <a name="soudris:hal-03273509"></a>
Dimitrios Soudris, Lazaros Papadopoulos, Christoph W Kessler, Dionysios D Kehagias, Athanasios Papadopoulos, Panos Seferlis, Alexander Chatzigeorgiou, Apostolos Ampatzoglou, Samuel Thibault, Raymond Namyst, Dirk Pleiter, Georgi Gaydadjiev, Tobias Becker,  and Matthieu Haefele.
**EXA2PRO programming environment**.
In *SAMOS XVIII: Architectures, Modeling, and Simulation*, Pythagorion, Greece, pages 202-209, July 2018.
ACM.
<a title="Web link" href="https://hal.inria.fr/hal-03273509" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-03273509');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://hal.inria.fr/hal-03273509/file/3229631.3239369.pdf" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-03273509/file/3229631.3239369.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[doi:<a href="http://dx.doi.org/10.1145/3229631.3239369">10.1145/3229631.3239369</a>]







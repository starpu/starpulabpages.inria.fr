---
layout: default
title: Publications of year 2008
permalink: publications/Year/2008.complete.html
full-width: True
---

## Publications of year 2008
### Thesis 


- <a name="Aug08Master"></a>
Cédric Augonnet.
**Vers des supports d'exécution capables d'exploiter les machines multicoeurs hétérogènes**.
Master Thesis, Université de Bordeaux, June 2008.
<a title="Web link" href="http://hal.inria.fr/inria-00289361" target="_blank" onclick="return trackOutboundLink('http://hal.inria.fr/inria-00289361');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="http://hal.inria.fr/inria-00289361/document" target="_blank" onclick="return trackOutboundLink('http://hal.inria.fr/inria-00289361/document');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>




### Conference articles 


- <a name="AugNam08HPPC"></a>
Cédric Augonnet and Raymond Namyst.
**A unified runtime system for heterogeneous multicore architectures**.
In *Proceedings of the International Euro-Par Workshops 2008, HPPC'08*, volume 5415 of *LNCS*, Las Palmas de Gran Canaria, Spain, pages 174-183, August 2008.
Springer.
**ISBN:** 978-3-642-00954-9.
<a title="Web link" href="http://hal.inria.fr/inria-00326917" target="_blank" onclick="return trackOutboundLink('http://hal.inria.fr/inria-00326917');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="http://hal.inria.fr/inria-00326917/document" target="_blank" onclick="return trackOutboundLink('http://hal.inria.fr/inria-00326917/document');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[doi:<a href="http://dx.doi.org/10.1007/978-3-642-00955-6_22">10.1007/978-3-642-00955-6_22</a>]







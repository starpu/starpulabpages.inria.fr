---
layout: default
title: Publications of year 2021
permalink: publications/Year/2021.complete.html
full-width: True
---

## Publications of year 2021
### Articles in journal, book chapters 


- <a name="agullo:hal-03348787"></a>
Emmanuel Agullo, Mirco Altenbernd, Hartwig Anzt, Leonardo Bautista-Gomez, Tommaso Benacchio, Luca Bonaventura, Hans-Joachim Bungartz, Sanjay Chatterjee, Florina M Ciorba, Nathan Debardeleben, Daniel Drzisga, Sebastian Eibl, Christian Engelmann, Wilfried N Gansterer, Luc Giraud, Dominik Göddeke, Marco Heisig, Fabienne Jézéquel, Nils Kohl, Sherry Xiaoye, Romain Lion, Miriam Mehl, Paul Mycek, Michael Obersteiner, Enrique S Quintana-Ortì, Francesco Rizzi, Ulrich Rüde, Martin Schulz, Fred Fung, Robert Speck, Linda Stals, Keita Teranishi, Samuel Thibault, Dominik Thönnes, Andreas Wagner,  and Barbara Wohlmuth.
**Resiliency in numerical algorithm design for extreme scale simulations**.
*International Journal of High Performance Computing Applications*, September 2021.
<a title="Web link" href="https://hal.inria.fr/hal-03348787" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-03348787');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://hal.inria.fr/hal-03348787/file/2010.13342.pdf" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-03348787/file/2010.13342.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[doi:<a href="http://dx.doi.org/10.1177/10943420211055188">10.1177/10943420211055188</a>]




- <a name="papadopoulos:hal-03318644"></a>
Lazaros Papadopoulos, Dimitrios Soudris, Christoph Kessler, August Ernstsson, Johan Ahlqvist, Nikos Vasilas, Athanasios I Papadopoulos, Panos Seferlis, Charles Prouveur, Matthieu Haefele, Samuel Thibault, Athanasios Salamanis, Theodoros Ioakimidis,  and Dionysios Kehagias.
**EXA2PRO: A Framework for High Development Productivity on Heterogeneous Computing Systems**.
*IEEE Transactions on Parallel and Distributed Systems*, August 2021.
<a title="Web link" href="https://hal.inria.fr/hal-03318644" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-03318644');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://hal.inria.fr/hal-03318644/file/EXA2PRO___TPDS.pdf" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-03318644/file/EXA2PRO___TPDS.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[doi:<a href="http://dx.doi.org/10.1109/TPDS.2021.3104257">10.1109/TPDS.2021.3104257</a>]




### Conference articles 


- <a name="denis:hal-03290121"></a>
Alexandre Denis, Emmanuel Jeannot,  and Philippe Swartvagher.
**Interferences between Communications and Computations in Distributed HPC Systems**.
In *ICPP 2021 - 50th International Conference on Parallel Processing*, Chicago / Virtual, United States, pages 11, August 2021.
<a title="Web link" href="https://hal.inria.fr/hal-03290121" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-03290121');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://hal.inria.fr/hal-03290121/file/rr.pdf" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-03290121/file/rr.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[doi:<a href="http://dx.doi.org/10.1145/3472456.3473516">10.1145/3472456.3473516</a>]




- <a name="gonthier:hal-03290998"></a>
Maxime Gonthier, Loris Marchal,  and Samuel Thibault.
**Locality-Aware Scheduling of Independent Tasks for Runtime Systems**.
In *COLOC: 5th workshop on data locality - 7th International European Conference on Parallel and Distributed Computing Workshops*, Lisbon, Portugal, August 2021.
<a title="Web link" href="https://hal.archives-ouvertes.fr/hal-03290998" target="_blank" onclick="return trackOutboundLink('https://hal.archives-ouvertes.fr/hal-03290998');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://hal.archives-ouvertes.fr/hal-03290998/file/coloc-cameraready-submitted.pdf" target="_blank" onclick="return trackOutboundLink('https://hal.archives-ouvertes.fr/hal-03290998/file/coloc-cameraready-submitted.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[doi:<a href="http://dx.doi.org/10.1007/978-3-031-06156-1_1">10.1007/978-3-031-06156-1_1</a>]




- <a name="pintohwc2021"></a>
Vinicius Garcia Pinto, Lucas Leandro Nesi, Marcelo Cogo Miletto,  and Lucas Mello Schnorr.
**Providing In-depth Performance Analysis for Heterogeneous Task-based Applications with StarVZ**.
In *2021 IEEE International Parallel and Distributed Processing Symposium (IPDPS)*, May 2021.
<a title="Web link" href="https://drive.google.com/file/d/18IYdLW9eiSDzwMM3RJV0Flp83OcBWp5R/view?usp=sharing" target="_blank" onclick="return trackOutboundLink('https://drive.google.com/file/d/18IYdLW9eiSDzwMM3RJV0Flp83OcBWp5R/view?usp=sharing');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
[doi:<a href="http://dx.doi.org/10.1109/IPDPSW52791.2021.00013">10.1109/IPDPSW52791.2021.00013</a>]




### Internal reports 


- <a name="gonthier:hal-03144290"></a>
Maxime Gonthier, Loris Marchal,  and Samuel Thibault.
**Locality-Aware Scheduling of Independant Tasks for Runtime Systems**.
Research Report RR-9394, Inria, 2021.
<a title="Web link" href="https://hal.inria.fr/hal-03144290" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-03144290');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://hal.inria.fr/hal-03144290v5/file/RR_coloc2021-submitted.pdf" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-03144290v5/file/RR_coloc2021-submitted.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>







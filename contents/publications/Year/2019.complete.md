---
layout: default
title: Publications of year 2019
permalink: publications/Year/2019.complete.html
full-width: True
---

## Publications of year 2019
### Articles in journal, book chapters 


- <a name="bramas:hal-02120736"></a>
Bérenger Bramas.
**Impact study of data locality on task-based applications through the Heteroprio scheduler**.
*PeerJ Computer Science*, May 2019.
<a title="Web link" href="https://hal.inria.fr/hal-02120736" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-02120736');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://hal.inria.fr/hal-02120736/file/peerj-cs-190.pdf" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-02120736/file/peerj-cs-190.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[doi:<a href="http://dx.doi.org/10.7717/peerj-cs.190">10.7717/peerj-cs.190</a>]




### Conference articles 


- <a name="alonazi:hal-02403109"></a>
A. AlOnazi, H. Ltaief, D. Keyes, I. Said,  and Samuel Thibault.
**Asynchronous Task-Based Execution of the Reverse Time Migration for the Oil and Gas Industry**.
In *2019 IEEE International Conference on Cluster Computing (CLUSTER)*, Albuquerque, USA, pages 1-11, September 2019.
IEEE.
<a title="Web link" href="https://hal.inria.fr/hal-02403109" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-02403109');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://hal.inria.fr/hal-02403109/file/2019_tbrtm_cluster.pdf" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-02403109/file/2019_tbrtm_cluster.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[doi:<a href="http://dx.doi.org/10.1109/CLUSTER.2019.8891054">10.1109/CLUSTER.2019.8891054</a>]




- <a name="leandronesi:hal-02275363"></a>
Lucas Leandro Nesi, Samuel Thibault, Luka Stanisic,  and Lucas Mello Schnorr.
**Visual Performance Analysis of Memory Behavior in a Task-Based Runtime on Hybrid Platforms**.
In *2019 19th IEEE/ACM International Symposium on Cluster, Cloud and Grid Computing (CCGRID)*, Larnaca, Cyprus, pages 142-151, May 2019.
IEEE.
<a title="Web link" href="https://hal.inria.fr/hal-02275363" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-02275363');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://hal.inria.fr/hal-02275363/file/CCGRID_camera_ready.pdf" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-02275363/file/CCGRID_camera_ready.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[doi:<a href="http://dx.doi.org/10.1109/CCGRID.2019.00025">10.1109/CCGRID.2019.00025</a>]




- <a name="lion:hal-02296118"></a>
Romain Lion.
**Tolérance aux pannes dans l'exécution distribuée de graphes de tâches**.
In *Conférence d'informatique en Parallélisme, Architecture et Système*, Anglet, France, June 2019.
<a title="Web link" href="https://hal.inria.fr/hal-02296118" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-02296118');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://hal.inria.fr/hal-02296118/file/Compas_Romain_LION_submitted_final.pdf" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-02296118/file/Compas_Romain_LION_submitted_final.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>




### Internal reports 


- <a name="alias:hal-02421327"></a>
Christophe Alias, Samuel Thibault,  and Laure Gonnord.
**A Compiler Algorithm to Guide Runtime Scheduling**.
Research Report RR-9315, INRIA Grenoble ; INRIA Bordeaux, December 2019.
<a title="Web link" href="https://hal.inria.fr/hal-02421327" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-02421327');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://hal.inria.fr/hal-02421327/file/RR-9315.pdf" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-02421327/file/RR-9315.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>







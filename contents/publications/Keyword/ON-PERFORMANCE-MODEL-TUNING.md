---
layout: default
title: On Performance Model Tuning
permalink: publications/Keyword/ON-PERFORMANCE-MODEL-TUNING.html
full-width: True
---

## On Performance Model Tuning
### Year 2017 


- <a name="agullo:hal-01474556"></a>
Emmanuel Agullo, Bérenger Bramas, Olivier Coulaud, Luka Stanisic,  and Samuel Thibault.
**Modeling Irregular Kernels of Task-based codes: Illustration with the Fast Multipole Method**.
Research Report RR-9036, INRIA Bordeaux, February 2017.
<a title="Web link" href="https://hal.inria.fr/hal-01474556" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-01474556');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://hal.inria.fr/hal-01474556/file/rapport.pdf" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-01474556/file/rapport.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>




### Year 2015 


- <a name="stanisic:hal-01180272"></a>
Luka Stanisic, Emmanuel Agullo, Alfredo Buttari, Abdou Guermouche, Arnaud Legrand, Florent Lopez,  and Brice Videau.
**Fast and Accurate Simulation of Multithreaded Sparse Linear Algebra Solvers**.
In *The 21st IEEE International Conference on Parallel and Distributed Systems*, Melbourne, Australia, December 2015.
<a title="Web link" href="https://hal.inria.fr/hal-01180272" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-01180272');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://hal.inria.fr/hal-01180272/file/QRMSTARSG_article.pdf" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-01180272/file/QRMSTARSG_article.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[doi:<a href="http://dx.doi.org/10.1109/ICPADS.2015.67">10.1109/ICPADS.2015.67</a>]




### Year 2009 


- <a name="AugThiNam09HPPC"></a>
Cédric Augonnet, Samuel Thibault,  and Raymond Namyst.
**Automatic Calibration of Performance Models on Heterogeneous Multicore Architectures**.
In *HPPC - Proceedings of the International Euro-Par Workshops, Highly Parallel Processing on a Chip*, volume 6043 of *LNCS*, Delft, The Netherlands, pages 56-65, August 2009.
Springer.
<a title="Web link" href="http://hal.inria.fr/inria-00421333" target="_blank" onclick="return trackOutboundLink('http://hal.inria.fr/inria-00421333');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="http://hal.inria.fr/inria-00421333/document" target="_blank" onclick="return trackOutboundLink('http://hal.inria.fr/inria-00421333/document');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[doi:<a href="http://dx.doi.org/10.1007/978-3-642-14122-5_9">10.1007/978-3-642-14122-5_9</a>]







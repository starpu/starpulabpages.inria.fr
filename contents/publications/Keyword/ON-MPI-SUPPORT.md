---
layout: default
title: On MPI Support
permalink: publications/Keyword/ON-MPI-SUPPORT.html
full-width: True
---

## On MPI Support
### Year 2023 


- <a name="agullo:hal-03936659"></a>
Emmanuel Agullo, Alfredo Buttari, Abdou Guermouche, Julien Herrmann,  and Antoine Jego.
**Task-based parallel programming for scalable matrix product algorithms**.
*ACM Transactions on Mathematical Software*, 2023.
<a title="Web link" href="https://hal.science/hal-03936659" target="_blank" onclick="return trackOutboundLink('https://hal.science/hal-03936659');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://hal.science/hal-03936659v2/file/journal-nocopy.pdf" target="_blank" onclick="return trackOutboundLink('https://hal.science/hal-03936659v2/file/journal-nocopy.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[doi:<a href="http://dx.doi.org/10.1145/3583560">10.1145/3583560</a>]




### Year 2022 


- <a name="lion:tel-04213186"></a>
Romain Lion.
**Réplication de données pour la tolérance aux pannes dans un support d'exécution distribué à base de tâches**.
Ph.D Thesis, Université de Bordeaux, December 2022.
<a title="Web link" href="https://theses.hal.science/tel-04213186" target="_blank" onclick="return trackOutboundLink('https://theses.hal.science/tel-04213186');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://theses.hal.science/tel-04213186/file/LION_ROMAIN_2022.pdf" target="_blank" onclick="return trackOutboundLink('https://theses.hal.science/tel-04213186/file/LION_ROMAIN_2022.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>




- <a name="swartvagher:tel-03989856"></a>
Philippe Swartvagher.
**On the Interactions between HPC Task-based Runtime Systems and Communication Libraries**.
Ph.D Thesis, Université de Bordeaux, November 2022.
<a title="Web link" href="https://theses.hal.science/tel-03989856" target="_blank" onclick="return trackOutboundLink('https://theses.hal.science/tel-03989856');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://theses.hal.science/tel-03989856/file/SWARTVAGHER_PHILIPPE__2022.pdf" target="_blank" onclick="return trackOutboundLink('https://theses.hal.science/tel-03989856/file/SWARTVAGHER_PHILIPPE__2022.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>




### Year 2021 


- <a name="agullo:hal-03348787"></a>
Emmanuel Agullo, Mirco Altenbernd, Hartwig Anzt, Leonardo Bautista-Gomez, Tommaso Benacchio, Luca Bonaventura, Hans-Joachim Bungartz, Sanjay Chatterjee, Florina M Ciorba, Nathan Debardeleben, Daniel Drzisga, Sebastian Eibl, Christian Engelmann, Wilfried N Gansterer, Luc Giraud, Dominik Göddeke, Marco Heisig, Fabienne Jézéquel, Nils Kohl, Sherry Xiaoye, Romain Lion, Miriam Mehl, Paul Mycek, Michael Obersteiner, Enrique S Quintana-Ortì, Francesco Rizzi, Ulrich Rüde, Martin Schulz, Fred Fung, Robert Speck, Linda Stals, Keita Teranishi, Samuel Thibault, Dominik Thönnes, Andreas Wagner,  and Barbara Wohlmuth.
**Resiliency in numerical algorithm design for extreme scale simulations**.
*International Journal of High Performance Computing Applications*, September 2021.
<a title="Web link" href="https://hal.inria.fr/hal-03348787" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-03348787');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://hal.inria.fr/hal-03348787/file/2010.13342.pdf" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-03348787/file/2010.13342.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[doi:<a href="http://dx.doi.org/10.1177/10943420211055188">10.1177/10943420211055188</a>]




### Year 2020 


- <a name="denis:hal-02872765"></a>
Alexandre Denis, Emmanuel Jeannot, Philippe Swartvagher,  and Samuel Thibault.
**Using Dynamic Broadcasts to improve Task-Based Runtime Performances**.
In *Euro-Par - 26th International European Conference on Parallel and Distributed Computing*, Warsaw, Poland, August 2020.
Rzadca and Malawski, Springer.
<a title="Web link" href="https://hal.inria.fr/hal-02872765" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-02872765');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://hal.inria.fr/hal-02872765/file/dynamic_broadcasts.pdf" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-02872765/file/dynamic_broadcasts.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[doi:<a href="http://dx.doi.org/10.1007/978-3-030-57675-2_28">10.1007/978-3-030-57675-2_28</a>]




- <a name="lion:hal-02970529"></a>
Romain Lion and Samuel Thibault.
**From tasks graphs to asynchronous distributed checkpointing with local restart**.
In *2020 IEEE/ACM 10th Workshop on Fault Tolerance for HPC at eXtreme Scale (FTXS)*, Atlanta, USA, November 2020.
<a title="Web link" href="https://hal.archives-ouvertes.fr/hal-02970529" target="_blank" onclick="return trackOutboundLink('https://hal.archives-ouvertes.fr/hal-02970529');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://hal.archives-ouvertes.fr/hal-02970529/file/2020001221.pdf" target="_blank" onclick="return trackOutboundLink('https://hal.archives-ouvertes.fr/hal-02970529/file/2020001221.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[doi:<a href="http://dx.doi.org/10.1109/FTXS51974.2020.00009">10.1109/FTXS51974.2020.00009</a>]




### Year 2019 


- <a name="lion:hal-02296118"></a>
Romain Lion.
**Tolérance aux pannes dans l'exécution distribuée de graphes de tâches**.
In *Conférence d'informatique en Parallélisme, Architecture et Système*, Anglet, France, June 2019.
<a title="Web link" href="https://hal.inria.fr/hal-02296118" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-02296118');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://hal.inria.fr/hal-02296118/file/Compas_Romain_LION_submitted_final.pdf" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-02296118/file/Compas_Romain_LION_submitted_final.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>




### Year 2017 


- <a name="agullo:hal-01618526"></a>
Emmanuel Agullo, Olivier Aumage, Mathieu Faverge, Nathalie Furmento, Florent Pruvost, Marc Sergent,  and Samuel Thibault.
**Achieving High Performance on Supercomputers with a Sequential Task-based Programming Model**.
*TPDS - IEEE Transactions on Parallel and Distributed Systems*, December 2017.
<a title="Web link" href="https://hal.inria.fr/hal-01618526" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-01618526');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://hal.inria.fr/hal-01618526/file/tpds14.pdf" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-01618526/file/tpds14.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[doi:<a href="http://dx.doi.org/10.1109/TPDS.2017.2766064">10.1109/TPDS.2017.2766064</a>]




### Year 2016 


- <a name="sergent:tel-01483666"></a>
Marc Sergent.
**Scalability of a task-based runtime system for dense linear algebra applications**.
Ph.D Thesis, Université de Bordeaux, December 2016.
<a title="Web link" href="https://tel.archives-ouvertes.fr/tel-01483666" target="_blank" onclick="return trackOutboundLink('https://tel.archives-ouvertes.fr/tel-01483666');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://tel.archives-ouvertes.fr/tel-01483666/file/SERGENT_MARC_2016.pdf" target="_blank" onclick="return trackOutboundLink('https://tel.archives-ouvertes.fr/tel-01483666/file/SERGENT_MARC_2016.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>




### Year 2014 


- <a name="agullo:hal-01283949"></a>
Emmanuel Agullo, Olivier Aumage, Mathieu Faverge, Nathalie Furmento, Florent Pruvost, Marc Sergent,  and Samuel Thibault.
**Harnessing clusters of hybrid nodes with a sequential task-based programming model**.
In *8th International Workshop on Parallel Matrix Algorithms and Applications*, July 2014.
<a title="Web link" href="https://hal.inria.fr/hal-01283949" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-01283949');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://hal.inria.fr/hal-01283949/file/pmaa14.pdf" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-01283949/file/pmaa14.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>




- <a name="augonnet:hal-00992208"></a>
Cédric Augonnet, Olivier Aumage, Nathalie Furmento, Samuel Thibault,  and Raymond Namyst.
**StarPU-MPI: Task Programming over Clusters of Machines Enhanced with Accelerators**.
Research Report RR-8538, INRIA, May 2014.
<a title="Web link" href="http://hal.inria.fr/hal-00992208" target="_blank" onclick="return trackOutboundLink('http://hal.inria.fr/hal-00992208');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="http://hal.inria.fr/hal-00992208/PDF/RR-8538.pdf" target="_blank" onclick="return trackOutboundLink('http://hal.inria.fr/hal-00992208/PDF/RR-8538.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>




### Year 2012 


- <a name="AugAumFurNamThi2012EuroMPI"></a>
Cédric Augonnet, Olivier Aumage, Nathalie Furmento, Raymond Namyst,  and Samuel Thibault.
**StarPU-MPI: Task Programming over Clusters of Machines Enhanced with Accelerators**.
In Siegfried Benkner Jesper Larsson Träff and Jack Dongarra, editors, *EuroMPI 2012*, volume 7490 of *LNCS*, September 2012.
Springer.
Note: Poster Session.
<a title="Web link" href="http://hal.inria.fr/hal-00725477" target="_blank" onclick="return trackOutboundLink('http://hal.inria.fr/hal-00725477');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="http://hal.inria.fr/hal-00725477/document" target="_blank" onclick="return trackOutboundLink('http://hal.inria.fr/hal-00725477/document');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>







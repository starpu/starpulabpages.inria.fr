---
layout: default
title: On Performance Visualization
permalink: publications/Keyword/ON-PERFORMANCE-VISUALIZATION.html
full-width: True
---

## On Performance Visualization
### Year 2023 


- <a name="denis:hal-04236246"></a>
Alexandre Denis, Emmanuel Jeannot, Philippe Swartvagher,  and Samuel Thibault.
**Tracing task-based runtime systems: Feedbacks from the StarPU case**.
*Concurrency and Computation: Practice and Experience*, pp 24, October 2023.
<a title="Web link" href="https://inria.hal.science/hal-04236246" target="_blank" onclick="return trackOutboundLink('https://inria.hal.science/hal-04236246');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://inria.hal.science/hal-04236246/file/article.pdf" target="_blank" onclick="return trackOutboundLink('https://inria.hal.science/hal-04236246/file/article.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[doi:<a href="http://dx.doi.org/10.1002/cpe.7920">10.1002/cpe.7920</a>]




- <a name="leandronesi:hal-04005071"></a>
Lucas Leandro Nesi, Vinicius Garcia Pinto, Lucas Mello Schnorr,  and Arnaud Legrand.
**Summarizing task-based applications behavior over many nodes through progression clustering**.
In *PDP 2023 - 31st Euromicro International Conference on Parallel, Distributed, and Network-Based Processing*, Naples, Italy, pages 1-8, March 2023.
<a title="Web link" href="https://hal.science/hal-04005071" target="_blank" onclick="return trackOutboundLink('https://hal.science/hal-04005071');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://hal.science/hal-04005071/file/PDP.pdf" target="_blank" onclick="return trackOutboundLink('https://hal.science/hal-04005071/file/PDP.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[doi:<a href="http://dx.doi.org/10.1109/PDP59025.2023.00014">10.1109/PDP59025.2023.00014</a>]




### Year 2022 


- <a name="miletto:hal-03298021"></a>
Marcelo Cogo Miletto, Lucas Leandro Nesi, Lucas Mello Schnorr,  and Arnaud Legrand.
**Performance Analysis of Irregular Task-Based Applications on Hybrid Platforms: Structure Matters**.
*Future Generation Computer Systems*, 135, October 2022.
<a title="Web link" href="https://inria.hal.science/hal-03298021" target="_blank" onclick="return trackOutboundLink('https://inria.hal.science/hal-03298021');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://inria.hal.science/hal-03298021v2/file/elsevier.pdf" target="_blank" onclick="return trackOutboundLink('https://inria.hal.science/hal-03298021v2/file/elsevier.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[doi:<a href="http://dx.doi.org/10.1016/j.future.2022.05.013">10.1016/j.future.2022.05.013</a>]




### Year 2021 


- <a name="pintohwc2021"></a>
Vinicius Garcia Pinto, Lucas Leandro Nesi, Marcelo Cogo Miletto,  and Lucas Mello Schnorr.
**Providing In-depth Performance Analysis for Heterogeneous Task-based Applications with StarVZ**.
In *2021 IEEE International Parallel and Distributed Processing Symposium (IPDPS)*, May 2021.
<a title="Web link" href="https://drive.google.com/file/d/18IYdLW9eiSDzwMM3RJV0Flp83OcBWp5R/view?usp=sharing" target="_blank" onclick="return trackOutboundLink('https://drive.google.com/file/d/18IYdLW9eiSDzwMM3RJV0Flp83OcBWp5R/view?usp=sharing');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
[doi:<a href="http://dx.doi.org/10.1109/IPDPSW52791.2021.00013">10.1109/IPDPSW52791.2021.00013</a>]




### Year 2019 


- <a name="leandronesi:hal-02275363"></a>
Lucas Leandro Nesi, Samuel Thibault, Luka Stanisic,  and Lucas Mello Schnorr.
**Visual Performance Analysis of Memory Behavior in a Task-Based Runtime on Hybrid Platforms**.
In *2019 19th IEEE/ACM International Symposium on Cluster, Cloud and Grid Computing (CCGRID)*, Larnaca, Cyprus, pages 142-151, May 2019.
IEEE.
<a title="Web link" href="https://hal.inria.fr/hal-02275363" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-02275363');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://hal.inria.fr/hal-02275363/file/CCGRID_camera_ready.pdf" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-02275363/file/CCGRID_camera_ready.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[doi:<a href="http://dx.doi.org/10.1109/CCGRID.2019.00025">10.1109/CCGRID.2019.00025</a>]




### Year 2018 


- <a name="garciapinto:hal-01616632"></a>
Vinicius Garcia Pinto, Lucas Mello Schnorr, Luka Stanisic, Arnaud Legrand, Samuel Thibault,  and Vincent Danjean.
**A Visual Performance Analysis Framework for Task-based Parallel Applications running on Hybrid Clusters**.
*CCPE - Concurrency and Computation: Practice and Experience*, 30, April 2018.
<a title="Web link" href="https://hal.inria.fr/hal-01616632" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-01616632');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://hal.inria.fr/hal-01616632/file/CCPE_article_submitted_2018_02_06.pdf" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-01616632/file/CCPE_article_submitted_2018_02_06.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[doi:<a href="http://dx.doi.org/10.1002/cpe.4472">10.1002/cpe.4472</a>]




- <a name="pinto:hal-01842038"></a>
Vinicius Garcia Pinto, Lucas Mello Schnorr, Arnaud Legrand, Samuel Thibault, Luka Stanisic,  and Vincent Danjean.
**Detecção de Anomalias de Desempenho em Aplicações de Alto Desempenho baseadas em Tarefas em Clusters Hìbridos**.
In *WPerformance - 17o Workshop em Desempenho de Sistemas Computacionais e de Comunicação*, Natal, Brazil, July 2018.
<a title="Web link" href="https://hal.inria.fr/hal-01842038" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-01842038');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://hal.inria.fr/hal-01842038/file/181587_1.pdf" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-01842038/file/181587_1.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>




### Year 2016 


- <a name="garciapinto:hal-01353962"></a>
Vinicius Garcia Pinto, Luka Stanisic, Arnaud Legrand, Lucas Mello Schnorr, Samuel Thibault,  and Vincent Danjean.
**Analyzing Dynamic Task-Based Applications on Hybrid Platforms: An Agile Scripting Approach**.
In *VPA - 3rd Workshop on Visual Performance Analysis*, Salt Lake City, USA, November 2016.
Note: Held in conjunction with SC16.
<a title="Web link" href="https://hal.inria.fr/hal-01353962" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-01353962');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://hal.inria.fr/hal-01353962v2/document" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-01353962v2/document');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[doi:<a href="http://dx.doi.org/10.1109/VPA.2016.008">10.1109/VPA.2016.008</a>]







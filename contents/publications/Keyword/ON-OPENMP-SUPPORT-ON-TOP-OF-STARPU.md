---
layout: default
title: On OpenMP Support on top of StarPU
permalink: publications/Keyword/ON-OPENMP-SUPPORT-ON-TOP-OF-STARPU.html
full-width: True
---

## On OpenMP Support on top of StarPU
### Year 2017 


- <a name="agullo:hal-01517153"></a>
Emmanuel Agullo, Olivier Aumage, Berenger Bramas, Olivier Coulaud,  and Samuel Pitoiset.
**Bridging the gap between OpenMP and task-based runtime systems for the fast multipole method**.
*IEEE Transactions on Parallel and Distributed Systems*, April 2017.
<a title="Web link" href="https://hal.inria.fr/hal-01517153" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-01517153');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://hal.inria.fr/hal-01517153/file/tpds_kstar_scalfmm_print.pdf" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-01517153/file/tpds_kstar_scalfmm_print.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[doi:<a href="http://dx.doi.org/10.1109/TPDS.2017.2697857">10.1109/TPDS.2017.2697857</a>]




### Year 2016 


- <a name="agullo:hal-01372022"></a>
Emmanuel Agullo, Olivier Aumage, Berenger Bramas, Olivier Coulaud,  and Samuel Pitoiset.
**Bridging the gap between OpenMP 4.0 and native runtime systems for the fast multipole method**.
Research Report RR-8953, Inria, March 2016.
<a title="Web link" href="https://hal.inria.fr/hal-01372022" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-01372022');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://hal.inria.fr/hal-01372022/file/RR-8953.pdf" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-01372022/file/RR-8953.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>




### Year 2014 


- <a name="virouleau:hal-01081974"></a>
Philippe Virouleau, Pierrick Brunet, François Broquedis, Nathalie Furmento, Samuel Thibault, Olivier Aumage,  and Thierry Gautier.
**Evaluation of OpenMP Dependent Tasks with the KASTORS Benchmark Suite**.
In *IWOMP2014 - 10th International Workshop on OpenMP*, Salvador, Brazil, pages 16 - 29, September 2014.
Springer.
<a title="Web link" href="https://hal.inria.fr/hal-01081974" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-01081974');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://hal.inria.fr/hal-01081974/document" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-01081974/document');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[doi:<a href="http://dx.doi.org/10.1007/978-3-319-11454-5_2">10.1007/978-3-319-11454-5_2</a>]







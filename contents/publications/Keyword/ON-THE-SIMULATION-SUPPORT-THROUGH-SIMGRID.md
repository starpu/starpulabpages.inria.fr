---
layout: default
title: On The Simulation Support through SimGrid
permalink: publications/Keyword/ON-THE-SIMULATION-SUPPORT-THROUGH-SIMGRID.html
full-width: True
---

## On The Simulation Support through SimGrid
### Year 2020 


- <a name="daoudi:hal-02933803"></a>
Idriss Daoudi, Philippe Virouleau, Thierry Gautier, Samuel Thibault,  and Olivier Aumage.
**sOMP: Simulating OpenMP Task-Based Applications with NUMA Effects**.
In *IWOMP 2020 - 16th International Workshop on OpenMP*, volume 12295 of *LNCS*, Austin, USA, September 2020.
Springer.
<a title="Web link" href="https://hal.inria.fr/hal-02933803" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-02933803');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://hal.inria.fr/hal-02933803/file/p05_daoudi.pdf" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-02933803/file/p05_daoudi.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[doi:<a href="http://dx.doi.org/10.1007/978-3-030-58144-2_13">10.1007/978-3-030-58144-2_13</a>]




- <a name="thibault:hal-02943753"></a>
Samuel Thibault, Luka Stanisic,  and Arnaud Legrand.
**Faithful Performance Prediction of a Dynamic Task-based Runtime System, an Opportunity for Task Graph Scheduling**.
In *SIAM Conference on Parallel Processing for Scientific Computing (SIAM PP 2020)*, Seattle, USA, February 2020.
<a title="Web link" href="https://hal.inria.fr/hal-02943753" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-02943753');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://hal.inria.fr/hal-02943753/file/20-02-15-siampp-seattle.pdf" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-02943753/file/20-02-15-siampp-seattle.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>




### Year 2015 


- <a name="stanisic:hal-01147997"></a>
Luka Stanisic, Samuel Thibault, Arnaud Legrand, Brice Videau,  and Jean-François Méhaut.
**Faithful Performance Prediction of a Dynamic Task-Based Runtime System for Heterogeneous Multi-Core Architectures**.
*CCPE - Concurrency and Computation: Practice and Experience*, pp 16, May 2015.
<a title="Web link" href="https://hal.inria.fr/hal-01147997" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-01147997');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://hal.inria.fr/hal-01147997/file/CCPE14_article.pdf" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-01147997/file/CCPE14_article.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[doi:<a href="http://dx.doi.org/10.1002/cpe.3555">10.1002/cpe.3555</a>]




- <a name="stanisic:hal-01180272"></a>
Luka Stanisic, Emmanuel Agullo, Alfredo Buttari, Abdou Guermouche, Arnaud Legrand, Florent Lopez,  and Brice Videau.
**Fast and Accurate Simulation of Multithreaded Sparse Linear Algebra Solvers**.
In *The 21st IEEE International Conference on Parallel and Distributed Systems*, Melbourne, Australia, December 2015.
<a title="Web link" href="https://hal.inria.fr/hal-01180272" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-01180272');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://hal.inria.fr/hal-01180272/file/QRMSTARSG_article.pdf" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-01180272/file/QRMSTARSG_article.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[doi:<a href="http://dx.doi.org/10.1109/ICPADS.2015.67">10.1109/ICPADS.2015.67</a>]




### Year 2014 


- <a name="stanisic:hal-01011633"></a>
Luka Stanisic, Samuel Thibault, Arnaud Legrand, Brice Videau,  and Jean-François Méhaut.
**Modeling and Simulation of a Dynamic Task-Based Runtime System for Heterogeneous Multi-Core Architectures**.
In *Euro-Par - 20th International Conference on Parallel Processing*, Porto, Portugal, August 2014.
Springer-Verlag.
<a title="Web link" href="http://hal.inria.fr/hal-01011633" target="_blank" onclick="return trackOutboundLink('http://hal.inria.fr/hal-01011633');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="http://hal.inria.fr/hal-01011633/PDF/StarPUSG_article.pdf" target="_blank" onclick="return trackOutboundLink('http://hal.inria.fr/hal-01011633/PDF/StarPUSG_article.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[doi:<a href="http://dx.doi.org/10.1007/978-3-319-09873-9_5">10.1007/978-3-319-09873-9_5</a>]




- <a name="stanisic:hal-00966862"></a>
Luka Stanisic, Samuel Thibault, Arnaud Legrand, Brice Videau,  and Jean-François Méhaut.
**Modeling and Simulation of a Dynamic Task-Based Runtime System for Heterogeneous Multi-Core Architectures**.
Research Report RR-8509, INRIA, March 2014.
<a title="Web link" href="https://inria.hal.science/hal-00966862" target="_blank" onclick="return trackOutboundLink('https://inria.hal.science/hal-00966862');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://inria.hal.science/hal-00966862/file/RR-8509.pdf" target="_blank" onclick="return trackOutboundLink('https://inria.hal.science/hal-00966862/file/RR-8509.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>







---
layout: default
title: On The Cell Support
permalink: publications/Keyword/ON-THE-CELL-SUPPORT.html
full-width: True
---

## On The Cell Support
### Year 2009 


- <a name="AugThiNamNij09Samos"></a>
Cédric Augonnet, Samuel Thibault, Raymond Namyst,  and Maik Nijhuis.
**Exploiting the Cell/BE architecture with the StarPU unified runtime system**.
In *SAMOS Workshop - International Workshop on Systems, Architectures, Modeling, and Simulation*, volume 5657 of *LNCS*, Samos, Greece, July 2009.
<a title="Web link" href="http://hal.inria.fr/inria-00378705" target="_blank" onclick="return trackOutboundLink('http://hal.inria.fr/inria-00378705');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="http://hal.inria.fr/inria-00378705/document" target="_blank" onclick="return trackOutboundLink('http://hal.inria.fr/inria-00378705/document');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[doi:<a href="http://dx.doi.org/10.1007/978-3-642-03138-0_36">10.1007/978-3-642-03138-0_36</a>]







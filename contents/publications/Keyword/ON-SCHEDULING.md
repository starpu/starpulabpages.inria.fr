---
layout: default
title: On Scheduling
permalink: publications/Keyword/ON-SCHEDULING.html
full-width: True
---

## On Scheduling
### Year 2024 


- <a name="gonthier:hal-04500281"></a>
Maxime Gonthier, Elisabeth Larsson, Loris Marchal, Carl Nettelblad,  and Samuel Thibault.
**Data-Driven Locality-Aware Batch Scheduling**.
In *APDCM 2024 - 26th Workshop on Advances in Parallel and Distributed Computational Models*, San Francisco, United States, May 2024.
38th IEEE International Parallel and Distributed Processing Symposium.
<a title="Web link" href="https://inria.hal.science/hal-04500281" target="_blank" onclick="return trackOutboundLink('https://inria.hal.science/hal-04500281');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://inria.hal.science/hal-04500281/file/Data-Driven%20Locality-Aware%20Batch%20Scheduling.pdf" target="_blank" onclick="return trackOutboundLink('https://inria.hal.science/hal-04500281/file/Data-Driven%20Locality-Aware%20Batch%20Scheduling.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>




### Year 2023 


- <a name="gonthier:tel-04260094"></a>
Maxime Gonthier.
**Scheduling Under Memory Constraint in Task-based Runtime Systems**.
Ph.D Thesis, Ecole normale supérieure de lyon - ENS LYON, September 2023.
<a title="Web link" href="https://theses.hal.science/tel-04260094" target="_blank" onclick="return trackOutboundLink('https://theses.hal.science/tel-04260094');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://theses.hal.science/tel-04260094/file/GONTHIER_Maxime_2023ENSL0061.pdf" target="_blank" onclick="return trackOutboundLink('https://theses.hal.science/tel-04260094/file/GONTHIER_Maxime_2023ENSL0061.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>




- <a name="gonthier:hal-03623220"></a>
Maxime Gonthier, Loris Marchal,  and Samuel Thibault.
**Taming data locality for task scheduling under memory constraint in runtime systems**.
*Future Generation Computer Systems*, 2023.
<a title="Web link" href="https://hal.inria.fr/hal-03623220" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-03623220');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://hal.inria.fr/hal-03623220v2/file/fgcs-paper-reviewers-round2-unmarked-submitted.pdf" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-03623220v2/file/fgcs-paper-reviewers-round2-unmarked-submitted.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[doi:<a href="http://dx.doi.org/10.1016/j.future.2023.01.024">10.1016/j.future.2023.01.024</a>]




- <a name="gonthier:hal-04090595"></a>
Maxime Gonthier, Loris Marchal,  and Samuel Thibault.
**Memory-Aware Scheduling Of Tasks Sharing Data On Multiple GPUs**.
ISC 2023 - ISC High Performance 2023, May 2023.
Note: Poster.
<a title="Web link" href="https://inria.hal.science/hal-04090595" target="_blank" onclick="return trackOutboundLink('https://inria.hal.science/hal-04090595');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://inria.hal.science/hal-04090595/file/Poster.pdf" target="_blank" onclick="return trackOutboundLink('https://inria.hal.science/hal-04090595/file/Poster.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>




- <a name="gonthier:hal-04090612"></a>
Maxime Gonthier, Loris Marchal,  and Samuel Thibault.
**Memory-Aware Scheduling of Tasks Sharing Data on Multiple GPUs**.
JLESC 2023 -15th JLESC Workshop, March 2023.
<a title="Web link" href="https://inria.hal.science/hal-04090612" target="_blank" onclick="return trackOutboundLink('https://inria.hal.science/hal-04090612');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://inria.hal.science/hal-04090612/file/jlesc%20%281%29.pdf" target="_blank" onclick="return trackOutboundLink('https://inria.hal.science/hal-04090612/file/jlesc%20%281%29.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>




### Year 2022 


- <a name="gonthier:hal-03552243"></a>
Maxime Gonthier, Samuel Thibault,  and Loris Marchal.
**Memory-Aware Scheduling of Tasks Sharing Data on Multiple GPUs with Dynamic Runtime Systems**.
In *IPDPS 2022 - 36th IEEE International Parallel & Distributed Processing Symposium*, Lyon, France, May 2022.
IEEE.
<a title="Web link" href="https://hal.inria.fr/hal-03552243" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-03552243');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://hal.inria.fr/hal-03552243/file/IPDPS-camera-ready.pdf" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-03552243/file/IPDPS-camera-ready.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[doi:<a href="http://dx.doi.org/10.1109/IPDPS53621.2022.00073">10.1109/IPDPS53621.2022.00073</a>]




### Year 2021 


- <a name="gonthier:hal-03290998"></a>
Maxime Gonthier, Loris Marchal,  and Samuel Thibault.
**Locality-Aware Scheduling of Independent Tasks for Runtime Systems**.
In *COLOC: 5th workshop on data locality - 7th International European Conference on Parallel and Distributed Computing Workshops*, Lisbon, Portugal, August 2021.
<a title="Web link" href="https://hal.archives-ouvertes.fr/hal-03290998" target="_blank" onclick="return trackOutboundLink('https://hal.archives-ouvertes.fr/hal-03290998');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://hal.archives-ouvertes.fr/hal-03290998/file/coloc-cameraready-submitted.pdf" target="_blank" onclick="return trackOutboundLink('https://hal.archives-ouvertes.fr/hal-03290998/file/coloc-cameraready-submitted.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[doi:<a href="http://dx.doi.org/10.1007/978-3-031-06156-1_1">10.1007/978-3-031-06156-1_1</a>]




- <a name="gonthier:hal-03144290"></a>
Maxime Gonthier, Loris Marchal,  and Samuel Thibault.
**Locality-Aware Scheduling of Independant Tasks for Runtime Systems**.
Research Report RR-9394, Inria, 2021.
<a title="Web link" href="https://hal.inria.fr/hal-03144290" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-03144290');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://hal.inria.fr/hal-03144290v5/file/RR_coloc2021-submitted.pdf" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-03144290v5/file/RR_coloc2021-submitted.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>




### Year 2019 


- <a name="bramas:hal-02120736"></a>
Bérenger Bramas.
**Impact study of data locality on task-based applications through the Heteroprio scheduler**.
*PeerJ Computer Science*, May 2019.
<a title="Web link" href="https://hal.inria.fr/hal-02120736" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-02120736');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://hal.inria.fr/hal-02120736/file/peerj-cs-190.pdf" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-02120736/file/peerj-cs-190.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[doi:<a href="http://dx.doi.org/10.7717/peerj-cs.190">10.7717/peerj-cs.190</a>]




- <a name="alias:hal-02421327"></a>
Christophe Alias, Samuel Thibault,  and Laure Gonnord.
**A Compiler Algorithm to Guide Runtime Scheduling**.
Research Report RR-9315, INRIA Grenoble ; INRIA Bordeaux, December 2019.
<a title="Web link" href="https://hal.inria.fr/hal-02421327" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-02421327');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://hal.inria.fr/hal-02421327/file/RR-9315.pdf" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-02421327/file/RR-9315.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>




### Year 2017 


- <a name="kumar:tel-01538516"></a>
Suraj Kumar.
**Scheduling of Dense Linear Algebra Kernels on Heterogeneous Resources**.
Ph.D Thesis, Université de Bordeaux, April 2017.
<a title="Web link" href="https://tel.archives-ouvertes.fr/tel-01538516" target="_blank" onclick="return trackOutboundLink('https://tel.archives-ouvertes.fr/tel-01538516');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://tel.archives-ouvertes.fr/tel-01538516/file/KUMAR_SURAL_2017.pdf" target="_blank" onclick="return trackOutboundLink('https://tel.archives-ouvertes.fr/tel-01538516/file/KUMAR_SURAL_2017.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>




- <a name="beaumont:hal-01386174"></a>
Olivier Beaumont, Lionel Eyraud-Dubois,  and Suraj Kumar.
**Approximation Proofs of a Fast and Efficient List Scheduling Algorithm for Task-Based Runtime Systems on Multicores and GPUs**.
In *2017 IEEE International Parallel and Distributed Processing Symposium (IPDPS)*, pages 768-777, May 2017.
<a title="Web link" href="https://hal.inria.fr/hal-01386174" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-01386174');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://hal.inria.fr/hal-01386174/file/heteroPrioApproxProofsRR.pdf" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-01386174/file/heteroPrioApproxProofsRR.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[doi:<a href="http://dx.doi.org/10.1109/IPDPS.2017.71">10.1109/IPDPS.2017.71</a>]




### Year 2016 


- <a name="agullo:hal-01223573"></a>
Emmanuel Agullo, Olivier Beaumont, Lionel Eyraud-Dubois,  and Suraj Kumar.
**Are Static Schedules so Bad ? A Case Study on Cholesky Factorization**.
In *Proceedings of the 30th IEEE International Parallel & Distributed Processing Symposium, IPDPS'16*, Chicago, IL, USA, May 2016.
IEEE.
<a title="Web link" href="https://hal.inria.fr/hal-01223573" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-01223573');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://hal.inria.fr/hal-01223573/file/heteroprioCameraReady-ieeeCompatiable.pdf" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-01223573/file/heteroprioCameraReady-ieeeCompatiable.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[doi:<a href="http://dx.doi.org/10.1109/IPDPS.2016.90">10.1109/IPDPS.2016.90</a>]




- <a name="JaBlHU2016a"></a>
Johan Janzén, David Black-Schaffer,  and Andra Hugo.
**Partitioning GPUs for Improved Scalability**.
In *IEEE 28th International Symposium on Computer Architecture and High Performance Computing (SBAC-PAD)*, October 2016.
<a title="Web link" href="http://ieeexplore.ieee.org/abstract/document/7789322/" target="_blank" onclick="return trackOutboundLink('http://ieeexplore.ieee.org/abstract/document/7789322/');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
[doi:<a href="http://dx.doi.org/10.1109/SBAC-PAD.2016.14">10.1109/SBAC-PAD.2016.14</a>]




### Year 2015 


- <a name="agullo:hal-01120507"></a>
Emmanuel Agullo, Olivier Beaumont, Lionel Eyraud-Dubois, Julien Herrmann, Suraj Kumar, Loris Marchal,  and Samuel Thibault.
**Bridging the Gap between Performance and Bounds of Cholesky Factorization on Heterogeneous Platforms**.
In *HCW'2015 - Heterogeneity in Computing Workshop of IPDPS*, Hyderabad, India, May 2015.
<a title="Web link" href="https://hal.inria.fr/hal-01120507" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-01120507');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://hal.inria.fr/hal-01120507/document" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-01120507/document');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[doi:<a href="http://dx.doi.org/10.1109/IPDPSW.2015.35">10.1109/IPDPSW.2015.35</a>]




### Year 2014 


- <a name="sergent:hal-00978364"></a>
Marc Sergent and Simon Archipoff.
**Modulariser les ordonnanceurs de tâches : une approche structurelle**.
In *Compas'2014*, Neuchâtel, Suisse, April 2014.
<a title="Web link" href="http://hal.inria.fr/hal-00978364" target="_blank" onclick="return trackOutboundLink('http://hal.inria.fr/hal-00978364');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="http://hal.inria.fr/hal-00978364/PDF/ordonnanceurs_modulaires.pdf" target="_blank" onclick="return trackOutboundLink('http://hal.inria.fr/hal-00978364/PDF/ordonnanceurs_modulaires.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>




### Year 2010 


- <a name="AugCleThiNam10ICPADS"></a>
Cédric Augonnet, Jérôme Clet-Ortega, Samuel Thibault,  and Raymond Namyst.
**Data-Aware Task Scheduling on Multi-Accelerator based Platforms**.
In *The 16th International Conference on Parallel and Distributed Systems (ICPADS)*, Shanghai, China, December 2010.
<a title="Web link" href="http://hal.inria.fr/inria-00523937" target="_blank" onclick="return trackOutboundLink('http://hal.inria.fr/inria-00523937');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="http://hal.inria.fr/inria-00523937/document" target="_blank" onclick="return trackOutboundLink('http://hal.inria.fr/inria-00523937/document');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[doi:<a href="http://dx.doi.org/10.1109/ICPADS.2010.129">10.1109/ICPADS.2010.129</a>]







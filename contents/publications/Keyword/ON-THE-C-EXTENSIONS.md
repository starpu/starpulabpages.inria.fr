---
layout: default
title: On The C Extensions
permalink: publications/Keyword/ON-THE-C-EXTENSIONS.html
full-width: True
---

## On The C Extensions
### Year 2013 


- <a name="LC13Report"></a>
Ludovic Courtès.
**C Language Extensions for Hybrid CPU/GPU Programming with StarPU**.
Research Report RR-8278, INRIA, April 2013.
<a title="Web link" href="http://hal.inria.fr/hal-00807033" target="_blank" onclick="return trackOutboundLink('http://hal.inria.fr/hal-00807033');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="http://hal.inria.fr/hal-00807033/PDF/RR-8278.pdf" target="_blank" onclick="return trackOutboundLink('http://hal.inria.fr/hal-00807033/PDF/RR-8278.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>







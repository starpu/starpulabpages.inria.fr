---
layout: default
title: On Parallel Tasks
permalink: publications/Keyword/ON-PARALLEL-TASKS.html
full-width: True
---

## On Parallel Tasks
### Year 2018 


- <a name="cojean:tel-01816341"></a>
Terry Cojean.
**Programmation of heterogeneous architectures using moldable tasks**.
Ph.D Thesis, Université de Bordeaux, March 2018.
<a title="Web link" href="https://tel.archives-ouvertes.fr/tel-01816341" target="_blank" onclick="return trackOutboundLink('https://tel.archives-ouvertes.fr/tel-01816341');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://tel.archives-ouvertes.fr/tel-01816341/file/COJEAN_TERRY_2018.pdf" target="_blank" onclick="return trackOutboundLink('https://tel.archives-ouvertes.fr/tel-01816341/file/COJEAN_TERRY_2018.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>




### Year 2016 


- <a name="cojean:hal-01409965"></a>
Terry Cojean, Abdou Guermouche, Andra Hugo, Raymond Namyst,  and Pierre-André Wacrenier.
**Resource aggregation for task-based Cholesky Factorization on top of modern architectures**.
*Parallel Computing*, 83:73-92, November 2016.
Note: This paper is submitted for review to the Parallel Computing special issue for HCW and HeteroPar 16 workshops.
<a title="Web link" href="https://hal.inria.fr/hal-01409965" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-01409965');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://hal.inria.fr/hal-01409965/file/submission.pdf" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-01409965/file/submission.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[doi:<a href="http://dx.doi.org/10.1016/j.parco.2018.10.007">10.1016/j.parco.2018.10.007</a>]




- <a name="beaumont:hal-01361992"></a>
Olivier Beaumont, Terry Cojean, Lionel Eyraud-Dubois, Abdou Guermouche,  and Suraj Kumar.
**Scheduling of Linear Algebra Kernels on Multiple Heterogeneous Resources**.
In *International Conference on High Performance Computing, Data, and Analytics (HiPC)*, Hyderabad, India, December 2016.
<a title="Web link" href="https://hal.inria.fr/hal-01361992" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-01361992');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://hal.inria.fr/hal-01361992v2/document" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-01361992v2/document');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[doi:<a href="http://dx.doi.org/10.1109/HiPC.2016.045">10.1109/HiPC.2016.045</a>]




- <a name="cojean:hal-01502749"></a>
Terry Cojean.
**Exploiting Two-Level Parallelism by Aggregating Computing Resources in Task-Based Applications Over Accelerator-Based Machines**.
In *SIAM Conference on Parallel Processing for Scientific Computing (SIAM PP 2016)*, Paris, France, April 2016.
<a title="Web link" href="https://inria.hal.science/hal-01502749" target="_blank" onclick="return trackOutboundLink('https://inria.hal.science/hal-01502749');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;




- <a name="cojean:hal-01410103"></a>
Terry Cojean.
**The StarPU Runtime System at Exascale ?**.
In *RESPA workshop at SC16*, Salt Lake City, Utah, United States, November 2016.
<a title="Web link" href="https://inria.hal.science/hal-01410103" target="_blank" onclick="return trackOutboundLink('https://inria.hal.science/hal-01410103');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;




- <a name="cojean:hal-01181135"></a>
Terry Cojean, Abdou Guermouche, Andra Hugo, Raymond Namyst,  and Pierre-André Wacrenier.
**Resource aggregation for task-based Cholesky Factorization on top of heterogeneous machines**.
In *HeteroPar'2016 workshop of Euro-Par*, Grenoble, France, August 2016.
<a title="Web link" href="https://hal.inria.fr/hal-01181135" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-01181135');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://hal.inria.fr/hal-01181135/file/papier%20%281%29.pdf" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-01181135/file/papier%20%281%29.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>




- <a name="cojean:hal-01355385"></a>
Terry Cojean, Abdou Guermouche, Andra-Ecaterina Hugo, Raymond Namyst,  and Pierre-André Wacrenier.
**Resource aggregation in task-based applications over accelerator-based multicore machines**.
In *HeteroPar'2016 worshop of Euro-Par*, Grenoble, France, August 2016.
<a title="Web link" href="https://inria.hal.science/hal-01355385" target="_blank" onclick="return trackOutboundLink('https://inria.hal.science/hal-01355385');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;







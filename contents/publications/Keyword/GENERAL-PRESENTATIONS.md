---
layout: default
title: General Presentations
permalink: publications/Keyword/GENERAL-PRESENTATIONS.html
full-width: True
---

## General Presentations
### Year 2018 


- <a name="thibault:tel-01959127"></a>
Samuel Thibault.
**On Runtime Systems for Task-based Programming on Heterogeneous Platforms**.
Habilitation à diriger des recherches, Université de Bordeaux, December 2018.
<a title="Web link" href="https://hal.inria.fr/tel-01959127" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/tel-01959127');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://hal.inria.fr/tel-01959127/file/hdr.pdf" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/tel-01959127/file/hdr.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
<a title="BIB" href="https://inria.hal.science/tel-01959127v1/bibtex" target="_blank" onclick="return trackOutboundLink('https://inria.hal.science/tel-01959127v1/bibtex');"><i class="fa fa-book" aria-hidden="true"></i></a>




### Year 2011 


- <a name="Aug11Thesis"></a>
Cédric Augonnet.
**Scheduling Tasks over Multicore machines enhanced with Accelerators: a Runtime System's Perspective**.
Ph.D Thesis, Université de Bordeaux, December 2011.
<a title="Web link" href="http://tel.archives-ouvertes.fr/tel-00777154" target="_blank" onclick="return trackOutboundLink('http://tel.archives-ouvertes.fr/tel-00777154');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="http://tel.archives-ouvertes.fr/tel-00777154/document" target="_blank" onclick="return trackOutboundLink('http://tel.archives-ouvertes.fr/tel-00777154/document');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>




- <a name="AugThiNamWac11CCPE"></a>
Cédric Augonnet, Samuel Thibault, Raymond Namyst,  and Pierre-André Wacrenier.
**StarPU: A Unified Platform for Task Scheduling on Heterogeneous Multicore Architectures**.
*CCPE - Concurrency and Computation: Practice and Experience, Special Issue: Euro-Par 2009*, 23:187-198, February 2011.
<a title="Web link" href="http://hal.inria.fr/inria-00550877" target="_blank" onclick="return trackOutboundLink('http://hal.inria.fr/inria-00550877');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="http://hal.inria.fr/inria-00550877/document" target="_blank" onclick="return trackOutboundLink('http://hal.inria.fr/inria-00550877/document');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[doi:<a href="http://dx.doi.org/10.1002/cpe.1631">10.1002/cpe.1631</a>]




### Year 2010 


- <a name="AugThiNamWac10RR7240"></a>
Cédric Augonnet, Samuel Thibault,  and Raymond Namyst.
**StarPU: a Runtime System for Scheduling Tasks over Accelerator-Based Multicore Machines**.
Research Report RR-7240, INRIA, March 2010.
<a title="Web link" href="http://hal.inria.fr/inria-00467677" target="_blank" onclick="return trackOutboundLink('http://hal.inria.fr/inria-00467677');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="http://hal.inria.fr/inria-00467677/document" target="_blank" onclick="return trackOutboundLink('http://hal.inria.fr/inria-00467677/document');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>




### Year 2009 


- <a name="Aug09Renpar19"></a>
Cédric Augonnet.
**StarPU: un support exécutif unifié pour les architectures multicoeurs hétérogènes**.
In *19èmes Rencontres Francophones du Parallélisme*, Toulouse, France, September 2009.
Note: Best Paper Award.
<a title="Web link" href="http://hal.inria.fr/inria-00411581" target="_blank" onclick="return trackOutboundLink('http://hal.inria.fr/inria-00411581');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="http://hal.inria.fr/inria-00411581/document" target="_blank" onclick="return trackOutboundLink('http://hal.inria.fr/inria-00411581/document');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>




- <a name="AugThiNamWac09Europar"></a>
Cédric Augonnet, Samuel Thibault, Raymond Namyst,  and Pierre-André Wacrenier.
**StarPU: A Unified Platform for Task Scheduling on Heterogeneous Multicore Architectures**.
In *Euro-Par - 15th International Conference on Parallel Processing*, volume 5704 of *LNCS*, Delft, The Netherlands, pages 863-874, August 2009.
Springer.
<a title="Web link" href="http://hal.inria.fr/inria-00384363" target="_blank" onclick="return trackOutboundLink('http://hal.inria.fr/inria-00384363');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="http://hal.inria.fr/inria-00384363/document" target="_blank" onclick="return trackOutboundLink('http://hal.inria.fr/inria-00384363/document');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[doi:<a href="http://dx.doi.org/10.1007/978-3-642-03869-3_80">10.1007/978-3-642-03869-3_80</a>]




### Year 2008 


- <a name="Aug08Master"></a>
Cédric Augonnet.
**Vers des supports d'exécution capables d'exploiter les machines multicoeurs hétérogènes**.
Master Thesis, Université de Bordeaux, June 2008.
<a title="Web link" href="http://hal.inria.fr/inria-00289361" target="_blank" onclick="return trackOutboundLink('http://hal.inria.fr/inria-00289361');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="http://hal.inria.fr/inria-00289361/document" target="_blank" onclick="return trackOutboundLink('http://hal.inria.fr/inria-00289361/document');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>




- <a name="AugNam08HPPC"></a>
Cédric Augonnet and Raymond Namyst.
**A unified runtime system for heterogeneous multicore architectures**.
In *Proceedings of the International Euro-Par Workshops 2008, HPPC'08*, volume 5415 of *LNCS*, Las Palmas de Gran Canaria, Spain, pages 174-183, August 2008.
Springer.
**ISBN:** 978-3-642-00954-9.
<a title="Web link" href="http://hal.inria.fr/inria-00326917" target="_blank" onclick="return trackOutboundLink('http://hal.inria.fr/inria-00326917');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="http://hal.inria.fr/inria-00326917/document" target="_blank" onclick="return trackOutboundLink('http://hal.inria.fr/inria-00326917/document');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[doi:<a href="http://dx.doi.org/10.1007/978-3-642-00955-6_22">10.1007/978-3-642-00955-6_22</a>]







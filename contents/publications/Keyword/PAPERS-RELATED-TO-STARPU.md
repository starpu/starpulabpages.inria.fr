---
layout: default
title: Papers related to StarPU
permalink: publications/Keyword/PAPERS-RELATED-TO-STARPU.html
full-width: True
---

## Papers related to StarPU
### Year 2022 


- <a name="agullo:hal-03588491"></a>
Emmanuel Agullo, Alfredo Buttari, Abdou Guermouche, Julien Herrmann,  and Antoine Jego.
**Task-Based Parallel Programming for Scalable Algorithms: application to Matrix Multiplication**.
Research Report 9461, Inria Bordeaux - Sud-Ouest, February 2022.
<a title="Web link" href="https://hal.inria.fr/hal-03588491" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-03588491');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://hal.inria.fr/hal-03588491/file/RR-9461.pdf" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-03588491/file/RR-9461.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>




### Year 2021 


- <a name="denis:hal-03290121"></a>
Alexandre Denis, Emmanuel Jeannot,  and Philippe Swartvagher.
**Interferences between Communications and Computations in Distributed HPC Systems**.
In *ICPP 2021 - 50th International Conference on Parallel Processing*, Chicago / Virtual, United States, pages 11, August 2021.
<a title="Web link" href="https://hal.inria.fr/hal-03290121" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-03290121');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://hal.inria.fr/hal-03290121/file/rr.pdf" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-03290121/file/rr.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[doi:<a href="http://dx.doi.org/10.1145/3472456.3473516">10.1145/3472456.3473516</a>]




### Year 2020 


- <a name="10.5555/3433701.3433783"></a>
Elliott Slaughter, Wei Wu, Yuankun Fu, Legend Brandenburg, Nicolai Garcia, Wilhem Kautz, Emily Marx, Kaleb S. Morris, Qinglei Cao, George Bosilca, Seema Mirchandaney, Wonchan Lee, Sean Treichler, Patrick McCormick,  and Alex Aiken.
**Task Bench: A Parameterized Benchmark for Evaluating Parallel Runtime Performance**.
In *Proceedings of the International Conference for High Performance Computing, Networking, Storage and Analysis*, SC '20, 2020.
IEEE Press.
**ISBN:** 9781728199986.
<a title="Web link" href="https://dl.acm.org/doi/10.5555/3433701.3433783" target="_blank" onclick="return trackOutboundLink('https://dl.acm.org/doi/10.5555/3433701.3433783');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://dl.acm.org/doi/pdf/10.5555/3433701.3433783" target="_blank" onclick="return trackOutboundLink('https://dl.acm.org/doi/pdf/10.5555/3433701.3433783');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[doi:<a href="http://dx.doi.org/10.5555/3433701.3433783">10.5555/3433701.3433783</a>]




### Year 2018 


- <a name="thoman2018taxonomy"></a>
Peter Thoman, Kiril Dichev, Thomas Heller, Roman Iakymchuk, Xavier Aguilar, Khalid Hasanov, Philipp Gschwandtner, Pierre Lemarinier, Stefano Markidis, Herbert Jordan,  and others.
**A taxonomy of task-based parallel programming technologies for high-performance computing**.
*The Journal of Supercomputing*, 74(4):1422-1434, 2018.
<a title="Web link" href="https://link.springer.com/article/10.1007%2Fs11227-018-2238-4" target="_blank" onclick="return trackOutboundLink('https://link.springer.com/article/10.1007%2Fs11227-018-2238-4');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://link.springer.com/content/pdf/10.1007/s11227-018-2238-4.pdf" target="_blank" onclick="return trackOutboundLink('https://link.springer.com/content/pdf/10.1007/s11227-018-2238-4.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[doi:<a href="http://dx.doi.org/10.1007/s11227-018-2238-4">10.1007/s11227-018-2238-4</a>]




### Year 2014 


- <a name="6937016"></a>
I. D. Mironescu and L. Vintan.
**Coloured Petri Net modelling of task scheduling on a heterogeneous computational node**.
In *2014 IEEE 10th International Conference on Intelligent Computer Communication and Processing (ICCP)*, pages 323-330, 2014.
<a title="Web link" href="https://ieeexplore.ieee.org/document/6937016" target="_blank" onclick="return trackOutboundLink('https://ieeexplore.ieee.org/document/6937016');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://www.researchgate.net/profile/Lucian_Vintan/publication/264744234_Coloured_Petri_Net_Modelling_of_Task_Scheduling_on_a_Heterogeneous_Computational_Node/links/53edbae80cf26b9b7dc5f79e/Coloured-Petri-Net-Modelling-of-Task-Scheduling-on-a-Heterogeneous-Computational-Node.pdf" target="_blank" onclick="return trackOutboundLink('https://www.researchgate.net/profile/Lucian_Vintan/publication/264744234_Coloured_Petri_Net_Modelling_of_Task_Scheduling_on_a_Heterogeneous_Computational_Node/links/53edbae80cf26b9b7dc5f79e/Coloured-Petri-Net-Modelling-of-Task-Scheduling-on-a-Heterogeneous-Computational-Node.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[doi:<a href="http://dx.doi.org/10.1109/ICCP.2014.6937016">10.1109/ICCP.2014.6937016</a>]







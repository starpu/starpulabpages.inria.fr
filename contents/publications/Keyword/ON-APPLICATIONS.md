---
layout: default
title: On Applications
permalink: publications/Keyword/ON-APPLICATIONS.html
full-width: True
---

## On Applications
### Year 2024 


- <a name="beaumont:hal-04690154"></a>
Olivier Beaumont, Jean-François David, Lionel Eyraud-Dubois,  and Samuel Thibault.
**Exploiting Processor Heterogeneity to Improve Throughput and Reduce Latency for Deep Neural Network Inference**.
In *SBAC-PAD 2024 - IEEE 36th International Symposium on Computer Architecture and High Performance Computing*, Hilo, Hawaii, United States, November 2024.
<a title="Web link" href="https://hal.science/hal-04690154" target="_blank" onclick="return trackOutboundLink('https://hal.science/hal-04690154');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://hal.science/hal-04690154v1/file/sbac_pad%20%281%29.pdf" target="_blank" onclick="return trackOutboundLink('https://hal.science/hal-04690154v1/file/sbac_pad%20%281%29.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>




- <a name="beaumont:hal-04668550"></a>
Olivier Beaumont, Jean-François David, Lionel Eyraud-Dubois,  and Samuel Thibault.
**StarONNX : Un ordonanceur dynamique pour une inférence rapide et à haut débit sur des ressources hétérogènes**.
In *Compas 2024 - Conférence francophone d'informatique en Parallélisme, Architecture et Système*, Nantes, France, July 2024.
<a title="Web link" href="https://inria.hal.science/hal-04668550" target="_blank" onclick="return trackOutboundLink('https://inria.hal.science/hal-04668550');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://inria.hal.science/hal-04668550/file/staronnx_compas2024.pdf" target="_blank" onclick="return trackOutboundLink('https://inria.hal.science/hal-04668550/file/staronnx_compas2024.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>




- <a name="beaumont:hal-04646530"></a>
Olivier Beaumont, Jean-François David, Lionel Eyraud-Dubois,  and Samuel Thibault.
**StarONNX: a Dynamic Scheduler for Low Latency and High Throughput Inference on Heterogeneous Resources**.
In *HeteroPar 2024 - 22ND INTERNATIONAL WORKSHOP Algorithms, Models and Tools for Parallel Computing on Heterogeneous Platforms*, HeteroPar'24 Proceedings, Madrid, Spain, August 2024.
EuroPar'24.
Note: (collocated with Euro-Par 2024).
<a title="Web link" href="https://inria.hal.science/hal-04646530" target="_blank" onclick="return trackOutboundLink('https://inria.hal.science/hal-04646530');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://inria.hal.science/hal-04646530/file/heteropar.pdf" target="_blank" onclick="return trackOutboundLink('https://inria.hal.science/hal-04646530/file/heteropar.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>




- <a name="pinto2024performance"></a>
Vinicius Garcia Pinto, João VF Lima, Vanderlei Munhoz, Daniel Cordeiro, Emilio Francesquini,  and Márcio Castro.
**Performance Evaluation of Dense Linear Algebra Kernels using Chameleon and StarPU on AWS**.
In *Simpósio em Sistemas Computacionais de Alto Desempenho (SSCAD)*, pages 300-311, 2024.
SBC.




### Year 2022 


- <a name="agullo:hal-03773985"></a>
Emmanuel Agullo, Olivier Coulaud, Alexandre Denis, Mathieu Faverge, Alain A. Franc, Jean-Marc Frigerio, Nathalie Furmento, Samuel Thibault, Adrien Guilbaud, Emmanuel Jeannot, Romain Peressoni,  and Florent Pruvost.
**Task-based randomized singular value decomposition and multidimensional scaling**.
Research Report 9482, Inria Bordeaux - Sud Ouest ; Inrae - BioGeCo, September 2022.
<a title="Web link" href="https://hal.inria.fr/hal-03773985" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-03773985');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://hal.inria.fr/hal-03773985/file/RR-9482.pdf" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-03773985/file/RR-9482.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>




### Year 2021 


- <a name="papadopoulos:hal-03318644"></a>
Lazaros Papadopoulos, Dimitrios Soudris, Christoph Kessler, August Ernstsson, Johan Ahlqvist, Nikos Vasilas, Athanasios I Papadopoulos, Panos Seferlis, Charles Prouveur, Matthieu Haefele, Samuel Thibault, Athanasios Salamanis, Theodoros Ioakimidis,  and Dionysios Kehagias.
**EXA2PRO: A Framework for High Development Productivity on Heterogeneous Computing Systems**.
*IEEE Transactions on Parallel and Distributed Systems*, August 2021.
<a title="Web link" href="https://hal.inria.fr/hal-03318644" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-03318644');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://hal.inria.fr/hal-03318644/file/EXA2PRO___TPDS.pdf" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-03318644/file/EXA2PRO___TPDS.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[doi:<a href="http://dx.doi.org/10.1109/TPDS.2021.3104257">10.1109/TPDS.2021.3104257</a>]




### Year 2020 


- <a name="lopes:hal-02914793"></a>
Rafael Alvares da Silva Lopes, Samuel Thibault,  and Alba Cristina Magalhães Alves de Melo.
**MASA-StarPU: Parallel Sequence Comparison with Multiple Scheduling Policies and Pruning**.
In *SBAC-PAD 2020 - IEEE 32nd International Symposium on Computer Architecture and High Performance Computing*, Porto, Portugal, September 2020.
<a title="Web link" href="https://hal.inria.fr/hal-02914793" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-02914793');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://hal.inria.fr/hal-02914793/file/lopes_rafael_paper25_sbacpad2020.pdf" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-02914793/file/lopes_rafael_paper25_sbacpad2020.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[doi:<a href="http://dx.doi.org/10.1109/SBAC-PAD49847.2020.00039">10.1109/SBAC-PAD49847.2020.00039</a>]




- <a name="soni:hal-02985721"></a>
Georgios Tzanos, Vineet Soni, Charles Prouveur, Matthieu Haefele, Stavroula Zouzoula, Lazaros Papadopoulos, Samuel Thibault, Nicolas Vandenbergen, Dirk Pleiter,  and Dimitrios Soudris.
**Applying StarPU runtime system to scientific applications: Experiences and lessons learned**.
In *Parallel Optimization using/for Multi and Many-core High Performance Computing (POMCO)*, Barcelona, Spain, December 2020.
<a title="Web link" href="https://hal.inria.fr/hal-02985721" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-02985721');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://hal.inria.fr/hal-02985721/file/POMCO2020-camera-ready.pdf" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-02985721/file/POMCO2020-camera-ready.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>




### Year 2019 


- <a name="alonazi:hal-02403109"></a>
A. AlOnazi, H. Ltaief, D. Keyes, I. Said,  and Samuel Thibault.
**Asynchronous Task-Based Execution of the Reverse Time Migration for the Oil and Gas Industry**.
In *2019 IEEE International Conference on Cluster Computing (CLUSTER)*, Albuquerque, USA, pages 1-11, September 2019.
IEEE.
<a title="Web link" href="https://hal.inria.fr/hal-02403109" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-02403109');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://hal.inria.fr/hal-02403109/file/2019_tbrtm_cluster.pdf" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-02403109/file/2019_tbrtm_cluster.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[doi:<a href="http://dx.doi.org/10.1109/CLUSTER.2019.8891054">10.1109/CLUSTER.2019.8891054</a>]




### Year 2018 


- <a name="refId0"></a>
Mohamed Essadki, Jonathan Jung, Adam Larat, Milan Pelletier,  and Vincent Perrier.
**A Task-Driven Implementation of a Simple Numerical Solver for Hyperbolic Conservation Laws**.
*ESAIM: ProcS*, 63:228-247, 2018.
<a title="Web link" href="https://doi.org/10.1051/proc/201863228" target="_blank" onclick="return trackOutboundLink('https://doi.org/10.1051/proc/201863228');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://www.esaim-proc.org/articles/proc/pdf/2018/03/proc_esaim2018_228.pdf" target="_blank" onclick="return trackOutboundLink('https://www.esaim-proc.org/articles/proc/pdf/2018/03/proc_esaim2018_228.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[doi:<a href="http://dx.doi.org/10.1051/proc/201863228">10.1051/proc/201863228</a>]




- <a name="soudris:hal-03273509"></a>
Dimitrios Soudris, Lazaros Papadopoulos, Christoph W Kessler, Dionysios D Kehagias, Athanasios Papadopoulos, Panos Seferlis, Alexander Chatzigeorgiou, Apostolos Ampatzoglou, Samuel Thibault, Raymond Namyst, Dirk Pleiter, Georgi Gaydadjiev, Tobias Becker,  and Matthieu Haefele.
**EXA2PRO programming environment**.
In *SAMOS XVIII: Architectures, Modeling, and Simulation*, Pythagorion, Greece, pages 202-209, July 2018.
ACM.
<a title="Web link" href="https://hal.inria.fr/hal-03273509" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-03273509');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://hal.inria.fr/hal-03273509/file/3229631.3239369.pdf" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-03273509/file/3229631.3239369.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[doi:<a href="http://dx.doi.org/10.1145/3229631.3239369">10.1145/3229631.3239369</a>]




### Year 2017 


- <a name="couteyencarpaye:hal-01507613"></a>
Jean-Marie Couteyen Carpaye, Jean Roman,  and Pierre Brenner.
**Design and Analysis of a Task-based Parallelization over a Runtime System of an Explicit Finite-Volume CFD Code with Adaptive Time Stepping**.
*International Journal of Computational Science and Engineering*, pp 1 - 22, 2017.
<a title="Web link" href="https://hal.inria.fr/hal-01507613" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-01507613');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://hal.inria.fr/hal-01507613/file/flusepa-task-hal-inria-preprint.pdf" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-01507613/file/flusepa-task-hal-inria-preprint.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[doi:<a href="http://dx.doi.org/10.1016/j.jocs.2017.03.008">10.1016/j.jocs.2017.03.008</a>]




- <a name="7973751"></a>
Olivier Aumage, Julien Bigot, HÃ©lÃ¨ne Coullon, Christian PÃ©rez,  and JÃ©rÃ´me Richard.
**Combining Both a Component Model and a Task-Based Model for HPC Applications: A Feasibility Study on GYSELA**.
In *2017 17th IEEE/ACM International Symposium on Cluster, Cloud and Grid Computing (CCGRID)*, pages 635-644, 2017.
[doi:<a href="http://dx.doi.org/10.1109/CCGRID.2017.88">10.1109/CCGRID.2017.88</a>]




- <a name="agullo:hal-01473475"></a>
Emmanuel Agullo, Alfredo Buttari, Mikko Byckling, Abdou Guermouche,  and Ian Masliah.
**Achieving high-performance with a sparse direct solver on Intel KNL**.
Research Report RR-9035, Inria Bordeaux Sud-Ouest ; CNRS-IRIT ; Intel corporation ; Université Bordeaux, February 2017.
<a title="Web link" href="https://hal.inria.fr/hal-01473475" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-01473475');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://hal.inria.fr/hal-01473475/file/RR-9035.pdf" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-01473475/file/RR-9035.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>




### Year 2016 


- <a name="balin2016fast"></a>
Nolwenn Balin, Guillaume Sylvand,  and Jérôme Robert.
**Fast methods applied to BEM solvers for acoustic propagation problems**.
In *22nd AIAA/CEAS Aeroacoustics Conference*, pages 2712, 2016.
[doi:<a href="http://dx.doi.org/10.2514/6.2016-2712">10.2514/6.2016-2712</a>]




- <a name="agullo:hal-01387482"></a>
Emmanuel Agullo, Bérenger Bramas, Olivier Coulaud, Martin Khannouz,  and Luka Stanisic.
**Task-based fast multipole method for clusters of multicore processors**.
Research Report RR-8970, Inria Bordeaux Sud-Ouest, October 2016.
<a title="Web link" href="https://hal.inria.fr/hal-01387482" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-01387482');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://hal.inria.fr/hal-01387482/file/report-8970.pdf" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-01387482/file/report-8970.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>




- <a name="agullo:hal-01316982"></a>
Emmanuel Agullo, Luc Giraud, Abdou Guermouche, Stojce Nakov,  and Jean Roman.
**Task-based Conjugate Gradient: from multi-GPU towards heterogeneous architectures**.
Research Report 8912, Inria Bordeaux Sud-Ouest, May 2016.
<a title="Web link" href="https://hal.inria.fr/hal-01316982" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-01316982');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://hal.inria.fr/hal-01316982/file/RR-8912.pdf" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-01316982/file/RR-8912.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>




### Year 2015 


- <a name="rossignon:tel-01230876"></a>
Corentin Rossignon.
**A fine grain model programming for parallelization of sparse linear solver**.
Ph.D Thesis, Université de Bordeaux, July 2015.
<a title="Web link" href="https://tel.archives-ouvertes.fr/tel-01230876" target="_blank" onclick="return trackOutboundLink('https://tel.archives-ouvertes.fr/tel-01230876');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://tel.archives-ouvertes.fr/tel-01230876/file/ROSSIGNON_CORENTIN_2015.pdf" target="_blank" onclick="return trackOutboundLink('https://tel.archives-ouvertes.fr/tel-01230876/file/ROSSIGNON_CORENTIN_2015.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>




- <a name="MaMiDuAuThiAoNa15"></a>
Vìctor Martìnez, David Michéa, Fabrice Dupros, Olivier Aumage, Samuel Thibault, Hideo Aochi,  and Philippe Olivier Alexandre Navaux.
**Towards seismic wave modeling on heterogeneous many-core architectures using task-based runtime system**.
In *SBAC-PAD - 27th International Symposium on Computer Architecture and High Performance Computing*, Florianopolis, Brazil, October 2015.
<a title="Web link" href="https://hal.inria.fr/hal-01182746" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-01182746');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://hal.inria.fr/hal-01182746/file/sbac2015_soumission.pdf" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-01182746/file/sbac2015_soumission.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[doi:<a href="http://dx.doi.org/10.1109/SBAC-PAD.2015.33">10.1109/SBAC-PAD.2015.33</a>]




### Year 2014 


- <a name="agullo:hal-00911856"></a>
Emmanuel Agullo, Bérenger Bramas, Olivier Coulaud, Eric Darve, Matthias Messner,  and Toru Takahashi.
**Task-Based FMM for Multicore Architectures**.
*SIAM Journal on Scientific Computing*, 36(1):66-93, 2014.
<a title="Web link" href="https://hal.inria.fr/hal-00911856" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-00911856');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://hal.inria.fr/hal-00911856/file/sisc-cpu.pdf" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-00911856/file/sisc-cpu.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[doi:<a href="http://dx.doi.org/10.1137/130915662">10.1137/130915662</a>]




- <a name="sylvain:hal-01005765"></a>
Sylvain Henry, Alexandre Denis, Denis Barthou, Marie-Christine Counilh,  and Raymond Namyst.
**Toward OpenCL Automatic Multi-Device Support**.
In Fernando Silva, Ines Dutra,  and Vitor Santos Costa, editors, *Euro-Par 2014*, Porto, Portugal, August 2014.
Springer.
<a title="Web link" href="http://hal.inria.fr/hal-01005765" target="_blank" onclick="return trackOutboundLink('http://hal.inria.fr/hal-01005765');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="http://hal.inria.fr/hal-01005765/PDF/final.pdf" target="_blank" onclick="return trackOutboundLink('http://hal.inria.fr/hal-01005765/PDF/final.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[doi:<a href="http://dx.doi.org/10.1007/978-3-319-09873-9_65">10.1007/978-3-319-09873-9_65</a>]




- <a name="lacoste:hal-00987094"></a>
Xavier Lacoste, Mathieu Faverge, Pierre Ramet, Samuel Thibault,  and George Bosilca.
**Taking advantage of hybrid systems for sparse direct solvers via task-based runtimes**.
In *HCW'2014 - Heterogeneity in Computing Workshop of IPDPS*, Phoenix, USA, May 2014.
IEEE.
Note: RR-8446.
<a title="Web link" href="http://hal.inria.fr/hal-00987094" target="_blank" onclick="return trackOutboundLink('http://hal.inria.fr/hal-00987094');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="http://hal.inria.fr/hal-00987094/PDF/sparsegpus.pdf" target="_blank" onclick="return trackOutboundLink('http://hal.inria.fr/hal-00987094/PDF/sparsegpus.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[doi:<a href="http://dx.doi.org/10.1109/IPDPSW.2014.9">10.1109/IPDPSW.2014.9</a>]




- <a name="agullo:hal-00974674"></a>
Emmanuel Agullo, Berenger Bramas, Olivier Coulaud, Eric Darve, Matthias Messner,  and Toru Takahashi.
**Task-based FMM for heterogeneous architectures**.
Research Report RR-8513, Inria Bordeaux - Sud-Ouest, April 2014.
<a title="Web link" href="https://hal.inria.fr/hal-00974674" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-00974674');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://hal.inria.fr/hal-00974674/file/RR-8513.pdf" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-00974674/file/RR-8513.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>




- <a name="lacoste:hal-00925017"></a>
Xavier Lacoste, Mathieu Faverge, Pierre Ramet, Samuel Thibault,  and George Bosilca.
**Taking advantage of hybrid systems for sparse direct solvers via task-based runtimes**.
Research Report RR-8446, INRIA, January 2014.
<a title="Web link" href="http://hal.inria.fr/hal-00925017" target="_blank" onclick="return trackOutboundLink('http://hal.inria.fr/hal-00925017');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="http://hal.inria.fr/hal-00925017/PDF/RR-8446.pdf" target="_blank" onclick="return trackOutboundLink('http://hal.inria.fr/hal-00925017/PDF/RR-8446.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>




- <a name="sergent:hal-00978602"></a>
Emmanuel Agullo, Olivier Aumage, Mathieu Faverge, Nathalie Furmento, Florent Pruvost, Marc Sergent,  and Samuel Thibault.
**Overview of Distributed Linear Algebra on Hybrid Nodes over the StarPU Runtime**.
SIAM Conference on Parallel Processing for Scientific Computing, February 2014.
<a title="Web link" href="http://hal.inria.fr/hal-00978602" target="_blank" onclick="return trackOutboundLink('http://hal.inria.fr/hal-00978602');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="http://hal.inria.fr/hal-00978602/PDF/siampp14.pdf" target="_blank" onclick="return trackOutboundLink('http://hal.inria.fr/hal-00978602/PDF/siampp14.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>




### Year 2013 


- <a name="Bor13Thesis"></a>
Cyril Bordage.
**Ordonnancement dynamique, adapté aux architectures hétérogènes, de la méthode multipôle pour les équations de Maxwell, en électromagnétisme**.
Ph.D Thesis, Université de Bordeaux, December 2013.
<a title="Web link" href="https://www.theses.fr/2013BOR15026" target="_blank" onclick="return trackOutboundLink('https://www.theses.fr/2013BOR15026');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://www.theses.fr/2013BOR15026/document" target="_blank" onclick="return trackOutboundLink('https://www.theses.fr/2013BOR15026/document');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>




- <a name="Hen13Thesis"></a>
Sylvain Henry.
**Modèles de programmation et supports exécutifs pour architectures hétérogènes**.
Ph.D Thesis, Université de Bordeaux, November 2013.
<a title="Web link" href="http://tel.archives-ouvertes.fr/tel-00948309" target="_blank" onclick="return trackOutboundLink('http://tel.archives-ouvertes.fr/tel-00948309');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="http://tel.archives-ouvertes.fr/tel-00948309/document" target="_blank" onclick="return trackOutboundLink('http://tel.archives-ouvertes.fr/tel-00948309/document');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>




- <a name="hen13fhpc"></a>
Sylvain Henry.
**ViperVM: a Runtime System for Parallel Functional High-Performance Computing on Heterogeneous Architectures**.
In *2nd Workshop on Functional High-Performance Computing (FHPC'13)*, Boston, USA, September 2013.
<a title="Web link" href="http://hal.inria.fr/hal-00851122" target="_blank" onclick="return trackOutboundLink('http://hal.inria.fr/hal-00851122');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="http://hal.inria.fr/hal-00851122/PDF/fhpc13.pdf" target="_blank" onclick="return trackOutboundLink('http://hal.inria.fr/hal-00851122/PDF/fhpc13.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[doi:<a href="http://dx.doi.org/10.1145/2502323.2502329">10.1145/2502323.2502329</a>]




- <a name="odajima:hal-00920915"></a>
Tetsuya Odajima, Taisuke Boku, Mitsuhisa Sato, Toshihiro Hanawa, Yuetsu Kodama, Raymond Namyst, Samuel Thibault,  and Olivier Aumage.
**Adaptive Task Size Control on High Level Programming for GPU/CPU Work Sharing**.
In *ICA3PP-2013 - The 13th International Conference on Algorithms and Architectures for Parallel Processing*, Vietri sul Mare, Italy, December 2013.
<a title="Web link" href="http://hal.inria.fr/hal-00920915" target="_blank" onclick="return trackOutboundLink('http://hal.inria.fr/hal-00920915');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="http://hal.inria.fr/hal-00920915/PDF/ADPC2013-117.pdf" target="_blank" onclick="return trackOutboundLink('http://hal.inria.fr/hal-00920915/PDF/ADPC2013-117.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[doi:<a href="http://dx.doi.org/10.1007/978-3-319-03889-6_7">10.1007/978-3-319-03889-6_7</a>]




- <a name="ohshima:hal-00926144"></a>
Satoshi Ohshima, Satoshi Katagiri, Kengo Nakajima, Samuel Thibault,  and Raymond Namyst.
**Implementation of FEM Application on GPU with StarPU**.
In *SIAM CSE13 - SIAM Conference on Computational Science and Engineering 2013*, Boston, USA, February 2013.
SIAM.
<a title="Web link" href="http://hal.inria.fr/hal-00926144" target="_blank" onclick="return trackOutboundLink('http://hal.inria.fr/hal-00926144');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;




- <a name="Ros13Renpar"></a>
Corentin Rossignon.
**Optimisation du produit matrice-vecteur creux sur architecture GPU pour un simulateur de reservoir**.
In *21èmes Rencontres Francophones du Parallélisme (RenPar'21)*, Grenoble, France, January 2013.
<a title="Web link" href="http://hal.inria.fr/hal-00773571" target="_blank" onclick="return trackOutboundLink('http://hal.inria.fr/hal-00773571');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="http://hal.inria.fr/hal-00773571/document" target="_blank" onclick="return trackOutboundLink('http://hal.inria.fr/hal-00773571/document');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>




- <a name="rossignon:hal-00858350"></a>
Corentin Rossignon, Pascal Hénon, Olivier Aumage,  and Samuel Thibault.
**A NUMA-aware fine grain parallelization framework for multi-core architecture**.
In *PDSEC - 14th IEEE International Workshop on Parallel and Distributed Scientific and Engineering Computing - 2013*, Boston, USA, May 2013.
<a title="Web link" href="http://hal.inria.fr/hal-00858350" target="_blank" onclick="return trackOutboundLink('http://hal.inria.fr/hal-00858350');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="http://hal.inria.fr/hal-00858350/PDF/taggre_pdsec_2013.pdf" target="_blank" onclick="return trackOutboundLink('http://hal.inria.fr/hal-00858350/PDF/taggre_pdsec_2013.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[doi:<a href="http://dx.doi.org/10.1109/IPDPSW.2013.204">10.1109/IPDPSW.2013.204</a>]




- <a name="henry:hal-00853423"></a>
Sylvain Henry, Denis Barthou, Alexandre Denis, Raymond Namyst,  and Marie-Christine Counilh.
**SOCL: An OpenCL Implementation with Automatic Multi-Device Adaptation Support**.
Research Report RR-8346, INRIA, August 2013.
<a title="Web link" href="https://inria.hal.science/hal-00853423" target="_blank" onclick="return trackOutboundLink('https://inria.hal.science/hal-00853423');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://inria.hal.science/hal-00853423/file/RR-8346.pdf" target="_blank" onclick="return trackOutboundLink('https://inria.hal.science/hal-00853423/file/RR-8346.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>




### Year 2012 


- <a name="HenDenBar2012TSI"></a>
Sylvain Henry, Alexandre Denis,  and Denis Barthou.
**Programmation unifiée multi-accélérateur OpenCL**.
*Techniques et Sciences Informatiques*, (8-9-10):1233-1249, 2012.
<a title="Web link" href="http://hal.inria.fr/hal-00772742" target="_blank" onclick="return trackOutboundLink('http://hal.inria.fr/hal-00772742');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="http://hal.inria.fr/hal-00772742/document" target="_blank" onclick="return trackOutboundLink('http://hal.inria.fr/hal-00772742/document');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>




- <a name="MahManAugThi12TSI"></a>
Sidi Ahmed Mahmoudi, Pierre Manneback, Cédric Augonnet,  and Samuel Thibault.
**Traitements d'Images sur Architectures Parallèles et Hétérogènes**.
*Technique et Science Informatiques*, 31(8-10):1183-1203, 2012.
<a title="Web link" href="http://hal.inria.fr/hal-00714858/" target="_blank" onclick="return trackOutboundLink('http://hal.inria.fr/hal-00714858/');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://hal.inria.fr/hal-00714858/document" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-00714858/document');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[doi:<a href="http://dx.doi.org/10.3166/tsi.31.1183-1203">10.3166/tsi.31.1183-1203</a>]




- <a name="BenkBajMarSanNamThiEuroPar2012"></a>
Siegfried Benkner, Enes Bajrovic, Erich Marth, Martin Sandrieser, Raymond Namyst,  and Samuel Thibault.
**High-Level Support for Pipeline Parallelism on Many-Core Architectures**.
In *Euro-Par - 18th International Conference on Parallel Processing*, Rhodes Island, Greece, August 2012.
<a title="Web link" href="http://hal.inria.fr/hal-00697020" target="_blank" onclick="return trackOutboundLink('http://hal.inria.fr/hal-00697020');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="http://hal.inria.fr/hal-00697020/PDF/europar2012-submitted.pdf" target="_blank" onclick="return trackOutboundLink('http://hal.inria.fr/hal-00697020/PDF/europar2012-submitted.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[doi:<a href="http://dx.doi.org/10.1007/978-3-642-32820-6_61">10.1007/978-3-642-32820-6_61</a>]




- <a name="bordage:hal-00773114"></a>
Cyril Bordage.
**Parallelization on Heterogeneous Multicore and Multi-GPU Systems of the Fast Multipole Method for the Helmholtz Equation Using a Runtime System**.
In *ADVCIMP12*, ADVCOMP 2012, The Sixth International Conference on Advanced Engineering Computing and Applications in Sciences, Barcelone, Spain, pages 90-95, September 2012.
IARIA.
<a title="Web link" href="https://inria.hal.science/hal-00773114" target="_blank" onclick="return trackOutboundLink('https://inria.hal.science/hal-00773114');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;




- <a name="kessler:hal-00776610"></a>
Christoph Kessler, Usman Dastgeer, Samuel Thibault, Raymond Namyst, Andrew Richards, Uwe Dolinsky, Siegfried Benkner, Jesper Larsson Träff,  and Sabri Pllana.
**Programmability and Performance Portability Aspects of Heterogeneous Multi-/Manycore Systems**.
In *DATE - Design, Automation and Test in Europe*, Dresden, Deutschland, March 2012.
**ISBN:** 978-3-9810801-8-6.
<a title="Web link" href="http://hal.inria.fr/hal-00776610" target="_blank" onclick="return trackOutboundLink('http://hal.inria.fr/hal-00776610');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="http://hal.inria.fr/hal-00776610/PDF/date12-paper.pdf" target="_blank" onclick="return trackOutboundLink('http://hal.inria.fr/hal-00776610/PDF/date12-paper.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[doi:<a href="http://dx.doi.org/10.1109/DATE.2012.6176582">10.1109/DATE.2012.6176582</a>]




### Year 2011 


- <a name="BenPllTraTsiDolAugBacKesMolOsi11IEEEMicro"></a>
Siegfried Benkner, Sabri Pllana, Jesper Larsson Träff, Philippas Tsigas, Uwe Dolinsky, Cédric Augonnet, Beverly Bachmayer, Christoph Kessler, David Moloney,  and Vitaly Osipov.
**PEPPHER: Efficient and Productive Usage of Hybrid Computing Systems**.
*IEEE Micro*, 31(5):28-41, September 2011.
**ISSN:** 0272-1732.
<a title="Web link" href="http://hal.inria.fr/hal-00648480" target="_blank" onclick="return trackOutboundLink('http://hal.inria.fr/hal-00648480');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="http://hal.inria.fr/hal-00648480/document" target="_blank" onclick="return trackOutboundLink('http://hal.inria.fr/hal-00648480/document');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[doi:<a href="http://dx.doi.org/10.1109/MM.2011.67">10.1109/MM.2011.67</a>]




- <a name="AguAugDonFavLanLtaTomAICCSA11"></a>
Emmanuel Agullo, Cédric Augonnet, Jack Dongarra, Mathieu Faverge, Julien Langou, Hatem Ltaief,  and Stanimire Tomov.
**LU factorization for accelerator-based systems**.
In *9th ACS/IEEE International Conference on Computer Systems and Applications (AICCSA 11)*, Sharm El-Sheikh, Egypt, June 2011.
<a title="Web link" href="http://hal.inria.fr/hal-00654193" target="_blank" onclick="return trackOutboundLink('http://hal.inria.fr/hal-00654193');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="http://hal.inria.fr/hal-00654193/document" target="_blank" onclick="return trackOutboundLink('http://hal.inria.fr/hal-00654193/document');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[doi:<a href="http://dx.doi.org/10.1109/AICCSA.2011.6126599">10.1109/AICCSA.2011.6126599</a>]




- <a name="AguAugDonFavLtaThiTom11IPDPS"></a>
Emmanuel Agullo, Cédric Augonnet, Jack Dongarra, Mathieu Faverge, Hatem Ltaief, Samuel Thibault,  and Stanimire Tomov.
**QR Factorization on a Multicore Node Enhanced with Multiple GPU Accelerators**.
In *25th IEEE International Parallel & Distributed Processing Symposium (IEEE IPDPS 2011)*, Anchorage, Alaska, USA, May 2011.
<a title="Web link" href="http://hal.inria.fr/inria-00547614" target="_blank" onclick="return trackOutboundLink('http://hal.inria.fr/inria-00547614');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="http://hal.inria.fr/inria-00547614/document" target="_blank" onclick="return trackOutboundLink('http://hal.inria.fr/inria-00547614/document');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[doi:<a href="http://dx.doi.org/10.1109/IPDPS.2011.90">10.1109/IPDPS.2011.90</a>]




- <a name="benkner:hal-00661320"></a>
Siegfried Benkner, Sabri Pllana, Jesper Larsson Träff, Philippas Tsigas, Andrew Richards, Raymond Namyst, Beverly Bachmayer, Christoph Kessler, David Moloney,  and Peter Sanders.
**The PEPPHER Approach to Programmability and Performance Portability for Heterogeneous many-core Architectures**.
In *ParCo*, Ghent, Belgium, August 2011.
<a title="Web link" href="https://inria.hal.science/hal-00661320" target="_blank" onclick="return trackOutboundLink('https://inria.hal.science/hal-00661320');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://inria.hal.science/hal-00661320/file/peppher_parco_2011.pdf" target="_blank" onclick="return trackOutboundLink('https://inria.hal.science/hal-00661320/file/peppher_parco_2011.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>




- <a name="DasKesThi11ParCo"></a>
Usman Dastgeer, Christoph Kessler,  and Samuel Thibault.
**Flexible runtime support for efficient skeleton programming on hybrid systems**.
In *ParCo - Proceedings of the International Conference on Parallel Computing*, volume 22 of *Advances of Parallel Computing*, Gent, Belgium, pages 159-166, August 2011.
<a title="Web link" href="http://hal.inria.fr/inria-00606200/" target="_blank" onclick="return trackOutboundLink('http://hal.inria.fr/inria-00606200/');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.699.6216&rep=rep1&type=pdf" target="_blank" onclick="return trackOutboundLink('http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.699.6216&rep=rep1&type=pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[doi:<a href="http://dx.doi.org/10.3233/978-1-61499-041-3-159">10.3233/978-1-61499-041-3-159</a>]




- <a name="Hen11Renpar"></a>
Sylvain Henry.
**Programmation multi-accélérateurs unifiée en OpenCL**.
In *20èmes Rencontres Francophones du Parallélisme (RenPar'20)*, Saint Malo, France, May 2011.
<a title="Web link" href="http://hal.archives-ouvertes.fr/hal-00643257" target="_blank" onclick="return trackOutboundLink('http://hal.archives-ouvertes.fr/hal-00643257');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="http://hal.archives-ouvertes.fr/hal-00643257/document" target="_blank" onclick="return trackOutboundLink('http://hal.archives-ouvertes.fr/hal-00643257/document');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>




- <a name="MahManAugThi11Renpar20"></a>
Sidi Ahmed Mahmoudi, Pierre Manneback, Cédric Augonnet,  and Samuel Thibault.
**Détection optimale des coins et contours dans des bases d'images volumineuses sur architectures multicoeurs hétérogènes**.
In *RenPar'20 - 20èmes Rencontres Francophones du Parallélisme*, Saint-Malo, France, May 2011.
<a title="Web link" href="http://hal.inria.fr/inria-00606195" target="_blank" onclick="return trackOutboundLink('http://hal.inria.fr/inria-00606195');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://hal.inria.fr/inria-00606195/document" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/inria-00606195/document');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>




### Year 2010 


- <a name="AguAugDonLtaNamThiTomGPUgems"></a>
Emmanuel Agullo, Cédric Augonnet, Jack Dongarra, Hatem Ltaief, Raymond Namyst, Samuel Thibault,  and Stanimire Tomov.
**A Hybridization Methodology for High-Performance Linear Algebra Software for GPUs**.
In Wen-mei W. Hwu, editor, *GPU Computing Gems*, volume 2.
Morgan Kaufmann, September 2010.
<a title="Web link" href="http://hal.inria.fr/inria-00547847" target="_blank" onclick="return trackOutboundLink('http://hal.inria.fr/inria-00547847');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="http://hal.inria.fr/inria-00547847/document" target="_blank" onclick="return trackOutboundLink('http://hal.inria.fr/inria-00547847/document');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[doi:<a href="http://dx.doi.org/10.1016/B978-0-12-385963-1.00034-4">10.1016/B978-0-12-385963-1.00034-4</a>]




- <a name="AguAugDonLtaNamRomThiTom10SAAHPC"></a>
Emmanuel Agullo, Cédric Augonnet, Jack Dongarra, Hatem Ltaief, Raymond Namyst, Jean Roman, Samuel Thibault,  and Stanimire Tomov.
**Dynamically scheduled Cholesky factorization on multicore architectures with GPU accelerators**.
In *SAAHPC - Symposium on Application Accelerators in High Performance Computing*, Knoxville, USA, July 2010.
<a title="Web link" href="http://hal.inria.fr/inria-00547616" target="_blank" onclick="return trackOutboundLink('http://hal.inria.fr/inria-00547616');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="http://hal.inria.fr/inria-00547616/document" target="_blank" onclick="return trackOutboundLink('http://hal.inria.fr/inria-00547616/document');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>







---
layout: default
title: On Recursive Tasks
permalink: publications/Keyword/ON-RECURSIVE-TASKS.html
full-width: True
---

## On Recursive Tasks
### Year 2024 


- <a name="furmento:hal-04548787"></a>
Nathalie Furmento, Abdou Guermouche, Gwenolé Lucas, Thomas Morin, Samuel Thibault,  and Pierre-André Wacrenier.
**Optimizing Parallel System Efficiency: Dynamic Task Graph Adaptation with Recursive Tasks**.
In *WAMTA 2024 - Workshop on Asynchronous Many-Task Systems and Applications 2024*, Knoxville, United States, February 2024.
https://wamta24.icl.utk.edu/.
<a title="Web link" href="https://inria.hal.science/hal-04548787" target="_blank" onclick="return trackOutboundLink('https://inria.hal.science/hal-04548787');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://inria.hal.science/hal-04548787/file/wamta24.pdf" target="_blank" onclick="return trackOutboundLink('https://inria.hal.science/hal-04548787/file/wamta24.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>




### Year 2023 


- <a name="lucas:tel-04316145"></a>
Gwenolé Lucas.
**On the Use of Hierarchical Task for Heterogeneous Architectures**.
Ph.D Thesis, Université de Bordeaux, October 2023.
<a title="Web link" href="https://theses.hal.science/tel-04316145" target="_blank" onclick="return trackOutboundLink('https://theses.hal.science/tel-04316145');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://theses.hal.science/tel-04316145/file/LUCAS_GWENOLE_2023.pdf" target="_blank" onclick="return trackOutboundLink('https://theses.hal.science/tel-04316145/file/LUCAS_GWENOLE_2023.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>




- <a name="faverge:hal-04088833"></a>
Mathieu Faverge, Nathalie Furmento, Abdou Guermouche, Gwenolé Lucas, Raymond Namyst, Samuel Thibault,  and Pierre-André Wacrenier.
**Programming Heterogeneous Architectures Using Hierarchical Tasks**.
*Concurrency and Computation: Practice and Experience*, 2023.
<a title="Web link" href="https://hal.science/hal-04088833" target="_blank" onclick="return trackOutboundLink('https://hal.science/hal-04088833');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://hal.science/hal-04088833v1/file/CCPEpaper.pdf" target="_blank" onclick="return trackOutboundLink('https://hal.science/hal-04088833v1/file/CCPEpaper.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[doi:<a href="http://dx.doi.org/10.1002/cpe.7811">10.1002/cpe.7811</a>]




- <a name="thibault:hal-04115280"></a>
Samuel Thibault.
**Vector operations, tiled operations, distributed execution, task graphs, ... What next ?**.
In *15th Joint Laboratory for Extreme Scale Computing (JLESC) Workshop*, Talence, France, March 2023.
<a title="Web link" href="https://inria.hal.science/hal-04115280" target="_blank" onclick="return trackOutboundLink('https://inria.hal.science/hal-04115280');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://inria.hal.science/hal-04115280/file/23-03-21-jlesc.pdf" target="_blank" onclick="return trackOutboundLink('https://inria.hal.science/hal-04115280/file/23-03-21-jlesc.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>




### Year 2022 


- <a name="faverge:hal-03789625"></a>
Mathieu Faverge, Nathalie Furmento, Abdou Guermouche, Gwenolé Lucas, Raymond Namyst, Samuel Thibault,  and Pierre-André Wacrenier.
**Programming Heterogeneous Architectures Using Hierarchical Tasks**.
In *HeteroPar 2022*, Glasgow, United Kingdom, pages 12, August 2022.
<a title="Web link" href="https://hal.science/hal-03789625" target="_blank" onclick="return trackOutboundLink('https://hal.science/hal-03789625');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://hal.science/hal-03789625/file/paper%20%281%29.pdf" target="_blank" onclick="return trackOutboundLink('https://hal.science/hal-03789625/file/paper%20%281%29.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[doi:<a href="http://dx.doi.org/10.1007/978-3-031-31209-0_7">10.1007/978-3-031-31209-0_7</a>]




- <a name="faverge:hal-03773486"></a>
Mathieu Faverge, Nathalie Furmento, Abdou Guermouche, Gwenolé Lucas, Samuel Thibault,  and Pierre-André Wacrenier.
**Programmation des architectures hétérogènes à l'aide de tâches hiérarchiques**.
In *COMPAS 2022 - Conférence francophone d'informatique en Parallélisme, Architecture et Système*, Amiens, France, July 2022.
<a title="Web link" href="https://hal.inria.fr/hal-03773486" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-03773486');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://hal.inria.fr/hal-03773486/file/ComPAS2022_paper_10.pdf" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-03773486/file/ComPAS2022_paper_10.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>




- <a name="faverge:hal-03609275"></a>
Mathieu Faverge, Nathalie Furmento, Gwenolé Lucas, Abdou Guermouche, Raymond Namyst, Samuel Thibault,  and Pierre-André Wacrenier.
**Programming Heterogeneous Architectures Using Hierarchical Tasks**.
Research Report RR-9466, Inria Bordeaux Sud-Ouest, March 2022.
<a title="Web link" href="https://hal.inria.fr/hal-03609275" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-03609275');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://hal.inria.fr/hal-03609275/file/RR-9466.pdf" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-03609275/file/RR-9466.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>




### Year 2017 


- <a name="chevalier:hal-01718280"></a>
Arthur Chevalier.
**Critical resources management and scheduling under StarPU**.
Master Thesis, Université de Bordeaux, September 2017.
<a title="Web link" href="https://hal.inria.fr/hal-01718280" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-01718280');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://hal.inria.fr/hal-01718280/file/Memoire.pdf" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-01718280/file/Memoire.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>




### Year 2016 


- <a name="cojean:hal-01410103"></a>
Terry Cojean.
**The StarPU Runtime System at Exascale ?**.
In *RESPA workshop at SC16*, Salt Lake City, Utah, United States, November 2016.
<a title="Web link" href="https://inria.hal.science/hal-01410103" target="_blank" onclick="return trackOutboundLink('https://inria.hal.science/hal-01410103');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;







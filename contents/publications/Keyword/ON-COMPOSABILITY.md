---
layout: default
title: On Composability
permalink: publications/Keyword/ON-COMPOSABILITY.html
full-width: True
---

## On Composability
### Year 2014 


- <a name="hugo:tel-01162975"></a>
Andra-Ecaterina Hugo.
**Composability of parallel codes on heterogeneous architectures**.
Ph.D Thesis, Université de Bordeaux, December 2014.
<a title="Web link" href="https://tel.archives-ouvertes.fr/tel-01162975" target="_blank" onclick="return trackOutboundLink('https://tel.archives-ouvertes.fr/tel-01162975');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://tel.archives-ouvertes.fr/tel-01162975/file/HUGO_ANDRA_2014.pdf" target="_blank" onclick="return trackOutboundLink('https://tel.archives-ouvertes.fr/tel-01162975/file/HUGO_ANDRA_2014.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>




- <a name="hugo:hal-01101045"></a>
Andra Hugo, Abdou Guermouche, Pierre-André Wacrenier,  and Raymond Namyst.
**Composing multiple StarPU applications over heterogeneous machines: A supervised approach**.
*International Journal of High Performance Computing Applications*, 28:285 - 300, February 2014.
<a title="Web link" href="https://inria.hal.science/hal-01101045" target="_blank" onclick="return trackOutboundLink('https://inria.hal.science/hal-01101045');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://inria.hal.science/hal-01101045/file/article.pdf" target="_blank" onclick="return trackOutboundLink('https://inria.hal.science/hal-01101045/file/article.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[doi:<a href="http://dx.doi.org/10.1177/1094342014527575">10.1177/1094342014527575</a>]




- <a name="hugo:hal-01101054"></a>
Andra Hugo, Abdou Guermouche, Pierre.-André Wacrenier,  and Raymond Namyst.
**A runtime approach to dynamic resource allocation for sparse direct solvers**.
In *43rd International Conference on Parallel Processing*, Minneapolis, United States, September 2014.
<a title="Web link" href="https://inria.hal.science/hal-01101054" target="_blank" onclick="return trackOutboundLink('https://inria.hal.science/hal-01101054');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://inria.hal.science/hal-01101054/file/AHugo.pdf" target="_blank" onclick="return trackOutboundLink('https://inria.hal.science/hal-01101054/file/AHugo.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[doi:<a href="http://dx.doi.org/10.1109/ICPP.2014.57">10.1109/ICPP.2014.57</a>]




### Year 2013 


- <a name="AH13Renpar"></a>
Andra Hugo.
**Le problème de la composition parallèle : une approche supervisée**.
In *21èmes Rencontres Francophones du Parallélisme (RenPar'21)*, Grenoble, France, January 2013.
<a title="Web link" href="http://hal.inria.fr/hal-00773610" target="_blank" onclick="return trackOutboundLink('http://hal.inria.fr/hal-00773610');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="http://hal.inria.fr/hal-00773610/document" target="_blank" onclick="return trackOutboundLink('http://hal.inria.fr/hal-00773610/document');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>




- <a name="hugo:hal-00824514"></a>
Andra Hugo, Abdou Guermouche, Raymond Namyst,  and Pierre-André Wacrenier.
**Composing multiple StarPU applications over heterogeneous machines: a supervised approach**.
In *Third International Workshop on Accelerators and Hybrid Exascale Systems*, Boston, USA, May 2013.
<a title="Web link" href="http://hal.inria.fr/hal-00824514" target="_blank" onclick="return trackOutboundLink('http://hal.inria.fr/hal-00824514');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="http://hal.inria.fr/hal-00824514/document" target="_blank" onclick="return trackOutboundLink('http://hal.inria.fr/hal-00824514/document');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[doi:<a href="http://dx.doi.org/10.1177/1094342014527575">10.1177/1094342014527575</a>]




### Year 2011 


- <a name="AH11Master"></a>
Andra Hugo.
**Composabilité de codes parallèles sur architectures hétérogènes**.
Master Thesis, Université de Bordeaux, June 2011.
<a title="Web link" href="http://hal.inria.fr/inria-00619654/en/" target="_blank" onclick="return trackOutboundLink('http://hal.inria.fr/inria-00619654/en/');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="http://hal.inria.fr/inria-00619654/document" target="_blank" onclick="return trackOutboundLink('http://hal.inria.fr/inria-00619654/document');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>







---
layout: default
title: On Memory Control
permalink: publications/Keyword/ON-MEMORY-CONTROL.html
full-width: True
---

## On Memory Control
### Year 2017 


- <a name="chevalier:hal-01718280"></a>
Arthur Chevalier.
**Critical resources management and scheduling under StarPU**.
Master Thesis, Université de Bordeaux, September 2017.
<a title="Web link" href="https://hal.inria.fr/hal-01718280" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-01718280');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://hal.inria.fr/hal-01718280/file/Memoire.pdf" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-01718280/file/Memoire.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>




### Year 2016 


- <a name="sergent:hal-01284004"></a>
Marc Sergent, David Goudin, Samuel Thibault,  and Olivier Aumage.
**Controlling the Memory Subscription of Distributed Applications with a Task-Based Runtime System**.
In *HIPS - 21st International Workshop on High-Level Parallel Programming Models and Supportive Environments*, Chicago, USA, May 2016.
<a title="Web link" href="https://hal.inria.fr/hal-01284004" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-01284004');"><i class="fa fa-globe" aria-hidden="true"></i></a>&nbsp;
<a title="PDF" href="https://hal.inria.fr/hal-01284004/file/PID4127657.pdf" target="_blank" onclick="return trackOutboundLink('https://hal.inria.fr/hal-01284004/file/PID4127657.pdf');"><i class="fas fa-file-pdf" aria-hidden="true"></i></a>
[doi:<a href="http://dx.doi.org/10.1109/IPDPSW.2016.105">10.1109/IPDPSW.2016.105</a>]







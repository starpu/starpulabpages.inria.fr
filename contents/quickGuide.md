---
layout: default
title: Quick Start Guide
permalink: quickGuide.html
full-width: True
---

# Quick Start Guide

## Using the StarPU sources

You can easily try StarPU on the Cholesky factorization, for
instance. Make sure to have `pkg-config`, [hwloc](http://www.open-mpi.org/projects/hwloc/) for proper CPU
control, as well as BLAS kernels installed and configured in your
environment for your computation units (e.g. MKL for CPUs and CUBLAS
for GPUs).

```sh
someversion=1.4.2 # for example
wget http://files.inria.fr/starpu/starpu-$someversion/starpu-${someversion}.tar.gz
tar xf starpu-${someversion}.tar.gz
cd starpu-${someversion}
mkdir build && cd build
../configure
make -j 12
STARPU_SCHED=dmdas ./examples/cholesky/cholesky_implicit -size $((960*40)) -nblocks 40
STARPU_SCHED=dmdas mpirun -np 4 -machinefile mymachines ./mpi/examples/matrix_decomposition/mpi_cholesky_distributed -size $((960*40*4)) -nblocks $((40*4))
```

Note that the dmdas scheduler uses performance models, and thus needs
calibration execution before exhibiting optimized performance (until the `model
something is not calibrated enough` messages go away).

To get a glimpse at what happened, you can get an execution trace by
installing [FxT](http://savannah.nongnu.org/projects/fkt) and [ViTE](http://vite.gforge.inria.fr/), and enabling traces:

``` sh
../configure --with-fxt
make -j 12
STARPU_FXT_TRACE=1 STARPU_SCHED=dmdas ./examples/cholesky/cholesky_implicit -size $((960*40)) -nblocks 40
./tools/starpu_fxt_tool -i /tmp/prof_file_${USER}_0
vite paje.trace
```

Starting with StarPU 1.1, it is also possible to reproduce the performance that
we show in our articles on our machines, by installing SimGrid, and then using
the simulation mode of StarPU using the performance models of our machines:

``` sh
../configure --enable-simgrid
make -j 12
STARPU_PERF_MODEL_DIR=$PWD/../tools/perfmodels/sampling STARPU_HOSTNAME=mirage STARPU_SCHED=dmdas ./examples/cholesky/cholesky_implicit -size $((960*40)) -nblocks 40
# size	ms	GFlops
38400	9915	1903.7
```

(MPI simulation is not supported yet)

## Using the StarPU docker image
Docker images for StarPU are available from `registry.gitlab.inria.fr/starpu/docker/starpu:XXX`.

Note that these images provide NVIDIA support for GPU devices. You
will find more informations on
<https://gitlab.inria.fr/starpu/starpu-docker>

Here is an example on how to use these images. Images for the master
version of StarPU and the latest stable releases are installed. The
list of tags is available at
<https://gitlab.inria.fr/starpu/starpu-docker/container_registry/2057>

``` sh
docker run -it registry.gitlab.inria.fr/starpu/starpu-docker/starpu:1.4.7
...
gitlab@42c20306e808:~$ starpu_machine_display
[starpu][42c20306e808][check_bus_config_file] No performance model for the bus, calibrating...
[starpu][42c20306e808][check_bus_config_file] ... done
Real hostname: 42c20306e808 (StarPU hostname: 42c20306e808)
StarPU has found :
...
gitlab@42c20306e808:~$ cd /usr/local/lib/starpu/examples
gitlab@42c20306e808:/usr/local/lib/starpu/examples$ ./vector_scal
[BEFORE] 1-th element    : 2.00
[BEFORE] (NX-1)th element: 204800.00
[AFTER] 1-th element     : 6.28 (should be 6.28)
[AFTER] (NX-1)-th element: 643072.00 (should be 643072.00)
[AFTER] Computation is correct
gitlab@42c20306e808:/usr/local/lib/starpu/examples$
```

## Using StarPU on national clusters
If you are using the Jean Zay supercomputer, StarPU is already
installed as a module. See [this page](http://www.idris.fr/jean-zay/cpu/jean-zay-cpu-starpu.html) to find out how to use it.


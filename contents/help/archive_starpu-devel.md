---
layout: default
title: Getting Help. Local archive for list starpu-devel
permalink: help/archive_starpu-devel.html
---
# StarPU: Getting Help. Local archive for list starpu-devel

##  [BUG REPORT] Incorrect result when partitioning temporary data asynchronously 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2024-05/msg00002.html>
##  StarPU new release v1.4.6 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2024-05/msg00001.html>
##  [StarPU for finite-difference method: shared/distributed/GPU parallelism] 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2024-05/msg00000.html>
##  StarPU new release v1.4.5 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2024-04/msg00000.html>
##  [StarPU] Linking problem when using out-of-core functions with g++ 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2024-03/msg00004.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2024-03/msg00005.html>
##  Profiling Tool 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2024-03/msg00000.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2024-03/msg00001.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2024-03/msg00002.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2024-03/msg00003.html>
##  StarPU new release v1.4.4 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2024-02/msg00000.html>
##  StarPU new release v1.4.3 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2024-01/msg00000.html>
##  Creating thread partitions 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2023-11/msg00020.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2023-11/msg00022.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2023-11/msg00023.html>
##  Creating tasks that are dependent on resources 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2023-11/msg00019.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2023-11/msg00021.html>
##  StarPU new release v1.4.2 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2023-11/msg00018.html>
##  Subject: Feedback on ImportError when importing  quot;starpu quot; in Python 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2023-11/msg00000.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2023-11/msg00001.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2023-11/msg00002.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2023-11/msg00003.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2023-11/msg00004.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2023-11/msg00005.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2023-11/msg00006.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2023-11/msg00007.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2023-11/msg00008.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2023-11/msg00009.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2023-11/msg00010.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2023-11/msg00011.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2023-11/msg00012.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2023-11/msg00013.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2023-11/msg00014.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2023-11/msg00015.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2023-11/msg00016.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2023-11/msg00017.html>
##  Questions about StarPU communication backends 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2023-10/msg00000.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2023-10/msg00001.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2023-10/msg00002.html>
##  Souci de starpu_value 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2023-08/msg00002.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2023-08/msg00004.html>
##  Sequential execution 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2023-07/msg00006.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2023-07/msg00007.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2023-07/msg00008.html>
##  Customization/Optimization for Scheduling Methods 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2023-07/msg00002.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2023-07/msg00004.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2023-07/msg00005.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2023-08/msg00000.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2023-08/msg00003.html>
##  Warning when running the code 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2023-07/msg00001.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2023-07/msg00003.html>
##  Question regarding Starpu 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2023-07/msg00000.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2023-08/msg00001.html>
##  StarPU v1.3.11 available 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2023-06/msg00000.html>
##  StarPU new release v1.4.1 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2023-05/msg00000.html>
##  StarPU new release v1.4.0 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2023-03/msg00000.html>
##  Corrupted large ooc files with unistd backend 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2022-12/msg00000.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2022-12/msg00001.html>
##  Mix two solvers on top of StarPU 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2022-10/msg00008.html>
##  Combiner deux solveurs à base de StarPU 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2022-10/msg00007.html>
##  mettre à jour votre compte 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2022-10/msg00003.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2022-10/msg00006.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2022-11/msg00000.html>
##  Questions sur Starpu 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2022-10/msg00001.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2022-10/msg00002.html>
##  bug ndim GPU 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2022-10/msg00000.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2022-10/msg00004.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2022-10/msg00005.html>
##  segfault in macox / windows 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2022-09/msg00004.html>
##  StarPU and arm64 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2022-09/msg00002.html>
##  Tester StarPU dans Julia 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2022-09/msg00000.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2022-09/msg00001.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2022-09/msg00003.html>
##  Trouble with OpenCL+MacOS+STARPU+GCC+MPI 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2022-08/msg00000.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2022-08/msg00001.html>
##  Issues while using of hypervisors 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2022-07/msg00000.html>
##  starpu_data_unregister_submit free non-NULL home buffer 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2022-04/msg00000.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2022-04/msg00001.html>
##  Using OpenACC to write GPU-accelerated kernels 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2022-03/msg00000.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2022-03/msg00001.html>
##  [Starpu-devel] StarPU and mingw64 error 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2022-02/msg00007.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2022-02/msg00008.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2022-02/msg00009.html>
##  Suspicious behavior of StarPU dmdar scheduler 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2022-02/msg00006.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2022-02/msg00010.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2022-02/msg00011.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2022-04/msg00002.html>
##  Get / access value of a variable attached to a StarPU handle 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2022-02/msg00000.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2022-02/msg00001.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2022-02/msg00002.html>
##  Port StarPU onto Graphcore IPUs 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2022-01/msg00000.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2022-01/msg00001.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2022-01/msg00002.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2022-01/msg00003.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2022-02/msg00012.html>
##  Regression in 1.3.9.: Codelet executed on disabled worker 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2021-12/msg00005.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2022-02/msg00003.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2022-02/msg00004.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2022-02/msg00005.html>
##  StarPU v1.3.9 available 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2021-10/msg00001.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2022-11/msg00001.html>
##  Problem in running task size overhead benchmark in starpu 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2021-07/msg00002.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2021-07/msg00003.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2021-07/msg00004.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2021-07/msg00005.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2021-07/msg00006.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2021-07/msg00007.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2021-07/msg00008.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2021-07/msg00009.html>
##  TREX 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2021-07/msg00000.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2021-07/msg00001.html>
##  Questions about StarPU in Fortran90 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2021-06/msg00000.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2021-06/msg00001.html>
##  StarPU v1.3.8 available 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2021-05/msg00002.html>
##  Suspicious check in the cuda memory allocation routine 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2021-05/msg00000.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2021-05/msg00001.html>
##  How to visualize StarPU data? 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2021-02/msg00003.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2021-02/msg00004.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2021-02/msg00005.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2021-02/msg00006.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2021-02/msg00007.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2021-02/msg00008.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2021-02/msg00009.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2021-02/msg00010.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2021-02/msg00011.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2021-02/msg00012.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2021-02/msg00013.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2021-02/msg00014.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2021-02/msg00015.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2021-02/msg00016.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2021-02/msg00017.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2021-02/msg00018.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2021-02/msg00019.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2021-02/msg00020.html>
##  starpu_fxt_tool -o option bug/patch 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2021-02/msg00000.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2021-02/msg00001.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2021-02/msg00002.html>
##  starpu configure 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2021-01/msg00001.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2021-01/msg00002.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2021-01/msg00003.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2021-01/msg00004.html>
## Joint workshop on SkePU/ComPU, StarPU, FTI, LIKWID, Chameleon/PaStiX/Maphys, PSBLAS/AMG4PSBLAS 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2021-01/msg00000.html>
## Migration of StarPU mailing lists 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2020-12/msg00000.html>
##  External Inria account for creating issue in StarPU gitlab 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2020-11/msg00002.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2020-11/msg00003.html>
##  Online Performance Monitoring With Performance Models in Fortran StarPU 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2020-10/msg00004.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2020-10/msg00005.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2020-10/msg00006.html>
##  Problems with installing starpu 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2020-10/msg00002.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2020-10/msg00003.html>
##  StarPU v1.3.7 available 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2020-10/msg00001.html>
##  StarPU v1.3.6 available 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2020-10/msg00000.html>
##  StarPU as mingw64 packages 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2020-09/msg00017.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2020-09/msg00020.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2020-10/msg00007.html>
##  StarPU and mingw64 error 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2020-09/msg00012.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2020-09/msg00013.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2020-09/msg00014.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2020-09/msg00015.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2020-09/msg00016.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2020-09/msg00018.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2020-09/msg00019.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2020-11/msg00000.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2020-11/msg00001.html>
##  StarPU-SimGrid : option 'enable-maxcudadev' non prise en compte 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2020-09/msg00007.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2020-09/msg00008.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2020-09/msg00009.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2020-09/msg00010.html>
##  StarVZ available at CRAN 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2020-09/msg00005.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2020-09/msg00006.html>
##  StarPU v1.3.5 available 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2020-08/msg00006.html>
##  StarPU : impossible de compiler avec SimGrid, MPI et Fxt sur PlaFRIM 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2020-08/msg00000.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2020-08/msg00001.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2020-08/msg00002.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2020-08/msg00003.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2020-08/msg00004.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2020-08/msg00005.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2020-09/msg00000.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2020-09/msg00001.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2020-09/msg00002.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2020-09/msg00003.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2020-09/msg00004.html>
##  StarPU: Migration to gitlab 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2020-07/msg00001.html>
##  Task Bench paper accepted 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2020-06/msg00018.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2020-06/msg00019.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2020-07/msg00000.html>
##  StarPU v1.2.10 released 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2020-06/msg00017.html>
##  StarPU v1.3.4 available 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2020-06/msg00016.html>
##  Question regarding StarPU's coherency model 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2020-05/msg00014.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2020-05/msg00015.html>
##  Problem with performance model calibration 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2020-05/msg00006.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2020-05/msg00007.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2020-05/msg00008.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2020-05/msg00009.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2020-05/msg00010.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2020-05/msg00011.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2020-05/msg00012.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2020-05/msg00013.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2020-06/msg00000.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2020-06/msg00001.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2020-06/msg00002.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2020-06/msg00003.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2020-06/msg00004.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2020-06/msg00005.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2020-06/msg00006.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2020-06/msg00007.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2020-06/msg00008.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2020-06/msg00009.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2020-06/msg00010.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2020-06/msg00011.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2020-06/msg00012.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2020-06/msg00013.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2020-06/msg00014.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2020-06/msg00015.html>
##  starpu_perfmodel_recdump broken 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2020-04/msg00008.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2020-04/msg00009.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2020-05/msg00000.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2020-05/msg00001.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2020-05/msg00002.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2020-05/msg00003.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2020-05/msg00004.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2020-05/msg00005.html>
##  Why starpu_mpi_barrier waits for all taks ? 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2020-04/msg00004.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2020-04/msg00005.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2020-04/msg00006.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2020-04/msg00007.html>
##  Commit d069c41 breaks compilation with nmad 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2020-04/msg00002.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2020-04/msg00003.html>
##  A question regarding the STARPU_REDUX flag 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2020-04/msg00000.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2020-04/msg00001.html>
##  purge in starpu 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2020-03/msg00011.html>
##  starpu_perfmodel_recdump and STARPU_NCPU / STARPU_NCUDA 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2020-03/msg00010.html>
##  Issue with distributed NUMA-aware StarPU and dmda scheduler 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2020-03/msg00004.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2020-03/msg00005.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2020-03/msg00006.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2020-03/msg00007.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2020-03/msg00008.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2020-03/msg00009.html>
##  StarPU-MPI-Simgrid with heterogeneous nodes 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2020-03/msg00002.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2020-03/msg00003.html>
##  duplicated executions 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2020-03/msg00000.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2020-03/msg00001.html>
##  Build error when cblas.h not available 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2020-02/msg00021.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2020-02/msg00022.html>
##  BUG: Gantt Diagram generation 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2020-02/msg00018.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2020-02/msg00019.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2020-02/msg00020.html>
##  Main thread not bound 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2020-02/msg00016.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2020-02/msg00017.html>
##  rendundant task dependencies??? 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2020-02/msg00015.html>
##  Incoherence between starpu_mpi_init and starpu_mpi_init_conf 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2020-02/msg00009.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2020-02/msg00010.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2020-02/msg00011.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2020-02/msg00012.html>
##  Profiling Starpu with Vite 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2020-02/msg00003.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2020-02/msg00004.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2020-02/msg00007.html>
##  StarPU overhead for MPI 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2020-01/msg00009.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2020-02/msg00000.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2020-02/msg00001.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2020-02/msg00002.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2020-02/msg00005.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2020-02/msg00006.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2020-02/msg00008.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2020-02/msg00013.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2020-02/msg00014.html>
##  StarPU v1.2.9 released 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2020-01/msg00008.html>
##  Problem with partitioning matrix with starpu_matrix_filter_block 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2020-01/msg00000.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2020-01/msg00001.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2020-01/msg00002.html>
##  Data Distribution using Starpumpi 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-12/msg00010.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-12/msg00012.html>
##  infinite loop if HOME not defined 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-12/msg00007.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-12/msg00008.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2020-01/msg00003.html>
##  Implementation question regarding output type 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-12/msg00006.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-12/msg00009.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-12/msg00011.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-12/msg00013.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-12/msg00014.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-12/msg00015.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-12/msg00016.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-12/msg00017.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-12/msg00018.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2020-01/msg00007.html>
##  Nvidia GTX 1060 visible in CUDA but not in StarPU 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-12/msg00003.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-12/msg00004.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-12/msg00005.html>
##  Problem with CUDA on GTX 1060 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-12/msg00001.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-12/msg00002.html>
##  StarPU changeable sizes and StarPU purge 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-12/msg00000.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2020-01/msg00004.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2020-01/msg00005.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2020-01/msg00006.html>
##  StarPU mpi support installation 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-11/msg00003.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-11/msg00004.html>
##  StarPU installation pkg-config 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-11/msg00001.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-11/msg00002.html>
##  StarPU v1.3.3 available 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-10/msg00005.html>
##  starpu / guix on plafrim partition dedicated for the November HPC school 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-10/msg00002.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-10/msg00003.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-10/msg00004.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-10/msg00006.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-10/msg00007.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-10/msg00008.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-10/msg00009.html>
##  Running Cholesky_implicit on 2 gpu nodes 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-10/msg00000.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-10/msg00001.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-11/msg00000.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-11/msg00005.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-11/msg00006.html>
##  Bug report: STARPU_SPMD does not handle buffers without explicit backing memory 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-08/msg00002.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-08/msg00003.html>
##  StarPU multiple nodes in a cluster 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-08/msg00000.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-08/msg00001.html>
##  StarPU and 3d FFT 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-07/msg00002.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-07/msg00003.html>
##  Question: STARPU_SPMD and buffer sharing 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-07/msg00000.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-07/msg00001.html>
##  StarPU v1.3.2 available 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-06/msg00004.html>
##  Request help for StarPU-MPI data_handle copy behaviour. 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-06/msg00000.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-06/msg00001.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-06/msg00002.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-06/msg00003.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-06/msg00005.html>
##  StarPU v1.1.8 released 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-05/msg00008.html>
##  [exa2pro] StarPU installation 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-05/msg00000.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-05/msg00001.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-05/msg00002.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-05/msg00003.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-05/msg00004.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-05/msg00005.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-05/msg00006.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-05/msg00007.html>
##  Not stable performance model 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-04/msg00020.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-04/msg00021.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-04/msg00022.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-04/msg00023.html>
##  STARPU bug report 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-04/msg00011.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-04/msg00012.html>
##  StarPU feature suggestion on a collective MPI operation 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-04/msg00007.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-04/msg00008.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-04/msg00009.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-04/msg00010.html>
##  StarPU v1.3.1 released 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-04/msg00006.html>
##  Birthday 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-04/msg00004.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-04/msg00005.html>
##  Control data movement to/from device 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-03/msg00033.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-03/msg00034.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-04/msg00000.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-04/msg00001.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-04/msg00002.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-04/msg00003.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-04/msg00013.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-04/msg00014.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-04/msg00015.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-04/msg00016.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-04/msg00017.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-04/msg00018.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-04/msg00019.html>
##  StarPU v1.3.0 released 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-03/msg00030.html>
##  Error find_cpu_from_numa_node: Assertion `current' failed. 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-03/msg00024.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-03/msg00025.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-03/msg00026.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-03/msg00027.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-03/msg00028.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-03/msg00029.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-03/msg00031.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-03/msg00032.html>
##  Second release candidate for StarPU v1.3.0 available 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-03/msg00023.html>
##  environment variable for instrumenting the write back to disk 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-03/msg00017.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-03/msg00018.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-03/msg00019.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-03/msg00020.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-03/msg00021.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-03/msg00022.html>
##  Issues met during installing and configuration of StarPU 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-03/msg00014.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-03/msg00015.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-03/msg00016.html>
##  Chemin vers python3 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-03/msg00012.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-03/msg00013.html>
##  detect_combined_workers.h 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-03/msg00010.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-03/msg00011.html>
##  StarPU v1.2.8 released 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-02/msg00038.html>
##  Pb avec la release 1.2.7 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-02/msg00024.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-02/msg00025.html>
##  Question sur les data partition 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-02/msg00020.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-02/msg00021.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-02/msg00022.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-02/msg00023.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-02/msg00026.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-02/msg00027.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-02/msg00030.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-02/msg00031.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-02/msg00033.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-02/msg00034.html>
##  StarPU code hangs when using a lot of GPU memory 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-02/msg00019.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-02/msg00040.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-03/msg00000.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-03/msg00001.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-03/msg00002.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-03/msg00003.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-03/msg00004.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-03/msg00005.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-03/msg00006.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-03/msg00007.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-03/msg00008.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-03/msg00009.html>
##  StarPU v1.2.7 released 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-02/msg00018.html>
##  Data movement per kernel 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-02/msg00016.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-02/msg00035.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-02/msg00037.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-02/msg00039.html>
##  execution time takes forever with cuda+large matrices+ooc test case 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-02/msg00006.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-02/msg00009.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-02/msg00036.html>
##  hwloc CUDA plugin missing 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-02/msg00002.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-02/msg00003.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-02/msg00004.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-02/msg00005.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-02/msg00007.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-02/msg00008.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-02/msg00010.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-02/msg00011.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-02/msg00012.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-02/msg00013.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-02/msg00014.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-02/msg00015.html>
##  StarPU 1.3 RC1 + peager problem 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-02/msg00001.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-02/msg00028.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-02/msg00029.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-02/msg00032.html>
##  First release candidate for StarPU v1.3.0 available 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-02/msg00000.html>
##  A question regarding starpu_fxt_autostart_profiling function 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-01/msg00034.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-01/msg00035.html>
##  StarPU 1.3 does not purge GPU memory 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-01/msg00019.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-01/msg00020.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-01/msg00021.html>
##  Starpu stats about disk, OOC, and swapping 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-01/msg00001.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-01/msg00002.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-01/msg00003.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-01/msg00004.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-01/msg00005.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-01/msg00006.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-01/msg00007.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-01/msg00008.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-01/msg00011.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-01/msg00012.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-01/msg00013.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-01/msg00014.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-01/msg00015.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-01/msg00016.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-01/msg00017.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-01/msg00018.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-01/msg00022.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-01/msg00023.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-01/msg00024.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-01/msg00025.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-01/msg00026.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-01/msg00027.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-01/msg00028.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-01/msg00029.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-01/msg00030.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-01/msg00031.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-01/msg00032.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-01/msg00033.html>
##  starpu-print-all-tasks output 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-12/msg00000.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-01/msg00000.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-01/msg00009.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-01/msg00010.html>
##  Reliance on Debian-specific .pc file for OpenBLAS 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-11/msg00004.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-11/msg00005.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-11/msg00006.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-11/msg00007.html>
##  Problem with starpu_mpi_wait_for_all and starpu_mpi_barrier functions 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-10/msg00052.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-11/msg00000.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-11/msg00001.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-11/msg00002.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-11/msg00003.html>
##  LAPACK symbol conflict with StarPU 1.3 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-10/msg00046.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-10/msg00047.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-10/msg00048.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-10/msg00049.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-10/msg00050.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-10/msg00051.html>
##  Problem with inserting tasks to default scheduling context after a parallel scheduling context has been created 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-10/msg00045.html>
##  CPU core binding mask and worker creation. 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-10/msg00040.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-10/msg00041.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-10/msg00042.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-10/msg00043.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-10/msg00044.html>
##  courses on StarPU 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-10/msg00038.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-10/msg00039.html>
##  Support for half precision 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-10/msg00034.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-10/msg00035.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-10/msg00036.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-10/msg00037.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-01/msg00036.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-01/msg00037.html>
##  Processor grid and data distribution 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-10/msg00030.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-10/msg00032.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-10/msg00033.html>
##  Out of core limitations 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-10/msg00027.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-10/msg00028.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-10/msg00029.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-10/msg00031.html>
##  [daxpy with StarPU-MPI] 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-10/msg00018.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-10/msg00019.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-10/msg00020.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-10/msg00022.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-10/msg00023.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-10/msg00024.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-10/msg00025.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-10/msg00026.html>
##  Empty scheduling contexts 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-10/msg00017.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-10/msg00021.html>
##  [StarPU+MPI debuging] 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-10/msg00010.html>
##  Data target node + STARPU_VARIABLE_NBUFFERS 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-10/msg00007.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-10/msg00008.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-10/msg00009.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-10/msg00011.html>
##  [check data registered correctly, starpu_mpi_data_register] 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-10/msg00003.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-10/msg00006.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-10/msg00012.html>
##  Performance decline from StarPU 1.2.3 to 1.2.4 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-09/msg00032.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-09/msg00033.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-09/msg00034.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-09/msg00035.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-09/msg00036.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-09/msg00037.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-09/msg00038.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-09/msg00039.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-09/msg00040.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-09/msg00041.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-09/msg00042.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-10/msg00000.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-10/msg00001.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-10/msg00002.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-10/msg00004.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-10/msg00005.html>
##  A possible bug in StarPU 1.3.xx 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-09/msg00031.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-09/msg00043.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-09/msg00044.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-09/msg00045.html>
##  StarPU v1.2.6 released 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-09/msg00030.html>
##  Issue with lws 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-09/msg00012.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-09/msg00013.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-09/msg00014.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-09/msg00015.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-09/msg00016.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-09/msg00017.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-09/msg00018.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-09/msg00019.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-09/msg00020.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-09/msg00021.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-09/msg00022.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-09/msg00023.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-09/msg00024.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-09/msg00025.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-09/msg00026.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-09/msg00027.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-09/msg00028.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-09/msg00029.html>
##  [low performance of LU factorisation] 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-09/msg00010.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-09/msg00011.html>
##  [selective access mode setting] 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-09/msg00004.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-09/msg00005.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-09/msg00006.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-09/msg00007.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-09/msg00008.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-09/msg00009.html>
##  Possible bug (scratch data + combined workers + version 1.3) 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-09/msg00000.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-09/msg00001.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-09/msg00002.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-09/msg00003.html>
##  question on CPU/GPU tasks distribution 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-08/msg00011.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-08/msg00012.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-08/msg00013.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-08/msg00014.html>
##  About concurrent parallel tasks and OpenMP 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-08/msg00009.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-08/msg00010.html>
##  Overlapping scheduling contexts and scheduling order 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-08/msg00008.html>
##  Task start and end time in SPMD-mode 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-08/msg00005.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-08/msg00007.html>
##  Mixing starpu_task_insert() and starpu_mpi_task_insert() 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-08/msg00004.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-08/msg00006.html>
##  StarPU v1.2.5 released 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-08/msg00003.html>
##  [trace interpretation] 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-07/msg00019.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-07/msg00021.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-07/msg00024.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-07/msg00025.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-07/msg00026.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-07/msg00027.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-07/msg00028.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-08/msg00000.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-08/msg00001.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-08/msg00002.html>
##  data_unregister_submit() does not flush data from GPU to CPU 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-07/msg00018.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-07/msg00020.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-07/msg00022.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-07/msg00023.html>
##  [tracking invalid/unregistered data] 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-07/msg00015.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-07/msg00016.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-07/msg00017.html>
##  Questions sur StarPU 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-07/msg00014.html>
##  [printing out DAG] 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-07/msg00011.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-07/msg00013.html>
##  segfault in StarPU memory allocator 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-07/msg00001.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-07/msg00002.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-07/msg00012.html>
##  exposing the task color field to starpu-task-insert 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-06/msg00013.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-06/msg00014.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-06/msg00015.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-06/msg00016.html>
##  [subtasks++] 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-06/msg00012.html>
##  Dépendances dynamiques 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-06/msg00002.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-06/msg00003.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-06/msg00004.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-06/msg00005.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-06/msg00006.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-06/msg00007.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-06/msg00008.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-06/msg00009.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-06/msg00010.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-06/msg00011.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-06/msg00017.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-06/msg00018.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-07/msg00000.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-07/msg00003.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-07/msg00004.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-07/msg00005.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-07/msg00006.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-07/msg00007.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-07/msg00008.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-07/msg00009.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-07/msg00010.html>
##  StarPU - starpu_codelet_pack_arg doesn't exist anymore ? 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-06/msg00000.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-06/msg00001.html>
##  Possible bug in performance model file parsing 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-05/msg00010.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-05/msg00011.html>
##  Check fail in StarPU 1.2.4 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-05/msg00006.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-05/msg00007.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-05/msg00008.html>
##  Passing a structure to starpu insert task 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-05/msg00000.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-05/msg00001.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-05/msg00004.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-10/msg00013.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-10/msg00014.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-10/msg00015.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-10/msg00016.html>
##  Performance profiling of tile low rank cholesky (HiCMA) with StarPU on distributed memory systems 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-04/msg00007.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-04/msg00008.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-04/msg00009.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-04/msg00010.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-04/msg00011.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-04/msg00012.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-04/msg00013.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-04/msg00014.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-04/msg00015.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-04/msg00016.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-04/msg00017.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-04/msg00018.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-05/msg00002.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-05/msg00003.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-05/msg00005.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-05/msg00009.html>
##  Utilisation de StarPU dans un code C++ 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-04/msg00001.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-04/msg00002.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-04/msg00003.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-04/msg00004.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-04/msg00005.html>
##  removing gray nodes from a dot file 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-03/msg00027.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-03/msg00028.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-04/msg00000.html>
##  registering handles 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-03/msg00023.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-03/msg00024.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-03/msg00025.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-03/msg00026.html>
##  support for cudaMemcpyPeer 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-03/msg00015.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-03/msg00016.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-03/msg00017.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-03/msg00018.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-03/msg00019.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-03/msg00020.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-03/msg00021.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-03/msg00022.html>
##  segfault occurring with starpu 1.2.3 and master branch of chameleon 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-03/msg00011.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-03/msg00012.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-03/msg00013.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-03/msg00014.html>
##  Nouvelle politique d'ordonnancement 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-03/msg00006.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-03/msg00007.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-03/msg00008.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-03/msg00009.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-03/msg00010.html>
##  StarPU training course at Maison de la Simulation 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-03/msg00005.html>
##  Questionsur les dépendancesexplicites 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-03/msg00003.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-03/msg00004.html>
##  Question sur les dépendances explicites 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-03/msg00001.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-03/msg00002.html>
##  StarPU 1.2.3 - Serious bug in OpenCL 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-02/msg00011.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-02/msg00012.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-02/msg00013.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-02/msg00014.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-02/msg00015.html>
##  Fwd: [LU factorisation: gdb debug output] 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-02/msg00008.html>
##  [segfault debugging] 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-01/msg00010.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-01/msg00011.html>
##  [Passing an array to StarPU task] 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-01/msg00006.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-01/msg00007.html>
##  StarPU: From SVN to GIT 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-01/msg00005.html>
##  [LU factorisation: gdb debug output] 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-12/msg00008.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-12/msg00009.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-12/msg00010.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-12/msg00011.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-12/msg00012.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-12/msg00013.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-12/msg00014.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-12/msg00015.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-12/msg00016.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-12/msg00017.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-12/msg00018.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-12/msg00019.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-12/msg00020.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-12/msg00021.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-12/msg00022.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-12/msg00023.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-12/msg00024.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-12/msg00025.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-12/msg00026.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-12/msg00027.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-12/msg00028.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-12/msg00029.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-01/msg00000.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-01/msg00001.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-01/msg00002.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-01/msg00003.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-01/msg00004.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-01/msg00008.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-01/msg00009.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-02/msg00000.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-02/msg00001.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-02/msg00002.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-02/msg00003.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-02/msg00004.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-02/msg00005.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-02/msg00006.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-02/msg00007.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-02/msg00009.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-02/msg00010.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-02/msg00016.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-02/msg00017.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-02/msg00018.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-02/msg00019.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-02/msg00020.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-02/msg00021.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-03/msg00000.html>
##  implicit asynchronous partitioning landed in trunk! 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-12/msg00007.html>
##  [StarPU 1.2.3 installation] 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-12/msg00004.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-12/msg00006.html>
##  [starpu_matrix_interface: nx, ny] 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-12/msg00001.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-12/msg00002.html>
##  Interest in StarPU Users' Days 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-11/msg00012.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-11/msg00015.html>
##  [starpu_matrix_filter_block: obtain block sizes, asynchronous interface] 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-11/msg00010.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-11/msg00011.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-12/msg00000.html>
##  automatic RAM allocation and CUDA worker issue 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-11/msg00008.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-11/msg00014.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-12/msg00003.html>
##  data access 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-11/msg00006.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-11/msg00007.html>
##  StarPU v1.2.3 released 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-11/msg00003.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-11/msg00004.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2018-04/msg00006.html>
##  Question about sequential transfers in Starpu/SimGrid 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-11/msg00002.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-11/msg00005.html>
##  StarPU Fortran support 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-11/msg00000.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-11/msg00001.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-11/msg00009.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-11/msg00013.html>
##  StarPU cannot find GPUs on Plafrim 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-09/msg00031.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-09/msg00032.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-09/msg00033.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-09/msg00034.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-09/msg00035.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-09/msg00036.html>
##  StarPU install 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-09/msg00029.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-09/msg00030.html>
##  Performance issue of StarPU on Intel self-hosted KNL 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-09/msg00000.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-09/msg00001.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-09/msg00002.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-09/msg00003.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-09/msg00004.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-09/msg00005.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-09/msg00006.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-09/msg00007.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-09/msg00008.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-09/msg00009.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-09/msg00010.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-09/msg00011.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-09/msg00012.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-09/msg00013.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-09/msg00014.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-09/msg00015.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-09/msg00016.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-09/msg00017.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-09/msg00018.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-09/msg00019.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-09/msg00020.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-09/msg00021.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-09/msg00022.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-09/msg00023.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-09/msg00024.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-09/msg00025.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-09/msg00026.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-09/msg00027.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-09/msg00028.html>
##  A build error with SVN version 21927 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-08/msg00013.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-08/msg00014.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-08/msg00015.html>
##  StarPU: Compilation errors on a selfhoster KNL 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-08/msg00010.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-08/msg00011.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-08/msg00012.html>
##  StarPU with FxT is missing uthash.h while building tools 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-08/msg00007.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-08/msg00009.html>
##  Task re-execution with StarPU 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-08/msg00000.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-08/msg00001.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-08/msg00002.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-08/msg00003.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-08/msg00004.html>
##  StarPU task cancellation/resubmission 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-06/msg00025.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-06/msg00026.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-06/msg00027.html>
##  StarPU on IBM Power8 systems 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-06/msg00017.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-06/msg00018.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-06/msg00019.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-06/msg00020.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-08/msg00005.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-08/msg00008.html>
##  Different nodes are owning W data 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-06/msg00012.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-06/msg00013.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-06/msg00015.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-06/msg00021.html>
##  StarPU will cope by trying to purge 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-06/msg00011.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-06/msg00014.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-06/msg00016.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-06/msg00022.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-06/msg00023.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-06/msg00024.html>
##  [PATCH] examples audio fix 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-06/msg00008.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-06/msg00009.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-06/msg00010.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-08/msg00006.html>
##  [PATCH] examples audio fix - define nite as global variable because old scope was not reacheable by starpu_data_filter, cublas fix and use CFLAGS variable on Makefile. 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-06/msg00007.html>
##  Data partition 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-06/msg00004.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-06/msg00005.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-06/msg00006.html>
##  build problem 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-06/msg00000.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-06/msg00001.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-06/msg00002.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-06/msg00003.html>
##  How to cite StarPU properly 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-05/msg00012.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-05/msg00013.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-05/msg00014.html>
##  Overlapping communications in modular-heft 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-05/msg00011.html>
##  StarPU v1.2.2 released 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-05/msg00004.html>
##  Executing Cholesky with StarPU 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-05/msg00003.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-05/msg00006.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-05/msg00007.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-05/msg00008.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-05/msg00009.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-05/msg00010.html>
##  Debugging starPU application with gdb 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-05/msg00002.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-05/msg00005.html>
##  starPU writes to /usr/local 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-04/msg00030.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-05/msg00000.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-05/msg00001.html>
##  Problème mode Redux de StarPU 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-04/msg00017.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-04/msg00018.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-04/msg00019.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-04/msg00020.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-04/msg00021.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-04/msg00022.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-04/msg00023.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-04/msg00024.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-04/msg00025.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-04/msg00026.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-04/msg00027.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-04/msg00028.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-04/msg00029.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-04/msg00031.html>
##  StarPU support for out of core algorithms (storage) 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-04/msg00014.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-04/msg00015.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-04/msg00016.html>
##  Using StarPU Environment Variables 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-04/msg00012.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-04/msg00013.html>
##  Contexts and setting Scheduling Policies 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-04/msg00008.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-04/msg00009.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-04/msg00010.html>
##  Weird error with HWLOC 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-04/msg00003.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-04/msg00005.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-04/msg00006.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-04/msg00007.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-04/msg00011.html>
##  Sample sort on starPU 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-04/msg00000.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-04/msg00001.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-04/msg00002.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-04/msg00004.html>
##  starPU and GCC plugins not supported 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-03/msg00026.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-03/msg00028.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-03/msg00032.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-03/msg00035.html>
##  Moving Workers from One Context To Another in Real Time 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-03/msg00025.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-03/msg00027.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-03/msg00029.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-03/msg00030.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-03/msg00031.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-03/msg00033.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-03/msg00034.html>
##  How to Make a Task Prefer a Worker Inside a Context 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-03/msg00024.html>
##  Tracing chameleon/starpu with EZTrace/ViTE: what is the kernel  quot;C quot; in the tracing? 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-03/msg00013.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-03/msg00014.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-03/msg00015.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-03/msg00016.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-03/msg00017.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-03/msg00018.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-03/msg00019.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-03/msg00020.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-03/msg00021.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-03/msg00022.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-03/msg00023.html>
##  [StarPU insert task] 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-03/msg00006.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-03/msg00007.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-03/msg00008.html>
##  StarPU v1.1.7 released 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-03/msg00003.html>
##  Problem with Intel MPI 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-03/msg00002.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-03/msg00005.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-03/msg00009.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-03/msg00010.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-03/msg00011.html>
##  Any tutorial on StarPU with C++? 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-03/msg00001.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-03/msg00004.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-03/msg00012.html>
##  StarPU v1.2.1 released 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-03/msg00000.html>
##  MPI tags limitation with Starpu 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-02/msg00013.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-02/msg00014.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-02/msg00015.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-02/msg00016.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-02/msg00017.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-02/msg00018.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-02/msg00019.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-02/msg00020.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-02/msg00021.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-02/msg00022.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-02/msg00023.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-02/msg00024.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-02/msg00025.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-02/msg00026.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-02/msg00027.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-02/msg00028.html>
##  Could STARPU_COMMUTE change order with a single worker ? 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-02/msg00011.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-02/msg00012.html>
##  A question regarding the MPI cache and starpu_mpi_cache_flush 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-02/msg00006.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-02/msg00007.html>
##  STARPU_STATS does not print statistics 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-02/msg00000.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-02/msg00001.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-02/msg00002.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-02/msg00003.html>
##  Detached communications 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-01/msg00007.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-01/msg00008.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-01/msg00009.html>
##  starpu_worker_get_ids_by_type does not work with STARPU_ANY_WORKER 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-01/msg00003.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-01/msg00004.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-01/msg00005.html>
##  Installing StarPU 1.2.0 (problems with icc) 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-01/msg00000.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-01/msg00001.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-01/msg00002.html>
##  Device OpenCL monopolisant 1 cœur CPU + usage CPU 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-12/msg00029.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-01/msg00006.html>
##  StarPU+SimGrid: FetchingInput computation 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-12/msg00011.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-12/msg00012.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-12/msg00013.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-12/msg00014.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-12/msg00015.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-12/msg00016.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-12/msg00017.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-12/msg00018.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-12/msg00019.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-12/msg00020.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-12/msg00021.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-12/msg00022.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-12/msg00023.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-12/msg00024.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-12/msg00025.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-12/msg00026.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-12/msg00027.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-12/msg00028.html>
##  Deadlock with starpu_calibrate 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-12/msg00000.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-12/msg00001.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-12/msg00002.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-12/msg00003.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-12/msg00004.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-12/msg00005.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-12/msg00006.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-12/msg00007.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-12/msg00008.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-12/msg00009.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-12/msg00010.html>
##  Fwd: startpu 1.2.0 compilation issues 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-11/msg00015.html>
##  startpu 1.2.0 compilation issues 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-11/msg00013.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-11/msg00016.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-11/msg00017.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-11/msg00018.html>
##  Implicit distributed flavour of examples/stencil example 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-11/msg00011.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-11/msg00012.html>
##  StarPU 1.2.0 not working with CUDA 8 and Ubuntu 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-11/msg00010.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-11/msg00014.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-11/msg00019.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-11/msg00020.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-11/msg00021.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-02/msg00004.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-02/msg00005.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-02/msg00008.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-02/msg00009.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2017-02/msg00010.html>
##  Re : Re: Suggestions about creating starpu threads 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-11/msg00009.html>
##  Re. Starpu not recognizing APU 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-11/msg00007.html>
##  Problem with Opencl and starpu 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-11/msg00000.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-11/msg00001.html>
##  ScalFMM parallèle distribué 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-10/msg00011.html>
##  OpenCl and Starpu - more details 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-10/msg00004.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-10/msg00006.html>
##  OpenCl and Starpu 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-10/msg00002.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-10/msg00003.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-10/msg00005.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-10/msg00007.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-10/msg00008.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-10/msg00009.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-10/msg00010.html>
##  Compilation de la version SVN avec l'option --enable-fxt-lock 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-09/msg00016.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-09/msg00017.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-09/msg00018.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-09/msg00019.html>
##  StarPu build error 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-09/msg00009.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-09/msg00010.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-09/msg00011.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-09/msg00012.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-09/msg00013.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-09/msg00014.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-09/msg00015.html>
##  help with trace.rec and tasks.rec 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-09/msg00000.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-09/msg00001.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-09/msg00002.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-09/msg00003.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-09/msg00004.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-09/msg00005.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-09/msg00006.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-09/msg00007.html>
##  segfault with dynamic partitioning and STARPU_PROFILING=1 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-08/msg00045.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-08/msg00046.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-08/msg00047.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-08/msg00049.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-08/msg00051.html>
##  Issues with asynchronous partitioning on temporary data 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-08/msg00043.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-08/msg00044.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-08/msg00048.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-08/msg00050.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-08/msg00052.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-08/msg00053.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-09/msg00008.html>
##  Quelques questions sur StarPU 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-08/msg00041.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-08/msg00042.html>
##  StarPU v1.1.6 released 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-08/msg00040.html>
##  StarPU v1.2.0 released 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-08/msg00039.html>
##  StarPU comparison with other libraries 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-08/msg00037.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-08/msg00038.html>
##  starpu autogen error with libtool on mac os x 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-08/msg00030.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-08/msg00031.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-08/msg00032.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-08/msg00033.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-08/msg00034.html>
##  StarPU v1.2.0rc6 released 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-08/msg00025.html>
##  hwloc detection problem when configuring starpu-svn-trunk and starpu-1.2.0rc5 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-08/msg00007.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-08/msg00008.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-08/msg00010.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-08/msg00011.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-08/msg00013.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-08/msg00015.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-08/msg00016.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-08/msg00018.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-08/msg00020.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-08/msg00021.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-08/msg00022.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-08/msg00023.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-08/msg00024.html>
##  Starpu  amp; Trace 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-08/msg00006.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-08/msg00026.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-08/msg00027.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-08/msg00029.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-08/msg00035.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-08/msg00036.html>
##  automake version detection 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-08/msg00004.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-08/msg00009.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-08/msg00014.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-08/msg00017.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-08/msg00019.html>
##  Deadlock in StarPU-MPI 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-08/msg00003.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-08/msg00028.html>
##  StarPU sur Mac 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-08/msg00000.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-08/msg00001.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-08/msg00002.html>
##  2 small patches 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-07/msg00002.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-07/msg00003.html>
##  DAG et volume de données 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-06/msg00010.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-06/msg00011.html>
##  Possible to Enable STARPU_WORKER_STATS=1 inside the code ? 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-06/msg00005.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-06/msg00006.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-06/msg00007.html>
##  huge regression in LWS scheduler 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-06/msg00002.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-06/msg00003.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-06/msg00004.html>
##  STARPU_LIMIT_MAX_NSUBMITTED_TASKS 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-06/msg00000.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-06/msg00001.html>
##  StarPU Asynchronous Partitioning 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-05/msg00021.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-05/msg00022.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-05/msg00023.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-05/msg00024.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-05/msg00025.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-05/msg00026.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-05/msg00027.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-05/msg00028.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-05/msg00029.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-05/msg00030.html>
##  Suggestions about creating starpu threads 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-05/msg00017.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-05/msg00018.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-05/msg00019.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-05/msg00020.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-05/msg00031.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-05/msg00032.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-06/msg00008.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-06/msg00009.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-10/msg00000.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-10/msg00001.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-11/msg00002.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-11/msg00003.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-11/msg00004.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-11/msg00005.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-11/msg00006.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-11/msg00008.html>
##  compil mac os x starpu-1.1: too many symbolic links 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-05/msg00001.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-05/msg00003.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-05/msg00004.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-05/msg00006.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-05/msg00008.html>
##  Fwd: Strong interest in contributions and integrations 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-05/msg00000.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-05/msg00005.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-05/msg00007.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-05/msg00009.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-05/msg00010.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-05/msg00011.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-05/msg00012.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-05/msg00013.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-05/msg00014.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-05/msg00015.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-05/msg00016.html>
##  Strong interest in contributions and integrations 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-04/msg00018.html>
##  Fortran interface for StarPU 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-04/msg00014.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-04/msg00015.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-07/msg00000.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-07/msg00001.html>
##  Engineer job offer: Improvement of a runtime system for the development of fast numerical methods. 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-04/msg00013.html>
##  StarPU Configuration error 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-04/msg00000.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-04/msg00001.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-04/msg00002.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-04/msg00003.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-04/msg00004.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-04/msg00005.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-04/msg00006.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-04/msg00007.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-04/msg00008.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-04/msg00009.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-04/msg00010.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-04/msg00011.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-04/msg00012.html>
##  ?Question performance des tâches 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-03/msg00006.html>
##  Question performance des tâches 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-03/msg00000.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-03/msg00001.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-03/msg00002.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-03/msg00003.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-03/msg00004.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-03/msg00005.html>
##  starpu_fxt fix: threadid type should be coherent (probably long unsigned) 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-02/msg00003.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-02/msg00004.html>
##  opencl codelet error messages 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-02/msg00002.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-04/msg00017.html>
##  Asking collaboration in PhD Research about Scheduling 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-01/msg00034.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-01/msg00035.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-01/msg00036.html>
##  Cannot use OpenCL on CPU 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-01/msg00024.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-01/msg00025.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-01/msg00026.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-01/msg00027.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-01/msg00028.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-01/msg00029.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-01/msg00030.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-01/msg00031.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-01/msg00032.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-01/msg00033.html>
##  Scheduling accelerators 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-01/msg00011.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-01/msg00012.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-01/msg00013.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-01/msg00014.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-01/msg00015.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-01/msg00016.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-01/msg00017.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-01/msg00018.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-01/msg00019.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-01/msg00020.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-01/msg00021.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-01/msg00022.html>
##  Performance with SOCL on multiple devices* 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-01/msg00010.html>
##  Performance with SOCL on multiple devices 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-01/msg00003.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-01/msg00004.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-01/msg00005.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-01/msg00006.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-01/msg00007.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-01/msg00008.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-01/msg00009.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-01/msg00023.html>
##  Questions about StarPU worker recongnization 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-12/msg00023.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-01/msg00000.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-01/msg00037.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-01/msg00038.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-01/msg00039.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-01/msg00040.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-02/msg00000.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-02/msg00001.html>
##  1.2.0rc5 problem with -lGL 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-12/msg00019.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-12/msg00020.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-12/msg00021.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-12/msg00022.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-01/msg00001.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-01/msg00002.html>
##  StarPU v1.2.0rc5 released 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-12/msg00018.html>
##  Contact Form Submission from Vishal 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-12/msg00016.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-12/msg00017.html>
##  StarPU does not accept too long opencl kernel sources 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-12/msg00010.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-12/msg00011.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2016-04/msg00016.html>
##  Task naming in traces 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-12/msg00003.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-12/msg00004.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-12/msg00005.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-12/msg00006.html>
##  comportement étrange avec StarPU 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-12/msg00000.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-12/msg00001.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-12/msg00002.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-12/msg00007.html>
##  Cannot find MPI 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-11/msg00004.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-11/msg00006.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-11/msg00007.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-11/msg00008.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-11/msg00009.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-11/msg00010.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-11/msg00011.html>
##  status of MPI distribution 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-11/msg00000.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-11/msg00001.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-11/msg00002.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-11/msg00003.html>
##  build starpu+simgrid mac os x 10.9 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-10/msg00002.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-10/msg00004.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-10/msg00005.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-10/msg00006.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-10/msg00007.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-10/msg00008.html>
##  retour experience starpu mac 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-10/msg00000.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-10/msg00001.html>
##  Fwd: Error report 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-09/msg00026.html>
##  Error report 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-09/msg00024.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-09/msg00025.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-09/msg00027.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-09/msg00028.html>
##  Memory leak starpu_task_build ? 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-09/msg00021.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-09/msg00022.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-09/msg00023.html>
##  nready traces starpu 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-09/msg00019.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-09/msg00020.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-12/msg00008.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-12/msg00009.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-12/msg00012.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-12/msg00013.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-12/msg00014.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-12/msg00015.html>
##  Worker Binding Problem 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-09/msg00006.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-09/msg00007.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-09/msg00008.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-09/msg00009.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-09/msg00010.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-09/msg00011.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-09/msg00012.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-09/msg00013.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-09/msg00014.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-09/msg00015.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-09/msg00016.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-09/msg00017.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-09/msg00018.html>
##  StarPU v1.1.5 released 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-09/msg00005.html>
##  SOCL on Ubuntu error 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-09/msg00000.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-09/msg00001.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-09/msg00002.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-09/msg00003.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-09/msg00004.html>
##  Traces do not work in the trunck version 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-08/msg00003.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-08/msg00004.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-08/msg00005.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-08/msg00006.html>
##  StarPU v1.2.0rc4 released 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-08/msg00002.html>
##  fortran error Starpu 1.2 RC3 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-08/msg00000.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-08/msg00001.html>
##  StarPU v1.2.0rc3 released 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-07/msg00014.html>
##  OpenCL 64bit Double errors 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-07/msg00012.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-07/msg00013.html>
##  Installation problem of StarPU 1.0 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-07/msg00005.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-07/msg00006.html>
##  starpu 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-07/msg00001.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-07/msg00002.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-07/msg00003.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-07/msg00004.html>
##  segfault MPI execution and starpu_fxt_tool 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-06/msg00010.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-06/msg00012.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-06/msg00013.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-06/msg00014.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-06/msg00015.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-06/msg00016.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-07/msg00000.html>
##  how compile Examples, makefile.am and makefile.in 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-06/msg00007.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-06/msg00008.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-06/msg00009.html>
##  [SimpleHelp] Declaring and Using a Codelet in C++ 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-06/msg00004.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-06/msg00005.html>
##  reducing STARPU_OPENCL_PIPELINE to 0 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-06/msg00003.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-06/msg00006.html>
##  OpenCL 2.0 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-06/msg00001.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-07/msg00007.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-07/msg00008.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-07/msg00009.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-07/msg00010.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-07/msg00011.html>
##  [PATCH] openmp: expose a global arbiter 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-06/msg00000.html>
##  Build Error with nvcc V7.0.27 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-05/msg00013.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-05/msg00014.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-05/msg00015.html>
##  Compiling Error 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-05/msg00009.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-05/msg00010.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-05/msg00012.html>
##  StarPU 1.2.0rc2, FAIL: datawizard/manual_reduction 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-05/msg00006.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-05/msg00007.html>
##  StarPU v1.2.0rc2 released 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-05/msg00005.html>
##  StarPU examples description 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-05/msg00003.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-05/msg00008.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-05/msg00011.html>
##  MPI datatypes for user-defined interfaces 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-05/msg00002.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-06/msg00002.html>
##  Segfault occurs when using openMPI memory pinning 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-05/msg00000.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-05/msg00001.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-05/msg00004.html>
##  Using Temanejo with StarPU 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-04/msg00000.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-04/msg00001.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-04/msg00002.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-04/msg00003.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-04/msg00004.html>
##  StarPU v1.2.0rc1 released 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-03/msg00002.html>
##  StarPU v1.1.4 released 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-03/msg00001.html>
##  starpu test application 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-03/msg00000.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-03/msg00003.html>
##  mpi_isend_detached / mpi_irecv_detached with starpu-trunk with tag 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-02/msg00054.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-02/msg00055.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-02/msg00056.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-02/msg00057.html>
##  Question about device performance 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-02/msg00052.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-02/msg00053.html>
##  Task which accesses multiple time the same handle with different access modes. 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-02/msg00042.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-02/msg00045.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-02/msg00046.html>
##  Build issues On Windows8.1 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-02/msg00015.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-02/msg00016.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-02/msg00017.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-02/msg00018.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-02/msg00019.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-02/msg00020.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-02/msg00021.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-02/msg00022.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-02/msg00023.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-02/msg00024.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-02/msg00025.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-02/msg00026.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-02/msg00027.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-02/msg00028.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-02/msg00029.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-02/msg00030.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-02/msg00031.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-02/msg00032.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-02/msg00033.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-02/msg00034.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-02/msg00035.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-02/msg00036.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-02/msg00037.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-02/msg00038.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-02/msg00039.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-02/msg00040.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-02/msg00041.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-02/msg00043.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-02/msg00044.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-02/msg00047.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-02/msg00051.html>
##  Unexpected behavior - Not making task ready when they could be 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-02/msg00010.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-02/msg00011.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-02/msg00012.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-02/msg00013.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-02/msg00014.html>
##  Link a Tag to a task using starpu_insert_task() 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-02/msg00003.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-02/msg00005.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-02/msg00006.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-02/msg00007.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-02/msg00008.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-02/msg00009.html>
##  Load balancing over MPI nodes 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-02/msg00002.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-02/msg00004.html>
##  starpu 1.1 and execute_on_worker 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-01/msg00005.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-01/msg00006.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-01/msg00007.html>
##  StarPU on multiple communicator 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-01/msg00002.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-01/msg00003.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-01/msg00004.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-01/msg00008.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-01/msg00009.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-02/msg00000.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-02/msg00001.html>
##  starpu release 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-12/msg00002.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-12/msg00003.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-02/msg00048.html>
##  StarPU Configuration/Build error 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-12/msg00000.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-12/msg00001.html>
##  poll: Should performance models depend on matrix ld? 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-11/msg00024.html>
##  starpu installation issues 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-11/msg00019.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-11/msg00020.html>
##  activity.data : nsubmitted, nready 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-11/msg00011.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-11/msg00012.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-11/msg00016.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-11/msg00017.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-11/msg00030.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-11/msg00031.html>
##  Assert failure with regenerate flag enabled 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-11/msg00007.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-11/msg00008.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-11/msg00009.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-11/msg00010.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-11/msg00013.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-11/msg00014.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-11/msg00015.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-11/msg00018.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-11/msg00021.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-11/msg00022.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-11/msg00023.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-11/msg00025.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-11/msg00026.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-11/msg00027.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-11/msg00028.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-11/msg00029.html>
##  Assert : Number of copy requests left is not zero 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-11/msg00000.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-11/msg00001.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-11/msg00002.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-11/msg00003.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-11/msg00004.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-11/msg00005.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-11/msg00006.html>
##  Overlapping communication with computation on GPU 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-10/msg00043.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-01/msg00000.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-01/msg00001.html>
##  Some interrogations about MPI and StarPU 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-10/msg00041.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-10/msg00042.html>
##  Failed assert in copy_driver.c 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-10/msg00023.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-10/msg00024.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-10/msg00025.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-10/msg00026.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-10/msg00027.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-10/msg00028.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-10/msg00029.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-10/msg00030.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-10/msg00031.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-10/msg00032.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-10/msg00033.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-10/msg00034.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-10/msg00035.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-10/msg00036.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-10/msg00037.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-10/msg00038.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-10/msg00039.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-10/msg00040.html>
##  StarPU Library Instances 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-10/msg00012.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-10/msg00016.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-10/msg00017.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-10/msg00020.html>
##  Performances and starpu_data_set_sequential_consistency_flag 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-10/msg00010.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-10/msg00011.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-10/msg00013.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-10/msg00014.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-10/msg00015.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-10/msg00018.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-10/msg00019.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-10/msg00021.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-10/msg00022.html>
##  possible source of : ../../src/datawizard/coherency.c:60: _starpu_select_src_node: Assertion `src_node_mask != 0' failed. 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-10/msg00008.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-10/msg00009.html>
##  Error in .starpu/sampling 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-09/msg00020.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-09/msg00021.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-10/msg00002.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-10/msg00003.html>
##  Compile problem when installing StarPU with Xeon Phi support 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-09/msg00018.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-09/msg00019.html>
##  StarPU: Data flow between tasks 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-09/msg00016.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-09/msg00022.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-10/msg00000.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-10/msg00001.html>
##  StarPU fails when using dmda/pheft with more than 1 GPU 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-09/msg00010.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-09/msg00012.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-09/msg00014.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-09/msg00015.html>
##  StarPU v1.1.3 released 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-09/msg00009.html>
##  Clarification on task graph 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-09/msg00006.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-09/msg00007.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-09/msg00008.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-09/msg00011.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-09/msg00013.html>
##  Lifetime needed for a starpu_data_handle_t 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-09/msg00004.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-09/msg00005.html>
##  basic example + makefile 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-09/msg00002.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-09/msg00003.html>
##  StarPU questions 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-08/msg00006.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-09/msg00000.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-09/msg00001.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-09/msg00017.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-09/msg00023.html>
##  Installation of Star-PU on Cray XE6/XC30 SYSTEMS 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-08/msg00004.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-08/msg00005.html>
##  Task pipelining commited to trunk 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-08/msg00003.html>
##  MPI and fake handler 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-08/msg00001.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-08/msg00002.html>
##  handles without sequential consistency, RW access mode and multiple memory nodes 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-07/msg00011.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-08/msg00000.html>
##  Unregistered handles after using reduction 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-07/msg00009.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-07/msg00010.html>
##  StarPU on Xeon Phi 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-07/msg00007.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-07/msg00008.html>
##  Difference between starpu_mpi_cache_flush() and starpu_mpi_cache_flush_all() ? 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-07/msg00005.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-07/msg00006.html>
##  About the prediction model of data transfer time 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-07/msg00000.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-07/msg00001.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-07/msg00002.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-07/msg00003.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-07/msg00004.html>
##  MPI scaling 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-06/msg00009.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-06/msg00013.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-06/msg00014.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-06/msg00016.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-06/msg00017.html>
##  Warning when some data handles have not been unregistered() at shutdown() time ? 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-06/msg00008.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-06/msg00010.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-06/msg00015.html>
##  StarPU v1.1.2 released 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-06/msg00000.html>
##  Task status invalid 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-05/msg00012.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-06/msg00002.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-06/msg00003.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-06/msg00004.html>
##  Configure option --with-magma not working 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-05/msg00008.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-05/msg00009.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-05/msg00010.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-05/msg00011.html>
##  New functionality + starpu_data_advise_as_important question 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-05/msg00006.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-05/msg00007.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-06/msg00001.html>
##  Bug when dealing with a huge number of tiles during distributed executions 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-05/msg00001.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-05/msg00003.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-05/msg00004.html>
##  Concurrent access to HASH_FIND_PTR(_cache_sent_data[dest],  amp;data, already_sent); 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-04/msg00049.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-04/msg00050.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-05/msg00000.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-05/msg00005.html>
##  Problem with STARPU_PROLOGUE_CALLBACK_POP 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-04/msg00037.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-04/msg00039.html>
##  Temporary buffer initialization 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-04/msg00034.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-04/msg00036.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-04/msg00038.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-04/msg00040.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-04/msg00041.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-04/msg00042.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-04/msg00043.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-04/msg00044.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-04/msg00045.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-04/msg00046.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-04/msg00047.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-04/msg00048.html>
##  STARPU_ENABLE_CUDA_GPU_GPU_DIRECT doesn't work 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-04/msg00032.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-04/msg00033.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-04/msg00035.html>
##  Bug: perfmodel_bus.c calling upload latency  quot;download latency quot; on CUDA? 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-04/msg00030.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-04/msg00031.html>
##  StarPU v1.1.1 released 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-04/msg00027.html>
##  APU optimization: is partition camping enough of a difference? 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-04/msg00025.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-04/msg00026.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-04/msg00028.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-04/msg00029.html>
##  Starpu-1.1.0 Configure --enable-blas-lib=goto 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-04/msg00021.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-04/msg00022.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-04/msg00023.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-04/msg00024.html>
##  Concurrent CUDA kernel execution 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-04/msg00013.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-04/msg00014.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-04/msg00019.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-04/msg00020.html>
##  Sequential consistency and handles 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-03/msg00049.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-04/msg00005.html>
##  GPU issue with r12137 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-03/msg00047.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-03/msg00048.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-03/msg00050.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-03/msg00051.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-03/msg00052.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-04/msg00000.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-04/msg00001.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-04/msg00002.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-04/msg00003.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-04/msg00004.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-04/msg00006.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-04/msg00007.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-04/msg00008.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-04/msg00009.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-04/msg00010.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-04/msg00011.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-04/msg00012.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-04/msg00015.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-04/msg00016.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-04/msg00017.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-04/msg00018.html>
##  macro issue 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-03/msg00045.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-03/msg00046.html>
##  Accessing task in callback function 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-03/msg00043.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-03/msg00044.html>
##  About dag.dot 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-03/msg00031.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-03/msg00033.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-03/msg00034.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-03/msg00035.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-03/msg00036.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-03/msg00037.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-03/msg00038.html>
##  starpu_data_unregister_submit() and MPI 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-03/msg00023.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-03/msg00024.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-03/msg00025.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-03/msg00026.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-03/msg00027.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-03/msg00028.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-03/msg00029.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-03/msg00030.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-03/msg00032.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-03/msg00039.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-03/msg00040.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-03/msg00041.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-03/msg00042.html>
##  pthread_barrier doesn't exist on OS X 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-03/msg00008.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-03/msg00009.html>
##  socl platform didn't recognize CPU 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-03/msg00005.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-03/msg00006.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-03/msg00007.html>
##  Void interface and StarPU-MPI 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-03/msg00000.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-03/msg00001.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-03/msg00002.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-03/msg00003.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-03/msg00004.html>
##  StarPU MPI and Dags display 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-02/msg00015.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-02/msg00016.html>
##  Using STARPU_COMMUTE with starpu_mpi_task_build() 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-02/msg00011.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-02/msg00012.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-02/msg00013.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-02/msg00017.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-03/msg00010.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-03/msg00011.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-03/msg00012.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-03/msg00013.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-03/msg00014.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-03/msg00015.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-03/msg00016.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-03/msg00017.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-03/msg00018.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-03/msg00019.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-03/msg00020.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-03/msg00021.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-03/msg00022.html>
##  Link on http://runtime.bordeaux.inria.fr/Publis/Keyword/STARPU.html 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-02/msg00003.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-02/msg00004.html>
##  Building StarPU 1.2 Ubuntu Error 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-02/msg00002.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-02/msg00010.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-02/msg00014.html>
##  Question about MPI usage 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-01/msg00017.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-01/msg00018.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-01/msg00019.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-01/msg00020.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-02/msg00000.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-02/msg00005.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-02/msg00006.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-02/msg00007.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-02/msg00008.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-02/msg00009.html>
##  dépendance de types 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-01/msg00007.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-01/msg00008.html>
##  [Inquiry]Starpu Windows Build 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-01/msg00004.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-01/msg00005.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-01/msg00006.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-01/msg00009.html>
##  nested task 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-01/msg00000.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-01/msg00001.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-01/msg00002.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-01/msg00003.html>
##  starpu v1.1.0 released 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-12/msg00016.html>
##  dépendance de données 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-12/msg00014.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-12/msg00015.html>
##  StarPU CUDA configuration error 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-12/msg00013.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-12/msg00017.html>
##  starpu v1.1.0rc4 released 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-12/msg00010.html>
##  starpu_mpi_insert_task() and profiling 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-12/msg00002.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-12/msg00003.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-12/msg00004.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-12/msg00005.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-12/msg00006.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-12/msg00007.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-12/msg00008.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-12/msg00009.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-12/msg00012.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-01/msg00010.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-01/msg00011.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-01/msg00012.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-01/msg00013.html>
##  memory overhead 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-12/msg00001.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-12/msg00011.html>
##  Starpu Test Results 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-11/msg00026.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-11/msg00027.html>
##  CUDA, StarPU and Visual Studio 2012 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-11/msg00018.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-11/msg00019.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-11/msg00020.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-11/msg00021.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-11/msg00022.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-11/msg00023.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-11/msg00024.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-11/msg00025.html>
##  starpu v1.1.0rc3 released 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-11/msg00017.html>
##  License 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-11/msg00015.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-11/msg00016.html>
##  Task distribution statistics for StarPU 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-11/msg00002.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-11/msg00003.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-11/msg00008.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-11/msg00009.html>
##  Is StarPU busy-waiting for tasks ? 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-10/msg00050.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-11/msg00000.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-11/msg00006.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-11/msg00007.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-12/msg00000.html>
##  New vite snapshot uploaded to debian 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-10/msg00048.html>
##  Only 2 OpenCL Devices available 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-10/msg00041.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-10/msg00042.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-11/msg00010.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-11/msg00011.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-11/msg00012.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-11/msg00013.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-11/msg00014.html>
##  Problems with memory in CUDA 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-10/msg00033.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-10/msg00034.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-10/msg00035.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-10/msg00036.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-10/msg00037.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-10/msg00038.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-10/msg00039.html>
##  DLLs required on Windows? 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-10/msg00032.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-10/msg00043.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-10/msg00044.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-10/msg00045.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-10/msg00046.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-10/msg00047.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-10/msg00049.html>
##  error in the documentation 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-10/msg00030.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-10/msg00031.html>
##  src/core/topology.c 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-10/msg00025.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-10/msg00026.html>
##  Conversion tasks 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-10/msg00021.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-10/msg00022.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-10/msg00023.html>
##  Problem with prio and tasks with several implementations 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-10/msg00019.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-10/msg00020.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-10/msg00024.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-10/msg00027.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-10/msg00028.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-10/msg00029.html>
##  Execute on 2 OpenCL devices 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-10/msg00016.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-10/msg00017.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-10/msg00018.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-10/msg00040.html>
##  Error building starpu1.1 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-10/msg00012.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-10/msg00013.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-10/msg00014.html>
##  compilation problem 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-10/msg00008.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-10/msg00015.html>
##  Problemes with doc building 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-10/msg00006.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-10/msg00007.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-10/msg00010.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-10/msg00011.html>
##  Bug in starpu_task_expected_length 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-10/msg00004.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-10/msg00005.html>
##  StarPU task_submit failure on gpu 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-10/msg00001.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-10/msg00002.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-10/msg00003.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-10/msg00009.html>
##  How to wait for a subset of the tasks to complete 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-09/msg00031.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-09/msg00032.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-09/msg00033.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-09/msg00034.html>
##  starpu_insert_task without submission 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-09/msg00026.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-09/msg00027.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-09/msg00030.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-10/msg00000.html>
##  StarPU with two different OpenCL devices 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-09/msg00022.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-09/msg00023.html>
##  STARPU Warning on CUDA update 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-08/msg00004.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-09/msg00024.html>
##  pre-processing on CPU for openCL task 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-08/msg00003.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-09/msg00025.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-09/msg00028.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-09/msg00029.html>
##  pheft problem? 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-08/msg00001.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-08/msg00002.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-11/msg00001.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-11/msg00004.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-11/msg00005.html>
##  Starpu-mpi 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-07/msg00042.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-07/msg00043.html>
##  Dumping performance models on windows 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-07/msg00041.html>
##  Freebsd and pthread_barrier_wait 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-07/msg00039.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-07/msg00040.html>
##  Magma/Plasma on StarPU 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-07/msg00025.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-07/msg00031.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-07/msg00033.html>
##  share a structure between devices? 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-07/msg00024.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-07/msg00026.html>
##  starpu v1.1.0rc2 released 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-07/msg00023.html>
##  OpenCL version in starpu 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-07/msg00021.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-07/msg00028.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-07/msg00046.html>
##  Strange behaviour using GPUs 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-07/msg00017.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-07/msg00018.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-07/msg00019.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-07/msg00020.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-07/msg00022.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-07/msg00027.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-07/msg00047.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-07/msg00048.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-08/msg00000.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-09/msg00000.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-09/msg00001.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-09/msg00002.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-09/msg00003.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-09/msg00004.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-09/msg00005.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-09/msg00006.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-09/msg00007.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-09/msg00008.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-09/msg00009.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-09/msg00010.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-09/msg00011.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-09/msg00012.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-09/msg00013.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-09/msg00014.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-09/msg00015.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-09/msg00016.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-09/msg00017.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-09/msg00018.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-09/msg00019.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-09/msg00020.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-09/msg00021.html>
##  Equivalence vs substitutability 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-07/msg00016.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-07/msg00029.html>
##  starpu 1.1.0rc1 fail to install on MacOS 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-07/msg00015.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-07/msg00030.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-07/msg00032.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-07/msg00034.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-07/msg00035.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-07/msg00036.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-07/msg00037.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-07/msg00038.html>
##  Fwd: Visiting INRIA Bordeaux 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-07/msg00014.html>
##  StarPU OpenMP+CUDA 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-07/msg00007.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-07/msg00008.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-07/msg00010.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-07/msg00011.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-07/msg00012.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-07/msg00013.html>
##  About task dependencies 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-07/msg00006.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-07/msg00009.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-07/msg00044.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-07/msg00045.html>
##  Question about matrix splicing 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-07/msg00000.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-07/msg00001.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-07/msg00002.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-07/msg00003.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-07/msg00004.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-07/msg00005.html>
##  Staticaly distributing task on one given GPU 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-06/msg00043.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-06/msg00044.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-06/msg00045.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-06/msg00046.html>
##  Error in manual 1.1.0rc1 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-06/msg00041.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-06/msg00042.html>
##  debugging? 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-06/msg00037.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-06/msg00038.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-06/msg00039.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-06/msg00040.html>
##  About Paje trace generation 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-06/msg00035.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-06/msg00036.html>
##  StarPU and MKL 11 (Intel Composer XE 2013) 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-06/msg00033.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-06/msg00034.html>
##  Error Report 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-06/msg00026.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-06/msg00027.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-06/msg00028.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-06/msg00029.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-06/msg00030.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-06/msg00031.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-06/msg00032.html>
##  SOCL installation 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-06/msg00015.html>
##  Problème sur SOCL 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-06/msg00013.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-06/msg00014.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-06/msg00016.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-06/msg00017.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-06/msg00022.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-06/msg00023.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-06/msg00024.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-06/msg00025.html>
##  How does StarPU get the OpenCL device ID ? 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-06/msg00011.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-06/msg00012.html>
##  Problems using parallel tasks 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-06/msg00003.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-06/msg00004.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-06/msg00005.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-06/msg00006.html>
##  .stapu directory location 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-06/msg00000.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-06/msg00001.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-06/msg00002.html>
##  FAIL: mpi_reduction 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-05/msg00010.html>
##  Checking for main -lopencl failed 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-05/msg00005.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-05/msg00006.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-05/msg00007.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-05/msg00008.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-05/msg00009.html>
##  About tracing a StarPU application 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-05/msg00003.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-05/msg00004.html>
##  Problem running CUDA task 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-05/msg00001.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-05/msg00002.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-06/msg00007.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-06/msg00008.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-06/msg00009.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-06/msg00010.html>
##  Problem if the task is too large 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-05/msg00000.html>
##  starpu_codelet must be global? 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-04/msg00028.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-04/msg00029.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-04/msg00030.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-04/msg00031.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-04/msg00032.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-04/msg00033.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-04/msg00034.html>
##  Featuring StarPU on the AMD Applications Showcase 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-04/msg00022.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-04/msg00027.html>
##  Fixed compilation error and failed tests 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-04/msg00020.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-06/msg00020.html>
##  StarPU / Mac (10.8.3) 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-04/msg00019.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-04/msg00024.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-04/msg00025.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-06/msg00018.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-06/msg00019.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-06/msg00021.html>
##  StarPU : my letter to santa 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-04/msg00017.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-04/msg00018.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-04/msg00023.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-04/msg00026.html>
##  bug de compilation ? .//etc/OpenCL/vendors/*.icd: file not found 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-04/msg00011.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-04/msg00012.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-04/msg00013.html>
##  StarPU gcc plugin not building 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-04/msg00003.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-04/msg00004.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-04/msg00005.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-04/msg00006.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-04/msg00007.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-04/msg00008.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-04/msg00009.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-04/msg00010.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-04/msg00014.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-04/msg00015.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-04/msg00016.html>
##  StsrPU 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-04/msg00000.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-04/msg00001.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-04/msg00002.html>
##  How to use starpu_mpi_task_insert 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-03/msg00027.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-03/msg00028.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-03/msg00029.html>
##  Error with MPI and _starpu_release_data_enforce_sequential_consistency 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-03/msg00023.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-03/msg00024.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-03/msg00025.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-03/msg00026.html>
##  StarPu 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-03/msg00021.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-03/msg00022.html>
##  CUDA Allocate problem 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-03/msg00017.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-03/msg00018.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-03/msg00019.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-03/msg00020.html>
##  Bug trace 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-03/msg00014.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-03/msg00015.html>
##  StarPU_OpenCl 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-03/msg00010.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-03/msg00011.html>
##  Tags and Sequential Consistency 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-03/msg00007.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-03/msg00008.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-03/msg00016.html>
##  starpu_malloc() when StarPU is not initialized yet (or has been shut down) 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-03/msg00005.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-03/msg00006.html>
##  error in installing starpu 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-03/msg00000.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-03/msg00001.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-03/msg00002.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-03/msg00003.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-03/msg00004.html>
##  How to get a rough completion estimate ? 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-02/msg00055.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-02/msg00056.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-02/msg00057.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-02/msg00058.html>
##  Avoiding CPU-CPU copy in OpenCL driver case 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-02/msg00048.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-02/msg00049.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-02/msg00050.html>
##  Example sched_ctx segfaults 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-02/msg00037.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-02/msg00038.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-02/msg00042.html>
##  Unified_programming_model 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-02/msg00034.html>
##  StarPU CUDA out of memory 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-02/msg00031.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-02/msg00032.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-02/msg00033.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-02/msg00035.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-02/msg00054.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-02/msg00060.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-02/msg00061.html>
##  hang with 8659 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-02/msg00028.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-02/msg00029.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-02/msg00030.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-02/msg00036.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-02/msg00039.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-02/msg00040.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-02/msg00041.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-02/msg00043.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-02/msg00044.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-02/msg00045.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-02/msg00046.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-02/msg00047.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-02/msg00051.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-02/msg00052.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-02/msg00053.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-02/msg00059.html>
##  Assert fail with 8659 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-02/msg00009.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-02/msg00010.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-02/msg00011.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-02/msg00012.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-02/msg00013.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-02/msg00014.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-02/msg00015.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-02/msg00016.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-02/msg00017.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-02/msg00019.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-02/msg00020.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-02/msg00021.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-02/msg00022.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-02/msg00023.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-02/msg00024.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-02/msg00025.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-02/msg00026.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-02/msg00027.html>
##  State of Windows support in StarPU 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-02/msg00005.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-02/msg00006.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-02/msg00007.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-02/msg00008.html>
##  [Starpu-commits] r8543 - trunk/tools 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-02/msg00000.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-02/msg00001.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-02/msg00002.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-02/msg00003.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-02/msg00004.html>
##  [Starpu-commits] r8507 - in trunk/src: datawizard/interfaces drivers/opencl 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-01/msg00034.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-01/msg00035.html>
##  Generating multiple trace 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-01/msg00022.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-01/msg00023.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-01/msg00026.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-01/msg00027.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-01/msg00028.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-01/msg00029.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-01/msg00030.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-01/msg00031.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-01/msg00032.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-01/msg00033.html>
##  several implementations 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-01/msg00012.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-01/msg00013.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-01/msg00014.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-01/msg00017.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-01/msg00018.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-01/msg00019.html>
##  StarPU/SimGrid 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-01/msg00005.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-01/msg00006.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-01/msg00007.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-01/msg00008.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-01/msg00009.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-01/msg00010.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-01/msg00011.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-01/msg00024.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-01/msg00025.html>
##  Unable to visualize Paje traces with ViTE 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-01/msg00001.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-01/msg00002.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-01/msg00003.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-01/msg00004.html>
##  Peppher: Running StarPU + OpenCL on Cora 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-12/msg00040.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-12/msg00041.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-12/msg00042.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-01/msg00000.html>
##  internal function _starpu_can_use_nth_implementation called with STARPU_ANY_WORKER 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-12/msg00036.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-12/msg00037.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-12/msg00038.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-12/msg00039.html>
##  Build error with icc (trunk) 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-12/msg00032.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-12/msg00033.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-12/msg00034.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-12/msg00035.html>
##  Commits are suspended --- Re: Merge of sched_ctx branch into the trunk 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-12/msg00030.html>
##  Merge of sched_ctx branch into the trunk 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-12/msg00016.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-12/msg00017.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-12/msg00018.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-12/msg00019.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-12/msg00020.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-12/msg00025.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-12/msg00026.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-12/msg00027.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-12/msg00028.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-12/msg00029.html>
##  Schedulers 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-12/msg00015.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-12/msg00031.html>
##  What would you like to see most in the schedulers ? 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-12/msg00012.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-12/msg00013.html>
##  [Starpu-commits] r8197 - in trunk: doc/chapters gcc-plugin/examples/cholesky src src/core src/sched_policies tests/sched_policies 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-12/msg00003.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-12/msg00004.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-12/msg00005.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-12/msg00006.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-12/msg00007.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-12/msg00008.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-12/msg00009.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-12/msg00010.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-12/msg00011.html>
##  [PATCH 5/5] Remove heft. 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-11/msg00023.html>
##  [PATCH 2/5] dmda*: add a push_task_notify method, taken from heft. 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-11/msg00022.html>
##  [PATCH 4/5] dm*: Add StarPU-Top support. 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-11/msg00021.html>
##  [PATCH 3/5] dmda*: take into account the data transfer time. 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-11/msg00020.html>
##  [PATCH 1/5] dmda*: Add a pre_exec_hook, taken from heft. 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-11/msg00019.html>
##  [PATCH 0/5] Merge heft and dmda. 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-11/msg00018.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-12/msg00000.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-12/msg00001.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-12/msg00002.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-12/msg00014.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-12/msg00021.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-12/msg00022.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-12/msg00023.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-12/msg00024.html>
##  Workloads on StarPU 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-11/msg00016.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-11/msg00017.html>
##  [Starpu-commits] r8070 - in trunk/mpi: examples examples/cholesky examples/complex examples/mpi_lu examples/stencil include src tests 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-11/msg00011.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-11/msg00012.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-11/msg00014.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-11/msg00015.html>
##  Heft/dmda: locking in compute_all_performance_predictions. 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-11/msg00008.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-11/msg00009.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-11/msg00010.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-11/msg00013.html>
##  Scheduler heft 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-11/msg00004.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-11/msg00005.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-11/msg00006.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-11/msg00007.html>
##  Temanejo support: graphically single-stepping tasks! 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-11/msg00003.html>
##  StarPU can_execute problem 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-11/msg00001.html>
##  Question About StarUP with AMD Architecture. 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-10/msg00081.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-11/msg00002.html>
##  Segmentation fault on core/jobs.c:215 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-10/msg00080.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-10/msg00082.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-11/msg00000.html>
##  [Starpu-commits] r7820 - in trunk/src: common core profiling util 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-10/msg00079.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-10/msg00083.html>
##  Problem in installation of StarPU 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-10/msg00078.html>
##  Regarding StarPU installation 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-10/msg00074.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-10/msg00075.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-10/msg00076.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-10/msg00077.html>
##  [PATCH v2 6/6] Reindent _dmda_push_task(). 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-10/msg00065.html>
##  [PATCH v2 5/6] src/sched_policies/deque_modeling_policy_data_aware.c: add a compute_all_performance_predictions() function. 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-10/msg00064.html>
##  [PATCH v2 4/6] heft.c: Remove the  quot;bundle quot; parameter from compute_all_performance_predictions(). 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-10/msg00063.html>
##  [PATCH v2 3/6] Heft: use a scheduler-specific fifo rather than pushing tasks directly to the workers' local tasks. 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-10/msg00062.html>
##  [PATCH v2 2/6] _starpu_fifo_push_task(): Fix  quot;TODO: if prio, put at back quot;. 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-10/msg00061.html>
##  [PATCH v2 1/6] src/sched_policies/heft.c: use an array of _starpu_fifo_taskq* rather than multiple global variables. 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-10/msg00060.html>
##  Fwd: Fwd: Fwd: starpu opencl app 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-10/msg00054.html>
##  starpu opencl app 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-10/msg00045.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-10/msg00046.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-10/msg00047.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-10/msg00057.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-10/msg00068.html>
##  pkg-config 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-10/msg00041.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-10/msg00042.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-10/msg00043.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-10/msg00044.html>
##  Un nouveau message ! 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-10/msg00038.html>
##  STARPU_CSR_GET_DEV_HANDLE for OpenCL? 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-10/msg00032.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-10/msg00033.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-10/msg00034.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-10/msg00035.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-10/msg00036.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-10/msg00037.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-10/msg00058.html>
##  [PATCH 1/7] src/sched_policies/heft.c: use an array of _starpu_fifo_taskq* rather than multiple global variables. 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-10/msg00029.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-10/msg00049.html>
##  [PATCH 7/7] Handle bundles in deque_modeling_policy_data_aware.c. 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-10/msg00028.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-10/msg00056.html>
##  [PATCH 6/7] Reindent _dmda_push_task(). 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-10/msg00027.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-10/msg00055.html>
##  [PATCH 5/7] src/sched_policies/deque_modeling_policy_data_aware.c: add a compute_all_performance_predictions() function. 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-10/msg00026.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-10/msg00053.html>
##  [PATCH 4/7] heft.c: Remove the  quot;bundle quot; parameter from compute_all_performance_predictions(). 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-10/msg00025.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-10/msg00051.html>
##  [PATCH 3/7] Heft: use a scheduler-specific fifo rather than pushing tasks directly to the workers' local tasks. 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-10/msg00024.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-10/msg00052.html>
##  [PATCH 2/7] _starpu_fifo_push_task(): Fix  quot;TODO: if prio, put at back quot;. 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-10/msg00023.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-10/msg00050.html>
##  [PATCH 0/7][Review needed] A first step towards the factorization of heft and dm*. 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-10/msg00022.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-10/msg00048.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-10/msg00059.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-10/msg00066.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-10/msg00067.html>
##  About StarPU 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-10/msg00018.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-10/msg00019.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-10/msg00020.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-10/msg00030.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-10/msg00031.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-10/msg00039.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-10/msg00040.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-10/msg00069.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-10/msg00070.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-10/msg00071.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-10/msg00072.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-10/msg00073.html>
##  heft scheduler with can_execute hook 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-10/msg00017.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-10/msg00021.html>
##  StarPU can_execute hook with multiple implementations.. 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-10/msg00013.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-10/msg00014.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-10/msg00015.html>
##  COO and other sparse interfaces in StarPU 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-10/msg00009.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-10/msg00010.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-10/msg00012.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-10/msg00016.html>
##  Tuning for Processor Affinity? 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-10/msg00008.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-10/msg00011.html>
##  SOCL and ICD 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-10/msg00000.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-10/msg00001.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-10/msg00002.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-10/msg00003.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-10/msg00004.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-10/msg00005.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-10/msg00006.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-10/msg00007.html>
##  OpenMP StarPU 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-09/msg00032.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-09/msg00033.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-09/msg00034.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-09/msg00035.html>
##  Informationn de starpu avec hwloc mais sans pkgconfig 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-09/msg00024.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-09/msg00026.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-09/msg00027.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-09/msg00028.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-09/msg00029.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-09/msg00030.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-09/msg00031.html>
##  Fwd: Informationn de starpu avec hwloc mais sans pkgconfig 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-09/msg00019.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-09/msg00020.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-09/msg00021.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-09/msg00022.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-09/msg00023.html>
##  free(handle) in _starpu_data_unregister 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-09/msg00016.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-09/msg00017.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-09/msg00018.html>
##  StarPU for distributed computing 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-09/msg00010.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-09/msg00011.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-09/msg00012.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-09/msg00013.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-09/msg00014.html>
##  Minor Makefile error in mpi/ 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-09/msg00007.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-09/msg00009.html>
##  Finding Nvidia capabilities when configuring StarPU. 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-09/msg00006.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-09/msg00008.html>
##  Erreur d'abort dans StarPU 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-09/msg00003.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-09/msg00004.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-09/msg00005.html>
##  pthread_rwlock_t-related failures on Darwin. 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-08/msg00016.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-09/msg00000.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-09/msg00001.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-09/msg00002.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-09/msg00015.html>
##  Data interfaces in StarPU 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-08/msg00012.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-08/msg00013.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-08/msg00014.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-08/msg00015.html>
##  make check in tools subdirectory (Re: [Starpu-commits] r7223 - trunk) 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-08/msg00011.html>
##  parallel_tasks test failures 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-08/msg00004.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-08/msg00007.html>
##  starpu in gentoo 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-08/msg00002.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-08/msg00003.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-08/msg00005.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-08/msg00010.html>
##  StarPU csr_interface problem 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-08/msg00001.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-08/msg00008.html>
##  starpu v1.0.2 released 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-08/msg00000.html>
##  Fwd: Re: [Starpu-commits] r6977 - in trunk: . src/core/perfmodel src/drivers/cuda 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-07/msg00024.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-07/msg00025.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-07/msg00026.html>
##  Should we use more hooks ? 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-07/msg00022.html>
##  Question sur starpu_data_set_rank 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-07/msg00019.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-07/msg00020.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-07/msg00027.html>
##  Bug autogen.sh 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-07/msg00016.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-07/msg00017.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-07/msg00018.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-07/msg00021.html>
##  doc/Makefile.am: replace stat(1) by something portable. 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-07/msg00014.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-07/msg00015.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-08/msg00006.html>
##  [Starpu-commits] r6902 - in trunk: . examples/basic_examples 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-07/msg00012.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-07/msg00013.html>
##  CPU data on a GPU kernel 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-07/msg00007.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-07/msg00008.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-07/msg00009.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-07/msg00010.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-07/msg00011.html>
##  Performance issues when specifying const and __attribute__ ((output)) 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-07/msg00000.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-07/msg00001.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-07/msg00002.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-07/msg00003.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-07/msg00005.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-07/msg00006.html>
##  Strange error  quot;  lt;starpu_task_submit gt; returned unexpected value:  lt;-19 gt; quot; 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-06/msg00039.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-06/msg00040.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-06/msg00041.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-06/msg00042.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-06/msg00043.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-06/msg00044.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-06/msg00045.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-06/msg00046.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-07/msg00004.html>
##  Drivers, schedulers: where should sched_mutex be locked ? 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-06/msg00036.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-06/msg00038.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-07/msg00023.html>
##  support de l'extension cl_khr_icd par socl 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-06/msg00030.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-06/msg00032.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-06/msg00033.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-06/msg00034.html>
##  DAG with Tag dependences 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-06/msg00029.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-06/msg00031.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-06/msg00035.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-06/msg00037.html>
##  GCC plugin and registering address of scalar 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-06/msg00018.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-06/msg00019.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-06/msg00020.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-06/msg00021.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-06/msg00022.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-06/msg00023.html>
##  CUDA task and GCC plugin 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-06/msg00012.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-06/msg00013.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-06/msg00014.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-06/msg00015.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-06/msg00016.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-08/msg00009.html>
##  make distcheck fails 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-06/msg00007.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-06/msg00011.html>
##  BCSR: the rowptr field. 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-06/msg00004.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-06/msg00005.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-06/msg00017.html>
##  Building StarPU with OpenCL on Ubuntu 11.10 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-06/msg00002.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-06/msg00003.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-06/msg00006.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-06/msg00008.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-06/msg00009.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-06/msg00010.html>
##  Error while make checking StarPU 1.0.1 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-06/msg00000.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-06/msg00001.html>
##  Lazy data unregistering 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-05/msg00012.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-05/msg00013.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-05/msg00014.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-05/msg00015.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-05/msg00017.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-05/msg00018.html>
##  Is pgreedy broken ? 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-05/msg00004.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-05/msg00005.html>
##  bad performance scaling behavior of cholesky decomposition 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-05/msg00002.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-05/msg00006.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-05/msg00007.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-05/msg00008.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-05/msg00009.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-05/msg00010.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-05/msg00011.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-05/msg00016.html>
##  starpufft + nvcc 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-05/msg00000.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-05/msg00001.html>
##  Compiler starpu sous windows pour ManyLoDs 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-04/msg00027.html>
##  Combined workers and OpenMP code 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-04/msg00023.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-04/msg00026.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-04/msg00028.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-04/msg00029.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-04/msg00030.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-04/msg00031.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-04/msg00032.html>
##  Requested Output 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-04/msg00014.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-04/msg00015.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-04/msg00016.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-04/msg00017.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-04/msg00019.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-04/msg00020.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-04/msg00021.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-04/msg00022.html>
##  Make the FxT-related code portable. 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-04/msg00013.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-04/msg00018.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-04/msg00024.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-04/msg00025.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-05/msg00003.html>
##  StarPU installation error 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-04/msg00009.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-04/msg00010.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-04/msg00011.html>
##  Constructing codelet-chain loop 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-04/msg00003.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-04/msg00004.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-04/msg00005.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-04/msg00006.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-04/msg00007.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-04/msg00008.html>
##  Nested codelets? 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-04/msg00001.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-04/msg00002.html>
##  Error handling in StarPU. 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-03/msg00053.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-03/msg00054.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-03/msg00056.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-03/msg00063.html>
##  OpenMP 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-03/msg00049.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-03/msg00050.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-03/msg00051.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-03/msg00052.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-03/msg00055.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-03/msg00057.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-03/msg00058.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-03/msg00059.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-03/msg00060.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-03/msg00061.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-03/msg00062.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-03/msg00064.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-03/msg00065.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-03/msg00066.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-03/msg00067.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-03/msg00068.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-04/msg00000.html>
##  [Starpu-commits] r6233 - trunk/src/datawizard/interfaces 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-03/msg00047.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-03/msg00048.html>
##  Fwd: (sans objet) 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-03/msg00037.html>
##  Fwd: help 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-03/msg00035.html>
##  Pastix should be fixed [Was: [Starpu-commits] r5692 - in trunk/src: core core/dependencies datawizard drivers/cpu drivers/cuda drivers/gordon drivers/opencl] 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-03/msg00033.html>
##  AMD Fusion: post-full-utilization computing? 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-03/msg00015.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-03/msg00017.html>
##  [BUG] tests/datawizard/reclaim.c: hangs forever in task_wait_for_all when using OpenCL. 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-03/msg00014.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-03/msg00016.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-03/msg00018.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-03/msg00019.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-03/msg00020.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-03/msg00029.html>
##  Question about header installation directory 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-03/msg00013.html>
##  [PATCH] src/sched_policies: add starpu_sched_policies_common.{c, h}, that will contain code that can easily be shared between schedulers. 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-03/msg00009.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-03/msg00010.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-03/msg00011.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-03/msg00012.html>
##  Use OpenCL on CPU devices. 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-03/msg00008.html>
##  Checksum files for releases and mirror site 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-03/msg00003.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-03/msg00004.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-03/msg00007.html>
##  OpenMP timing with StarPU pheft 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-03/msg00001.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-03/msg00002.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-03/msg00005.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-03/msg00006.html>
##  fxt 0.2.5 released 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-03/msg00000.html>
##  Patch request for the multiformat interface 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-02/msg00085.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-02/msg00086.html>
##  [StarPU] Data prefetch. 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-02/msg00083.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-02/msg00084.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-02/msg00087.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-02/msg00088.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-02/msg00089.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-02/msg00090.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-02/msg00091.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-02/msg00092.html>
##  [Starpu-commits] r5692 - in trunk/src: core core/dependencies datawizard drivers/cpu drivers/cuda drivers/gordon drivers/opencl 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-02/msg00078.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-02/msg00079.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-02/msg00081.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-02/msg00082.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-03/msg00025.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-03/msg00026.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-03/msg00027.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-03/msg00028.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-03/msg00045.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-03/msg00046.html>
##  StarPU Question regarding gated connections 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-02/msg00074.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-02/msg00075.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-02/msg00076.html>
##  StarPU's limitations 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-02/msg00063.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-02/msg00064.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-02/msg00065.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-02/msg00071.html>
##  Fwd: [Starpu-commits] r5692 - in trunk/src: core core/dependencies datawizard drivers/cpu drivers/cuda drivers/gordon drivers/opencl 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-02/msg00061.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-02/msg00066.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-02/msg00067.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-02/msg00069.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-02/msg00070.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-02/msg00072.html>
##  starpu v1.0.0rc2 released 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-02/msg00052.html>
##  OpenMP example 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-02/msg00049.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-02/msg00050.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-02/msg00051.html>
##  starpu_task_destroy 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-02/msg00044.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-02/msg00047.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-02/msg00048.html>
##  starpu_task_destroy and OpenMP example 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-02/msg00043.html>
##  [Starpu-commits] r5610 - trunk/gcc-plugin/examples/vector_scal 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-02/msg00040.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-02/msg00041.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-02/msg00042.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-02/msg00045.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-02/msg00046.html>
##  multi-chip multicore task scheduling 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-02/msg00027.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-02/msg00028.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-02/msg00029.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-02/msg00030.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-02/msg00031.html>
##  (sans objet) 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-02/msg00026.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-02/msg00055.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-02/msg00056.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-02/msg00057.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-03/msg00021.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-03/msg00022.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-03/msg00023.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-03/msg00024.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-03/msg00030.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-03/msg00031.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-03/msg00032.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-03/msg00034.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-03/msg00036.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-03/msg00038.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-03/msg00039.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-03/msg00040.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-03/msg00041.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-03/msg00042.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-03/msg00043.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-03/msg00044.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-06/msg00026.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-06/msg00027.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-06/msg00028.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-02/msg00001.html>
##  [Memory leaks] Tasks with NULL codelets. 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-02/msg00025.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-02/msg00032.html>
##  Performance decreasing by adding empty tasks 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-02/msg00022.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-02/msg00033.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-02/msg00034.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-02/msg00035.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-02/msg00036.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-02/msg00038.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-02/msg00054.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-02/msg00058.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-02/msg00059.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-02/msg00060.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-02/msg00062.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-02/msg00073.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-02/msg00077.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-02/msg00080.html>
##  StarPU on macos ? 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-02/msg00020.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-02/msg00021.html>
##  Ready for rc2 ? 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-02/msg00015.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-02/msg00023.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-02/msg00024.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-02/msg00053.html>
##  StarPU make check failure 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-02/msg00010.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-02/msg00011.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-02/msg00012.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-02/msg00013.html>
##  Performances regression 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-02/msg00005.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-02/msg00006.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-02/msg00007.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-02/msg00009.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-02/msg00037.html>
##  Seek and destroy memory leaks. 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-02/msg00000.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-02/msg00001.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-02/msg00002.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-02/msg00003.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-02/msg00004.html>
##  [Starpu-commits] r5392 - in trunk: . doc/chapters 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-01/msg00022.html>
##  [patch request] Double comparison 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-01/msg00017.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-01/msg00018.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-01/msg00019.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-01/msg00020.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-01/msg00021.html>
##  [BUG] examples/heat/heat sometimes hangs. 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-01/msg00016.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-02/msg00039.html>
## [Starpu-commits] r5299 - trunk/doc/chapters 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-01/msg00013.html>
##  [Fwd: [Starpu-commits] r5299 - trunk/doc/chapters] 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-01/msg00012.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-01/msg00014.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-01/msg00015.html>
##  starpu v1.0.0rc1 released 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-01/msg00011.html>
##  [Review needed] nreaders might be 0 in _starpu_add_writer_after_readers. 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-01/msg00009.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-01/msg00010.html>
##  Compare GPU/CPU execution. 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-01/msg00005.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-01/msg00006.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-01/msg00007.html>
##  initialised SCRATCH buffer 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-01/msg00000.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-01/msg00001.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-01/msg00002.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-01/msg00003.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-01/msg00004.html>
##  Query about threads/tasks in starpu 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-12/msg00021.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-12/msg00022.html>
##  Environment variables over starpu_conf? 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-12/msg00018.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-02/msg00008.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-02/msg00014.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-02/msg00016.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-02/msg00017.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-02/msg00018.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-02/msg00019.html>
##  [Starpu-commits] r4894 - in trunk: . include src/core src/core/dependencies src/core/perfmodel src/datawizard src/sched_policies 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-12/msg00017.html>
##  [Starpu-commits] [Review needed] Move $HOME/.starpu/sampling to $XDG_CACHE_HOME/starpu/sampling 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-12/msg00009.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-12/msg00010.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-12/msg00011.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-12/msg00012.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-12/msg00013.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-12/msg00014.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-12/msg00015.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-12/msg00016.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-12/msg00020.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-01/msg00008.html>
##  How to use a latest version of starpu library 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-12/msg00004.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-12/msg00005.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-12/msg00006.html>
##  Query about Worker execution time 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-12/msg00003.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-12/msg00007.html>
##  Query about profiling data 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-12/msg00000.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-12/msg00001.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-12/msg00002.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-12/msg00008.html>
##  Script to run buildbot profiles against your source code 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-11/msg00027.html>
##  Error - No worker may execute this task 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-11/msg00021.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-11/msg00022.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-11/msg00023.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-11/msg00024.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-11/msg00025.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-11/msg00026.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-11/msg00028.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-11/msg00029.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-11/msg00030.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-11/msg00031.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-11/msg00032.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-11/msg00033.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-11/msg00034.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-11/msg00035.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-11/msg00036.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-11/msg00037.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-11/msg00038.html>
##  Modification of the StarPU Public API 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-11/msg00020.html>
##  Linker errors in make while trying to build starpu 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-11/msg00015.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-11/msg00016.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-11/msg00017.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-11/msg00018.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-11/msg00019.html>
##  Performance metric 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-11/msg00006.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-11/msg00007.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-11/msg00008.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-11/msg00009.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-11/msg00010.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-11/msg00011.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-11/msg00012.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-11/msg00013.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-11/msg00014.html>
##  Re : How to add own example code to build using Starpu 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-11/msg00004.html>
##  Re : How to add own example code to build using Starpu 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-11/msg00001.html>
##  How to add own example code to build using Starpu 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-11/msg00000.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-11/msg00003.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-11/msg00005.html>
##  Starpu vector_scal example execution error on 32-bit linux system 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-10/msg00018.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-11/msg00002.html>
##  Segmentation fault when I try to execute the basic example Vector Scale using OpenCL kernel. 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-10/msg00011.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-10/msg00012.html>
##  Error while building StarpU library 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-10/msg00008.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-10/msg00009.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-10/msg00010.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-10/msg00013.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-10/msg00014.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-10/msg00015.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-10/msg00016.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-10/msg00017.html>
##  [Starpu-bugs] Model debug compile problem 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-10/msg00002.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-10/msg00003.html>
##  StarPU 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-10/msg00001.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-10/msg00004.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-10/msg00005.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-06/msg00024.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2012-06/msg00025.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-01/msg00015.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-01/msg00016.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-01/msg00020.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-01/msg00021.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-03/msg00009.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-03/msg00012.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2013-03/msg00013.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-01/msg00014.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-01/msg00015.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-01/msg00016.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-06/msg00005.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-06/msg00006.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-06/msg00007.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-06/msg00011.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-06/msg00012.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-06/msg00018.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-06/msg00019.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-06/msg00020.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-06/msg00021.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-06/msg00022.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-10/msg00004.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-10/msg00005.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-10/msg00006.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2014-10/msg00007.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-02/msg00049.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2015-02/msg00050.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2019-02/msg00017.html>
##  [Starpu-bugs] starpu tests failed 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-10/msg00000.html>
##  StarPU CUDA problem 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-09/msg00025.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-09/msg00026.html>
##  Applications allowed to do computations in their own cores ? 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-09/msg00023.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-09/msg00024.html>
##  Perfmodel format. 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-09/msg00021.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-09/msg00022.html>
##  Questions around StarPU 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-09/msg00015.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-09/msg00018.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-09/msg00019.html>
##  Fwd: [Starpu-commits] r4125 - trunk 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-09/msg00013.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-09/msg00014.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-09/msg00020.html>
##  Questions autour de StarPU 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-09/msg00012.html>
##  Bug avec le prefetch 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-09/msg00011.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-09/msg00016.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-09/msg00017.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-09/msg00027.html>
##  Integrating MPI and starpu 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-09/msg00008.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-09/msg00009.html>
##  increment_redux_v2 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-09/msg00007.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-09/msg00010.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-10/msg00006.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-10/msg00007.html>
##  Introducing the GNU C Extensions 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-09/msg00002.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-09/msg00005.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-09/msg00006.html>
##  StarPU: subgraph_repeat_regenerate.c 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-09/msg00001.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-09/msg00003.html>
##  OpenCL backend on CPU 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-09/msg00000.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-09/msg00004.html>
##  Problem with CUDA 4.0 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-08/msg00004.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-08/msg00005.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-08/msg00006.html>
##  Installation problem 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-08/msg00002.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-08/msg00003.html>
##  OpenCL problem 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-07/msg00013.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-08/msg00001.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-12/msg00019.html>
##  Fwd: Re: StarPU OpenCL on Mac 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-07/msg00008.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-07/msg00011.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-07/msg00012.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-08/msg00000.html>
##  StarPU OpenCL on Mac 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-07/msg00005.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-07/msg00006.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-07/msg00007.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-07/msg00009.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-07/msg00010.html>
##  [Starpu-commits] r3978 - in trunk: . socl socl/include socl/include/CL socl/src 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-07/msg00000.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-07/msg00001.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-07/msg00002.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-07/msg00003.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-07/msg00004.html>
##  StarPU: questions about performance model 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-06/msg00004.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-06/msg00005.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-06/msg00010.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-06/msg00011.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-06/msg00012.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-06/msg00013.html>
##  Reporting a bug 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-06/msg00003.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-06/msg00006.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-06/msg00007.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-06/msg00008.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-06/msg00009.html>
##  StarPu: performance model doesn't work 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-06/msg00000.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-06/msg00001.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-06/msg00002.html>
##  StarPU 0.9 is released! 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-05/msg00000.html>
##  Documentation Patch for Building on Windows 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-04/msg00041.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-04/msg00042.html>
##  Suppressing MSVC warnings on StarPU macros 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-04/msg00039.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-04/msg00040.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-04/msg00043.html>
##  Header Issue (C++ and MSVC) 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-04/msg00037.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-04/msg00038.html>
##  StarPU with C++ (possibly extern) problem 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-04/msg00035.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-04/msg00036.html>
##  starpu_data_unregister() failing at pthread_spin_unlock() 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-04/msg00033.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-04/msg00034.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-04/msg00044.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-04/msg00045.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-04/msg00046.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-04/msg00047.html>
##  Patch: Add a const qualifier to suppress a long list of warnings 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-04/msg00031.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-04/msg00032.html>
##  [MACOSX] problem with examples 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-04/msg00015.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-04/msg00016.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-04/msg00017.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-04/msg00018.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-04/msg00019.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-04/msg00020.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-04/msg00021.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-04/msg00022.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-04/msg00023.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-04/msg00024.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-04/msg00025.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-04/msg00026.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-04/msg00027.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-04/msg00028.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-04/msg00029.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-04/msg00030.html>
##  compile fix - sync_and_notify_data.c 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-04/msg00012.html>
##  setting the clBuildProgram() option parameter 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-04/msg00009.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-04/msg00010.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-04/msg00011.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-04/msg00013.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-04/msg00014.html>
##  Compiling starpu on Mac OS 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-03/msg00020.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-03/msg00021.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-03/msg00022.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-03/msg00024.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-03/msg00025.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-03/msg00026.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-04/msg00000.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-04/msg00001.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-04/msg00002.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-04/msg00003.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-04/msg00004.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-04/msg00005.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-04/msg00006.html>
##  Building starpu examples with MSVC 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-03/msg00017.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-03/msg00018.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-03/msg00019.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-03/msg00023.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-04/msg00007.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-04/msg00008.html>
##  StarPU task overhead 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-03/msg00013.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-03/msg00014.html>
##  Fwd: Re: StarPU: OpenCL do not work well 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-03/msg00011.html>
##  StarPU: OpenCL do not work well 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-03/msg00009.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-03/msg00010.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-03/msg00012.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-03/msg00015.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-03/msg00016.html>
##  Windows version of StarPU 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-03/msg00008.html>
##  Asynchronous Task execution problem 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-03/msg00003.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-03/msg00004.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-03/msg00005.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-03/msg00006.html>
##  Getting StarPU running 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-03/msg00002.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-03/msg00007.html>
##  Installation of starpu on macbook 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-03/msg00000.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-03/msg00001.html>
##  StarPU with Windows and Visual C++ port? 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-02/msg00021.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-02/msg00022.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-02/msg00023.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-02/msg00024.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-02/msg00025.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-02/msg00027.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-02/msg00028.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-02/msg00029.html>
##  Building StarPU from SVN on OSX [patch] 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-02/msg00018.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-02/msg00019.html>
##  Building StarPU from SVN on OSX 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-02/msg00013.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-02/msg00016.html>
##  StarPU SVN and OpenCL on CPU 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-02/msg00012.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-02/msg00014.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-02/msg00015.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-02/msg00017.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-02/msg00020.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-02/msg00026.html>
##  Building StarPU for OpenCL and Linux 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-02/msg00009.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-02/msg00010.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-02/msg00011.html>
##  Building StarPU on Windows 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-02/msg00004.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-02/msg00005.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-02/msg00006.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-02/msg00007.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-02/msg00008.html>
##  StarPU and OpenMP threads 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-02/msg00002.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-02/msg00003.html>
##  StarPU SSE and nvcc compiler 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-02/msg00000.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-02/msg00001.html>
##  Deadlock when calling starpu_shutdown 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-01/msg00000.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2011-01/msg00001.html>
##  OpenCL in StarPU 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2010-12/msg00000.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2010-12/msg00001.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2010-12/msg00002.html>
##  [Starpu-commits] r2912 - trunk/src/drivers/opencl 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2010-11/msg00009.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2010-11/msg00010.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2010-11/msg00011.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2010-11/msg00012.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2010-11/msg00013.html>
##  SOCL 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2010-11/msg00000.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2010-11/msg00001.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2010-11/msg00002.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2010-11/msg00003.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2010-11/msg00004.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2010-11/msg00005.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2010-11/msg00006.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2010-11/msg00007.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2010-11/msg00008.html>
##  Deadlocks 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2010-08/msg00005.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2010-09/msg00000.html>
##  StarPU configuration issues 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2010-08/msg00000.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2010-08/msg00001.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2010-08/msg00002.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2010-08/msg00003.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2010-08/msg00004.html>
##  test 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2010-07/msg00010.html>
##  clEnqueueReadBufferRect / clEnqueueWriteBufferRect 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2010-07/msg00005.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2010-07/msg00006.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2010-07/msg00007.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2010-07/msg00008.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2010-07/msg00009.html>
##  Filter names 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2010-07/msg00003.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2010-07/msg00004.html>
##  Matrix Filters 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2010-07/msg00000.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2010-07/msg00001.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2010-07/msg00002.html>
##  pthread_mutex_unlock : Invalid argument 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2010-05/msg00006.html>
##  Problem with pthread_mutex_lock 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2010-05/msg00004.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2010-05/msg00005.html>
##  Huge API Update: Function renaming 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2010-05/msg00002.html>
##  Function renaming 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2010-05/msg00000.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2010-05/msg00001.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2010-05/msg00003.html>
##  Termination of strassen with opencl 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2010-04/msg00003.html>
##  Nightly snapshots 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2010-04/msg00002.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2010-04/msg00004.html>
##  Example spmv 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2010-04/msg00000.html>
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2010-04/msg00001.html>
##  Pointer arithmetic and OpenCL 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2010-03/msg00001.html>
##  Test message 
- <https://sympa.inria.fr/sympa/arc/starpu-devel/2010-03/msg00000.html>

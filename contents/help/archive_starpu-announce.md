---
layout: default
title: Getting Help. Local archive for list starpu-announce
permalink: help/archive_starpu-announce.html
---
# StarPU: Getting Help. Local archive for list starpu-announce

##  StarPU new release v1.4.6 
- <https://sympa.inria.fr/sympa/arc/starpu-announce/2024-05/msg00000.html>
##  StarPU new release v1.4.5 
- <https://sympa.inria.fr/sympa/arc/starpu-announce/2024-04/msg00000.html>
##  StarPU new release v1.4.4 
- <https://sympa.inria.fr/sympa/arc/starpu-announce/2024-02/msg00000.html>
##  StarPU new release v1.4.3 
- <https://sympa.inria.fr/sympa/arc/starpu-announce/2024-01/msg00000.html>
##  StarPU new release v1.4.2 
- <https://sympa.inria.fr/sympa/arc/starpu-announce/2023-11/msg00000.html>
##  StarPU v1.3.11 available 
- <https://sympa.inria.fr/sympa/arc/starpu-announce/2023-06/msg00000.html>
##  StarPU new release v1.4.1 
- <https://sympa.inria.fr/sympa/arc/starpu-announce/2023-05/msg00000.html>
##  StarPU new release v1.4.0 
- <https://sympa.inria.fr/sympa/arc/starpu-announce/2023-03/msg00000.html>
##  StarPU v1.3.9 available 
- <https://sympa.inria.fr/sympa/arc/starpu-announce/2021-10/msg00000.html>
- <https://sympa.inria.fr/sympa/arc/starpu-announce/2022-11/msg00000.html>
##  StarPU v1.3.8 available 
- <https://sympa.inria.fr/sympa/arc/starpu-announce/2021-05/msg00000.html>
## Migration of StarPU mailing lists 
- <https://sympa.inria.fr/sympa/arc/starpu-announce/2020-12/msg00000.html>
##  StarPU v1.3.7 available 
- <https://sympa.inria.fr/sympa/arc/starpu-announce/2020-10/msg00001.html>
##  StarPU v1.3.6 available 
- <https://sympa.inria.fr/sympa/arc/starpu-announce/2020-10/msg00000.html>
##  StarPU v1.3.5 available 
- <https://sympa.inria.fr/sympa/arc/starpu-announce/2020-08/msg00000.html>
##  StarPU: Migration to gitlab 
- <https://sympa.inria.fr/sympa/arc/starpu-announce/2020-07/msg00000.html>
##  StarPU v1.2.10 released 
- <https://sympa.inria.fr/sympa/arc/starpu-announce/2020-06/msg00001.html>
##  StarPU v1.3.4 available 
- <https://sympa.inria.fr/sympa/arc/starpu-announce/2020-06/msg00000.html>
##  StarPU v1.2.9 released 
- <https://sympa.inria.fr/sympa/arc/starpu-announce/2020-01/msg00000.html>
##  StarPU v1.3.3 available 
- <https://sympa.inria.fr/sympa/arc/starpu-announce/2019-10/msg00000.html>
##  StarPU v1.3.2 available 
- <https://sympa.inria.fr/sympa/arc/starpu-announce/2019-06/msg00000.html>
##  StarPU v1.1.8 released 
- <https://sympa.inria.fr/sympa/arc/starpu-announce/2019-05/msg00000.html>
##  StarPU v1.3.1 released 
- <https://sympa.inria.fr/sympa/arc/starpu-announce/2019-04/msg00000.html>
##  StarPU v1.3.0 released 
- <https://sympa.inria.fr/sympa/arc/starpu-announce/2019-03/msg00001.html>
##  Second release candidate for StarPU v1.3.0 available 
- <https://sympa.inria.fr/sympa/arc/starpu-announce/2019-03/msg00000.html>
##  StarPU v1.2.8 released 
- <https://sympa.inria.fr/sympa/arc/starpu-announce/2019-02/msg00002.html>
##  StarPU v1.2.7 released 
- <https://sympa.inria.fr/sympa/arc/starpu-announce/2019-02/msg00001.html>
##  First release candidate for StarPU v1.3.0 available 
- <https://sympa.inria.fr/sympa/arc/starpu-announce/2019-02/msg00000.html>
##  StarPU v1.2.6 released 
- <https://sympa.inria.fr/sympa/arc/starpu-announce/2018-09/msg00000.html>
##  StarPU v1.2.5 released 
- <https://sympa.inria.fr/sympa/arc/starpu-announce/2018-08/msg00000.html>
##  StarPU training course at Maison de la Simulation 
- <https://sympa.inria.fr/sympa/arc/starpu-announce/2018-03/msg00000.html>
##  StarPU: From SVN to GIT 
- <https://sympa.inria.fr/sympa/arc/starpu-announce/2018-01/msg00000.html>
##  implicit asynchronous partitioning landed in trunk! 
- <https://sympa.inria.fr/sympa/arc/starpu-announce/2017-12/msg00000.html>
##  Interest in StarPU Users' Days 
- <https://sympa.inria.fr/sympa/arc/starpu-announce/2017-11/msg00002.html>
##  StarPU v1.2.3 released 
- <https://sympa.inria.fr/sympa/arc/starpu-announce/2017-11/msg00000.html>
- <https://sympa.inria.fr/sympa/arc/starpu-announce/2017-11/msg00001.html>
- <https://sympa.inria.fr/sympa/arc/starpu-announce/2018-04/msg00000.html>
##  StarPU v1.2.2 released 
- <https://sympa.inria.fr/sympa/arc/starpu-announce/2017-05/msg00000.html>
##  Fwd: [hpc-announce] PATC training Runtime systems for heterogeneous platform programming, May 29-30, Maison de la Simulation (Saclay) 
- <https://sympa.inria.fr/sympa/arc/starpu-announce/2017-04/msg00000.html>
##  StarPU v1.1.7 released 
- <https://sympa.inria.fr/sympa/arc/starpu-announce/2017-03/msg00001.html>
##  StarPU v1.2.1 released 
- <https://sympa.inria.fr/sympa/arc/starpu-announce/2017-03/msg00000.html>
##  StarPU v1.1.6 released 
- <https://sympa.inria.fr/sympa/arc/starpu-announce/2016-08/msg00002.html>
##  StarPU v1.2.0 released 
- <https://sympa.inria.fr/sympa/arc/starpu-announce/2016-08/msg00001.html>
##  StarPU v1.2.0rc6 released 
- <https://sympa.inria.fr/sympa/arc/starpu-announce/2016-08/msg00000.html>
##  Engineer job offer: Improvement of a runtime system for the development of fast numerical methods. 
- <https://sympa.inria.fr/sympa/arc/starpu-announce/2016-04/msg00000.html>
##  StarPU v1.2.0rc5 released 
- <https://sympa.inria.fr/sympa/arc/starpu-announce/2015-12/msg00000.html>
##  StarPU v1.1.5 released 
- <https://sympa.inria.fr/sympa/arc/starpu-announce/2015-09/msg00000.html>
##  StarPU v1.2.0rc4 released 
- <https://sympa.inria.fr/sympa/arc/starpu-announce/2015-08/msg00000.html>
##  StarPU v1.2.0rc3 released 
- <https://sympa.inria.fr/sympa/arc/starpu-announce/2015-07/msg00000.html>
##  StarPU v1.2.0rc2 released 
- <https://sympa.inria.fr/sympa/arc/starpu-announce/2015-05/msg00000.html>
##  Fwd: [calcul] Formation PATC  quot;Runtime systems for heterogeneous platform programming quot;, 4-5 juin, Inria Bordeaux Sud-Ouest 
- <https://sympa.inria.fr/sympa/arc/starpu-announce/2015-04/msg00000.html>
##  StarPU v1.2.0rc1 released 
- <https://sympa.inria.fr/sympa/arc/starpu-announce/2015-03/msg00001.html>
##  StarPU v1.1.4 released 
- <https://sympa.inria.fr/sympa/arc/starpu-announce/2015-03/msg00000.html>
##  StarPU talk at maison de la simulation in Paris 
- <https://sympa.inria.fr/sympa/arc/starpu-announce/2014-11/msg00000.html>
##  StarPU v1.1.3 released 
- <https://sympa.inria.fr/sympa/arc/starpu-announce/2014-09/msg00000.html>
##  Task pipelining commited to trunk 
- <https://sympa.inria.fr/sympa/arc/starpu-announce/2014-08/msg00001.html>
##  fxt 0.3.0 released 
- <https://sympa.inria.fr/sympa/arc/starpu-announce/2014-08/msg00000.html>
##  StarPU v1.1.2 released 
- <https://sympa.inria.fr/sympa/arc/starpu-announce/2014-06/msg00000.html>
##  StarPU v1.1.1 released 
- <https://sympa.inria.fr/sympa/arc/starpu-announce/2014-04/msg00000.html>
##  starpu v1.1.0 released 
- <https://sympa.inria.fr/sympa/arc/starpu-announce/2013-12/msg00001.html>
##  starpu v1.1.0rc4 released 
- <https://sympa.inria.fr/sympa/arc/starpu-announce/2013-12/msg00000.html>
##  starpu v1.1.0rc3 released 
- <https://sympa.inria.fr/sympa/arc/starpu-announce/2013-11/msg00000.html>
##  starpu v1.1.0rc2 released 
- <https://sympa.inria.fr/sympa/arc/starpu-announce/2013-07/msg00000.html>
##  starpu v1.1.0rc1 released 
- <https://sympa.inria.fr/sympa/arc/starpu-announce/2013-05/msg00000.html>
##  C Languages Extensions for StarPU 
- <https://sympa.inria.fr/sympa/arc/starpu-announce/2013-04/msg00000.html>
## [plafrim-users] Summer School � Programming Heterogeneous Parallel Architectures � 
- <https://sympa.inria.fr/sympa/arc/starpu-announce/2013-03/msg00001.html>
##  [Fwd: Summer School « Programming Heterogeneous Parallel Architectures »] 
- <https://sympa.inria.fr/sympa/arc/starpu-announce/2013-03/msg00000.html>
##  StarPU v1.0.5 released 
- <https://sympa.inria.fr/sympa/arc/starpu-announce/2013-02/msg00000.html>
##  Temanejo support: graphically single-stepping tasks! 
- <https://sympa.inria.fr/sympa/arc/starpu-announce/2012-11/msg00001.html>
##  StarPU at SuperComputing'12 
- <https://sympa.inria.fr/sympa/arc/starpu-announce/2012-11/msg00000.html>
##  Problem in installation of StarPU 
- <https://sympa.inria.fr/sympa/arc/starpu-announce/2012-10/msg00001.html>
##  StarPU v1.0.4 released 
- <https://sympa.inria.fr/sympa/arc/starpu-announce/2012-10/msg00000.html>
##  StarPU v1.0.3 released 
- <https://sympa.inria.fr/sympa/arc/starpu-announce/2012-09/msg00000.html>
##  starpu v1.0.2 released 
- <https://sympa.inria.fr/sympa/arc/starpu-announce/2012-08/msg00000.html>
##  StarPU v1.0.1 released 
- <https://sympa.inria.fr/sympa/arc/starpu-announce/2012-05/msg00000.html>
##  StarPU v1.0.0 released 
- <https://sympa.inria.fr/sympa/arc/starpu-announce/2012-03/msg00002.html>
##  StarPU 1.0.0rc4 released 
- <https://sympa.inria.fr/sympa/arc/starpu-announce/2012-03/msg00001.html>
##  StarPU 1.0.0rc3 released 
- <https://sympa.inria.fr/sympa/arc/starpu-announce/2012-03/msg00000.html>
##  starpu v1.0.0rc2 released 
- <https://sympa.inria.fr/sympa/arc/starpu-announce/2012-02/msg00000.html>
##  starpu v1.0.0rc1 released 
- <https://sympa.inria.fr/sympa/arc/starpu-announce/2012-01/msg00000.html>

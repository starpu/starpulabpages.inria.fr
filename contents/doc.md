---
layout: default
title: Documentation
permalink: doc.html
full-width: True
---

# Documentation

The StarPU documentation is available in PDF and in HTML. Several [parts](https://files.inria.fr/starpu/doc/doc.html) are
available, depending on the level of details that is desired:

- An introduction ([HTML](https://files.inria.fr/starpu/doc/html_web_introduction/) [PDF](https://files.inria.fr/starpu/doc/starpu_web_introduction.pdf))
  is available to get a grasp on the principles in a couple of pages.
- Installation ([HTML](https://files.inria.fr/starpu/doc/html_web_installation/) [PDF](https://files.inria.fr/starpu/doc/starpu_web_installation.pdf))
  should be read if you need to install StarPU yourself.
- Basics ([HTML](https://files.inria.fr/starpu/doc/html_web_basics/) [PDF](https://files.inria.fr/starpu/doc/starpu_web_basics.pdf))
  is the *must-read* document that covers in a few dozen pages the basics of
  StarPU.
- Applications ([HTML](https://files.inria.fr/starpu/doc/html_web_applications/) [PDF](https://files.inria.fr/starpu/doc/starpu_web_applications.pdf))
  cover an application example.
- Performance ([HTML](https://files.inria.fr/starpu/doc/html_web_performances/) [PDF](https://files.inria.fr/starpu/doc/starpu_web_performances.pdf))
  covers performance optimization.
- The FAQ ([HTML](https://files.inria.fr/starpu/doc/html_web_faq/) [PDF](https://files.inria.fr/starpu/doc/starpu_web_faq.pdf))
  covers the frequently-asked questions and is also a must-check.
- Languages ([HTML](https://files.inria.fr/starpu/doc/html_web_languages/) [PDF](https://files.inria.fr/starpu/doc/starpu_web_languages.pdf))
  cover the bindings in Fortran, Java, Python, OpenMP.
- Extensions ([HTML](https://files.inria.fr/starpu/doc/html_web_extensions/) [PDF](https://files.inria.fr/starpu/doc/starpu_web_extensions.pdf))
  cover all the various non-basic features of StarPU.

To take with you on the beach:

- Everything altogether ([HTML](https://files.inria.fr/starpu/doc/html/) [PDF](https://files.inria.fr/starpu/doc/starpu.pdf))
  contains all the parts mentioned above in a single document.

And for StarPU hackers:

- The developer's internal documentation ([HTML](https://files.inria.fr/starpu/doc/html_dev/) [PDF](https://files.inria.fr/starpu/doc/starpu_dev.pdf))
  is also available.

Please note that these documents are up-to-date with the latest
official release of StarPU.

## Master
The documentation for the master version, is available [here](https://files.inria.fr/starpu/testing/master/).

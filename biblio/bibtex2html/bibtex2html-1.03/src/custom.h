/*************************************************************************
 * custom.h - from bibtex2html distribution
 *
 * $Id: custom.h,v 2.4 2002/06/20 07:50:08 greg Exp $
 *
 * Copyright � INRIA 2002, Gregoire Malandain
 *
 * The purpose of this software is to automatically produce html pages from 
 * bibtex files, and to provide access to the bibtex entries by several 
 * criteria: year of publication, category of publication and author name, 
 * from an index page. 
 * see http://www-sop.inria.fr/epidaure/personnel/malandain/codes/bibtex2html.html
 *
 * AUTHOR:
 * Gregoire Malandain (greg@sophia.inria.fr)
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * CREATION DATE: 
 * 
 *
 * ADDITIONS, CHANGES
 *
 */



#ifndef _custom_h_
#define _custom_h_

#ifdef __cplusplus
extern "C" {
#endif



#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <custom-common.h>

#include <custom-environ.h>
#include <custom-layout.h>
#include <custom-bibitem.h>


extern void load_default_style( typeArgDescription *customization_strings,
			 typeSpecificFileDescription *single_output,
			 typeSpecificFileDescription *index,
			 typeSpecificFileDescription *author,
			 typeSpecificFileDescription *category,
			 typeSpecificFileDescription *keyword,
			 typeSpecificFileDescription *reduced_year,
			 typeSpecificFileDescription *complete_year,
			 typeSpecificFileDescription *complete_biblio,
			 typeSpecificFileDescription *default_desc );

extern int get_nb_description_arguments( typeArgDescription *t );

extern typeArgDescription * build_customization_strings( typeEnvironmentDescription *environ,
						  typeGenericFileDescription *generic_file,
						  typeSpecificFileDescription *single_output_file,
						  typeSpecificFileDescription *index_file,
						  typeSpecificFileDescription *author_file,
						  typeSpecificFileDescription *category_file,
						  typeSpecificFileDescription *keyword_file,
						  typeSpecificFileDescription *reduced_year_file,
						  typeSpecificFileDescription *complete_year_file,
						  typeSpecificFileDescription *complete_biblio_file,
						  typeSpecificFileDescription *default_file,
						  typeBibtexItemDescription *bibitem );


extern int check_directory( char *dir );
extern int make_directory( char *dir );


extern void check_all_icons( typeEnvironmentDescription *e,
			 typeBibtexItemDescription *b, char *dir );

extern void copy_all_icons( typeEnvironmentDescription *e,
			typeBibtexItemDescription *b );

extern void print_customization_strings( FILE *fd, typeArgDescription *d, 
					 int print_null_string );
extern void print_customization_strings_keys( FILE *fd, typeArgDescription *d );

extern void set_verbose_in_custom();
extern void unset_verbose_in_custom();

#ifdef __cplusplus
}
#endif

#endif

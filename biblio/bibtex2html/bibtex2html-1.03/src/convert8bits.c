#include <stdio.h>
#include <stdlib.h>
#include <string.h>


static int _verbose_ = 1;
static char *options = "";
static char *details = "";

static void usage( char *prg, char *str, int flag )
{
  fprintf( stderr, "\n" );
  fprintf( stderr, "%s:\n%s\n", prg, options );
  if ( flag ) 
    fprintf( stderr, "%s\n", details );
  if ( str != NULL ) 
    fprintf( stderr, "Error: %s\n", str );
}


int main( int argc, char *argv[] )
{
  FILE *fin, *fout;
  int i, c, d;
  int l = 0;
  char *filein = (char*)NULL;
  char *fileout = (char*)NULL;


  /**************************************************
   *
   * no arg, print help
   * 
   **************************************************/

  if ( argc == 1 ) {
    usage( argv[0], NULL, 0 );
    return( 0 );
  }
  for ( i=1; i<argc; i++ ) {

    if ( argv[i][0] == '-' ) {
      
      if ( strcmp ( argv[i], "-help" ) == 0  || 
	   strcmp ( argv[i], "--help" ) == 0 ) {
	usage( argv[0], NULL, 1 );
	return( 0 );
      }
      else if ( strcmp ( argv[i], "-verbose" ) == 0 || 
	   (strcmp ( argv[i], "-v" ) == 0 && argv[i][2] == '\0') ) {
	_verbose_ = 1;
      }
      else if ( strcmp ( argv[i], "-no-verbose" ) == 0 || 
	   (strcmp ( argv[i], "-nv" ) == 0 && argv[i][3] == '\0') ) {
	_verbose_ = 0;
      }
    }

    else {
      if ( filein == (char*)NULL ) {
	filein = argv[i];
      }
      else {
	if ( fileout == (char*)NULL ) {
	  fileout = argv[i];
	}
	else {
	  usage( argv[0], "too much file name when parsing", 0 );
	  return( 0 );
	}
      }
    }
    
  }
  
  if ( filein == (char*)NULL || fileout == (char*)NULL ) {
    usage( argv[0], "not enough file name when parsing", 0 );
    return( 0 );
  }
  

  fin = fopen ( argv[1], "r" );

  if ( fin == NULL ) {
    fprintf( stderr, "error when opening '%s'\n", argv[1] );
    usage( argv[0], "error when opening input file", 0 );
    return( 0 );
  }
    
  fout = fopen ( argv[2], "w" );

  if ( fout == NULL ) {
    fprintf( stderr, "error when opening '%s'\n", argv[1] );
    usage( argv[0], "error when opening output file", 0 );
    return( 0 );
  }
  
  while ( (c=fgetc( fin )) != EOF ) {

    if ( c == '\n' ) l++;

    if ( c >= 0 && c <= 127 ) {
      fputc( c, fout );
    }
    else {

      switch ( c ) {
	
      case 177 : /* +/- */
	fputs( "+/-", fout );
	break;

      case 192 : fputs( "\\`A" , fout ); break;
      case 193 : fputs( "\\'A" , fout ); break;
      case 194 : fputs( "\\^A" , fout ); break;
      case 195 : fputs( "\\~A" , fout ); break;
      case 196 : fputs( "\\\"A" , fout ); break;

      case 200 : fputs( "\\`E" , fout ); break;
      case 201 : fputs( "\\'E" , fout ); break;
      case 202 : fputs( "\\^E" , fout ); break;
      case 203 : fputs( "\\\"E" , fout ); break;

      case 204 : fputs( "\\`I" , fout ); break;
      case 205 : fputs( "\\'I" , fout ); break;
      case 206 : fputs( "\\^I" , fout ); break;
      case 207 : fputs( "\\\"I" , fout ); break;

      case 210 : fputs( "\\`O" , fout ); break;
      case 211 : fputs( "\\'O" , fout ); break;
      case 212 : fputs( "\\^O" , fout ); break;
      case 213 : fputs( "\\~O" , fout ); break;
      case 214 : fputs( "\\\"O" , fout ); break;

      case 217 : fputs( "\\`U" , fout ); break;
      case 218 : fputs( "\\'U" , fout ); break;
      case 219 : fputs( "\\^U" , fout ); break;
      case 220 : fputs( "\\\"U" , fout ); break;

      case 224 : fputs( "\\`a" , fout ); break;
      case 225 : fputs( "\\'a" , fout ); break;
      case 226 : fputs( "\\^a" , fout ); break;
      case 227 : fputs( "\\~a" , fout ); break;
      case 228 : fputs( "\\\"a" , fout ); break;

      case 231 : fputs( "\\c{c}" , fout ); break;

      case 232 : fputs( "\\`e" , fout ); break;
      case 233 : fputs( "\\'e" , fout ); break;
      case 234 : fputs( "\\^e" , fout ); break;
      case 235 : fputs( "\\\"e" , fout ); break;

      case 236 : fputs( "\\`i" , fout ); break;
      case 237 : fputs( "\\'i" , fout ); break;
      case 238 : fputs( "\\^i" , fout ); break;
      case 239 : fputs( "\\\"i" , fout ); break;

      case 241 : fputs( "\\~n" , fout ); break;

      case 242 : fputs( "\\`o" , fout ); break;
      case 243 : fputs( "\\'o" , fout ); break;
      case 244 : fputs( "\\^o" , fout ); break;
      case 246 : fputs( "\\\"o" , fout ); break;

      case 249 : fputs( "\\`u" , fout ); break;
      case 250 : fputs( "\\'u" , fout ); break;
      case 251 : fputs( "\\^u" , fout ); break;
      case 252 : fputs( "\\\"u" , fout ); break;

      case 253 : fputs( "\\'y" , fout ); break;

      default :
	fprintf( stdout, "at line #%d: '%c'='\\%d'='\\%x'\n", l+1, c, c, c );
	fprintf( stdout, "        not changed\n" );
	fputc( c, fout );
	break;

      }

    }
  }

  fclose( fout );
  fclose( fin );
    
  return( 1 );
}
 

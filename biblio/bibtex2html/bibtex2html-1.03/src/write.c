/*************************************************************************
 * write.c - from bibtex2html distribution
 *
 * $Id: write.c,v 2.37 2004/07/20 15:19:35 greg Exp $
 *
 * Copyright � INRIA 2001, Gregoire Malandain
 *
 * The purpose of this software is to automatically produce html pages from 
 * bibtex files, and to provide access to the bibtex entries by several 
 * criteria: year of publication, category of publication and author name, 
 * from an index page. 
 * see http://www-sop.inria.fr/epidaure/personnel/malandain/codes/bibtex2html.html
 *
 * AUTHOR:
 * Gregoire Malandain (greg@sophia.inria.fr)
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * CREATION DATE: 
 * 
 *
 * ADDITIONS, CHANGES
 *
 */


/* for time() in write_ctime()
 */
#include <time.h>




#include <write.h>
#include <read.h>

static int _debug_ = 0;
static int _verbose_ = 1;
static int _verbose_on_stderr_ = 1;


/* static int _do_write_all_fields_ = 0; */
/*
static int _do_use_icons_ = 0;
*/


void print_bibtex_item( FILE *f, typeItem *item );
void print_bibtex_item_from_bibtex_entry( FILE *f, typeItem *item );








/****************************************
 *
 * misc
 *
 ****************************************/

int field_contains_string( char *f, char *s )
{
  char *tmp = f;
  int l;

  if ( f == NULL || *f == '\0' || s == NULL || *s == '\0' )
    return( 0 );
  l = strlen ( s );
  do {
    while ( strncasecmp( tmp, s, 1 ) != 0 && *tmp != '\0' ) tmp ++;
    if ( *tmp == '\0' ) return( 0 );
    if ( (int)strlen( tmp ) <  l ) return( 0 );
    if ( ( tmp == f 
	   || ( ( tmp[-1] < 'a' ||  tmp[-1] > 'z' )
		&& ( tmp[-1] < 'A' ||  tmp[-1] > 'Z' )
		&& ( tmp[-1] < '0' ||  tmp[-1] > '9' ) ) )
	 && strncasecmp( tmp, s, l ) == 0 ) return( 1 );
    tmp ++;
  } while ( *tmp != '\0' );
  return( 0 );
}


/****************************************
 *
 * basic writing procedures
 *
 ****************************************/

int write_string_or_file( FILE *fd, char *s )
{
  char *proc = "write_string_or_file";
  FILE *f;
  char str[ STD_STR_LENGTH ];
  char *t = s;
  int may_be_a_file = 1;
  int i, l, n;

  if ( fd == NULL ) {
    if ( _verbose_ )
      fprintf( stderr, "%s: try to write into (FILE*)NULL ?!\n", proc );
    return( -1 );
  }
  
  if ( s == NULL ) return( 0 );
  if ( s[0] == '\0' ) return( 0 );
  l = strlen( s );
  if ( l <= 0 ) return( 0 );

  for ( t = s, n = 0, i = 0; i < l && may_be_a_file == 1; t ++, i++ ) {
    switch ( *t ) {
    default :
      if ( ('a' <= *t && *t <= 'z') ||
	   ('A' <= *t && *t <= 'Z') )
	n++;
      break;
    case ' ' :
    case ';' :
    case '>' :
    case '<' :
    case '\\' :
    case '&' :
    case '\'' :
    case '"' :
    case '\n' :
    case '\t' :
      may_be_a_file = 0;
    }
  }

  if ( n == 0 ) may_be_a_file = 0;

  if ( may_be_a_file == 1 && (f = fopen( s, "r" )) != NULL ) {
    while ( fgets( str, STD_STR_LENGTH, f ) != NULL ) {
      fprintf( fd, "%s", str );
    }
    fclose( f );
  } 
  else {
    /* fprintf( fd, s ); */
    for ( t = s, i = 0; i < l; t ++, i++ ) {
      switch ( *t ) {
      default : fprintf( fd, "%c", *t ); break;
      case '\\' :
	t ++;   i ++; 
	switch( *t ) {
	default : fprintf( fd, "\\%c", *t ); break;
	case 'n' : fprintf( fd, "\n" ); break;
	case 't' : fprintf( fd, "\t" ); break;
	}
      }
    }
  }
  
  return( 1 );
}

void write_item_customized_string( FILE *fd, 
				   char *string,
				   typeTagElement *e,
				   char *endstring )
{
  if ( string == NULL || string == '\0' ) return;
  write_string_or_file( fd, e->start );
  write_string_or_file( fd, string );
  write_string_or_file( fd, e->end );
  if ( endstring != NULL ) fprintf( fd, "%s", endstring );
}










/****************************************
 *
 * main writing procedures
 *
 ****************************************/



int write_all_authors( FILE *index, typeListOfItems *li, 
		       typeListOfNames *ln, 
		       typeEnvironmentDescription *envdesc,
		       typeGenericFileDescription *gfiledesc,
		       typeSpecificFileDescription *sfiledesc,
		       typeBibtexItemDescription *itemdesc,
		       int write_bibtex_entry )
{
  char *proc = "write_all_authors";
  FILE *fd = NULL;
  char filename[256];
  char str_file[256];
  char str_page[256];
  
  int n, a;
  int i;
  int is_author;
  int last_anchor='A'-1;
  int nb_names = 0;
  int anchor_has_to_be_set[27];

  enumTypeCategory category = CAT_UNKNOWN;
  enumTypeCategory current_category = CAT_UNKNOWN;

  if ( !sfiledesc->create_pages ) return( 0 );
  if ( li->n == 0 || ln->n == 0 ) return( 0 );

  

  /* sort the items
  */  
  if ( li->sort[0] != _CATEGORY_ )
    sort_list_of_items( li, _CATEGORY_ );
  


  /*---------------------------------------
    - INDEX MANAGEMENT (START)
    ---------------------------------------*/

  for (i=0; i<27; i++ ) anchor_has_to_be_set[i] = 0;

  if ( index != NULL 
       && sfiledesc->build_index
       && sfiledesc->build_initials_index
       && envdesc->use_html_links_between_pages ) {

    /* what are the existing initials ?
     */
    for ( a=0; a<ln->n; a++ ) {

      /*
      if ( ln->name[a].nreferences <= 0 
	 && strcmp( ln->name[a].keyName, ln->name[a].equivKeyName ) == 0 )
	continue;
      */

      switch ( ln->name[a].status ) {
      default :
	continue;
      case TO_BE_INDEXED :
	break;
      }
      if ( strcmp(ln->name[a].keyName, key_unknown_name) != 0 )
	anchor_has_to_be_set[ ln->name[a].keyName[0] - 'A' ] = 1;
      else
	anchor_has_to_be_set[ 26 ] = 1;
    }

    write_string_or_file( index, sfiledesc->initials.table.start );
    for ( a=0, i='A'; i<='Z'; a++, i++ ) {
      if ( a % sfiledesc->initials.cells_per_row == 0 ) {
	if ( a > 0 )
	  write_string_or_file( index, sfiledesc->initials.row.end );
	write_string_or_file( index, sfiledesc->initials.row.start );
      }
      write_string_or_file( index, sfiledesc->initials.cell.start );
      /* this is perhaps not necessary to
	 prefix the anchor with the file name
	 fprintf( index, "<a href=\"%s.%s#", envdesc->filename_index, envdesc->filename_extension );
      */
      if ( anchor_has_to_be_set[ i - 'A' ] ) {
	fprintf( index, "<a href=\"#" );
	if ( sfiledesc->anchor_prefix != NULL && sfiledesc->anchor_prefix[0] != '\0' )
	  fprintf( index, "%s", sfiledesc->anchor_prefix );
	fprintf( index, "%c\">", i );
      }
      fprintf( index, "%c", i );
      if ( anchor_has_to_be_set[ i - 'A' ] ) fprintf( index, "</a>" );
      write_string_or_file( index, sfiledesc->initials.cell.end );
    }

    if ( a % sfiledesc->initials.cells_per_row == 0 ) {
      if ( a > 0 )
	write_string_or_file( index, sfiledesc->initials.row.end );
      write_string_or_file( index, sfiledesc->initials.row.start );
    }
    
    /* this is to handle the case of no author
       yielding an '?' in the alphabetical table
     */
    write_string_or_file( index, sfiledesc->initials.cell.start );
    /* this is perhaps not necessary to
       prefix the anchor with the file name
       fprintf( index, "<a href=\"%s.%s#", envdesc->filename_index, envdesc->filename_extension );
    */
    if ( anchor_has_to_be_set[ 26 ] ) {
      fprintf( index, "<a href=\"#" );
      if ( sfiledesc->anchor_prefix != NULL && sfiledesc->anchor_prefix[0] != '\0' )
	fprintf( index, "%s", sfiledesc->anchor_prefix );
      fprintf( index, "UNKNOWN\">" );
    }
    if ( anchor_has_to_be_set[ 26 ] ) fprintf( index, "</a>" );
    a++;
    write_string_or_file( index, sfiledesc->initials.cell.end );

    /* finish the table
     */
    while ( a % sfiledesc->initials.cells_per_row != 0 ) {
      a++;
      if ( sfiledesc->initials.empty_cell.start != NULL )
	write_string_or_file( index, sfiledesc->initials.empty_cell.start );
      else
	write_string_or_file( index, sfiledesc->initials.cell.start );
      if ( sfiledesc->initials.empty_cell.end != NULL )
	write_string_or_file( index, sfiledesc->initials.empty_cell.end );
      else
	write_string_or_file( index, sfiledesc->initials.cell.end );
    }
    write_string_or_file( index, sfiledesc->initials.table.end );
  }

  /*---------------------------------------
    - INDEX MANAGEMENT (END)
    ---------------------------------------*/



  /* let's go
     process all names
  */
  for ( nb_names = 0, a=0; a<ln->n; a++ ) {

    /*
      if ( ln->name[a].nreferences <= 0 
	 && strcmp( ln->name[a].keyName, ln->name[a].equivKeyName ) == 0 )
      continue;
    */
    switch ( ln->name[a].status ) {
    default :
      continue;
    case PAGE_TO_BE_GENERATED :
    case TO_BE_INDEXED :
      break;
    }
    


    /*---------------------------------------
      - AUTHOR FILE MANAGEMENT (START)
      ---------------------------------------*/

    sprintf( filename, "%s/%s.%s", 
	     envdesc->directory_authors, ln->name[a].equivKeyName,
	     envdesc->filename_extension );

    /* create the file for real
     */
    if ( strcmp( ln->name[a].keyName, ln->name[a].equivKeyName ) == 0 ) {

      fd = fopen( filename, "w" );
      if ( fd == NULL ) {
	if ( _verbose_ ) {
	  fprintf( stderr, "%s: unable to open file '%s' for writing\n", proc, filename );
	}
	return( 0 );
      }
      
      if ( _verbose_ ) {
	if ( _verbose_on_stderr_ )
	  fprintf( stderr, "%s: open '%s' for writing     \r", proc, filename );
	else
	  fprintf( stdout, "%s: open '%s' for writing     \n", proc, filename );
      }
      
      /* file and page titles
       */
      if ( !write_bibtex_entry ) {
	if ( strcmp(ln->name[a].keyName, key_unknown_name) != 0 ) {
	  sprintf( str_file, sfiledesc->file_title, ln->name[a].firstName, ln->name[a].lastName );
	  sprintf( str_page, sfiledesc->page_title, ln->name[a].firstName, ln->name[a].lastName );
	  write_begin_of_file( fd, gfiledesc, sfiledesc, str_file, str_page );
	}
	else {
	  write_begin_of_file( fd, gfiledesc, sfiledesc, sfiledesc->default_file_title, 
			       sfiledesc->default_page_title );
	}
      }
      
      current_category = CAT_UNKNOWN;    
      for (i=0; i<li->n; i++ ) {
	
	/* test if author:
	   it can be an author or an editor
	*/
	is_author = 0;
	for ( n=0; is_author == 0 && n<li->item[i]->nb_authors; n++ ) {
	  if ( strcmp( li->item[i]->authors_names[n].equivKeyName, 
		       ln->name[a].keyName ) == 0 )
	    is_author = 1;
	}
	
	switch ( li->item[i]->type_item ) {
	case BOOK :
	case INBOOK :
	  if ( li->item[i]->nb_authors > 0 ) break;
	case PROCEEDINGS :
	for ( n=0; is_author == 0 && n<li->item[i]->nb_editors; n++ ) {
	  if ( strcmp( li->item[i]->editors_names[n].equivKeyName, 
		       ln->name[a].keyName ) == 0 )
	    is_author = 1;
	}
	break;
	default :
	  break;
	}
	
	if ( is_author == 0 ) continue;
	
	
	/* ok, we've got an author
	   see if we've got a new category
	   if yes, we may have to end the previous list
	*/
	category = li->item[i]->type_cat;
	
	if ( category != current_category ) {
	  
	  if ( !write_bibtex_entry ) {
	    if ( current_category != CAT_UNKNOWN )
	      write_string_or_file( fd, gfiledesc->list_tag.end );
	    
	    write_string_or_file( fd, gfiledesc->page_subtitle_tag.start );
	    if ( envdesc->use_html_links_between_pages 
		 && envdesc->use_html_links_towards_categories_pages )
	      fprintf( fd, "<a href=\"../%s/%s.%s\">", 
		       envdesc->directory_categories, filename_category[category],
		       envdesc->filename_extension );
	    fprintf( fd,  desc_category[category] );
	    if ( envdesc->use_html_links_between_pages 
		 && envdesc->use_html_links_towards_categories_pages )
	      fprintf( fd, "</a>" );
	    write_string_or_file( fd, gfiledesc->page_subtitle_tag.end );
	  
	    write_string_or_file( fd, gfiledesc->list_tag.start );
	  }
	  
	  current_category = category;
	}
	
	write_item( fd, li->item[i],
		    envdesc, gfiledesc, sfiledesc, itemdesc, write_bibtex_entry );
	
      } /* for (i=0; i<li->n; i++ ) */
      
      if ( fd != NULL ) {
	if ( !write_bibtex_entry ) {
	  write_string_or_file( fd, gfiledesc->list_tag.end );
	  write_end_of_file( fd, gfiledesc, sfiledesc );
	}
	fclose( fd );
      }
    }

    /*---------------------------------------
      - AUTHOR FILE MANAGEMENT (END)
      ---------------------------------------*/

    /*---------------------------------------
      - INDEX MANAGEMENT (START)
      ---------------------------------------*/

    if ( index != NULL 
	 && sfiledesc->build_index
	 && ln->name[a].status == TO_BE_INDEXED ) {
      

      /* if the first letter of the current name IS NOT the current first letter
	 we have to
	 1/ end the table (if any)
	 2/ put anchors (if needed) for the first letters
	    between the current one and the one of the current name
	 3/ begin a new table
      */

      if ( (ln->name[a].keyName[0] != last_anchor) ||
	   (strcmp(ln->name[a].keyName, key_unknown_name) == 0) ) {
	
	if ( nb_names > 0 ) {
	  while( (nb_names) % sfiledesc->index.cells_per_row != 0 ) {
	    nb_names ++;
	    if ( sfiledesc->index.empty_cell.start != NULL )
	      write_string_or_file( index, sfiledesc->index.empty_cell.start );
	    else
	      write_string_or_file( index, sfiledesc->index.cell.start );
	    if ( sfiledesc->index.empty_cell.end != NULL )
	      write_string_or_file( index, sfiledesc->index.empty_cell.end );
	    else
	      write_string_or_file( index, sfiledesc->index.cell.end );
	  } 
	  write_string_or_file( index, sfiledesc->index.row.end );
	} 
	else {
	  write_string_or_file( index, sfiledesc->index.table.start );
	}


	/* counts the name for this letter
	 */
	nb_names = 1;
      }
      
      /* otherwise we get a name for the current first letter
       */
      else {
	nb_names ++;
      }


      /* put the current name in the table
	 see if the row is complete
	 -> see if previous the row must be ended 
	 -> see if the first column contains first letters
	 and so on
      */
      if ( nb_names % sfiledesc->index.cells_per_row == 1 ) {

	if ( nb_names > 1 )
	  write_string_or_file( index, sfiledesc->index.row.end );
	
	write_string_or_file( index, sfiledesc->index.row.start );

	if ( sfiledesc->put_initials_in_index != 0 ) {
	  if ( nb_names == 1 ) {
	    write_string_or_file( index, sfiledesc->index.extra_cell.start );
	    if ( strcmp(ln->name[a].keyName, key_unknown_name) != 0 )
	      fprintf( index, "%c", ln->name[a].keyName[0] );
	    else
	      fprintf( index, "?" );
	    write_string_or_file( index, sfiledesc->index.extra_cell.end );
	  }
	  else {
	    if ( sfiledesc->index.empty_extra_cell.start != NULL )
	      write_string_or_file( index, sfiledesc->index.empty_extra_cell.start );
	    else
	      write_string_or_file( index, sfiledesc->index.extra_cell.start );
	    if ( sfiledesc->index.empty_extra_cell.end != NULL )
	      write_string_or_file( index, sfiledesc->index.empty_extra_cell.end );
	    else
	      write_string_or_file( index, sfiledesc->index.extra_cell.end );
	  }
	}
      }
      
      /* write the name in the index
	 write the anchors here
       */
      write_string_or_file( index, sfiledesc->index.cell.start );
      if ( index != NULL 
	   && sfiledesc->build_index
	   && ln->name[a].status == TO_BE_INDEXED 
	   && nb_names == 1 ) {
	if ( strcmp(ln->name[a].keyName, key_unknown_name) != 0 ) {
	  last_anchor = ln->name[a].keyName[0];
	  if ( envdesc->use_html_links_between_pages && sfiledesc->build_initials_index ) {
	    if ( sfiledesc->anchor_prefix == NULL || sfiledesc->anchor_prefix[0] == '\0' ) 
	      fprintf( index, "<a name=\"%c\"></a>", last_anchor );
	    else 
	      fprintf( index, "<a name=\"%s%c\"></a>", sfiledesc->anchor_prefix, last_anchor );
	  }
	} 
	else {
	  if ( envdesc->use_html_links_between_pages && sfiledesc->build_initials_index ) {
	    if ( sfiledesc->anchor_prefix == NULL || sfiledesc->anchor_prefix[0] == '\0' ) 
	      fprintf( index, "<a name=\"UNKNOWN\"></a>" );
	    else 
	      fprintf( index, "<a name=\"%sUNKNOWN\"></a>", sfiledesc->anchor_prefix );
	  }
	}
      } /* end of anchor writing */
      if ( envdesc->use_html_links_between_pages )
	fprintf( index, "<a href=\"%s\">", filename );
      write_item_customized_string( index, ln->name[a].firstName,
				    &(sfiledesc->index.arg1_in_cell), NULL );
      if ( ln->name[a].firstName != NULL && ln->name[a].firstName[0] != '\0' 
	   && ln->name[a].lastName != NULL && ln->name[a].lastName[0] != '\0' )
	fprintf( index, " " );
      write_item_customized_string( index, ln->name[a].lastName,
				    &(sfiledesc->index.arg2_in_cell), NULL );
      if ( envdesc->use_html_links_between_pages )
	fprintf( index, "</a>" );
      write_string_or_file( index, sfiledesc->index.cell.end );
    }

    /*---------------------------------------
      - INDEX MANAGEMENT (END)
      ---------------------------------------*/

  } /* for ( nb_names = 0, a=0; a<ln->n; a++ ) */



  if ( _verbose_ ) {
    if ( _verbose_on_stderr_ )
      fprintf( stderr, "\n" );
    else
      fprintf( stdout, "\n" );
  }

  

  /*---------------------------------------
    - INDEX MANAGEMENT (START)
    ---------------------------------------*/
  
  if ( index != NULL && sfiledesc->build_index ) {

    /* complete the current row 
       and the current table
    */
    while( nb_names % sfiledesc->index.cells_per_row != 0 ) {
      nb_names ++;
      write_string_or_file( index, sfiledesc->index.cell.start );
      write_string_or_file( index, sfiledesc->index.cell.end );
    }
    write_string_or_file( index, sfiledesc->index.row.end );
    write_string_or_file( index, sfiledesc->index.table.end );

    fprintf( index, "\n" );
  }

  /*---------------------------------------
    - INDEX MANAGEMENT (END)
    ---------------------------------------*/

  return( 1 );
}










/*
 flag: allows links
*/
int write_all_keywords( FILE *index, typeListOfItems *li, 
			typeListOfNames *ln, 
			typeEnvironmentDescription *envdesc,
			typeGenericFileDescription *gfiledesc,
			typeSpecificFileDescription *sfiledesc,
			typeBibtexItemDescription *itemdesc,
		       int write_bibtex_entry )
{
  char *proc = "write_all_keywords";
  FILE *fd = NULL;
  char filename[256];
  char str_file[256];
  char str_page[256];
  
  int n, a;
  int i, j;
  int is_keyword;
  int last_anchor='0'-1;
  int nb_keywords = 0;
  int anchor_has_to_be_set[26];

  //enumTypeCategory category = CAT_UNKNOWN;
  //enumTypeCategory current_category = CAT_UNKNOWN;
  int year = 0;
  int current_year = 0;

  if ( !sfiledesc->create_pages ) return( 0 );
  if ( li->n == 0 || ln->n == 0 ) return( 0 );



  /* sort the items
  */
//  if ( li->sort[0] != _CATEGORY_ )
  //  sort_list_of_items( li, _CATEGORY_ );
    sort_list_of_items( li, _YEAR_ );



  /*---------------------------------------
    - INDEX MANAGEMENT (START)
    ---------------------------------------*/
  for (i=0; i<26; i++ ) anchor_has_to_be_set[i] = 0;

  if ( index != NULL 
       && sfiledesc->build_index
       && sfiledesc->build_initials_index
       && envdesc->use_html_links_between_pages ) {
    
    /* what are the existing initials ?
     */
    for ( a=0; a<ln->n; a++ ) {
      
      switch ( ln->name[a].status ) {
      default :
	continue;
      case TO_BE_INDEXED :
	break;
      }
      if ( strcmp(ln->name[a].keyName, key_unknown_name) != 0 )
	if ( ln->name[a].keyName[0] >= 'A' && ln->name[a].keyName[0] <= 'Z' )
	  anchor_has_to_be_set[ ln->name[a].keyName[0] - 'A' ] = 1;
    }

    write_string_or_file( index, sfiledesc->initials.table.start );
    for ( a=0, i='A'; i<='Z'; a++, i++ ) {
      if ( a % sfiledesc->initials.cells_per_row == 0 ) {
	if ( a > 0 )
	  write_string_or_file( index, sfiledesc->initials.row.end );
	write_string_or_file( index, sfiledesc->initials.row.start );
      }
      write_string_or_file( index, sfiledesc->initials.cell.start );
      /* this is perhaps not necessary to
	 prefix the anchor with the file name
	 fprintf( index, "<a href=\"%s.%s#", envdesc->filename_index, envdesc->filename_extension );
      */
      if ( anchor_has_to_be_set[ i - 'A' ] ) {
	fprintf( index, "<a href=\"#" );
	if ( sfiledesc->anchor_prefix != NULL && sfiledesc->anchor_prefix[0] != '\0' )
	  fprintf( index, "%s", sfiledesc->anchor_prefix );
	fprintf( index, "%c\">", i );
      }
      fprintf( index, "%c", i );
      if ( anchor_has_to_be_set[ i - 'A' ] ) fprintf( index, "</a>" );
      write_string_or_file( index, sfiledesc->initials.cell.end );
    }
    
    /* finish the table
     */
    while ( a % sfiledesc->initials.cells_per_row != 0 ) {
      a++;
      if ( sfiledesc->initials.empty_cell.start != NULL )
	write_string_or_file( index, sfiledesc->initials.empty_cell.start );
      else
	write_string_or_file( index, sfiledesc->initials.cell.start );
      if ( sfiledesc->initials.empty_cell.end != NULL )
	write_string_or_file( index, sfiledesc->initials.empty_cell.end );
      else
	write_string_or_file( index, sfiledesc->initials.cell.end );
    }
    write_string_or_file( index, sfiledesc->initials.table.end );
  }

  /*---------------------------------------
    - INDEX MANAGEMENT (END)
    ---------------------------------------*/



  /* let's go
     process all keywords
  */
  for ( nb_keywords = 0, a=0; a<ln->n; a++ ) {

    switch ( ln->name[a].status ) {
    default :
      continue;
    case PAGE_TO_BE_GENERATED :
    case TO_BE_INDEXED :
      break;
    }
    


    /*---------------------------------------
      - KEYWORD FILE MANAGEMENT (START)
      ---------------------------------------*/

    sprintf( filename, "%s/%s.%s", 
	     envdesc->directory_keywords, ln->name[a].equivKeyName,
	     envdesc->filename_extension );

    /* create the file for real
     */
    if ( strcmp( ln->name[a].keyName, ln->name[a].equivKeyName ) == 0 ) {

      fd = fopen( filename, "w" );
      if ( fd == NULL ) {
	if ( _verbose_ ) {
	  fprintf( stderr, "%s: unable to open file '%s' for writing\n", proc, filename );
	}
	return( 0 );
      }
      
      if ( _verbose_ ) {
	if ( _verbose_on_stderr_ )
	  fprintf( stderr, "%s: open '%s' for writing     \r", proc, filename );
	else
	  fprintf( stdout, "%s: open '%s' for writing     \n", proc, filename );
      }
      
      /* file and page titles
       */
      if ( !write_bibtex_entry ) {
	sprintf( str_file, sfiledesc->file_title, ln->name[a].lastName );
	sprintf( str_page, sfiledesc->page_title, ln->name[a].lastName );
	write_begin_of_file( fd, gfiledesc, sfiledesc, str_file, str_page );
      }
      
      





      current_year = 0;
      for (i=0; i<li->n; i++ ) {
	
	/* test if keyword is present in list of keywords

	 */
	is_keyword = 0;
	for ( n=0; is_keyword == 0 && n<li->item[i]->nb_keywords; n++ ) {
	  if ( compare_key_names( li->item[i]->keywords[n].equivKeyName, 
				  ln->name[a].keyName ) == 0 )
	    is_keyword = 1;
	}
	
	/* test if keyword is present in
	   - title 
	   - abstract
	   - keywords
	*/
	if ( is_keyword == 0 && field_contains_string( li->item[i]->field[TITLE], ln->name[a].lastName ) == 1 )
	  is_keyword = 1;
	if ( is_keyword == 0 && field_contains_string( li->item[i]->field[ABSTRACT], ln->name[a].lastName ) == 1 )
	  is_keyword = 1;
	for ( j=0; is_keyword == 0 && j < li->item[i]->nb_keywords; j++ ) {
	  if ( field_contains_string( li->item[i]->keywords[j].lastName, ln->name[a].lastName ) == 1 )
	    is_keyword = 1;
	}
	

	if ( is_keyword == 0 ) continue;
	
		
	/* ok, we've got an keyword
	   see if we've got a new category
	   if yes, we may have to end the previous list
	*/
	year = li->item[i]->year;
	
	if ( year != current_year ) {
	  
	  if ( !write_bibtex_entry ) {
	    if ( current_year != 0 )
	      write_string_or_file( fd, gfiledesc->list_tag.end );
	    
	    write_string_or_file( fd, gfiledesc->page_subtitle_tag.start );
//	    if ( envdesc->use_html_links_between_pages 
//		 && envdesc->use_html_links_towards_categories_pages )
//	      fprintf( fd, "<a href=\"../%s/%s.%s\">", 
//		       envdesc->directory_categories, filename_category[category],
//		       envdesc->filename_extension );
	    fprintf( fd,  "Year %d", year );
	    if ( envdesc->use_html_links_between_pages 
		 && envdesc->use_html_links_towards_categories_pages )
	      fprintf( fd, "</a>" );
	    write_string_or_file( fd, gfiledesc->page_subtitle_tag.end );
	    
	    write_string_or_file( fd, gfiledesc->list_tag.start );
	  }

	  current_year = year;
	}
	
	write_item( fd, li->item[i],
		    envdesc, gfiledesc, sfiledesc, itemdesc, write_bibtex_entry );
	
      }
      
      if ( fd != NULL ) {
	if ( !write_bibtex_entry ) {
	  write_string_or_file( fd, gfiledesc->list_tag.end );
	  write_end_of_file( fd, gfiledesc, sfiledesc );
	}
	fclose( fd );
      }
    }

    /*---------------------------------------
      - KEYWORD FILE MANAGEMENT (END)
      ---------------------------------------*/

    /*---------------------------------------
      - INDEX MANAGEMENT (START)
      ---------------------------------------*/

    if ( index != NULL 
	 && sfiledesc->build_index
	 && ln->name[a].status == TO_BE_INDEXED ) {
      

      /* if the first letter of the current name IS NOT the current first letter
	 we have to
	 1/ end the table (if any)
	 2/ put anchors (if needed) for the first letters
	    between the current one and the one of the current name
	 3/ begin a new table
      */

      if ( (ln->name[a].keyName[0] != last_anchor) ||
	   (strcmp(ln->name[a].keyName, key_unknown_name) == 0) ) {
	
	if ( nb_keywords > 0 ) {
	  while( nb_keywords % sfiledesc->index.cells_per_row != 0 ) {
	    nb_keywords ++;
	    if ( sfiledesc->index.empty_cell.start != NULL )
	      write_string_or_file( index, sfiledesc->index.empty_cell.start );
	    else
	      write_string_or_file( index, sfiledesc->index.cell.start );
	    if ( sfiledesc->index.empty_cell.end != NULL )
	      write_string_or_file( index, sfiledesc->index.empty_cell.end );
	    else
	      write_string_or_file( index, sfiledesc->index.cell.end );
	  } 
	  write_string_or_file( index, sfiledesc->index.row.end );
	}
	else {
	  write_string_or_file( index, sfiledesc->index.table.start );
	}

	/* counts the keywords for this letter
	 */
	nb_keywords = 1;
      }
      
      /* otherwise we get a keyword for the current first letter
       */
      else {
	nb_keywords ++;
      }

      /* put the current keyword in the table
	 see if the row is complete
	 -> see if previous the row must be ended 
	 -> see if the first column contains first letters
	 and so on
      */
      if ( nb_keywords % sfiledesc->index.cells_per_row == 1 ) {

	if ( nb_keywords > 1 )
	  write_string_or_file( index, sfiledesc->index.row.end );

	write_string_or_file( index, sfiledesc->index.row.start );

	if ( sfiledesc->put_initials_in_index != 0 ) {
	  if ( nb_keywords == 1 ) {
	    write_string_or_file( index, sfiledesc->index.extra_cell.start );
	    if ( strcmp(ln->name[a].keyName, key_unknown_name) != 0 )
	      fprintf( index, "%c", ln->name[a].keyName[0] );
	    else
	      fprintf( index, "?" );
	    write_string_or_file( index, sfiledesc->index.extra_cell.end );
	  }
	  else {
	    if ( sfiledesc->index.empty_extra_cell.start != NULL )
	      write_string_or_file( index, sfiledesc->index.empty_extra_cell.start );
	    else
	      write_string_or_file( index, sfiledesc->index.extra_cell.start );
	    if ( sfiledesc->index.empty_extra_cell.end != NULL )
	      write_string_or_file( index, sfiledesc->index.empty_extra_cell.end );
	    else
	      write_string_or_file( index, sfiledesc->index.extra_cell.end );
	  }
	}
      }

      /* write the keyword in the index
	 write the anchors here
       */
      write_string_or_file( index, sfiledesc->index.cell.start );
      if ( index != NULL 
	   && sfiledesc->build_index
	   && ln->name[a].status == TO_BE_INDEXED 
	   && nb_keywords == 1 ) {
	if ( strcmp(ln->name[a].keyName, key_unknown_name) != 0 ) {
	  last_anchor = ln->name[a].keyName[0];
	  if ( envdesc->use_html_links_between_pages && sfiledesc->build_initials_index ) {
	    if ( sfiledesc->anchor_prefix == NULL || sfiledesc->anchor_prefix[0] == '\0' ) 
	      fprintf( index, "<a name=\"%c\"></a>", last_anchor );
	    else 
	      fprintf( index, "<a name=\"%s%c\"></a>", sfiledesc->anchor_prefix, last_anchor );
	  }
	} 
	else {
	  if ( envdesc->use_html_links_between_pages && sfiledesc->build_initials_index ) {
	    if ( sfiledesc->anchor_prefix == NULL || sfiledesc->anchor_prefix[0] == '\0' ) 
	      fprintf( index, "<a name=\"UNKNOWN\"></a>" );
	    else 
	      fprintf( index, "<a name=\"%sUNKNOWN\"></a>", sfiledesc->anchor_prefix );
	  }
	}
      } /* end of anchor writing */
      if ( envdesc->use_html_links_between_pages )
	fprintf( index, "<a href=\"%s\">", filename );
      write_item_customized_string( index, ln->name[a].lastName,
				    &(sfiledesc->index.arg1_in_cell), NULL );
      if ( envdesc->use_html_links_between_pages )
 	fprintf( index, "</a>" );
      write_string_or_file( index, sfiledesc->index.cell.end );
    }

    /*---------------------------------------
      - INDEX MANAGEMENT (END)
      ---------------------------------------*/
    
  } /* for ( nb_keywords = 0, a=0; a<ln->n; a++ ) */



  if ( _verbose_ ) {
    if ( _verbose_on_stderr_ )
      fprintf( stderr, "\n" );
    else
      fprintf( stdout, "\n" );
  }

  

  /*---------------------------------------
    - INDEX MANAGEMENT (START)
    ---------------------------------------*/
  
  if ( index != NULL && sfiledesc->build_index ) {

    /* complete the current row 
       and the current table
    */
    while( nb_keywords % sfiledesc->index.cells_per_row != 0 ) {
      nb_keywords ++;
      write_string_or_file( index, sfiledesc->index.cell.start );
      write_string_or_file( index, sfiledesc->index.cell.end );
    }
    write_string_or_file( index, sfiledesc->index.row.end );
    write_string_or_file( index, sfiledesc->index.table.end );

    /* write remaining anchors (if needed)
     */
    if ( envdesc->use_html_links_between_pages && sfiledesc->build_initials_index ) {
      while ( last_anchor != 'Z' ) {
	last_anchor ++;
	if ( last_anchor > '9' && last_anchor < 'A' ) {
	  while( last_anchor < 'A' ) last_anchor++;
	}
	if ( sfiledesc->anchor_prefix == NULL || sfiledesc->anchor_prefix[0] == '\0' ) 
	  fprintf( index, "<a name=\"%c\"></a>", last_anchor );
	else 
	  fprintf( index, "<a name=\"%s%c\"></a>", sfiledesc->anchor_prefix, 
		   last_anchor );
      }
      if ( strcmp(ln->name[ln->n-1].keyName, key_unknown_name) != 0 ) {
	if ( sfiledesc->anchor_prefix == NULL || sfiledesc->anchor_prefix[0] == '\0' ) 
	  fprintf( index, "<a name=\"UNKNOWN\"></a>" );
	else 
	  fprintf( index, "<a name=\"%sUNKNOWN\"></a>", sfiledesc->anchor_prefix );
      }
    }
    
    fprintf( index, "\n" );
  }

  /*---------------------------------------
    - INDEX MANAGEMENT (END)
    ---------------------------------------*/

  return( 1 );
}










int write_all_categories( FILE *index, typeListOfItems *li,
			  typeEnvironmentDescription *envdesc,
			  typeGenericFileDescription *gfiledesc,
			  typeSpecificFileDescription *sfiledesc,
			  typeBibtexItemDescription *itemdesc,
			  int write_bibtex_entry )
{
  char *proc = "write_all_categories";
  FILE *fd = NULL;
  char filename[256];
  char str_file[256];
  char str_page[256];
  
  int current_year = NO_YEAR;
  int year;
  int nb_cat = 0;
  int i;

  enumTypeCategory category = CAT_UNKNOWN;
  enumTypeCategory current_category = CAT_UNKNOWN;


  if ( !sfiledesc->create_pages ) return( 0 );
  if ( li->n == 0 ) return( 0 );



  /* sort the items
  */
  if ( li->sort[0] != _CATEGORY_ )
    sort_list_of_items( li, _CATEGORY_ );



  /*---------------------------------------
    - INDEX MANAGEMENT (START)
    ---------------------------------------*/
  if ( index != NULL && sfiledesc->build_index ) {
    write_string_or_file( index, sfiledesc->index.table.start );
  }
  /*---------------------------------------
    - INDEX MANAGEMENT (END)
    ---------------------------------------*/



  /* let's go
     process all bibtex items
  */
  for (i=0; i<li->n; i++ ) {

    category = li->item[i]->type_cat;

    /* new category
       - close the previous file (if any)
       - update the index
    */

    if ( category != current_category ) {

      nb_cat ++;

    /*---------------------------------------
      - CATEGORY FILE MANAGEMENT (START)
      ---------------------------------------*/
    
      if ( fd != NULL ) {
	if ( !write_bibtex_entry ) {
	  write_string_or_file( fd, gfiledesc->list_tag.end );
	  write_end_of_file( fd, gfiledesc, sfiledesc );
	}
	fclose( fd );
	fd = NULL;
      }

      sprintf( filename, "%s/%s.%s", envdesc->directory_categories, 
	       filename_category[category],
	       envdesc->filename_extension );

      fd = fopen( filename, "w" );
      if ( fd == NULL ) {
	if ( _verbose_ ) {
	  fprintf( stderr, "%s: unable to open file '%s' for writing\n", proc, filename );
	}
	return( 0 );
      }
      
      if ( _verbose_ ) {
	if ( _verbose_on_stderr_ )
	  fprintf( stderr, "%s: open '%s' for writing     \r", proc, filename );
	else
	  fprintf( stdout, "%s: open '%s' for writing     \n", proc, filename );
      }

      
      /* file and page titles
       */
      if ( !write_bibtex_entry ) {
	sprintf( str_file, sfiledesc->file_title, desc_category[category] );
	sprintf( str_page, sfiledesc->page_title, desc_category[category] );
	write_begin_of_file( fd, gfiledesc, sfiledesc, str_file, str_page );
      }


      current_category = category;
      current_year = NO_YEAR;

      /*---------------------------------------
	- CATEGORY FILE MANAGEMENT (END)
	---------------------------------------*/

      /*---------------------------------------
	- INDEX MANAGEMENT (START)
	---------------------------------------*/

      if ( index != NULL && sfiledesc->build_index ) {

	if ( nb_cat % sfiledesc->index.cells_per_row == 1 ) {
	  if ( nb_cat > 1 )
	    write_string_or_file( index, sfiledesc->index.row.end );
	  write_string_or_file( index, sfiledesc->index.row.start );
	}

	write_string_or_file( index, sfiledesc->index.cell.start );
	if ( envdesc->use_html_links_between_pages )
	  fprintf( index, "<a href=\"%s\">", filename );
	fprintf( index, desc_category[category] );
	if ( envdesc->use_html_links_between_pages )
	  fprintf( index, "</a>" );
	write_string_or_file( index, sfiledesc->index.cell.end );

      }

      /*---------------------------------------
	- INDEX MANAGEMENT (END)
	---------------------------------------*/

    } /* if ( category != current_category ) */



    /*---------------------------------------
      - CATEGORY FILE MANAGEMENT (START)
      ---------------------------------------*/

    year = li->item[i]->year;

    if ( year != current_year ) {
      
      if ( !write_bibtex_entry ) {
	if ( current_year != NO_YEAR )
	  write_string_or_file( fd, gfiledesc->list_tag.end );
	
	if ( year != NO_YEAR )
	  sprintf( str_page, "%d", year );
	else
	  sprintf( str_page, "no valid year" );
      
	write_string_or_file( fd, gfiledesc->page_subtitle_tag.start );
	if ( envdesc->use_html_links_between_pages  
	     && envdesc->use_html_links_towards_reduced_years_pages ) {
	  if ( year != NO_YEAR )
	    fprintf( fd, "<a href=\"../%s/%d.%s\">", 
		     envdesc->directory_years, year,
		     envdesc->filename_extension );
	  else
	    fprintf( fd, "<a href=\"../%s/UNKNOWN.%s\">", 
		     envdesc->directory_years,
		     envdesc->filename_extension );
	}
	fprintf( fd, str_page );
	if ( envdesc->use_html_links_between_pages   
	     && envdesc->use_html_links_towards_reduced_years_pages ) 
	  fprintf( fd, "</a>" );
	write_string_or_file( fd, gfiledesc->page_subtitle_tag.end );

	write_string_or_file( fd, gfiledesc->list_tag.start );
      }

      current_year = year;
    }
    
    write_item( fd, li->item[i],
		  envdesc, gfiledesc, sfiledesc, itemdesc, write_bibtex_entry );

  } /* for (i=0; i<li->n; i++ ) */
  

  if ( fd != NULL ) {
    if ( !write_bibtex_entry ) {
      write_string_or_file( fd, gfiledesc->list_tag.end );
      write_end_of_file( fd, gfiledesc, sfiledesc );
    }
    fclose( fd );
  }

  /*---------------------------------------
    - CATEGORY FILE MANAGEMENT (START)
    ---------------------------------------*/

  if ( _verbose_ ) {
    if ( _verbose_on_stderr_ )
      fprintf( stderr, "\n" );
    else
      fprintf( stdout, "\n" );
  }

  /*---------------------------------------
    - INDEX MANAGEMENT (START)
    ---------------------------------------*/
  
  if ( index != NULL && sfiledesc->build_index ) {
    while ( nb_cat % sfiledesc->index.cells_per_row !=0 ) {
      nb_cat ++;
      if ( sfiledesc->initials.empty_cell.start != NULL )
	write_string_or_file( index, sfiledesc->index.empty_cell.start );
      else
	write_string_or_file( index, sfiledesc->index.cell.start );
      if ( sfiledesc->initials.empty_cell.end != NULL )
	write_string_or_file( index, sfiledesc->index.empty_cell.end );
      else
	write_string_or_file( index, sfiledesc->index.cell.end );
    }
    write_string_or_file( index, sfiledesc->index.row.end );
    write_string_or_file( index, sfiledesc->index.table.end );
  }

  /*---------------------------------------
    - INDEX MANAGEMENT (END)
    ---------------------------------------*/

  return( 1 );
}












int write_all_years(  FILE *index, typeListOfItems *li,
		      typeEnvironmentDescription *envdesc,
		      typeGenericFileDescription *gfiledesc,
		      typeSpecificFileDescription *sfiledesc,
		      typeBibtexItemDescription *itemdesc,
		      int write_bibtex_entry )
{
  char *proc = "write_all_years";
  FILE *fd = NULL;
  char filename[256];
  char str_file[256];
  char str_page[256];

  int current_year = NO_YEAR;
  int year;
  int i;
  int nb_year = 0;

  enumTypeCategory current_category = CAT_UNKNOWN;
  enumTypeCategory category;
  

  if ( !sfiledesc->create_pages ) return( 0 );
  if ( li->n == 0 ) return( 0 );



  /* sort the items
  */
  if ( li->sort[0] != _YEAR_ )
    sort_list_of_items( li, _YEAR_ );



  /*---------------------------------------
    - INDEX MANAGEMENT (START)
    ---------------------------------------*/
  if ( index != NULL && sfiledesc->build_index ) {
    write_string_or_file( index, sfiledesc->index.table.start );
  }
  /*---------------------------------------
    - INDEX MANAGEMENT (END)
    ---------------------------------------*/



  /* let's go
     process all bibtex items
  */
  for (i=0; i<li->n; i++ ) {

    year = li->item[i]->year;



    /* new year
       - close the previous file (if any)
       - update the index
       Be careful:
          if the first item has no year, we have year==current_year
          so test also the file pointer
    */

    if ( year != current_year || fd == NULL ) {

      nb_year++;

    /*---------------------------------------
      - YEAR FILE MANAGEMENT (START)
      ---------------------------------------*/
    
      if ( fd != NULL ) {
	if ( !write_bibtex_entry ) {
	  write_string_or_file( fd, gfiledesc->list_tag.end );
	  write_end_of_file( fd, gfiledesc, sfiledesc );
	}
	fclose( fd );
	fd = NULL;
      }
      
      if ( sfiledesc->write_complete_suffix ) {
	if ( year != NO_YEAR )
	  sprintf( filename, "%s/%d.complete.%s", envdesc->directory_years, year,
		   envdesc->filename_extension );
	else 
	  sprintf( filename, "%s/UNKNOWN.complete.%s", envdesc->directory_years,
		   envdesc->filename_extension );
      }
      else {
	if ( year != NO_YEAR )
	  sprintf( filename, "%s/%d.%s", envdesc->directory_years, year,
		   envdesc->filename_extension );
	else 
	  sprintf( filename, "%s/UNKNOWN.%s", envdesc->directory_years,
		   envdesc->filename_extension );
      }

      fd = fopen( filename, "w" );
      if ( fd == NULL ) {
	if ( _verbose_ ) {
	  fprintf( stderr, "%s: unable to open file '%s' for writing\n", proc, filename );
	}
	return( 0 );
      }

      if ( _verbose_ ) {
	if ( _verbose_on_stderr_ )
	  fprintf( stderr, "%s: open '%s' for writing     \r", proc, filename );
	else
	  fprintf( stdout, "%s: open '%s' for writing     \n", proc, filename );
      }


      /* file and page titles
       */
      if ( !write_bibtex_entry ) {
	if ( year != NO_YEAR ) {
	  if ( sfiledesc->file_title != NULL )
	    sprintf( str_file, sfiledesc->file_title, year );
	  else 
	    sprintf( str_file, "%d", year );
	  if ( sfiledesc->page_title != NULL )
	    sprintf( str_page, sfiledesc->page_title, year );
	  else 
	    sprintf( str_page, "%d", year );
	}
	else {
	  sprintf( str_file, sfiledesc->default_file_title );
	  sprintf( str_page, sfiledesc->default_page_title );
	}
	write_begin_of_file( fd, gfiledesc, sfiledesc, str_file, str_page );
      }



      current_year = year;
      current_category = CAT_UNKNOWN;

      /*---------------------------------------
	- CATEGORY FILE MANAGEMENT (END)
	---------------------------------------*/

      /*---------------------------------------
	- INDEX MANAGEMENT (START)
	---------------------------------------*/

      if ( index != NULL && sfiledesc->build_index ) {

	if ( nb_year % sfiledesc->index.cells_per_row == 1 ) {
	  if ( nb_year > 1 ) 
	    write_string_or_file( index, sfiledesc->index.row.end );
	  write_string_or_file( index, sfiledesc->index.row.start );
	}

	write_string_or_file( index, sfiledesc->index.cell.start );
	if ( envdesc->use_html_links_between_pages )
	  fprintf( index, "<a href=\"%s\">", filename );
	if ( year != NO_YEAR )
	  fprintf( index, "%d", year );
	else
	  fprintf( index, "no year" );
	if ( envdesc->use_html_links_between_pages ) 
	  fprintf( index, "</a>" );
	write_string_or_file( index, sfiledesc->index.cell.end );

      }

      /*---------------------------------------
	- INDEX MANAGEMENT (END)
	---------------------------------------*/

    } /* if ( year != current_year ) */



    /*---------------------------------------
      - YEAR FILE MANAGEMENT (START)
      ---------------------------------------*/

    category = li->item[i]->type_cat;

    if ( category != current_category ) {

      if ( !write_bibtex_entry ) {
	if ( current_category != CAT_UNKNOWN )
	  write_string_or_file( fd, gfiledesc->list_tag.end );

	write_string_or_file( fd, gfiledesc->page_subtitle_tag.start );
	if ( envdesc->use_html_links_between_pages 
	     && envdesc->use_html_links_towards_categories_pages ) 
	  fprintf( fd, "<a href=\"../%s/%s.%s\">", 
		   envdesc->directory_categories, filename_category[category],
		   envdesc->filename_extension );
	fprintf( fd, desc_category[category] );
	if ( envdesc->use_html_links_between_pages 
	     && envdesc->use_html_links_towards_categories_pages ) 
	  fprintf( fd, "</a>" );
	write_string_or_file( fd, gfiledesc->page_subtitle_tag.end );
	
	write_string_or_file( fd, gfiledesc->list_tag.start );
      }

      current_category = category;
    }
    
    write_item( fd, li->item[i],
		  envdesc, gfiledesc, sfiledesc, itemdesc, write_bibtex_entry );

  } /* for (i=0; i<li->n; i++ ) */
  

  if ( fd != NULL ) {
    if ( !write_bibtex_entry ) {
      write_string_or_file( fd, gfiledesc->list_tag.end );
      write_end_of_file( fd, gfiledesc, sfiledesc );
    }
    fclose( fd );
  }

  /*---------------------------------------
    - CATEGORY FILE MANAGEMENT (START)
    ---------------------------------------*/

  if ( _verbose_ ) {
    if ( _verbose_on_stderr_ )
      fprintf( stderr, "\n" );
    else
      fprintf( stdout, "\n" );
  }

  /*---------------------------------------
    - INDEX MANAGEMENT (START)
    ---------------------------------------*/
  
  if ( index != NULL && sfiledesc->build_index ) {
    while( nb_year % sfiledesc->index.cells_per_row != 0 ) {
      nb_year ++;
      if ( sfiledesc->initials.empty_cell.start != NULL )
	write_string_or_file( index, sfiledesc->index.empty_cell.start );
      else
	write_string_or_file( index, sfiledesc->index.cell.start );
      if ( sfiledesc->initials.empty_cell.end != NULL )
	write_string_or_file( index, sfiledesc->index.empty_cell.end );
      else
	write_string_or_file( index, sfiledesc->index.cell.end );
    }
    write_string_or_file( index, sfiledesc->index.row.end );
    write_string_or_file( index, sfiledesc->index.table.end );
  }

  /*---------------------------------------
    - INDEX MANAGEMENT (END)
    ---------------------------------------*/
  
  return( 1 );
}











int write_all_items( FILE *f,
		      typeListOfItems *li, enumSortCriterium c, 
		      typeEnvironmentDescription *envdesc,
		      typeGenericFileDescription *gfiledesc,
		      typeSpecificFileDescription *sfiledesc,
		      typeBibtexItemDescription *itemdesc,
		       int write_bibtex_entry )
{
  char *proc = "write_all_items";
  FILE *fd = f;
  char filename[256];
  char str_file[256];
  char str_page[256];

  int current_year = NO_YEAR;
  int year;
  int i;
  int nb_year;

  enumTypeCategory category = CAT_UNKNOWN;
  enumTypeCategory current_category = CAT_UNKNOWN;
  /*  char *anchor_year = "YEAR";  */


  if ( !sfiledesc->create_pages ) return( 0 );
  if ( li->n == 0 ) return( 0 );


  /* do we already have a file descriptor ?
   */
  if ( fd == NULL ) {
    sprintf( filename, "%s/%s.%s", envdesc->directory_biblio,
	     envdesc->filename_complete_biblio, envdesc->filename_extension );
    fd = fopen( filename, "w" );
    if ( fd == NULL ) {
      if ( _verbose_ ) {
	fprintf( stderr, "%s: unable to open file '%s' for writing\n", proc, filename );
      }
      return( 0 );
    }

    if ( _verbose_ ) {
      if ( _verbose_on_stderr_ )
	fprintf( stderr, "%s: open '%s' for writing     \r", proc, filename );
      else
	fprintf( stdout, "%s: open '%s' for writing     \n", proc, filename );
    }
  }



  /* sort the items
   */
  if ( li->sort[0] != c ) 
    sort_list_of_items( li, c );
  
  /* file and page titles
   */
  if ( !write_bibtex_entry ) {
    switch ( li->sort[0] ) {
    default :     
      sprintf( str_file, sfiledesc->default_file_title );
      sprintf( str_page, sfiledesc->default_page_title ); 
      break;
    case _NAME_ : 
    case _CATEGORY_ : 
    case _YEAR_ : 
      sprintf( str_file, sfiledesc->file_title, desc_sort_criterium[c] );
      sprintf( str_page, sfiledesc->page_title, desc_sort_criterium[c] );
    }
    write_begin_of_file( fd, gfiledesc, sfiledesc, str_file, str_page );
  }


  /* write the items 
   */
  switch ( c ) {

  default :
  case _NAME_ :

    if ( !write_bibtex_entry ) {
      write_string_or_file( fd, gfiledesc->list_tag.start );
    }
    for (i=0; i<li->n; i++ ) {
      write_item( fd, li->item[i],
		  envdesc, gfiledesc, sfiledesc, itemdesc, write_bibtex_entry );
    }
    if ( !write_bibtex_entry ) {
      write_string_or_file( fd, gfiledesc->list_tag.end );
    }
    break;
    
  case _CATEGORY_ :

    current_category = CAT_UNKNOWN;
    for (i=0; i<li->n; i++ ) {
      /*
       * get the category
       */
      category = li->item[i]->type_cat;

      if ( category != current_category ) {
	
	if ( !write_bibtex_entry ) {
	  if ( current_category != CAT_UNKNOWN )
	    write_string_or_file( fd, gfiledesc->list_tag.end );
	  
	  write_string_or_file( fd, gfiledesc->page_subtitle_tag.start );
	  fprintf( fd,  desc_category[category] );
	  write_string_or_file( fd, gfiledesc->page_subtitle_tag.end );
	  
	  write_string_or_file( fd, gfiledesc->list_tag.start );
	}

	current_category = category;
      }

      write_item( fd, li->item[i],
		  envdesc, gfiledesc, sfiledesc, itemdesc, write_bibtex_entry );
    }
    if ( !write_bibtex_entry ) {
      write_string_or_file( fd, gfiledesc->list_tag.end );
    }
    break;

  case _YEAR_ :

    if ( !write_bibtex_entry 
	 && envdesc->use_html_links_towards_urls 
	 && sfiledesc->write_local_menu ) {

      current_year = NO_YEAR;
      nb_year=0;

      write_string_or_file( fd, sfiledesc->index.table.start );
      for (i=0; i<li->n; i++ ) {
	
	year = li->item[i]->year;
	if ( year != current_year ) {

	  nb_year++;
	  if ( nb_year % sfiledesc->index.cells_per_row == 1 ) {
	    if ( nb_year > 1 )
	      write_string_or_file( fd, sfiledesc->index.row.end );
	    write_string_or_file( fd, sfiledesc->index.row.start );
	  }
	  
	  write_string_or_file( fd, sfiledesc->index.cell.start );
	  
	  fprintf( fd, "<a href=\"%s.%s#", envdesc->filename_complete_biblio,
		   envdesc->filename_extension );
	  if ( sfiledesc->anchor_prefix != NULL && sfiledesc->anchor_prefix[0] != '\0' )
	    fprintf( fd, "%s", sfiledesc->anchor_prefix );
	  if ( year != NO_YEAR )
	    fprintf( fd, "%d\">%d", year, year );
	  else
	    fprintf( fd, "UNKNOWN\">no year" );
	  fprintf( fd, "</a>" );

	  write_string_or_file( fd, sfiledesc->index.cell.end );

	  current_year = year;
        }

      }

      while( nb_year % sfiledesc->index.cells_per_row != 0 ) {
	nb_year ++;
	write_string_or_file( fd, sfiledesc->index.cell.start );
	write_string_or_file( fd, sfiledesc->index.cell.end );
      }
      write_string_or_file( fd, sfiledesc->index.row.end );
      write_string_or_file( fd, sfiledesc->index.table.end );

    }


    
    current_year = NO_YEAR;

    for (i=0; i<li->n; i++ ) {

      /*
       * get the year
       */
      year = li->item[i]->year;
      if ( year != current_year ) {

	if ( !write_bibtex_entry ) {
	  if ( current_year != NO_YEAR )
	    write_string_or_file( fd, gfiledesc->list_tag.end );

	  write_string_or_file( fd, gfiledesc->page_subtitle_tag.start );
	  if ( envdesc->use_html_links_towards_urls && sfiledesc->write_local_menu ) {
	    fprintf( fd, "<a name=\"" );
	    if ( sfiledesc->anchor_prefix != NULL && sfiledesc->anchor_prefix[0] != '\0' )
	      fprintf( fd, sfiledesc->anchor_prefix );
	    if ( year != NO_YEAR )
	      fprintf( fd, "%d\"></a>", year );
	    else
	      fprintf( fd, "UNKNOWN\"></a>" );
	  }
	  if ( year != NO_YEAR )
	    fprintf( fd, "%d", year );
	  else
	    fprintf( fd, "no year" );
	  
	  write_string_or_file( fd, gfiledesc->page_subtitle_tag.end );
	  write_string_or_file( fd, gfiledesc->list_tag.start );
	}

	current_year = year;
      }

      write_item( fd, li->item[i],
		  envdesc, gfiledesc, sfiledesc, itemdesc, write_bibtex_entry );
    }

    if ( !write_bibtex_entry ) {
      write_string_or_file( fd, gfiledesc->list_tag.end );
    }
    break;

  }

  if ( !write_bibtex_entry ) {
    write_end_of_file( fd, gfiledesc, sfiledesc );
  }
  return( 1 );
}




























/***************************************************
 *
 *
 *
 ***************************************************/










void write_credits( FILE *fd, char *str )
{
  write_string_or_file( fd, str );
}





#ifdef WIN32

/* Modified as proposed by Ramon Miralles Ricos 
 */
#include <windows.h>
#include <stdio.h>
#include <lmcons.h> /* definition of UNLEN */

void write_username( FILE *fd )
{
    LPTSTR lpszSystemInfo;          /* pointer to system information string */
                                    /* LPTSTR = 32-bit pointer to a character string */
    DWORD cchBuff = 256;            /* size of user name                    */
                                    /* DWORD = 32-bit unsigned integer */
    TCHAR tchBuffer[UNLEN + 1];     /* buffer for expanded string           */
                                    /* TCHAR = typedef char */
                    
    lpszSystemInfo = tchBuffer;

    /* Get and display the user name.
     */
    GetUserName( lpszSystemInfo, &cchBuff );

    fprintf( fd,"%s", lpszSystemInfo );
}

#else

/* for getlogin() in write_username()
 */
#include <unistd.h>

/* for getpwuid() in write_username()
 */
#include <pwd.h>
#include <sys/types.h>


void write_username( FILE *fd )
{
  struct passwd *pwd = NULL;
  if ( getlogin() != NULL ) {
    fprintf( fd, "%s", getlogin() );
  }
  else if ( (pwd = getpwuid(geteuid())) != NULL ) {
    fprintf( fd, "%s", pwd->pw_name );
  }
}

#endif





void write_pages_author( FILE *fd, typeTagElement *e, char *name )
{

  write_string_or_file( fd, e->start );
  if ( name == NULL || name[0] == '\0' )
    write_username( fd );
  else
    write_string_or_file( fd, name );
  write_string_or_file( fd, e->end );
}




void write_ctime( FILE *fd )
{
  time_t t;
  t = time( NULL );
  fprintf( fd, "%s", ctime( &t ) );
}
void write_date( FILE *fd, typeTagElement *e)
{
  write_string_or_file( fd, e->start );
  write_ctime( fd );
  write_string_or_file( fd, e->end );
}





void write_disclaimer( FILE *fd, typeGenericFileDescription *d )
{
  if ( (d->disclaimer_1 == NULL 
	|| d->disclaimer_1[0] == '\0')
       && (d->disclaimer_2 == NULL 
	   || d->disclaimer_2[0] == '\0') ) return;
  
  write_string_or_file( fd, d->disclaimer.content_field.start );

  if ( d->disclaimer.prefix != NULL 
       && d->disclaimer.prefix[0] != '\0' )
    write_string_or_file( fd, d->disclaimer.prefix );

  if (d->disclaimer_1 != NULL 
      && d->disclaimer_1[0] != '\0') {
    write_string_or_file( fd, d->disclaimer.content.start );
    write_string_or_file( fd, d->disclaimer_1 );
    write_string_or_file( fd, d->disclaimer.content.end );
  }
  
  if (d->disclaimer_2 != NULL 
      && d->disclaimer_2[0] != '\0') {
    write_string_or_file( fd, d->disclaimer.content.start );
    write_string_or_file( fd, d->disclaimer_2 );
    write_string_or_file( fd, d->disclaimer.content.end );
  }
  
  write_string_or_file( fd, d->disclaimer.content_field.end );
}





/* *** start of file ****
  <start_of_file> 
                  
     <start_of_main_title>
        <main_title>      
     <end_of_main_title>  
                          
     <start_of_body>      
        <header_of_body> 
*/
void write_begin_of_file( FILE *fd, typeGenericFileDescription *d, 
			  typeSpecificFileDescription *s,
			  char *file_title,
			  char *page_title )
{
  char *proc = "write_begin_of_file";

  if ( fd == NULL ) {
    if ( _verbose_ )
      fprintf( stderr, "%s: try to write into (FILE*)NULL ?!\n", proc );
    return;
  }

  write_string_or_file( fd, d->file_tag.start );

  write_string_or_file( fd, d->head_tag.start );

  write_string_or_file( fd, d->file_title_tag.start );

  if ( file_title != NULL && file_title[0] != '\0' ) 
    fprintf( fd, file_title );
  else if ( s->default_file_title != NULL && s->default_file_title[0] != '\0' ) 
    write_string_or_file( fd, s->default_file_title );
  else 
    fprintf( fd, "No title" );

  write_string_or_file( fd, d->file_title_tag.end );

  write_string_or_file( fd, d->head_tag.end );
  
  write_string_or_file( fd, d->body_tag.start );

  write_string_or_file( fd, d->header_of_body );
  write_string_or_file( fd, s->header_of_body );

  write_string_or_file( fd, d->page_title_tag.start );

  if ( page_title != NULL && page_title[0] != '\0' ) 
    fprintf( fd, page_title );
  else if ( s->default_page_title != NULL && s->default_page_title[0] != '\0' ) 
    write_string_or_file( fd, s->default_page_title );
  else 
    fprintf( fd, "No title" );

  write_string_or_file( fd, d->page_title_tag.end );

  write_string_or_file( fd, d->header_of_contents );
  write_string_or_file( fd, s->header_of_contents );
}



/* *** end of file ****
   ...
        <footer_of_body>         
        <separator><disclaimer>  
        <separator><date><author>
        <separator><credits>     
     <end_of_body>               
                                 
  <end_of_file>                  
*/
void write_end_of_file( FILE *fd, typeGenericFileDescription *d,
			typeSpecificFileDescription *s ) 
{
  char *proc = "write_end_of_file";

  if ( fd == NULL ) {
    if ( _verbose_ )
      fprintf( stderr, "%s: try to write into (FILE*)NULL ?!\n", proc );
    return;
  }

  write_string_or_file( fd, s->footer_of_contents );
  write_string_or_file( fd, d->footer_of_contents );

  if ( (s->write_disclaimer 
	&& ((d->disclaimer_1 != NULL && d->disclaimer_1[0] != '\0')
	    || (d->disclaimer_2 != NULL && d->disclaimer_2[0] != '\0')))
       || s->write_date 
       || s->write_author 
       || (s->write_credits && d->credits != NULL) )
    write_string_or_file( fd, d->separator );

  if( s->write_disclaimer ) {
    write_disclaimer( fd, d );
  }
  
  if ( s->write_date || s->write_author ) {
    if( s->write_disclaimer ) write_string_or_file( fd, d->separator );
    if( s->write_date )       write_date( fd, &(d->date_tag) );
    if( s->write_author )     write_pages_author( fd, &(d->author_tag), 
						  d->author_name  );
  }

  if( s->write_credits ) {
    if ( s->write_date || s->write_author || s->write_disclaimer )
      write_string_or_file( fd, d->separator );
    write_credits( fd, d->credits );
  }

  write_string_or_file( fd, s->footer_of_body );
  write_string_or_file( fd, d->footer_of_body );

  write_string_or_file( fd, d->body_tag.end );
  write_string_or_file( fd, d->file_tag.end );
}





/***************************************************
 *
 *
 *
 ***************************************************/














/* write a link
   
   1) if LINK 
   
      1a) if ICON         <a href=...><IMG SRC=ICON></a>\n

      1b) if string_link  <a href=...>link</a>\n

      1c) else            <content><a href=...>link</a></content>\n

   2) else                <field>prefix<content>link</content></field>

*/
void write_item_link( FILE *fd, typeEnvironmentDescription *envdesc,
		     char *link, typeEnvElement *tag )
{
  if ( link == NULL || link[0] == '\0' ) return;

  
  if ( envdesc->use_html_links_towards_urls ) {

    /* it seems there is an icon
     */
    if ( envdesc->use_icons && tag->use_icon 
	 && tag->icon != NULL && tag->icon[0] != '\0' ) {
      fprintf( fd, " <a href=\"%s\">", link );
      fprintf( fd, "<img align=\"middle\" border=\"0\" src=\"../%s/%s\">",
	       envdesc->directory_icons, tag->icon );
      fprintf( fd, "</a> " );
      return;
    }
    
    /* no icon but a string
     */
    if ( tag->link != NULL && tag->link[0] != '\0' ) {
      write_string_or_file( fd, tag->link_field.start );
      fprintf( fd, "<a href=\"%s\">", link );
      write_string_or_file( fd, tag->link );
      fprintf( fd, "</a>" );
      write_string_or_file( fd, tag->link_field.end );
      return;
    }
    
    /* no icon, no string, only the link itself
     */
    
    write_string_or_file( fd, tag->link_field.start );
    fprintf( fd, "<a href=\"%s\">", link );
    write_string_or_file( fd, tag->content.start );
    fprintf( fd, "%s", link );
    write_string_or_file( fd, tag->content.end );
    fprintf( fd, "</a>" );
    write_string_or_file( fd, tag->link_field.end );

    return;
  }


  /* no link
   */

  write_string_or_file( fd, tag->content_field.start );
  if ( tag->prefix != NULL && tag->prefix[0] != '\0' ) {
    write_string_or_file( fd, tag->prefix );
  }
  write_string_or_file( fd, tag->content.start );
  fprintf( fd, "%s", link );
  write_string_or_file( fd, tag->content.end );
  write_string_or_file( fd, tag->content_field.end );
}



void write_item_doi( FILE *fd, typeEnvironmentDescription *envdesc,
		     char *link, typeEnvElement *tag )
{
  if ( link == NULL || link[0] == '\0' ) return;

  if ( envdesc->use_html_links_towards_urls ) {

    /* it seems there is an icon
     */
    if ( envdesc->use_icons && tag->use_icon 
	 && tag->icon != NULL && tag->icon[0] != '\0' ) {
      fprintf( fd, " <a href=\"http://dx.doi.org/%s\">", link );
      fprintf( fd, "<img align=\"middle\" border=\"0\" src=\"../%s/%s\">",
	       envdesc->directory_icons, tag->icon );
      fprintf( fd, "</a> " );
      return;
    }
    
    /* no icon but a string
     */
    if ( tag->link != NULL && tag->link[0] != '\0' ) {
      write_string_or_file( fd, tag->link_field.start );
      fprintf( fd, "<a href=\"http://dx.doi.org/%s\">", link );
      write_string_or_file( fd, tag->link );
      fprintf( fd, "</a>" );
      write_string_or_file( fd, tag->link_field.end );
      return;
    }
    
    /* no icon, no string, only the link itself
     */
    
    write_string_or_file( fd, tag->link_field.start );
    fprintf( fd, "<a href=\"http://dx.doi.org/%s\">", link );
    write_string_or_file( fd, tag->content.start );
    fprintf( fd, "%s", link );
    write_string_or_file( fd, tag->content.end );
    fprintf( fd, "</a>" );
    write_string_or_file( fd, tag->link_field.end );
    
    return;
  }


  /* no link
   */

  write_string_or_file( fd, tag->content_field.start );
  if ( tag->prefix != NULL && tag->prefix[0] != '\0' ) {
    write_string_or_file( fd, tag->prefix );
  }
  write_string_or_file( fd, tag->content.start );
  fprintf( fd, "%s", link );
  write_string_or_file( fd, tag->content.end );
  write_string_or_file( fd, tag->content_field.end );
}



void write_item_keywords( FILE *fd, typeItem *item,
			  typeEnvironmentDescription *envdesc,
			  typeBibtexItemDescription *itemdesc )
{
  int n, nt;
  int write_non_indexed_keywords = 1;

  if ( item->nb_keywords <= 0 ) return;

  /* count indexed keywords
   */
  for ( nt=0, n=0; n<item->nb_keywords; n++ ) {
    switch ( item->keywords[n].status ) {
    default :
      continue;
    case PAGE_TO_BE_GENERATED :
    case TO_BE_INDEXED :
      break;
    }
    nt++;
  }
  if ( !write_non_indexed_keywords && nt <= 0 ) return; 

  write_string_or_file( fd, itemdesc->bib_keywords.prefix );

  for ( n=0; n<item->nb_keywords; n++ ) {

    switch ( item->keywords[n].status ) {
    default :
      if ( !write_non_indexed_keywords ) continue;
      break;
    case PAGE_TO_BE_GENERATED :
    case TO_BE_INDEXED :
      if ( envdesc->use_html_links_between_pages 
	   && envdesc->use_html_links_towards_keywords_pages )
	fprintf( fd, "<a href=\"../%s/%s.%s\">", envdesc->directory_keywords, 
		item->keywords[n].keyName, envdesc->filename_extension );
      break;
    }
    
    write_string_or_file( fd, itemdesc->bib_keywords.content.start );
    fprintf( fd, item->keywords[n].lastName );
    write_string_or_file( fd, itemdesc->bib_keywords.content.end );
    
    switch ( item->keywords[n].status ) {
    default :
      break;
    case PAGE_TO_BE_GENERATED :
    case TO_BE_INDEXED :
      if ( envdesc->use_html_links_between_pages 
	   && envdesc->use_html_links_towards_keywords_pages )
	fprintf( fd, "</a>" );
      break;
    }
   
    if ( n <item->nb_keywords-1 )
      fprintf( fd, itemdesc->bib_comma );
    else
      fprintf( fd, itemdesc->bib_dot );
  }
}





void write_item_one_additional_field( FILE *fd, typeItem *item,
				      int real_write, int real_fprintf,
				      char *contents,
				      typeEnvElement *tag, 
				      typeEnvironmentDescription *envdesc )
{
  if ( contents == NULL || contents[0] == '\0' ) return;
  if ( real_write <= 0 ) return;
  
  if ( real_write == 2 ) {

    write_string_or_file( fd, tag->content_field.start );
    write_string_or_file( fd, tag->prefix );
    write_string_or_file( fd, tag->content.start );
    if ( contents != NULL && contents[0] != '\0' ) {
      if ( real_fprintf )
	/* some strings should never be considered as file names (eg bibtex file)
	 */
	fprintf( fd, "%s", contents );
      else
	/* some strings may contains the '%' character (eg abstract) 
	 */
	write_string_or_file( fd, contents );
    }
    write_string_or_file( fd, tag->content.end );
    write_string_or_file( fd, tag->content_field.end );
    return;
  }

  /* real_write == 1
   */

  if ( envdesc->use_html_links_between_pages 
       && envdesc->use_html_links_towards_complete_years_pages ) {

    /* there seems to be a string
     */
    if ( tag->link != NULL && tag->link[0] != '\0' ) {
      write_string_or_file( fd, tag->link_field.start );
      if ( item->year != NO_YEAR )
	fprintf( fd, "<a href=\"../%s/%d.complete.%s", 
		 envdesc->directory_years, item->year, 
		 envdesc->filename_extension );
      else 
	fprintf( fd, "<a href=\"../%s/UNKNOWN.complete.%s", 
		 envdesc->directory_years,
		 envdesc->filename_extension );
      if ( item->bibtex_key != NULL && item->bibtex_key[0] != '\0' ) 
	fprintf( fd, "#%s", item->bibtex_key );
      fprintf( fd, "\">" );
      write_string_or_file( fd, tag->link );
      fprintf( fd, "</a>" );
      write_string_or_file( fd, tag->link_field.end );
      return;
    }

    /* no string 
     */
    write_string_or_file( fd, tag->link_field.start );
    if ( item->year != NO_YEAR )
      fprintf( fd, "<a href=\"../%s/%d.complete.%s#%s\">", 
	       envdesc->directory_years, item->year, 
	       envdesc->filename_extension, item->bibtex_key );
    else 
      fprintf( fd, "<a href=\"../%s/UNKNOWN.complete.%s#%s\">", 
	       envdesc->directory_years,
	       envdesc->filename_extension, item->bibtex_key );
    write_string_or_file( fd, tag->content.start );
    /*
    for ( i=0; i<n && contents[i]!='\0'; i++ )
      fprintf( fd, "%c", contents[i] );
    */
    write_string_or_file( fd, contents );
    write_string_or_file( fd, tag->content.end );
    fprintf( fd, "</a>" );
    write_string_or_file( fd, tag->link_field.end );
    return;
  }

}



void write_item_additional_fields( FILE *fd, typeItem *item,
				    typeEnvironmentDescription *envdesc,
				    typeGenericFileDescription *gfiledesc,
				    typeSpecificFileDescription *sfiledesc,
				    typeBibtexItemDescription *itemdesc )
{
#if (defined(_INRIA_) || defined(_HALINRIA_))
  int fieldlinks[] = {URL, URLPUBLISHER, PDF, POSTSCRIPT, PS, HALURL };
  int nfieldlinks = 6;
#else
  int fieldlinks[] = {URL, URLPUBLISHER, PDF, POSTSCRIPT, PS };
  int nfieldlinks = 5;
#endif
  int l;
  typeEnvElement *tagfieldlink;
  
  write_item_one_additional_field( fd, item,  2, 0,
				   item->field[ISBN],
				   &(itemdesc->bib_isbn), envdesc );
  write_item_one_additional_field( fd, item,  2, 0,
				   item->field[ISSN],
				   &(itemdesc->bib_issn), envdesc );
  
#if (defined(_INRIA_) || defined(_HALINRIA_))
  write_item_one_additional_field( fd, item,  2, 0,
				   item->field[HALIDENTIFIANT],
				   &(itemdesc->bib_hal_identifiant), envdesc );
#endif

    /* possible links 
     URL,
     URLPUBLISHER,
     PDF,
     POSTSCRIPT,
     PS,
#if (defined(_INRIA_) || defined(_HALINRIA_))
     HALURL
#endif
  */

  for ( l=0 ; l<nfieldlinks; l++ ) {
    if ( item->field[fieldlinks[l]] == NULL 
	 || item->field[fieldlinks[l]][0] == '\0' 
	 || strlen(item->field[fieldlinks[l]]) < 3 ) 
      continue;
    
    /* default environment element
     */
    switch ( fieldlinks[l] ) {
    default :
    case URL :
    case URLPUBLISHER :
      tagfieldlink = &(itemdesc->bib_url);
      break;
    case PDF :
      tagfieldlink = &(itemdesc->bib_pdf);
      break;
    case PS :
    case POSTSCRIPT :
      tagfieldlink = &(itemdesc->bib_postscript);
      break;
#if (defined(_INRIA_) || defined(_HALINRIA_))
    case HALURL :
      tagfieldlink = &(itemdesc->bib_url_hal);
      break;
#endif
    }

    /* adapt environment with respect to link
     */
    if ( 1 ) {
      if ( strncmp( &(item->field[fieldlinks[l]][strlen(item->field[fieldlinks[l]]) - 4]), ".pdf", 4 ) == 0 )
	tagfieldlink = &(itemdesc->bib_pdf);
      if ( strncmp( &(item->field[fieldlinks[l]][strlen(item->field[fieldlinks[l]]) - 3]), ".ps", 3 ) == 0 )
	tagfieldlink = &(itemdesc->bib_postscript);
      if ( strncmp( &(item->field[fieldlinks[l]][strlen(item->field[fieldlinks[l]]) - 6]), ".ps.gz", 6 ) == 0 )
	tagfieldlink = &(itemdesc->bib_postscript);
      if ( strncmp( &(item->field[fieldlinks[l]][strlen(item->field[fieldlinks[l]]) - 5]), ".html", 5 ) == 0 )
	tagfieldlink = &(itemdesc->bib_url);
      if ( strncmp( &(item->field[fieldlinks[l]][strlen(item->field[fieldlinks[l]]) - 4]), ".htm", 4 ) == 0 )
	tagfieldlink = &(itemdesc->bib_url);
    }

    write_item_link( fd, envdesc, item->field[fieldlinks[l]], tagfieldlink );
  }

  
  /*
  write_item_link( fd, envdesc, item->field[URL], &(itemdesc->bib_url) );
  write_item_link( fd, envdesc, item->field[PDF], &(itemdesc->bib_pdf) );
  write_item_link( fd, envdesc, item->field[PS], 
		   &(itemdesc->bib_postscript) );
  write_item_link( fd, envdesc, item->field[POSTSCRIPT], 
		   &(itemdesc->bib_postscript) );
  */

  write_item_doi( fd, envdesc, item->field[DOI], &(itemdesc->bib_doi) );

  write_item_keywords( fd, item, envdesc, itemdesc );



  write_item_one_additional_field( fd, item,  sfiledesc->write_bibtex_abstract, 0,
				    item->field[ABSTRACT],
				    &(itemdesc->bib_abstract), envdesc );
  
  write_item_one_additional_field( fd, item, sfiledesc->write_bibtex_annote, 0,
				    item->field[ANNOTE],
				    &(itemdesc->bib_annote), envdesc );
   
  write_item_one_additional_field( fd, item, sfiledesc->write_bibtex_comments, 0,
				    item->field[COMMENTS],
				    &(itemdesc->bib_comments), envdesc );
  
  write_item_one_additional_field( fd, item, sfiledesc->write_bibtex_key, 0,
				    item->bibtex_key,
				    &(itemdesc->bib_bibtex_key), envdesc );
  

  write_item_one_additional_field( fd, item, sfiledesc->write_bibtex_file, 1,
				    item->bibtex_file,
				    &(itemdesc->bib_bibtex_file), envdesc );

  write_item_one_additional_field( fd, item, sfiledesc->write_bibtex_entry, 0,
				    item->bibtex_entry,
				    &(itemdesc->bib_bibtex_entry), envdesc );
}





void write_item_note( FILE *fd, typeItem *item, 
		      typeBibtexItemDescription *itemdesc,
		      char *endstring )
{
  if ( item->field[NOTE] == NULL || item->field[NOTE][0] == '\0' ) 
    return;

  if ( itemdesc->bib_note.prefix != NULL 
       && strlen( itemdesc->bib_note.prefix ) > 0 )
    fprintf( fd, itemdesc->bib_note.prefix );
  
  if ( item->field[NOTE][0] >= 'a' && item->field[NOTE][0] <= 'z' ) {
    fprintf( fd, "%c", item->field[NOTE][0]+'A'-'a' );
  } else {
    fprintf( fd, "%c", item->field[NOTE][0] );
  }
  fprintf( fd, "%s", &(item->field[NOTE][1]) );
  
  /* In most cases,
     what is wanted is a final dot, but it may also be included in the note itself,
     thus check it.
  */
  if ( endstring != NULL ) {
    if ( item->field[NOTE][ strlen(item->field[NOTE])-1 ] != endstring[0] )
      fprintf( fd, "%s", endstring );
  }
}





/* write names 
   
   A name is made as follows
   <name> = <bib_author.start><firstname> <lastname><bib_author.end>

   CASE #names = 1
   -> "<name[1]>"
   
   CASE #names = 2
   -> "<name[1]> and <name[2]>"

   CASE #names > 2
   -> "<name[1]>, <name[2]>, ... <name[N-1]>, and <name[N]>"
*/
void write_item_names( FILE *fd, 
		       typeName *names,
		       int nb_names,
		       typeEnvironmentDescription *envdesc,
		       typeBibtexItemDescription *itemdesc )
{
  int n;
  
  if ( nb_names == 0 ) return;

  for (n=0; n<nb_names; n ++ ) {

    /* if necessary, write separator between
       two names.
       #names = 2 =>  <name1> and <name2>
       #names > 2 =>  <name1>, <name2>, ... <name(N-1)>, and <name(N)>
    */
    if ( n == 1 && nb_names == 2 ) {
      fprintf( fd, itemdesc->bib_and );
    } else if ( n > 0 ) {
      fprintf( fd, itemdesc->bib_comma );
      if ( n == nb_names-1 ) 
	fprintf( fd, itemdesc->bib_and );
    }

    /* if html link is required
       write begin of tag for link
    */
    if ( envdesc->use_html_links_between_pages 
	 && envdesc->use_html_links_towards_authors_pages
	 && ( names[n].status == TO_BE_INDEXED 
	      || names[n].status == PAGE_TO_BE_GENERATED ) ) {
      fprintf( fd, "<a href=\"../%s/%s.%s\">", 
	       envdesc->directory_authors,
	       names[n].equivKeyName,
	       envdesc->filename_extension );
    }

    /* write first name 
     */
    if ( names[n].firstName != NULL && names[n].firstName[0] != '\0' ) {
      write_string_or_file( fd, itemdesc->bib_firstname.start );
      fprintf( fd, "%s", names[n].firstName );
      write_string_or_file( fd, itemdesc->bib_firstname.end );
      /* write separator between first name and last name 
       */
      if ( names[n].lastName != NULL && names[n].lastName[0] != '\0' )
	fprintf( fd, " " );
    }

    /* write last name 
     */
    if ( names[n].lastName != NULL && names[n].lastName[0] != '\0' ) {
      write_string_or_file( fd, itemdesc->bib_lastname.start );
      fprintf( fd, "%s", names[n].lastName );
      write_string_or_file( fd, itemdesc->bib_lastname.end );
    }

    /* if html link is required
       write end of tag for link
    */
    if ( envdesc->use_html_links_between_pages 
	 && envdesc->use_html_links_towards_authors_pages
	 && ( names[n].status == TO_BE_INDEXED 
	      || names[n].status == PAGE_TO_BE_GENERATED ) )
      fprintf( fd, "</a>" );
  }
}





void write_item_authors( FILE *fd, typeItem *item, 
			 typeEnvironmentDescription *envdesc,
			 typeBibtexItemDescription *itemdesc )
{
  if ( item->nb_authors == 0 ) return;
  write_item_names( fd, item->authors_names, item->nb_authors,
		    envdesc, itemdesc );
  /* at the end of all names
   */
  fprintf( fd, itemdesc->bib_dot );
}





void write_item_editors( FILE *fd, typeItem *item, 
			 typeEnvironmentDescription *envdesc,
			 typeBibtexItemDescription *itemdesc,
			 char *endstring )
{
  if ( item->nb_editors == 0 ) return;
  write_item_names( fd, item->editors_names, item->nb_editors,
		    envdesc, itemdesc );

  /* at the end of all names
   */
  if ( item->nb_editors == 1 ) fprintf( fd, ", editor" );
  else fprintf( fd, ", editors" );

  if ( endstring != NULL ) fprintf( fd, "%s", endstring );
}





void write_item_year( FILE *fd, typeItem *item ) 
{
  if ( item->year == NO_YEAR ) {
    if ( item->field[YEAR] == NULL || item->field[YEAR][0] == '\0' ) {
      fprintf( fd, "????" );
    } else {
      fprintf( fd, "%s", item->field[YEAR] );
    }
    return;
  }
  if ( strlen( item->field[YEAR] ) != 4 ) {
    fprintf( fd, "%s", item->field[YEAR] );
  } else {
    fprintf( fd, "%d", item->year );
  }
  
}





void write_item_pages( FILE *fd, char *str, char *pages )
{
  unsigned int i;
  if ( pages == NULL || pages[0] == '\0' ) return;
  if ( str != NULL && str[0] != '\0' )
    fprintf( fd, "%s", str );
  fprintf( fd, "%c", pages[0] );
  for ( i=1; i<strlen( pages ); i++ ) {
    if ( pages[i] == '-' && pages[i-1] == '-' ) continue;
    fprintf( fd, "%c", pages[i] );
  }
}





void write_item_month_and_year( FILE *fd, typeItem *item, char *endstring )
{
  if ( item->field[MONTH] != NULL && item->field[MONTH][0] != '\0' ) {
    fprintf( fd, "%s", item->field[MONTH] );
    if ( item->field[YEAR] != NULL && item->field[YEAR][0] != '\0' ) {
      fprintf( fd, " " );
    }
  }
  if ( item->field[YEAR] != NULL && item->field[YEAR][0] != '\0' )
    write_item_year( fd, item );
  if ( endstring != NULL ) fprintf( fd, "%s", endstring );
}




void write_item_title( FILE *fd, typeItem *item, 
		       typeBibtexItemDescription *itemdesc,
		       char *endstring )
{

  write_item_customized_string( fd, item->field[TITLE], 
				&(itemdesc->bib_title), endstring );
}





void write_item_booktitle( FILE *fd, typeItem *item, 
		       typeBibtexItemDescription *itemdesc,
		       char *endstring )
{

  write_item_customized_string( fd, item->field[BOOKTITLE], 
				&(itemdesc->bib_booktitle), endstring );
}




void write_item_volume_number_series( FILE *fd, typeItem *item,
				      typeBibtexItemDescription *itemdesc,
				      char *endstring )
{
  if ( (item->field[VOLUME] != NULL && item->field[VOLUME][0] != '\0') ||
       (item->field[NUMBER] != NULL && item->field[NUMBER][0] != '\0') ||
       (item->field[SERIES] != NULL && item->field[SERIES][0] != '\0') ) 
    fprintf( fd, itemdesc->bib_comma );

  if ( item->field[VOLUME] != NULL && item->field[VOLUME][0] != '\0' ) {
    fprintf( fd, "volume %s", item->field[VOLUME] );
    if ( item->field[SERIES] != NULL && item->field[SERIES][0] != '\0' ) {
      fprintf( fd, " of " );
      write_item_customized_string( fd, item->field[SERIES],
				    &(itemdesc->bib_series), NULL );
    }
  } 

  else if ( item->field[NUMBER] != NULL && item->field[NUMBER][0] != '\0' ) {
    fprintf( fd, "number %s", item->field[NUMBER] );
    if ( item->field[SERIES] != NULL && item->field[SERIES][0] != '\0' )
      fprintf( fd, " of %s", item->field[SERIES] );
  } 

  else if ( item->field[SERIES] != NULL && item->field[SERIES][0] != '\0' ) {
    fprintf( fd, "%s", item->field[SERIES] );
  }

  if ( endstring != NULL ) fprintf( fd, "%s", endstring );
}













/* An article from a journal or magazine

   required fields
   author, title, journal, year

   optional fields
   volume, number, pages, month, note

   additional fields
   abstract, annote, comments
   url, postscript
*/

void write_article( FILE *fd, typeItem *item,
		    typeEnvironmentDescription *envdesc,
		    typeGenericFileDescription *gfiledesc,
		    typeSpecificFileDescription *sfiledesc,
		    typeBibtexItemDescription *itemdesc )
{
  write_item_authors( fd, item, envdesc, itemdesc );
  write_item_title( fd, item, itemdesc, itemdesc->bib_dot );

  write_item_customized_string( fd, item->field[JOURNAL],
				    &(itemdesc->bib_journal), itemdesc->bib_comma );

  if ( item->field[VOLUME] != NULL && item->field[VOLUME][0] != '\0' )
    fprintf( fd, "%s", item->field[VOLUME] );
  if ( item->field[NUMBER] != NULL && item->field[NUMBER][0] != '\0' )
    fprintf( fd, "(%s)", item->field[NUMBER] );
  if ( item->field[PAGES] != NULL && item->field[PAGES][0] != '\0' ) {
    if ( (item->field[VOLUME] != NULL && item->field[VOLUME][0] != '\0') ||
	 (item->field[NUMBER] != NULL && item->field[NUMBER][0] != '\0') ) {
      /* fprintf( fd, ":%s", item->field[PAGES] ); */
      write_item_pages( fd, ":", item->field[PAGES] );
    } else {
      /* fprintf( fd, "pp %s", item->field[PAGES] ); */
      write_item_pages( fd, "pp ", item->field[PAGES] );
    }
  }

  if ( (item->field[VOLUME] != NULL && item->field[VOLUME][0] != '\0') ||
       (item->field[NUMBER] != NULL && item->field[NUMBER][0] != '\0') ||
       (item->field[PAGES]  != NULL && item->field[PAGES][0]  != '\0') ) {
    fprintf( fd, itemdesc->bib_comma );
  }

  write_item_month_and_year( fd, item, itemdesc->bib_dot );
    
  write_item_note( fd, item, itemdesc, itemdesc->bib_dot );

  write_item_additional_fields( fd, item, envdesc, gfiledesc, sfiledesc, itemdesc );
}





/* A book with an explicit publisher.

   required fields
   author or editor, title, publisher, year

   optional fields
   volume, number, series, address, edition, month, note

   additional fields
   abstract, annote, comments
   url, postscript
 */

void write_book( FILE *fd, typeItem *item,
		 typeEnvironmentDescription *envdesc,
		 typeGenericFileDescription *gfiledesc,
		 typeSpecificFileDescription *sfiledesc,
		 typeBibtexItemDescription *itemdesc )
{
  if ( item->nb_authors > 0 ) 
    write_item_authors( fd, item, envdesc, itemdesc );
  else
    /* write editors
       originally: write links
    */
    write_item_editors( fd, item, envdesc, itemdesc, itemdesc->bib_dot );

  write_item_title( fd, item, itemdesc, NULL );  

  write_item_volume_number_series( fd, item, itemdesc, itemdesc->bib_dot );

  if ( item->field[PUBLISHER] != NULL && item->field[PUBLISHER][0] != '\0' ) {
    fprintf( fd, "%s", item->field[PUBLISHER] );
    fprintf( fd, itemdesc->bib_comma );
    
  }
  if ( item->field[ADDRESS] != NULL && item->field[ADDRESS][0] != '\0' ) {
    fprintf( fd, "%s", item->field[ADDRESS] );
    fprintf( fd, itemdesc->bib_comma );
  }
  if ( item->field[EDITION] != NULL && item->field[EDITION][0] != '\0' ) {
    fprintf( fd, "%s edition", item->field[EDITION] );
    fprintf( fd, itemdesc->bib_comma );
  }

  write_item_month_and_year( fd, item, itemdesc->bib_dot );

  write_item_note( fd, item, itemdesc, itemdesc->bib_dot );

  write_item_additional_fields( fd, item, envdesc, gfiledesc, sfiledesc, itemdesc );
}





/* A work that is printed and bound,
   but without a named publisher or sponsoring institution.

   required fields
   title

   optional fields
   author, howpublished, address, month, year, note

   additional fields
   abstract, annote, comments
   url, postscript
 */

void write_booklet( FILE *fd, typeItem *item,
		    typeEnvironmentDescription *envdesc,
		    typeGenericFileDescription *gfiledesc,
		    typeSpecificFileDescription *sfiledesc,
		    typeBibtexItemDescription *itemdesc )
{
  write_item_authors( fd, item, envdesc, itemdesc );
  write_item_title( fd, item, itemdesc, itemdesc->bib_dot );

  if ( item->field[HOWPUBLISHED] != NULL && item->field[HOWPUBLISHED][0] != '\0' ) {
    fprintf( fd, "%s", item->field[HOWPUBLISHED] );
  }
  if ( item->field[ADDRESS] != NULL && item->field[ADDRESS][0] != '\0' ) {
    if ( item->field[HOWPUBLISHED] != NULL && item->field[HOWPUBLISHED][0] != '\0' )
      fprintf( fd, itemdesc->bib_comma );
    fprintf( fd, "%s", item->field[ADDRESS] );
  }
  if ( (item->field[MONTH] != NULL && item->field[MONTH][0] != '\0') ||
       (item->field[YEAR] != NULL && item->field[YEAR][0] != '\0') ) {
    if ( (item->field[HOWPUBLISHED] != NULL && item->field[HOWPUBLISHED][0] != '\0') ||
	 (item->field[ADDRESS] != NULL && item->field[ADDRESS][0] != '\0') )
      fprintf( fd, itemdesc->bib_comma );

    write_item_month_and_year( fd, item, itemdesc->bib_dot );

  }
  
  write_item_note( fd, item, itemdesc, itemdesc->bib_dot );
  
  write_item_additional_fields( fd, item, envdesc, gfiledesc, sfiledesc, itemdesc );

}





/* A part of a book,
   which may be a chapter (or section or whatever) and/or a range of pages.

   required fields
   author or editor, title, chapter and/or pages, publisher, year.

   optional fields
   volume or number, series, type, address, edition, month, note.
*/
void write_inbook( FILE *fd, typeItem *item,
		   typeEnvironmentDescription *envdesc,
		   typeGenericFileDescription *gfiledesc,
		   typeSpecificFileDescription *sfiledesc,
		   typeBibtexItemDescription *itemdesc )
{

  if ( item->nb_authors > 0 ) 
    write_item_authors( fd, item, envdesc, itemdesc );
  else
    /* write editors
       originally: write links
    */
    write_item_editors( fd, item, envdesc, itemdesc, itemdesc->bib_dot );

  write_item_title( fd, item, itemdesc, NULL );

  if ( item->field[VOLUME] != NULL && item->field[VOLUME][0] != '\0' ) {
    fprintf( fd, itemdesc->bib_comma );
    fprintf( fd, "volume %s", item->field[VOLUME] );
    if ( item->field[SERIES] != NULL && item->field[SERIES][0] != '\0' ) {
      fprintf( fd, " of " );
      write_item_customized_string( fd, item->field[SERIES],
				    &(itemdesc->bib_series), NULL );
    }
  }
  
  if ( item->field[CHAPTER] != NULL && item->field[CHAPTER][0] != '\0' ) {
    fprintf( fd, itemdesc->bib_comma );
    if ( item->field[TYPE] != NULL && item->field[TYPE][0] != '\0' )
      fprintf( fd, "%s ", item->field[TYPE] );
    else 
      fprintf( fd, "chapter " );
    fprintf( fd, "%s", item->field[CHAPTER] );
  }

  if ( item->field[PAGES] != NULL && item->field[PAGES][0] != '\0' ) {
    fprintf( fd, itemdesc->bib_comma );
    /* fprintf( fd, "pages %s", item->field[PAGES] ); */
    write_item_pages( fd, "pages ", item->field[PAGES] );
  }

  fprintf( fd, itemdesc->bib_dot );

  if ( ((item->field[NUMBER] != NULL && item->field[NUMBER][0] != '\0') ||
	(item->field[SERIES] != NULL && item->field[SERIES][0] != '\0')) &&
       (item->field[VOLUME] == NULL || item->field[VOLUME][0] == '\0') ) {
    if ( item->field[NUMBER] != NULL && item->field[NUMBER][0] != '\0' ) {
      fprintf( fd, "Number %s", item->field[NUMBER] );
      if ( item->field[SERIES] != NULL && item->field[SERIES][0] != '\0' ) {
	fprintf( fd, " in %s", item->field[SERIES] );
      }
    } else {
      fprintf( fd, "%s", item->field[SERIES] );
    }
    fprintf( fd, itemdesc->bib_dot );
  }
  
  if ( item->field[PUBLISHER] != NULL && item->field[PUBLISHER][0] != '\0' ) {
    fprintf( fd, "%s", item->field[PUBLISHER] );
    fprintf( fd, itemdesc->bib_comma );
  }
  if ( item->field[ADDRESS] != NULL && item->field[ADDRESS][0] != '\0' ) {
    fprintf( fd, "%s", item->field[ADDRESS] );
    fprintf( fd, itemdesc->bib_comma );
  }
  if ( item->field[EDITION] != NULL && item->field[EDITION][0] != '\0' ) {
    fprintf( fd, "%s edition", item->field[EDITION] );
    fprintf( fd, itemdesc->bib_comma );
  }

  write_item_month_and_year( fd, item, itemdesc->bib_dot );
  
  write_item_note( fd, item, itemdesc, itemdesc->bib_dot );

  write_item_additional_fields( fd, item, envdesc, gfiledesc, sfiledesc, itemdesc );

}





/* A part of a book having its own title.

   required fields
   author, title, booktitle, publisher, year

   optional fields
   editor, volume or number, series, type, chapter, pages,
   address, edition, month, note.

   additional fields
   abstract, annote, comments
   url, postscript
 */
void write_incollection( FILE *fd, typeItem *item,
			 typeEnvironmentDescription *envdesc,
			 typeGenericFileDescription *gfiledesc,
			 typeSpecificFileDescription *sfiledesc,
			 typeBibtexItemDescription *itemdesc )
{
  write_item_authors( fd, item, envdesc, itemdesc );
  write_item_title( fd, item, itemdesc, itemdesc->bib_dot );

  fprintf( fd, "In " );
  /* write editors
     originally: do not write links
  */
  if ( item->nb_editors  > 0 )
    write_item_editors( fd, item, envdesc, itemdesc, ", " );
  

  if ( item->field[BOOKTITLE] != NULL && item->field[BOOKTITLE][0] != '\0' ) 
    write_item_customized_string( fd, item->field[BOOKTITLE],
				  &(itemdesc->bib_booktitle), NULL );

  write_item_volume_number_series( fd, item, itemdesc, NULL );
    
  if ( item->field[CHAPTER] != NULL && item->field[CHAPTER][0] != '\0' ) {
    fprintf( fd, itemdesc->bib_comma );
    if ( item->field[TYPE] != NULL && item->field[TYPE][0] != '\0' )
      fprintf( fd, "%s ", item->field[TYPE] );
    else 
      fprintf( fd, "chapter " );
    fprintf( fd, "%s", item->field[CHAPTER] );
  }
  
  if ( item->field[PAGES] != NULL && item->field[PAGES][0] != '\0' ) {
    fprintf( fd, itemdesc->bib_comma );
    /* fprintf( fd, "pages %s", item->field[PAGES] ); */
    write_item_pages( fd, "pages ", item->field[PAGES] );
  }

  fprintf( fd, itemdesc->bib_dot );
  
  if ( item->field[PUBLISHER] != NULL && item->field[PUBLISHER][0] != '\0' ) {
    fprintf( fd, "%s", item->field[PUBLISHER] );
    fprintf( fd, itemdesc->bib_comma );
  }
  if ( item->field[ADDRESS] != NULL && item->field[ADDRESS][0] != '\0' ) {
    fprintf( fd, "%s", item->field[ADDRESS] );
    fprintf( fd, itemdesc->bib_comma );
  }
  if ( item->field[EDITION] != NULL && item->field[EDITION][0] != '\0' ) {
    fprintf( fd, "%s edition", item->field[EDITION] );
    fprintf( fd, itemdesc->bib_comma );
  }

  write_item_month_and_year( fd, item, itemdesc->bib_dot );

  write_item_note( fd, item, itemdesc, itemdesc->bib_dot );

  write_item_additional_fields( fd, item, envdesc, gfiledesc, sfiledesc, itemdesc );
}





/* An article in a conference proceedings.

   required fields
   author, title, booktitle, year

   optional fields
   editor, volume or number, series, pages 
   address, month, organization, publisher, note.

 */
void write_inproceedings( FILE *fd, typeItem *item,
			  typeEnvironmentDescription *envdesc,
			  typeGenericFileDescription *gfiledesc,
			  typeSpecificFileDescription *sfiledesc,
			  typeBibtexItemDescription *itemdesc )
{
  write_item_authors( fd, item, envdesc, itemdesc );
  write_item_title( fd, item, itemdesc, itemdesc->bib_dot );

  fprintf( fd, "In " );
  /* write editors
     originally: do not write links
  */
  if ( item->nb_editors  > 0 )
    write_item_editors( fd, item, envdesc, itemdesc, itemdesc->bib_comma );

  
  if ( item->field[BOOKTITLE] != NULL && item->field[BOOKTITLE][0] != '\0' ) 
    write_item_customized_string( fd, item->field[BOOKTITLE],
				  &(itemdesc->bib_booktitle), NULL );
  
  write_item_volume_number_series( fd, item, itemdesc, itemdesc->bib_comma );

  if ( item->field[ADDRESS] != NULL && item->field[ADDRESS][0] != '\0' ) {
    fprintf( fd, "%s", item->field[ADDRESS] );
    fprintf( fd, itemdesc->bib_comma );
  }
  if ( item->field[PAGES] != NULL && item->field[PAGES][0] != '\0' ) {
    /* fprintf( fd, "pages %s", item->field[PAGES] ); */
    write_item_pages( fd, "pages ", item->field[PAGES] );
    fprintf( fd, itemdesc->bib_comma );
  }

  write_item_month_and_year( fd, item, itemdesc->bib_dot );
  
  if ( item->field[ORGANIZATION] != NULL && item->field[ORGANIZATION][0] != '\0' ) {
    fprintf( fd, "%s", item->field[ORGANIZATION] );
    if ( item->field[PUBLISHER] != NULL && item->field[PUBLISHER][0] != '\0' ) {
      fprintf( fd, itemdesc->bib_comma );
      fprintf( fd, "%s", item->field[PUBLISHER] );
    }
    fprintf( fd, itemdesc->bib_dot );
  } 
  else if ( item->field[PUBLISHER] != NULL && item->field[PUBLISHER][0] != '\0' ) {
    fprintf( fd, "%s", item->field[PUBLISHER] );
    fprintf( fd, itemdesc->bib_dot );
  }

  write_item_note( fd, item, itemdesc, itemdesc->bib_dot );

  write_item_additional_fields( fd, item, envdesc, gfiledesc, sfiledesc, itemdesc );
}





/* The proceedings of a conference.

   required fields
   title, year

   optional fields
   editor, volume or number, series, 
   address, month, organization, publisher, note.

   additional fields
   abstract, annote, comments
   url, postscript
*/
void write_proceedings( FILE *fd, typeItem *item,
			typeEnvironmentDescription *envdesc,
			typeGenericFileDescription *gfiledesc,
			typeSpecificFileDescription *sfiledesc,
			typeBibtexItemDescription *itemdesc )
{
  /* write editors
     originally: write links
  */
  if ( item->nb_editors > 0 )
    write_item_editors( fd, item, envdesc, itemdesc, itemdesc->bib_dot );

  if ( item->field[TITLE] != NULL && item->field[TITLE][0] != '\0' ) {
    write_item_title( fd, item, itemdesc, NULL );
  }
  else if ( item->field[BOOKTITLE] != NULL && item->field[BOOKTITLE][0] != '\0' ) {
    write_item_booktitle( fd, item, itemdesc, NULL );
  }

  write_item_volume_number_series( fd, item, itemdesc, itemdesc->bib_comma );

  if ( item->field[ADDRESS] != NULL && item->field[ADDRESS][0] != '\0' ) {
    fprintf( fd, "%s", item->field[ADDRESS] );
    fprintf( fd, itemdesc->bib_comma );
  }

  write_item_month_and_year( fd, item, itemdesc->bib_dot );
  
  if ( item->field[ORGANIZATION] != NULL && item->field[ORGANIZATION][0] != '\0' ) {
    fprintf( fd, "%s", item->field[ORGANIZATION] );
    if ( item->field[PUBLISHER] != NULL && item->field[PUBLISHER][0] != '\0' ) {
      fprintf( fd, itemdesc->bib_comma );
      fprintf( fd, "%s", item->field[PUBLISHER] );
    }
    fprintf( fd, itemdesc->bib_dot );
  } 
  else if ( item->field[PUBLISHER] != NULL && item->field[PUBLISHER][0] != '\0' ) {
    fprintf( fd, "%s", item->field[PUBLISHER] );
    fprintf( fd, itemdesc->bib_dot );
  }

  write_item_note( fd, item, itemdesc, itemdesc->bib_dot );

  write_item_additional_fields( fd, item, envdesc, gfiledesc, sfiledesc, itemdesc );
}





/* A PhD thesis, or a  Master's thesis.

   required fields
   author, title, school, year

   optional fields
   type, address, month, note.

   additional fields
   abstract, annote, comments
   url, postscript
 */
void write_thesis_like( FILE *fd, typeItem *item,
			typeEnvironmentDescription *envdesc,
			typeGenericFileDescription *gfiledesc,
			typeSpecificFileDescription *sfiledesc,
			typeBibtexItemDescription *itemdesc,
			char *default_type )
{
  write_item_authors( fd, item, envdesc, itemdesc );
  write_item_title( fd, item, itemdesc, itemdesc->bib_dot );

  if ( item->field[TYPE] != NULL && item->field[TYPE][0] != '\0' )
    fprintf( fd, "%s", item->field[TYPE] );
  else
    fprintf( fd, "%s", default_type );
  fprintf( fd, itemdesc->bib_comma );
  
  if ( item->field[SCHOOL] != NULL && item->field[SCHOOL][0] != '\0' ) {
    fprintf( fd, "%s", item->field[SCHOOL] );
    fprintf( fd, itemdesc->bib_comma );
  }
  if ( item->field[ADDRESS] != NULL && item->field[ADDRESS][0] != '\0' ) {
    fprintf( fd, "%s", item->field[ADDRESS] );
    fprintf( fd, itemdesc->bib_comma );
  }

  write_item_month_and_year( fd, item, itemdesc->bib_dot );

  write_item_note( fd, item, itemdesc, itemdesc->bib_dot );

  write_item_additional_fields( fd, item, envdesc, gfiledesc, sfiledesc, itemdesc );
}





/* A report published by a school or other institution,
   usually numbered within a series.

   required fields
   author, title, institution, year

   optional fields
   type, number, address, month, note.
*/
void write_techreport_like( FILE *fd, typeItem *item,
			    typeEnvironmentDescription *envdesc,
			    typeGenericFileDescription *gfiledesc,
			    typeSpecificFileDescription *sfiledesc,
			    typeBibtexItemDescription *itemdesc,
			    char *default_type )
{
  write_item_authors( fd, item, envdesc, itemdesc );
  write_item_title( fd, item, itemdesc, itemdesc->bib_dot );

  if ( item->field[TYPE] != NULL && item->field[TYPE][0] != '\0' )
    fprintf( fd, "%s", item->field[TYPE] );
  else 
    fprintf( fd, "%s", default_type );
  
  if ( item->field[NUMBER] != NULL && item->field[NUMBER][0] != '\0' )
    fprintf( fd, " %s", item->field[NUMBER] );

  fprintf( fd, itemdesc->bib_comma );

  if ( item->field[INSTITUTION] != NULL && item->field[INSTITUTION][0] != '\0' ) {
    fprintf( fd, "%s", item->field[INSTITUTION] );
    fprintf( fd, itemdesc->bib_comma );
  }

  if ( item->field[ADDRESS] != NULL && item->field[ADDRESS][0] != '\0' ) {
    fprintf( fd, "%s", item->field[ADDRESS] );
    fprintf( fd, itemdesc->bib_comma );
  }

  write_item_month_and_year( fd, item, itemdesc->bib_dot );

  write_item_note( fd, item, itemdesc, itemdesc->bib_dot );

  write_item_additional_fields( fd, item, envdesc, gfiledesc, sfiledesc, itemdesc );
}






/* @standard : title, author, number, year, month, institution


*/
void write_standard( FILE *fd, typeItem *item,
			    typeEnvironmentDescription *envdesc,
			    typeGenericFileDescription *gfiledesc,
			    typeSpecificFileDescription *sfiledesc,
			    typeBibtexItemDescription *itemdesc )
{
  write_item_authors( fd, item, envdesc, itemdesc );
  write_item_title( fd, item, itemdesc, itemdesc->bib_dot );

  if ( item->field[NUMBER] != NULL && item->field[NUMBER][0] != '\0' )
    fprintf( fd, " %s", item->field[NUMBER] );

  fprintf( fd, itemdesc->bib_comma );

  if ( item->field[INSTITUTION] != NULL && item->field[INSTITUTION][0] != '\0' ) {
    fprintf( fd, "%s", item->field[INSTITUTION] );
    fprintf( fd, itemdesc->bib_comma );
  }

  if ( item->field[ADDRESS] != NULL && item->field[ADDRESS][0] != '\0' ) {
    fprintf( fd, "%s", item->field[ADDRESS] );
    fprintf( fd, itemdesc->bib_comma );
  }

  write_item_month_and_year( fd, item, itemdesc->bib_dot );

  write_item_note( fd, item, itemdesc, itemdesc->bib_dot );

  write_item_additional_fields( fd, item, envdesc, gfiledesc, sfiledesc, itemdesc );
}






/* @patent : title, author, number, year, address, note 

*/
void write_patent( FILE *fd, typeItem *item,
			    typeEnvironmentDescription *envdesc,
			    typeGenericFileDescription *gfiledesc,
			    typeSpecificFileDescription *sfiledesc,
			    typeBibtexItemDescription *itemdesc )
{
  write_item_authors( fd, item, envdesc, itemdesc );
  write_item_title( fd, item, itemdesc, itemdesc->bib_dot );

  if ( item->field[NUMBER] != NULL && item->field[NUMBER][0] != '\0' )
    fprintf( fd, " %s", item->field[NUMBER] );

  fprintf( fd, itemdesc->bib_comma );

  if ( item->field[ADDRESS] != NULL && item->field[ADDRESS][0] != '\0' ) {
    fprintf( fd, "%s", item->field[ADDRESS] );
    fprintf( fd, itemdesc->bib_comma );
  }

  write_item_month_and_year( fd, item, itemdesc->bib_dot );

  write_item_note( fd, item, itemdesc, itemdesc->bib_dot );

  write_item_additional_fields( fd, item, envdesc, gfiledesc, sfiledesc, itemdesc );
}




/* Technical documentation.

   required fields
   title
   
   optional fields 
   author, organization, address, edition, month, year, note
*/
void write_manual( FILE *fd, typeItem *item,
		   typeEnvironmentDescription *envdesc,
		   typeGenericFileDescription *gfiledesc,
		   typeSpecificFileDescription *sfiledesc,
		   typeBibtexItemDescription *itemdesc )
{
  write_item_authors( fd, item, envdesc, itemdesc );
  write_item_title( fd, item, itemdesc, itemdesc->bib_dot );


  if ( (item->field[ORGANIZATION] != NULL && item->field[ORGANIZATION][0] != '\0')  ||
       (item->field[ADDRESS] != NULL && item->field[ADDRESS][0] != '\0') ||
       (item->field[EDITION] != NULL && item->field[EDITION][0] != '\0') ||
       (item->field[MONTH] != NULL && item->field[MONTH][0] != '\0') ||
       (item->field[YEAR] != NULL && item->field[YEAR][0] != '\0') ) {
    
    if ( item->field[ORGANIZATION] != NULL && item->field[ORGANIZATION][0] != '\0' ) {
      fprintf( fd, "%s", item->field[ORGANIZATION] );
      if ( (item->field[ADDRESS] != NULL && item->field[ADDRESS][0] != '\0') ||
	   (item->field[EDITION] != NULL && item->field[EDITION][0] != '\0') ||
	   (item->field[MONTH] != NULL && item->field[MONTH][0] != '\0') ||
	   (item->field[YEAR] != NULL && item->field[YEAR][0] != '\0') )
	fprintf( fd, itemdesc->bib_comma );
    }
    
    if( item->field[ADDRESS] != NULL && item->field[ADDRESS][0] != '\0' ) {
      fprintf( fd, "%s", item->field[ADDRESS] );
      if ( (item->field[EDITION] != NULL && item->field[EDITION][0] != '\0') ||
	   (item->field[MONTH] != NULL && item->field[MONTH][0] != '\0') ||
	   (item->field[YEAR] != NULL && item->field[YEAR][0] != '\0') )
	fprintf( fd, itemdesc->bib_comma );
    }
    
    if ( item->field[EDITION] != NULL && item->field[EDITION][0] != '\0' ) {
      fprintf( fd, "%s edition", item->field[EDITION] );
      if ( (item->field[MONTH] != NULL && item->field[MONTH][0] != '\0') ||
	   (item->field[YEAR] != NULL && item->field[YEAR][0] != '\0') )
	fprintf( fd, itemdesc->bib_comma );
    }

    write_item_month_and_year( fd, item, itemdesc->bib_dot );
  }
  
  write_item_note( fd, item, itemdesc, itemdesc->bib_dot );
  
  write_item_additional_fields( fd, item, envdesc, gfiledesc, sfiledesc, itemdesc );
}





/* Use this type when nothing else fits.

   optional fields 
   author, title, howpublished, month, year, note
*/
void write_misc( FILE *fd, typeItem *item,
		 typeEnvironmentDescription *envdesc,
		 typeGenericFileDescription *gfiledesc,
		 typeSpecificFileDescription *sfiledesc,
		 typeBibtexItemDescription *itemdesc )
{
  write_item_authors( fd, item, envdesc, itemdesc );
  write_item_title( fd, item, itemdesc, NULL );
  
  if ( item->field[HOWPUBLISHED] != NULL && item->field[HOWPUBLISHED][0] != '\0' ) {
    if ( item->field[TITLE] != NULL && item->field[TITLE][0] != '\0' )
      fprintf( fd, itemdesc->bib_dot );
    fprintf( fd, "%s", item->field[HOWPUBLISHED] );
  }

  if ( (item->field[MONTH] != NULL && item->field[MONTH][0] != '\0') ||
       (item->field[YEAR] != NULL && item->field[YEAR][0] != '\0') ) {

    if ( item->field[TITLE] != NULL && item->field[TITLE][0] != '\0' )
      fprintf( fd, itemdesc->bib_comma );

    write_item_month_and_year( fd, item, itemdesc->bib_dot );

  } else {
    if ( (item->field[HOWPUBLISHED] != NULL && item->field[HOWPUBLISHED][0] != '\0') ||
	 (item->field[TITLE] != NULL && item->field[TITLE][0] != '\0') )
      fprintf( fd, itemdesc->bib_dot );
  }

  write_item_note( fd, item, itemdesc, itemdesc->bib_dot );

  write_item_additional_fields( fd, item, envdesc, gfiledesc, sfiledesc, itemdesc );
}





/* A document having an author and title,
   but not formally published.

   required fields
   author, title, note

   optional fields 
   month, year
*/
void write_unpublished( FILE *fd, typeItem *item,
			typeEnvironmentDescription *envdesc,
			typeGenericFileDescription *gfiledesc,
			typeSpecificFileDescription *sfiledesc,
			typeBibtexItemDescription *itemdesc )
{
  write_item_authors( fd, item, envdesc, itemdesc );
  write_item_title( fd, item, itemdesc, itemdesc->bib_dot );

  write_item_note( fd, item, itemdesc, NULL );
  
  if ( (item->field[MONTH] != NULL && item->field[MONTH][0] != '\0') ||
       (item->field[YEAR] != NULL && item->field[YEAR][0] != '\0') ) {
    fprintf( fd, itemdesc->bib_comma );
    write_item_month_and_year( fd, item, NULL );
  }

  fprintf( fd, itemdesc->bib_dot );

  write_item_additional_fields( fd, item, envdesc, gfiledesc, sfiledesc, itemdesc );
}





void write_item( FILE *fd, typeItem *item,
		 typeEnvironmentDescription *envdesc,
		 typeGenericFileDescription *gfiledesc,
		 typeSpecificFileDescription *sfiledesc,
		 typeBibtexItemDescription *itemdesc,
		 int write_bibtex_entry )
{

  if ( write_bibtex_entry ) {
    fprintf( fd, "\n%s\n", item->bibtex_entry );
    return;
  }

  write_string_or_file( fd, gfiledesc->item_tag.start );

  if ( envdesc->use_html_links_between_pages && sfiledesc->write_anchor ) {
    if ( item->bibtex_key != NULL && item->bibtex_key[0] != '\0' )
      fprintf( fd, "<a name=\"%s\"></a>", item->bibtex_key );
  }

  switch( item->type_item ) {
  default :
    break;
  case ARTICLE :
    write_article( fd, item,
		envdesc, gfiledesc, sfiledesc, itemdesc );
    break;
  case BOOK :
    write_book( fd, item,
		envdesc, gfiledesc, sfiledesc, itemdesc );
    break;
  case BOOKLET :
    write_booklet( fd, item,
		envdesc, gfiledesc, sfiledesc, itemdesc );
    break;
  case INBOOK :
    write_inbook( fd, item,
		envdesc, gfiledesc, sfiledesc, itemdesc );
    break;
  case INCOLLECTION :
    write_incollection( fd, item,
		envdesc, gfiledesc, sfiledesc, itemdesc );
    break;
  case INPROCEEDINGS :
    write_inproceedings( fd, item,
		envdesc, gfiledesc, sfiledesc, itemdesc );
    break;

  case PROCEEDINGS :
    write_proceedings( fd, item,
		       envdesc, gfiledesc, sfiledesc, itemdesc );
    break;

  case PHDTHESIS :
    write_thesis_like( fd, item,
		envdesc, gfiledesc, sfiledesc, itemdesc, "PhD thesis" );
    break;
  case MASTERSTHESIS :
    write_thesis_like( fd, item,
		envdesc, gfiledesc, sfiledesc, itemdesc, "Master's thesis" );
    break;

  case TECHREPORT :
    write_techreport_like( fd, item,
			   envdesc, gfiledesc, sfiledesc, itemdesc, "Technical report" );
    break;
  case PATENT :
    write_patent( fd, item,
		  envdesc, gfiledesc, sfiledesc, itemdesc );
    break;

  case STANDARD :
    write_standard( fd, item,
		  envdesc, gfiledesc, sfiledesc, itemdesc );
    break;

  case MANUAL :
    write_manual( fd, item,
		envdesc, gfiledesc, sfiledesc, itemdesc );
    break;
  case MISC :
    write_misc( fd, item,
		envdesc, gfiledesc, sfiledesc, itemdesc );
    break;
  case UNPUBLISHED :
    write_unpublished( fd, item,
		envdesc, gfiledesc, sfiledesc, itemdesc );
    break;
  }

  write_string_or_file( fd, gfiledesc->item_tag.end );

  if ( fflush( fd ) != 0 ) {
    if ( _debug_ )
      fprintf( stderr, "write_item: unable to 'flush' open file\n" );
  }
}










/************************************************************
 *
 *
 * WRITING BIBTEX
 *
 ************************************************************/





void print_bibtex_string( FILE *f, char *str )
{
  if ( str == NULL || *str == '\0' ) return;
  /* translation into 7 bits (to be done)
   */
  fprintf( f, "%s", str );
}


void print_bibtex_long_string( FILE *f, char *string,
			       char *offset, int length, int line_length )
{
  int l = length;
  int k;
  char *tmp;
  char *str = string;
  
  if ( str == NULL || *str == '\0' ) return;

  /* translation into 7 bits (to be done)
   */
  if ( (int)strlen(str) + l <= line_length ) {
    fprintf( f, "%s", str );
    return;
  }
  
  while ( *str != '\0' ) {

    /* write one single space
     */
    if ( IS_SPACE_OR_RETURN( *str ) ) {
      fprintf( f, " " );
      l ++;
      while( IS_SPACE_OR_RETURN( *str ) && *str != '\0' ) str ++;
    }
    if ( *str == '\0' ) return;

    /* get word length
     */
    tmp = str;
    k = 0;
    while( !(IS_SPACE_OR_RETURN( *tmp )) && *tmp != '\0' ) {
      tmp++; k++;
    }
    
    /* write on a new line ?
     */
    if ( k+l > line_length ) {
      fprintf( f, "\n" );
      fprintf( f, "%s%s", offset, offset );
      l = 2 * strlen( offset );
    }

    /* write word
     */
    while( !(IS_SPACE_OR_RETURN( *str )) && *str != '\0' ) {
      fprintf( f, "%c", *str );
      str ++;
      l++;
    }
    
  }
}





void print_bibtex_item_keywords( FILE *f, typeName *theKeywords, int nb_keywords,
			      char *offset, int length, int line_length )
{
  int i;
  int l = length;
  int k;

  for ( i=0; i<nb_keywords; i++ ) {

    k = strlen( theKeywords[i].lastName) + strlen( theKeywords[i].firstName ) + 1;
    if ( k+l > line_length ) {
      fprintf( f, "\n" );
      fprintf( f, "%s%s", offset, offset );
      l = 2*strlen(offset);
    }

    print_bibtex_string( f, theKeywords[i].lastName );
    l += strlen( theKeywords[i].lastName );

    if ( i+1 < nb_keywords ) {
      fprintf( f, ", " );
      l += 2;
    }
  }
}





void print_bibtex_item_names( FILE *f, typeName *theNames, int nb_names,
			      char *offset, int length, int line_length )
{
  int i;
  int l = length;
  int k;

  for ( i=0; i<nb_names; i++ ) {

    k = strlen( theNames[i].lastName) + strlen( theNames[i].firstName ) + 1;
    if ( k+l > line_length ) {
      fprintf( f, "\n" );
      fprintf( f, "%s%s", offset, offset );
      l = 2*strlen(offset);
    }

    if ( count_words( theNames[i].lastName ) == 0 ) {
      if ( count_words( theNames[i].firstName ) == 0 ) {
	continue;
      }
      print_bibtex_string( f, theNames[i].firstName );
      l += strlen( theNames[i].firstName );
    }
    else if ( count_words( theNames[i].lastName ) == 1 ) {
      print_bibtex_string( f, theNames[i].firstName );
      fprintf( f, " " );
      print_bibtex_string( f, theNames[i].lastName );
      l += strlen( theNames[i].firstName ) + 1 + strlen( theNames[i].lastName );
    }
    else {
      print_bibtex_string( f, theNames[i].lastName );
      fprintf( f, ", " );
      print_bibtex_string( f, theNames[i].firstName );
      l += strlen( theNames[i].firstName ) + 1 + strlen( theNames[i].lastName );
    }
    
    if ( i+1 < nb_names ) {
      fprintf( f, " and " );
      l += 5;
    }
  }
}








void print_bibtex_item( FILE *f, typeItem *item )
{
  enumTypeItem type_item = item->type_item;
  int line_length = 75;
  int nfields, i;
  int a_field_has_been_written;
  enumTypeBibtexField type_field;
  char *offset = "   ";
  

  /* must count the number of fields to be written
   */
  nfields = 0;
  for ( i=0; i<DESC_FIELD_NB; i++ ) {
    switch( desc_fields_bibtex_item[type_item][i] ) {
      
    case _REQUIRED_ :
      /*    case _ALTERNAT_ :*/
    case _OPTIONAL_ :
      nfields ++;
      break;

    default :
    case _IGNORED__ :
      switch( i ) {
      case AUTHOR :
	if ( item->nb_authors >= 1 ) nfields ++;
	break;
      case EDITOR :
	if ( item->nb_editors >= 1 ) nfields ++;
	break;
      case KEYWORD :
	break;
      case KEYWORDS :
	if ( item->nb_keywords >= 1 ) nfields ++;
	break;
      default :
	if ( item->field[i] != NULL && item->field[i][0] != '\0' ) nfields ++;
	break;
      }
    }
    
  }
  

  /* go for writing
   */
  fprintf( f, "@%s{", desc_item[type_item] );
  if ( item->bibtex_key != NULL && item->bibtex_key != '\0' )
    fprintf( f, "%s", item->bibtex_key );
  fprintf( f, ",\n" );
  
    for ( i=0; i<DESC_FIELD_NB; i++ ) {

      a_field_has_been_written = 1;
      type_field = desc_fields_bibtex_item[type_item][i];

      switch( i ) {

      case KEYWORD : 
	a_field_has_been_written = 0;
	break;

      case KEYWORDS : 
	if ( item->nb_keywords > 0 ) {
	  fprintf( f, "%s%-12s = ", offset, desc_field[i] );
	  fprintf( f, "{" );
	  print_bibtex_item_keywords( f, item->keywords, item->nb_keywords,
				      offset, strlen(offset)+12+4, line_length );
	  fprintf( f, "}" );
	} 
	else {
	  switch( type_field ) {
	  default :
	  case _IGNORED__ :
	    a_field_has_been_written = 0;
	    break;
	  case _REQUIRED_ :
	    fprintf( f, "%s%-12s = {}", offset, desc_field[i] );
	    break;
	    /*
	  case _ALTERNAT_ :
	    fprintf( f, "%sALT%-9s = {}", offset, desc_field[i] );
	    break;
	    */
	  case _OPTIONAL_ :
	    fprintf( f, "%sOPT%-9s = {}", offset, desc_field[i] );
	    break;
	  }
	}
	break;

      case AUTHOR : 
	if ( i == AUTHOR && item->nb_authors > 0 ) {
	  fprintf( f, "%s%-12s = ", offset, desc_field[i] );
	  fprintf( f, "{" );
	  print_bibtex_item_names( f, item->authors_names, item->nb_authors,
				   offset, strlen(offset)+12+4, line_length );
	  fprintf( f, "}" );
	}
	else {
	  switch( type_field ) {
	  default :
	  case _IGNORED__ :
	    a_field_has_been_written = 0;
	    break;
	  case _REQUIRED_ :
	    fprintf( f, "%s%-12s = {}", offset, desc_field[i] );
	    break;
	    /*
	  case _ALTERNAT_ :
	    fprintf( f, "%sALT%-9s = {}", offset, desc_field[i] );
	    break;
	    */
	  case _OPTIONAL_ :
	    fprintf( f, "%sOPT%-9s = {}", offset, desc_field[i] );
	    break;
	  }
	}
	break;
	
      case EDITOR :
	if ( i == EDITOR && item->nb_editors > 0 ) {
	  fprintf( f, "%s%-12s = ", offset, desc_field[i] );
	  fprintf( f, "{" );
	  print_bibtex_item_names( f, item->editors_names, item->nb_editors,
				   offset, strlen(offset)+12+4, line_length );
	  fprintf( f, "}" );
	}
	else {
	  switch( type_field ) {
	  default :
	  case _IGNORED__ :
	    a_field_has_been_written = 0;
	    break;
	  case _REQUIRED_ :
	    fprintf( f, "%s%-12s = {}", offset, desc_field[i] );
	    break;
	    /*
	  case _ALTERNAT_ :
	    fprintf( f, "%sALT%-9s = {}", offset, desc_field[i] );
	    break;
	    */
	  case _OPTIONAL_ :
	    fprintf( f, "%sOPT%-9s = {}", offset, desc_field[i] );
	    break;
	  }
	}
	break;
	
      case YEAR :
	if ( item->year != NO_YEAR ) {
	  fprintf( f, "%s%-12s = {%d}", offset, desc_field[i], item->year );
	}
	else {
	  switch( type_field ) {
	  default :
	  case _IGNORED__ :
	    a_field_has_been_written = 0;
	    break;
	  case _REQUIRED_ :
	    fprintf( f, "%s%-12s = {}", offset, desc_field[i] );
	    break;
	    /*
	  case _ALTERNAT_ :
	    fprintf( f, "%sALT%-9s = {}", offset, desc_field[i] );
	    break;
	    */
	  case _OPTIONAL_ :
	    fprintf( f, "%sOPT%-9s = {}", offset, desc_field[i] );
	    break;
	  }
	}
	break;
	  
      default :

	/* non-empty fields are printed
	   only empty fields of type _REQUIRED_ or _OPTIONAL_ are printed
	*/

	if ( item->field[i] != NULL && item->field[i][0] != '\0' ) {
	  fprintf( f, "%s%-12s = ", offset, desc_field[i] );
	  fprintf( f, "{" );
	  if ( i == TITLE || i == ABSTRACT ) {
	    print_bibtex_long_string( f, item->field[i], offset, strlen(offset)+12+4, line_length );
	  }
	  else {
	    fprintf( f, "%s", item->field[i] );
	  }
	  fprintf( f, "}" );
	}
	else {
	  switch( desc_fields_bibtex_item[type_item][i] ) {
	  default :
	  case _IGNORED__ :
	    a_field_has_been_written = 0;
	    break;
	  case _REQUIRED_ :
	    fprintf( f, "%s%-12s = {}", offset, desc_field[i] );
	    break;
	    /*
	  case _ALTERNAT_ :
	    fprintf( f, "%sALT%-9s = {}", offset, desc_field[i] );
	    break;
	    */
	  case _OPTIONAL_ :
	    fprintf( f, "%sOPT%-9s = {}", offset, desc_field[i] );
	    break;
	  }
	}
	break;

      } /* end of switch */
      
      if ( a_field_has_been_written == 1 ) {
	nfields --;
	if ( nfields >= 1 ) fprintf( f, "," );
	fprintf( f, "\n" );
      }
      
    }

  
  fprintf( f, "}\n" );
}









void print_bibtex_item_from_bibtex_entry( FILE *f, typeItem *item )
{
  char *str = item->bibtex_entry;
  char *tmp;
  int i;
  int open_brace, open_double_quote;
  enumTypeItem type_item = UNKNOWN_TYPE_ITEM;
  
  int l, k;
  int indent = 2;
  int start = 15;
  int line_length = 75;

  if ( str == NULL || str[0] == '\0' ) {
    fprintf(stderr, " unable to write item\n" );
    return;
  }

  tmp = str;
  /* get type
   */
  for ( i=0; type_item == UNKNOWN_TYPE_ITEM && strncmp( desc_item[i], "XXX", 3 ) != 0 ; i++ ) 
    if ( strncasecmp( &(tmp[1]), desc_item[i], strlen(desc_item[i]) ) == 0 )
      type_item = i;

  /* can be mismatched
   */
  if ( type_item == BOOK ) {
    if ( strncasecmp( &(tmp[1]), desc_item[BOOKLET], strlen(desc_item[BOOKLET]) ) == 0 )
      type_item = BOOKLET;
  }
  if ( type_item == CONFERENCE )
    type_item = INPROCEEDINGS;

  tmp ++;
  fprintf( f, "@" );

  switch ( type_item ) {
  case BOOK :
  case PROCEEDINGS : 
  case PHDTHESIS :     
  case INBOOK :  
  case INCOLLECTION : 
  case ARTICLE : 
  case INPROCEEDINGS :  
  case CONFERENCE : 
  case TECHREPORT : 
  case BOOKLET : 
  case MANUAL : 
  case MASTERSTHESIS :  
  case PATENT : 
  case STANDARD : 
  case MISC : 
  case UNPUBLISHED : 
    fprintf( f, "%c", desc_item[type_item][0] );
    for ( i=1; i<(int)strlen(desc_item[type_item]); i++ )
      fprintf( f, "%c", desc_item[type_item][i]-'A'+'a' );
    while ( *tmp != '{' ) tmp ++;
    break;
  default :
    if ( tmp[0] >= 'a' && tmp[0] >= 'z' )
      fprintf( f, "%c", tmp[0]+'A'-'a' );
    else
      fprintf( f, "%c", tmp[0] );
    tmp ++;
    while ( *tmp != '{' ) {
      if ( tmp[0] >= 'a' && tmp[0] >= 'z' )
	fprintf( f, "%c", tmp[0] );
      else
	fprintf( f, "%c", tmp[0]-'A'+'a' );
    }
    tmp ++;
  }

  
  fprintf( f, "{%s,\n", item->bibtex_key );

  
  /* go to ','
     skip ','
  */
  open_brace = 1;
  while ( *tmp != ',' ) tmp++;
  tmp++;


  /* the key has been written,
   */

  while ( open_brace > 0 ) {
    
    /* FIND A FIELD
       skip everything until 
       a letter or end
    */
    while ( open_brace > 0 &&
	    (*tmp < 'A' || *tmp > 'z' || (*tmp > 'Z' && *tmp < 'a')) ) {
      if ( *tmp == '}' ) open_brace --;
      tmp ++;
    }
    if ( open_brace == 0 ) continue;
    
    /* here we've got a letter
       write the field
    */
    l = 0;
    for ( i=0; i<indent; i++, l++ ) fprintf( f, " " );
    while ((*tmp >= 'A' && *tmp <= 'Z') || (*tmp >= 'a' && *tmp <= 'z')) {
      fprintf( f, "%c", *tmp );
      tmp ++;
      l++;
    }

    /* go to '='
       and skip it
     */
    while ( open_brace > 0 && *tmp != '=' ) {
      if ( *tmp == '}' ) open_brace --;
      tmp ++;
    }
    if ( open_brace == 0 ) continue;
    tmp++;


    /* write =
       add some blanks
    */
    fprintf( f, " =" );
    while ( l < start ) {
      fprintf( f, " " );
      l++;
    }

    /* remove the blanks
     */
    while ( open_brace > 0 && ( IS_SPACE( *tmp ) ) ) {
      if ( *tmp == '}' ) open_brace --;
      tmp ++;
    }
    if ( open_brace == 0 ) continue;

    /* WRITE THE FIELD VALUE
       get the field value 
       until ',' or end
    */
    open_double_quote = 0;
    while ( open_brace != 0 && (open_double_quote != 0 || open_brace != 1 || *tmp != ',')) {
      if ( *tmp == '\"' && tmp[-1] != '\\' ) open_double_quote ++;
      if ( open_double_quote == 2 ) open_double_quote = 0;
      if ( *tmp == '}' ) open_brace --;
      if ( *tmp == '{' ) open_brace ++;
      if ( open_brace == 0 ) continue;

      if ( *tmp == '\n' ) {
	if ( tmp[1] == '\n' ) {
	  /* empty line
	     write it if it is the field value
	   */
	  if ( open_double_quote > 0 || open_brace > 1 ) {
	    fprintf( f, "\n\n" );
	    l = 0;
	    for ( i=0; i<indent; i++, l++ ) fprintf( f, " " );
	    tmp ++;
	    tmp ++;
	  }
	}
      }

      if ( *tmp == ' ' && ( open_double_quote > 0 || open_brace > 1 ) ) {
	str = tmp;   k = l;
	str ++;      k ++;
	while ( *str != ' ' && *str != '\n' ) {
	  str ++; k ++;
	}

	if ( k >= line_length ) {

	  tmp ++;
	  if ( *tmp == '\"' && tmp[-1] != '\\' ) open_double_quote ++;
	  if ( open_double_quote == 2 ) open_double_quote = 0;
	  if ( *tmp == '}' ) open_brace --;
	  if ( *tmp == '{' ) open_brace ++;

	  fprintf( f, "\n" );
	  l = 0;
	  for ( i=0; i<indent; i++, l++ ) fprintf( f, " " );
	}

      }

      
      if ( open_brace == 0 ) continue;
      
      fprintf( f, "%c", *tmp );
      tmp ++;
      l++;
    }

    if ( *tmp == ',' ) {
      fprintf( f, ",\n" );
      tmp ++;
    }
  }
  
  fprintf( f, "}\n" );

  fprintf( f, "\n\n\n" );
}


void print_bibliography( FILE *f, typeListOfItems *li, typeListOfStrings *ls,
			 int ac, char *av[] )
{
  int i;

  if ( f == NULL ) {
  
    if ( ls != NULL ) {
      printf( "\n" );
      for (i=0; i<ls->n; i++ ) {
	printf( "@STRING{ %s = \"%s\" }\n", ls->string[i].key, ls->string[i].str );
      }
      printf( "\n\n\n\n" );
    }
    if ( li != NULL ) {
      printf( "\n" );
      for (i=0; i<li->n; i++ ) {
	if ( li->item[i]->bibtex_entry != NULL 
	     && li->item[i]->bibtex_entry[0] != '\0' ) {
	  printf( "%s\n", li->item[i]->bibtex_entry );
	  printf( "\n" );
	}
      }
      printf( "\n" );
    }

  } /* f == NULL */
  

  /* write 'header'
   */
  fprintf( f, "\n\n\n" );
  fprintf( f, "%%\n%%\n%%\n" );
  fprintf( f, "%% automatically generated\n" );
  fprintf( f, "%% %%" );
  for ( i=0; i<ac; i++ ) fprintf( f, " %s", av[i] );
  fprintf( f, "\n" );
  fprintf( f, "%% Date: ");
  write_ctime( f );
  fprintf( f, "\n" );
  fprintf( f, "%% Author: ");
  write_username( f );
  fprintf( f, "\n" );
  fprintf( f, "%%\n%%\n%%\n" );
  fprintf( f, "\n\n\n\n\n" );

  if ( ls != NULL ) {
    for (i=0; i<ls->n; i++ ) {
      fprintf( f, "@STRING{ %s = \"%s\" }\n", ls->string[i].key, ls->string[i].str );
    }
    fprintf( f, "\n\n\n\n\n" );
  }

  if ( li != NULL ) {
    for (i=0; i<li->n; i++ ) {
      print_bibtex_item( f, li->item[i] );
      fprintf( f, "\n" );
    }
  }
}












/***************************************************
 *
 *
 *
 ***************************************************/





void set_verbose_on_stderr_in_write()
{
  _verbose_on_stderr_ = 1;
}
void set_verbose_on_stdout_in_write()
{
  _verbose_on_stderr_ = 0;
}

void set_verbose_in_write()
{
  _verbose_ = 1;
}
void unset_verbose_in_write()
{
  _verbose_ = 0;
}

void set_debug_in_write()
{
  _debug_ = 1;
}
void unset_debug_in_write()
{
  _debug_ = 0;
}

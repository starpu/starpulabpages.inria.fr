/*************************************************************************
 * custom-bibitem.c - from bibtex2html distribution
 *
 * $Id: custom-bibitem.c,v 2.10 2005/07/21 08:20:36 greg Exp $
 *
 * Copyright � INRIA 2002, Gregoire Malandain
 *
 * The purpose of this software is to automatically produce html pages from 
 * bibtex files, and to provide access to the bibtex entries by several 
 * criteria: year of publication, category of publication and author name, 
 * from an index page. 
 * see http://www-sop.inria.fr/epidaure/personnel/malandain/codes/bibtex2html.html
 *
 * AUTHOR:
 * Gregoire Malandain (greg@sophia.inria.fr)
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * CREATION DATE: 
 * 
 *
 * ADDITIONS, CHANGES
 *
 */


#include <custom-bibitem.h>

/* some global constant
   for predefined styles
 */
   
char *default_bibitem_description[] = 
{ "@STRING{ bib_doi.icon = \"doi.gif\" }",
  "@STRING{ bib_doi.prefix = \"doi:\" }",
  "@STRING{ bib_doi.link_field.start = \"[doi:\" }",
  "@STRING{ bib_doi.link_field.end = \"]\n\" }",
  "@STRING{ bib_doi.content_field.start = \"[\" }",
  "@STRING{ bib_doi.content_field.end = \"]\n\" }",
  "@STRING{ bib_pdf.icon = \"pdf.gif\" }",
  "@STRING{ bib_pdf.link = \"PDF\" }",
  "@STRING{ bib_pdf.prefix = \"PDF = \" }",
  "@STRING{ bib_pdf.link_field.start = \"[\" }",
  "@STRING{ bib_pdf.link_field.end = \"]\n\" }",
  "@STRING{ bib_postscript.icon = \"ps.gif\" }",
  "@STRING{ bib_postscript.link = \"POSTSCRIPT\" }",
  "@STRING{ bib_postscript.prefix = \"POSTSCRIPT = \" }",
  "@STRING{ bib_postscript.link_field.start = \"[\" }",
  "@STRING{ bib_postscript.link_field.end = \"]\n\" }",
  "@STRING{ bib_url.icon = \"www.gif\" }",
  "@STRING{ bib_url.link = \"WWW\" }",
  "@STRING{ bib_url.prefix = \"WWW = \" }",
  "@STRING{ bib_url.link_field.start = \"[\" }",
  "@STRING{ bib_url.link_field.end = \"]\n\" }",
  "@STRING{ bib_url_hal.icon = \"hal.gif\" }",
  "@STRING{ bib_url_hal.link = \"HAL\" }",
  "@STRING{ bib_url_hal.prefix = \"HAL = \" }",
  "@STRING{ bib_url_hal.link_field.start = \"[\" }",
  "@STRING{ bib_url_hal.link_field.end = \"]\n\" }",
  "@STRING{ bib_abstract.link_field.start = \" [\" }",
  "@STRING{ bib_abstract.link_field.end = \"]\n\" }",
  "@STRING{ bib_abstract.link   = \"Abstract\" }",
  "@STRING{ bib_abstract.prefix = \"Abstract: \" }",
  "@STRING{ bib_annote.link   = \" [Annotation] \" }",
  "@STRING{ bib_annote.prefix = \"Annotation: \" }",
  "@STRING{ bib_bibtex_key.link   = \"bibtex-key\" }",
  "@STRING{ bib_bibtex_key.prefix = \"bibtex-key = \" }",
  "@STRING{ bib_bibtex_key.content_field.start = \"[\" }",
  "@STRING{ bib_bibtex_key.content_field.end   = \"]\n\" }",
  "@STRING{ bib_bibtex_file.link   = \"bibtex file\" }",
  "@STRING{ bib_bibtex_file.prefix = \"bibtex file = \" }",
  "@STRING{ bib_bibtex_file.content_field.start = \"[\" }",
  "@STRING{ bib_bibtex_file.content_field.end   = \"]\n\" }",
  "@STRING{ bib_bibtex_entry.link   = \"bibtex-entry\" }",
  "@STRING{ bib_bibtex_entry.link_field.start = \"[\" }",
  "@STRING{ bib_bibtex_entry.link_field.end   = \"]\n\" }",
  "@STRING{ bib_comments.link   = \" [Comments] \" }",
  "@STRING{ bib_comments.prefix = \"Comments: \" }",
  "@STRING{ bib_keywords.link   = \" [Keyword(s)] \" }",
  "@STRING{ bib_keywords.prefix = \"Keyword(s): \" }",
  "@STRING{ bib_note.link   = \" [Note] \" }",
  "@STRING{ bib_note.prefix = \"Note: \" }",
  "XXX"
};

char *default_bibitem_description_html[] = 
{ "@STRING{ bib_booktitle.start = \"<em>\" }",
  "@STRING{ bib_booktitle.end = \"</em>\" }",
  "@STRING{ bib_journal.start = \"<em>\" }",
  "@STRING{ bib_journal.end = \"</em>\" }",
  "@STRING{ bib_series.start = \"<em>\" }",
  "@STRING{ bib_series.end = \"</em>\" }",
  "@STRING{ bib_title.start = \"<strong>\" }",

  "@STRING{ bib_title.end = \"</strong>\" }",

  "@STRING{ bib_abstract.prefix = \"<td><strong>Abstract:</strong></td>\n\" }",
  "@STRING{ bib_abstract.content_field.start = \"<center>\n\
<table border=1 align=center width=80%>\n\
<tr>\n\" }",
  "@STRING{ bib_abstract.content_field.end= \"\n</tr></table></center>\n\" }",
  "@STRING{ bib_abstract.content.start = \"<td>\n\" }",
  "@STRING{ bib_abstract.content.end = \"</td>\n\" }",

  "@STRING{ bib_annote.prefix = \"<td><strong>Annotation:</strong></td>\n\" }",
  "@STRING{ bib_annote.content_field.start = \"<center>\n\
<table border=1 align=center width=80%>\n\
<tr>\n\" }",
  "@STRING{ bib_annote.content_field.end= \"\n</tr></table></center>\n\" }",
  "@STRING{ bib_annote.content.start = \"<td>\n\" }",
  "@STRING{ bib_annote.content.end = \"</td>\n\" }",

  "@STRING{ bib_bibtex_entry.content.start = \"<br /><pre>\n\" }",
  "@STRING{ bib_bibtex_entry.content.end = \"\n</pre>\n\" }",

  "@STRING{ bib_bibtex_file.content.start = \"<tt>\" }",
  "@STRING{ bib_bibtex_file.content.end = \"</tt>\" }",
  
  "@STRING{ bib_comments.prefix = \"<td><strong>Comments:</strong></td>\n\" }",
  "@STRING{ bib_comments.content_field.start = \"<center>\n\
<table border=1 align=center width=80%>\n\
<tr>\n\" }",
  "@STRING{ bib_comments.content_field.end= \"\n</tr></table></center>\n\" }",
  "@STRING{ bib_comments.content.start = \"<td>\n\" }",
  "@STRING{ bib_comments.content.end = \"</td>\n\" }",

  "@STRING{ bib_keywords.prefix = \"<br />\n<u>Keywords:</u>\n\" }",
  
  "@STRING{ bib_note.prefix = \"<br />\n<strong>Note:</strong> \" }",

  "@STRING{ bib_isbn.prefix = \"<strong>ISBN:</strong> \" }",
  "@STRING{ bib_isbn.content.end = \".\n\" }",
  "@STRING{ bib_issn.prefix = \"<strong>ISSN:</strong> \" }",
  "@STRING{ bib_issn.content.end = \".\n\" }",

  "@STRING{ bib_hal_identifiant.prefix = \"<strong>HAL ID:</strong> \" }",
  "@STRING{ bib_hal_identifiant.content.end = \".\n\" }",

  "@STRING{ bib_dot = \".\n\" }",
  "@STRING{ bib_comma = \",\n\" }",
  "@STRING{ bib_and = \" and \" }",
  "XXX"
};



char *default_bibitem_description_latex[] =
{ "@STRING{ bib_booktitle.start = \"{\\em \" }",
  "@STRING{ bib_booktitle.end = \"}\" }",
  "@STRING{ bib_journal.start = \"{\\it \" }",
  "@STRING{ bib_journal.end = \"}\" }",
  "@STRING{ bib_series.start = \"{\\em \" }",
  "@STRING{ bib_series.end = \"}\" }",
  "@STRING{ bib_title.start = \"{\\bf \" }",
  "@STRING{ bib_title.end = \"}\" }",

  "@STRING{ bib_annote.field.start   = \"\n\\begin{description}\n\" }",
  "@STRING{ bib_annote.field.en      = \"\\end{description}\n\" }",
  "@STRING{ bib_annote.prefix = \"\\item[Annotation]\n\" }",
  "@STRING{ bib_annote.content.start = \"\" }",
  "@STRING{ bib_annote.content.end   = \"\n\" }",

  "@STRING{ bib_abstract.field.start   = \"\n\\begin{description}\n\" }",
  "@STRING{ bib_abstract.field.en      = \"\\end{description}\n\" }",
  "@STRING{ bib_abstract.prefix = \"\\item[Abstract]\n\" }",
  "@STRING{ bib_abstract.content.start = \"\" }",
  "@STRING{ bib_abstract.content.end   = \"\n\" }",

  "@STRING{ bib_comments.field.start   = \"\n\\begin{description}\n\" }",
  "@STRING{ bib_comments.field.en      = \"\\end{description}\n\" }",
  "@STRING{ bib_comments.prefix = \"\\item[Comments]\n\" }",
  "@STRING{ bib_comments.content.start = \"\" }",
  "@STRING{ bib_comments.content.end   = \"\n\" }",

  "@STRING{ bib_bibtex_entry.content.start = \"\\begin{verbatim}\n\" }",
  "@STRING{ bib_bibtex_entry.content.end   = \"\n\\end{verbatim}\n\" }",

  "@STRING{ bib_keywords.prefix = \"\n\\\\{\\bf Keywords:} \n\" }",

  "@STRING{ bib_note.prefix = \"\\\\{\\bf Note: }\" }",

  "@STRING{ bib_dot = \". \" }",
  "@STRING{ bib_comma = \", \" }",
  "@STRING{ bib_and = \" and \" }",
  "XXX"
};



char *default_bibitem_description_txt[] = 
{ "@STRING{ bib_bib_note_title = \"\n Note: \" }",
  "@STRING{ bib_dot = \". \" }",
  "@STRING{ bib_comma = \", \" }",
  "@STRING{ bib_and = \" and \" }",
  "XXX"
};



char *default_bibitem_description_none[] =
{
  "XXX"
};




/* description of the structure
 */
typeArgDescription bibitem_description[] = 
{ { "bib_firstname",   _TAG_,  NULL },
  { "bib_lastname",    _TAG_,  NULL },
  { "bib_booktitle",   _TAG_,  NULL },
  { "bib_journal",     _TAG_,  NULL },
  { "bib_series",      _TAG_,  NULL },
  { "bib_title",       _TAG_,  NULL },
  { "bib_doi",         _ENV_,  NULL },
  { "bib_pdf",         _ENV_,  NULL },
  { "bib_postscript",  _ENV_,  NULL },
  { "bib_url",         _ENV_,  NULL },
  { "bib_url_hal",     _ENV_,  NULL },
  { "bib_abstract",    _ENV_,  NULL },
  { "bib_annote",      _ENV_,  NULL },
  { "bib_bibtex_entry",_ENV_,  NULL },
  { "bib_bibtex_file", _ENV_,  NULL },
  { "bib_bibtex_key",  _ENV_,  NULL },
  { "bib_comments",    _ENV_,  NULL },
  { "bib_hal_identifiant", _ENV_,  NULL },
  { "bib_isbn",        _ENV_,  NULL },
  { "bib_issn",        _ENV_,  NULL },
  { "bib_keywords",    _ENV_,  NULL },
  { "bib_note",        _ENV_,  NULL },
  { "bib_dot",         _STRING_, NULL },
  { "bib_comma",       _STRING_, NULL },
  { "bib_and",         _STRING_, NULL },
  { "XXX",             _STRING_, NULL }
};










void init_bibtex_item_description( typeBibtexItemDescription *b )
{
  init_tag_element( &(b->bib_firstname) );
  init_tag_element( &(b->bib_lastname) );
  init_tag_element( &(b->bib_booktitle) );
  init_tag_element( &(b->bib_journal) );
  init_tag_element( &(b->bib_series) );
  init_tag_element( &(b->bib_title) );

  init_env_element( &(b->bib_note) );

  init_env_element( &(b->bib_doi) );
  init_env_element( &(b->bib_pdf) );
  init_env_element( &(b->bib_postscript) );
  init_env_element( &(b->bib_url) );
  init_env_element( &(b->bib_url_hal) );

  init_env_element( &(b->bib_abstract) );
  init_env_element( &(b->bib_annote) );
  init_env_element( &(b->bib_bibtex_entry) );
  init_env_element( &(b->bib_bibtex_file) );
  init_env_element( &(b->bib_bibtex_key) );
  init_env_element( &(b->bib_comments) );
  init_env_element( &(b->bib_hal_identifiant) );
  init_env_element( &(b->bib_isbn) );
  init_env_element( &(b->bib_issn) );
  init_env_element( &(b->bib_keywords) );

  b->bib_comma = NULL;
  b->bib_dot = NULL;

  b->bib_and = NULL;
}


void free_bibtex_item_description( typeBibtexItemDescription *b )
{
  free_tag_element( &(b->bib_firstname) );
  free_tag_element( &(b->bib_lastname) );
  free_tag_element( &(b->bib_booktitle) );
  free_tag_element( &(b->bib_journal) );
  free_tag_element( &(b->bib_series) );
  free_tag_element( &(b->bib_title) );

  free_env_element( &(b->bib_note) );

  free_env_element( &(b->bib_doi) );
  free_env_element( &(b->bib_pdf) );
  free_env_element( &(b->bib_postscript) );
  free_env_element( &(b->bib_url) );
  free_env_element( &(b->bib_url_hal) );

  free_env_element( &(b->bib_abstract) );
  free_env_element( &(b->bib_annote) );
  free_env_element( &(b->bib_bibtex_entry) );
  free_env_element( &(b->bib_bibtex_file) );
  free_env_element( &(b->bib_bibtex_key) );
  free_env_element( &(b->bib_comments) );
  free_env_element( &(b->bib_hal_identifiant) );
  free_env_element( &(b->bib_isbn) );
  free_env_element( &(b->bib_issn) );
  free_env_element( &(b->bib_keywords) );

  if ( b->bib_comma != NULL ) FREE( b->bib_comma, "free_bibtex_item_description" );
  if ( b->bib_dot != NULL ) FREE( b->bib_dot, "free_bibtex_item_description" );

  if ( b->bib_and != NULL ) FREE( b->bib_and, "free_bibtex_item_description" );

  init_bibtex_item_description( b );
}



/* add bibtem strings to customization strings
 */

int add_bibitem_strings( typeArgDescription *custom,
			 int length,
			 typeBibtexItemDescription *b,
			 typeArgDescription *bd )
{
  char *proc = "add_bibitem_strings";
  int l = length;
  int i, j, n;
  
  for ( i = 0, j = 0; strncmp( bd[i].key, "XXX", 3 ) != 0; i ++, j += n ) {
    
    n = 0;
    
    switch( bd[i].type ) {

    default :
      fprintf( stderr, "%s: unknown arg type\n", proc );
      break;

    case _STRING_ :

      custom[l+j]     = bd[i];
      if ( strcmp( bd[i].key, "bib_and" ) == 0 )
	custom[l+j].arg = &(b->bib_and);
      else if ( strcmp( bd[i].key, "bib_comma" ) == 0 )
	custom[l+j].arg = &(b->bib_comma);
      else if ( strcmp( bd[i].key, "bib_dot" ) == 0 )
	custom[l+j].arg = &(b->bib_dot);
      else {
	custom[l+j].arg = NULL;
	fprintf( stderr, "%s: unable to link key '%s' with arg\n",
		 proc, bd[i].key );
      }     
      n = 1;
      break;

    case _TAG_ :
      
      if ( strcmp( bd[i].key, "bib_firstname" ) == 0 )
	n = add_tag_element( custom, l+j, bd[i].key, &(b->bib_firstname) );
      else if ( strcmp( bd[i].key, "bib_lastname" ) == 0 )
	n = add_tag_element( custom, l+j, bd[i].key, &(b->bib_lastname) );
      else if ( strcmp( bd[i].key, "bib_booktitle" ) == 0 )
	n = add_tag_element( custom, l+j, bd[i].key, &(b->bib_booktitle) );
      else if ( strcmp( bd[i].key, "bib_journal" ) == 0 )
	n = add_tag_element( custom, l+j, bd[i].key, &(b->bib_journal) );
      else if ( strcmp( bd[i].key, "bib_series" ) == 0 )
	n = add_tag_element( custom, l+j, bd[i].key, &(b->bib_series) );
      else if ( strcmp( bd[i].key, "bib_title" ) == 0 )
	n = add_tag_element( custom, l+j, bd[i].key, &(b->bib_title) );
      else {
	custom[l+j].arg = NULL;
	fprintf( stderr, "%s: unable to link key '%s' with arg\n",
		 proc, bd[i].key );
      }  
      break;
      
    case _ENV_ :
      
      if ( strcmp( bd[i].key, "bib_doi" ) == 0 )
	n = add_env_element( custom, l+j, bd[i].key, &(b->bib_doi) );
      else if ( strcmp( bd[i].key, "bib_pdf" ) == 0 )
	n = add_env_element( custom, l+j, bd[i].key, &(b->bib_pdf) );
      else if ( strcmp( bd[i].key, "bib_postscript" ) == 0 )
	n = add_env_element( custom, l+j, bd[i].key, &(b->bib_postscript) );
      else if ( strcmp( bd[i].key, "bib_url" ) == 0 )
	n = add_env_element( custom, l+j, bd[i].key, &(b->bib_url) );
      else if ( strcmp( bd[i].key, "bib_url_hal" ) == 0 )
	n = add_env_element( custom, l+j, bd[i].key, &(b->bib_url_hal) );
      else if ( strcmp( bd[i].key, "bib_abstract" ) == 0 )
	n = add_env_element( custom, l+j, bd[i].key, &(b->bib_abstract) );
      else if ( strcmp( bd[i].key, "bib_annote" ) == 0 )
	n = add_env_element( custom, l+j, bd[i].key, &(b->bib_annote) );
      else if ( strcmp( bd[i].key, "bib_bibtex_entry" ) == 0 )
	n = add_env_element( custom, l+j, bd[i].key, &(b->bib_bibtex_entry) );
      else if ( strcmp( bd[i].key, "bib_bibtex_file" ) == 0 )
	n = add_env_element( custom, l+j, bd[i].key, &(b->bib_bibtex_file) );
      else if ( strcmp( bd[i].key, "bib_bibtex_key" ) == 0 )
	n = add_env_element( custom, l+j, bd[i].key, &(b->bib_bibtex_key) );
      else if ( strcmp( bd[i].key, "bib_comments" ) == 0 )
	n = add_env_element( custom, l+j, bd[i].key, &(b->bib_comments) );
      else if ( strcmp( bd[i].key, "bib_hal_identifiant" ) == 0 )
	n = add_env_element( custom, l+j, bd[i].key, &(b->bib_hal_identifiant) );
      else if ( strcmp( bd[i].key, "bib_isbn" ) == 0 )
	n = add_env_element( custom, l+j, bd[i].key, &(b->bib_isbn) );
      else if ( strcmp( bd[i].key, "bib_issn" ) == 0 )
	n = add_env_element( custom, l+j, bd[i].key, &(b->bib_issn) );
      else if ( strcmp( bd[i].key, "bib_keywords" ) == 0 )
	n = add_env_element( custom, l+j, bd[i].key, &(b->bib_keywords) );
      else if ( strcmp( bd[i].key, "bib_note" ) == 0 )
	n = add_env_element( custom, l+j, bd[i].key, &(b->bib_note) );
      else {
	custom[l+j].arg = NULL;
	fprintf( stderr, "%s: unable to link key '%s' with arg\n",
		 proc, bd[i].key );
      }
      break;

    }
  }
  return ( l+j );
}		 



/*************************************************************************
 * custom.c - from bibtex2html distribution
 *
 * $Id: custom.c,v 2.12 2004/07/20 15:19:35 greg Exp $
 *
 * Copyright � INRIA 2002, Gregoire Malandain
 *
 * The purpose of this software is to automatically produce html pages from 
 * bibtex files, and to provide access to the bibtex entries by several 
 * criteria: year of publication, category of publication and author name, 
 * from an index page. 
 * see http://www-sop.inria.fr/epidaure/personnel/malandain/codes/bibtex2html.html
 *
 * AUTHOR:
 * Gregoire Malandain (greg@sophia.inria.fr)
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * CREATION DATE: 
 * 
 *
 * ADDITIONS, CHANGES
 *
 */


static int _verbose_ = 1;

/* for stat() in check_directory()
 */
#include <sys/stat.h>
#include <sys/types.h>



#include <custom.h>
#include <read.h>


/**************************************************
 *
 * load default customization strings
 *
 **************************************************/
void load_default_style( typeArgDescription *customization_strings,
			 typeSpecificFileDescription *single_output,
			 typeSpecificFileDescription *index,
			 typeSpecificFileDescription *author,
			 typeSpecificFileDescription *category,
			 typeSpecificFileDescription *keyword,
			 typeSpecificFileDescription *reduced_year,
			 typeSpecificFileDescription *complete_year,
			 typeSpecificFileDescription *complete_biblio,
			 typeSpecificFileDescription *default_desc )
{
  load_predefined_strings( customization_strings, 
			   default_environment_description );
  load_predefined_strings( customization_strings, 
			   default_specific_file_description );
  load_predefined_strings( customization_strings, 
			   default_bibitem_description );

  update_file_description_with_default( single_output, default_desc );
  update_file_description_with_default( index, default_desc );
  update_file_description_with_default( author, default_desc );
  update_file_description_with_default( category, default_desc );
  update_file_description_with_default( keyword, default_desc );
  update_file_description_with_default( reduced_year, default_desc );
  update_file_description_with_default( complete_year, default_desc );
  update_file_description_with_default( complete_biblio, default_desc );

  init_specific_file_description( default_desc );
}


/**************************************************
 *
 * build customization strings
 *
 **************************************************/



/* get the numbers of arguments
   in a typeArgDescription array
*/
static int nb_arguments( typeArgDescription *t ) 
{
  char *proc = "nb_arguments";
  int i, n, nb;

  if ( t == (typeArgDescription *)NULL ) return( 0 );

  for ( i = 0, nb = 0; strncmp( t[i].key, "XXX", 3 ) != 0; i ++, nb += n ) {
    /* 
       fprintf( stderr, "key[%d]='%s'\n", i,  t[i].key ); 
    */
    n = 0;
    switch ( t[i].type ) {
    default : 
      fprintf( stderr, "%s: unknown type, key ='%s'\n", proc, t[i].key );
      break;
    case _INT_ :
    case _STRING_ : n = 1;  break;
    case _TAG_    : n = add_tag_element( NULL, 0, NULL, NULL ); break;
    case _ENV_    : n = add_env_element( NULL, 0, NULL, NULL ); break;
    case _TABLE_  : n = add_table_element( NULL, 0, NULL, NULL ); break;
    }
  }
  return( nb );
}



/* get the numbers of description elements
   in a typeArgDescription array
*/

int get_nb_description_arguments( typeArgDescription *t ) 
{
  char *proc = "get_nb_description_arguments";
  int i, nb;

  if ( t == (typeArgDescription *)NULL ) return( 0 );

  for ( i = 0, nb = 0; strncmp( t[i].key, "XXX", 3 ) != 0; i ++, nb ++ )
    ;
  return( nb );
}



typeArgDescription * build_customization_strings( typeEnvironmentDescription *environ,
						  typeGenericFileDescription *generic_file,
						  typeSpecificFileDescription *single_output_file,
						  typeSpecificFileDescription *index_file,
						  typeSpecificFileDescription *author_file,
						  typeSpecificFileDescription *category_file,
						  typeSpecificFileDescription *keyword_file,
						  typeSpecificFileDescription *reduced_year_file,
						  typeSpecificFileDescription *complete_year_file,
						  typeSpecificFileDescription *complete_biblio_file,
						  typeSpecificFileDescription *default_file,
						  typeBibtexItemDescription *bibitem )
{
  char *proc = "build_customization_string";
  typeArgDescription *d = NULL;
  int es, gfs, sfs, bs;
  int n, l;

  /* environment: context strings
   */
  es = nb_arguments( environment_description );
  if ( _verbose_ )
    fprintf( stderr, "\n%s:\n there are %d environment description strings\n", proc, es );

  /* file: context strings
   */
  gfs = nb_arguments( generic_file_description );
  if ( _verbose_ )
    fprintf( stderr, " there are %d file generic description strings\n", gfs );
  sfs = nb_arguments( specific_file_description );
  if ( _verbose_ )
    fprintf( stderr, " there are %d file specific description strings\n", sfs );
  
  /* bibtex item: context strings
   */
  bs = nb_arguments( bibitem_description );
  if ( _verbose_ )
    fprintf( stderr, " there are %d bibitem description strings\n", bs );


  /* there are
     1 global environment
     1 generic file
     8 types of files (including default)
     1 bibitem context
     + 1 end string
  */
  n = es + gfs + 9 * sfs + bs + 1;

  if ( _verbose_ )
    fprintf( stderr, " will allocate %d strings\n", n );
  d = ( typeArgDescription *)MALLOC( n * sizeof( typeArgDescription ), "build_customization_strings" );
  if ( d == NULL ) {
    if ( _verbose_ ) 
      fprintf( stderr, " unable to allocate customization strings\n" );
    return( NULL );
  }

  
  /* build the strings
   */
  l = 0;

  l = add_environment_strings( d, l, environ, environment_description );

  l = add_generic_file_strings( d, l, generic_file, generic_file_description );

  l = add_specific_file_strings( d, l, "single_output",   single_output_file,
				 specific_file_description );
  l = add_specific_file_strings( d, l, "index",           index_file, 
				 specific_file_description );
  l = add_specific_file_strings( d, l, "author",          author_file, 
				 specific_file_description );
  l = add_specific_file_strings( d, l, "category",        category_file, 
				 specific_file_description );
  l = add_specific_file_strings( d, l, "keyword",         keyword_file, 
				 specific_file_description );
  l = add_specific_file_strings( d, l, "reduced_year",    reduced_year_file, 
				 specific_file_description );
  l = add_specific_file_strings( d, l, "complete_year",   complete_year_file, 
				 specific_file_description );
  l = add_specific_file_strings( d, l, "complete_biblio", complete_biblio_file, 
				 specific_file_description );
  l = add_specific_file_strings( d, l, "default",         default_file, 
				 specific_file_description );

  l = add_bibitem_strings( d, l, bibitem, bibitem_description );

  if ( _verbose_ ) 
    fprintf( stderr, "%s: build %d strings\n\n", proc, l );

  /* end item
   */
  sprintf( d[l].key, "XXX" );

  return( d );
}










/**************************************************
 *
 * directory management
 *
 **************************************************/

int check_directory( char *dir ) 
{
  char *proc = "check_directory";
  struct stat stbuf;

  if ( dir == NULL || dir[0] == '\0' ) {
    if ( _verbose_ )
      fprintf( stderr, "%s: invalid directory name\n", proc );
    return( -1 );
  }
  if ( stat( dir, &stbuf ) != 0 ) {
    if ( _verbose_ )
      fprintf( stderr, "%s: can't stat directory '%s' (doesn't exist?)\n",
	       proc, dir );
    return( -2 );
  }
  if ( (stbuf.st_mode & S_IFDIR) == 0 ) {
    if ( _verbose_ )
      fprintf( stderr, "%s: '%s' is not a directory\n", proc, dir );
    return( -3 );
  }
#ifdef WIN32
  if ( (stbuf.st_mode & S_IWRITE) == 0 ) 
#else
  if ( (stbuf.st_mode & S_IWUSR) == 0 ) 
#endif
    {
    if ( _verbose_ )
      fprintf( stderr, "%s: '%s' is not writable\n", proc, dir );
    return( -4 );
  }
  return( 1 );
}


#ifdef WIN32
#include <direct.h>
#else
/* these includes are supposed to be needed for mkdir 
   (from the man pages) but their absence does not yield
   any compilation error (?).
*/ 
#include <fcntl.h>
#include <unistd.h>
#endif

int make_directory( char *dir ) 
{
  char *proc = "make_directory";
  struct stat stbuf;

  if ( dir == NULL || dir[0] == '\0' ) {
    if ( _verbose_ )
      fprintf( stderr, "%s: invalid directory name\n", proc );
    return( -5 );
  }
  if ( stat( dir, &stbuf ) != 0 ) {
#ifdef WIN32
    if ( mkdir( dir ) != 0 ) 
#else
    if ( mkdir( dir, (mode_t)0755 ) != 0 ) 
#endif    
      {
      if ( _verbose_ )
	fprintf( stderr, "%s: unable to create directory '%s'\n", proc, dir );
      return( -6 );
    }
    fprintf( stderr, "%s: has created directory '%s'\n", proc, dir );
  }
  return( check_directory( dir ) );
}









/**************************************************
 *
 * icons management
 *
 **************************************************/

static void check_one_icon( char *dir, char *file, int *use )
{
  char *proc = "check_one_icon";
  struct stat stbuf;
  char name[256];

  if ( *use != 0 && file != NULL && file[0] != '\0' ) {
    if ( dir == NULL || dir[0] == '\0') {
      sprintf( name, "%s", file );
    }
    else {
      sprintf( name, "%s/%s", dir, file );
    }
    if ( stat( name, &stbuf ) != 0 ) {
      fprintf( stderr, "%s: unable to open '%s'\n", proc, name );
      *use = 0;
      return;
    }
    if ( stbuf.st_size == 0 )  {
      fprintf( stderr, "%s: '%s' has a size of 0\n", proc, name );
      *use = 0;
      return;
    }
    if ( _verbose_ )
      fprintf( stderr, "%s: size of '%s' is %d\n", proc, name, (int)stbuf.st_size );
  }
  else 
    *use = 0;
}


void check_all_icons( typeEnvironmentDescription *e,
		      typeBibtexItemDescription *b, 
		      char *dir )
{
  if ( !e->use_html_links_towards_urls || !e->use_icons ) {
    b->bib_doi.use_icon = 0;
    b->bib_pdf.use_icon = 0;
    b->bib_postscript.use_icon = 0;
    b->bib_url.use_icon = 0;
    b->bib_url_hal.use_icon = 0;
    return;
  }
  check_one_icon( dir, b->bib_doi.icon,        &(b->bib_doi.use_icon) );
  check_one_icon( dir, b->bib_pdf.icon,        &(b->bib_pdf.use_icon) );
  check_one_icon( dir, b->bib_postscript.icon, &(b->bib_postscript.use_icon) );
  check_one_icon( dir, b->bib_url.icon,        &(b->bib_url.use_icon) );
  check_one_icon( dir, b->bib_url_hal.icon,    &(b->bib_url_hal.use_icon) );
}



static void copy_one_icon( char *fromdir, char *file, char *todir, int *use )
{

  char cmd[512];
  if ( *use != 0 
       && fromdir != NULL && fromdir[0] != '\0'
       && todir != NULL && todir[0] != '\0' 
       && file != NULL && file[0] != '\0' ) {
#ifdef WIN32
    sprintf( cmd, "copy %s\\%s %s", fromdir, file, todir );
#else
    sprintf( cmd, "cp -p %s/%s %s/", fromdir, file, todir );
#endif
    (void)system( cmd );
  }
  else 
    *use = 0;

}



void copy_all_icons( typeEnvironmentDescription *e,
		 typeBibtexItemDescription *b )
{

  check_all_icons( e, b, e->iconsdir );

  if ( !e->use_html_links_towards_urls || !e->use_icons ) {
    b->bib_doi.use_icon = 0;
    b->bib_pdf.use_icon = 0;
    b->bib_postscript.use_icon = 0;
    b->bib_url.use_icon = 0;
    b->bib_url_hal.use_icon = 0;
    return;
  }

  copy_one_icon( e->iconsdir, b->bib_doi.icon,
		 e->directory_icons, &(b->bib_doi.use_icon) );
  copy_one_icon( e->iconsdir, b->bib_pdf.icon,
		 e->directory_icons, &(b->bib_pdf.use_icon) );
  copy_one_icon( e->iconsdir, b->bib_postscript.icon,
		 e->directory_icons, &(b->bib_postscript.use_icon) );
  copy_one_icon( e->iconsdir, b->bib_url.icon,
		 e->directory_icons, &(b->bib_url.use_icon) );
  copy_one_icon( e->iconsdir, b->bib_url_hal.icon,
		 e->directory_icons, &(b->bib_url_hal.use_icon) );

  check_all_icons( e, b, e->directory_icons );
  
}


/**************************************************
 *
 * print the customization strings
 *
 **************************************************/

static void print_string_of_char( FILE *fd, char *string )
{
  char *t;
  
  if ( string == NULL ) {

    fprintf( fd, "NULL" );

  }
  else {
    
    fprintf( fd, "\"" );
    for ( t = string; *t != '\0'; t++ ) {
      switch( *t ) {
      case '\n' : fprintf( fd, "\\n" ); break;
      case '\t' : fprintf( fd, "\\t" ); break;
      case '%' : fprintf( fd, "%%" ); break;
      default :
	fprintf( fd, "%c", *t );
      }
    }
    fprintf( fd, "\"" );

  }
}


static void print_arg_as_bibtex_string( FILE *fd,
					char *prefix,
					char *key,
					void *arg,
					enumTypeArg type,
					int print_null_string )
{
  char *proc = "print_arg_as_bibtex_string";

  switch ( type ) {

  default :
    fprintf( stderr, "%s: unhandled type for key '%s'?\n",
	     proc, key );
    break;

  case _INT_ :
  
    if ( prefix != NULL )
      fprintf( fd, "@string{ %s_%s = ", prefix, key );
    else
      fprintf( fd, "@string{ %s = ", key );
    fprintf( fd, "%d", *((int*)arg) ); 
    fprintf( fd, " }\n" );
    break;
    
  case _STRING_ :

    if ( !print_null_string )
      if ( (char*)arg == NULL || strlen( ((char*)arg) ) <= 0 ) break;

    if ( prefix != NULL )
      fprintf( fd, "@string{ %s_%s = ", prefix, key );
    else
      fprintf( fd, "@string{ %s = ", key );
    /* fprintf( fd, "\"%s\"", (*(char**)arg) ); */
    print_string_of_char( fd, *(char**)arg );
    fprintf( fd, " }\n" );
    break;

  }
    
}



/**************************************************
 *
 * misc
 *
 **************************************************/

void print_customization_strings( FILE *fd, typeArgDescription *d, 
				  int print_null_string )
{
  int i;
  fprintf( fd, "\n" );
  for ( i = 0; strncmp( d[i].key, "XXX", 3 ) != 0; i++ ) {
    print_arg_as_bibtex_string( fd, NULL, d[i].key, d[i].arg, d[i].type, print_null_string );
  }
  fprintf( fd, "\n" );
}



void print_customization_strings_keys( FILE *fd, typeArgDescription *d )
{
  int i;
  fprintf( fd, "\n" );
  for ( i = 0; strncmp( d[i].key, "XXX", 3 ) != 0; i++ ) {
    switch ( d[i].type ) {
    case _TAG_ :
      /*
      fprintf( fd, "#%4da = %s.start\n", i, d[i].key );
      fprintf( fd, "#%4db = %s.end  \n", i, d[i].key );
      */
      fprintf( fd, "    %s.start\n", d[i].key );
      fprintf( fd, "    %s.end  \n", d[i].key );
      break;
    default :
      /*
      fprintf( fd, "#%4d  = %s\n", i, d[i].key );
      */
      fprintf( fd, "    %s\n", d[i].key );
    }
  }
  fprintf( fd, "\n" );
}





void set_verbose_in_custom()
{
  _verbose_ = 1;
}
void unset_verbose_in_custom()
{
  _verbose_ = 0;
}



/*************************************************************************
 * biblio.h - from bibtex2html distribution
 *
 * $Id: biblio.h,v 2.13 2004/07/20 15:19:35 greg Exp $
 *
 * Copyright � INRIA 2001, Gregoire Malandain
 *
 * The purpose of this software is to automatically produce html pages from 
 * bibtex files, and to provide access to the bibtex entries by several 
 * criteria: year of publication, category of publication and author name, 
 * from an index page. 
 * see http://www-sop.inria.fr/epidaure/personnel/malandain/codes/bibtex2html.html
 *
 * AUTHOR:
 * Gregoire Malandain (greg@sophia.inria.fr)
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * CREATION DATE: 
 * 
 *
 * ADDITIONS, CHANGES
 *
 */



#ifndef _biblio_h_
#define _biblio_h_

#ifdef __cplusplus
extern "C" {
#endif

#include <stdio.h>

#define SHORT_STR_LENGTH 128
#define STD_STR_LENGTH 1024
#define LONG_STR_LENGTH 32768


#define NO_YEAR -9999


typedef enum {
  _REQUIRED_,
  _ALTERNAT_,
  _OPTIONAL_,
  _IGNORED__
} enumTypeBibtexField;



/* Be careful, 
   this enumeration order has to be the same
   as the order of *desc_sort_criterium[]
*/
typedef enum {
  _NAME_,
  _CATEGORY_,
  _TYPE_,
  _YEAR_,
  _TITLE_,
  _BIBKEY_,
  _JOURNAL_,
  _BOOKTITLE_,
  _NONE_
} enumSortCriterium;

extern const char *desc_sort_criterium[];





/* Be careful, 
   this enumeration order has to be the same
   as the order of *desc_category[]
*/
typedef enum {
  CAT_BOOK,
  CAT_THESIS,
  CAT_ARTICLE,
  CAT_CONFERENCE,
  CAT_REPORTS,
  CAT_PATENT_STANDARD,
  CAT_MANUALS_BOOKLETS,
  CAT_MISC,
  CAT_UNKNOWN
} enumTypeCategory;

extern const char *desc_category[];
extern const char *filename_category[];





/* Be careful, 
   this enumeration order has to be the same
   as the order of *desc_item[]
*/
typedef enum {
  ARTICLE,
  BOOK,
  BOOKLET,
  CONFERENCE,
  INBOOK,
  INCOLLECTION,
  INPROCEEDINGS, 
  MANUAL,
  MASTERSTHESIS,
  MISC,
  PHDTHESIS,   
  PROCEEDINGS,
  TECHREPORT,
  UNPUBLISHED,

  PATENT,
  STANDARD,

  UNKNOWN_TYPE_ITEM
} enumTypeItem;





extern const char *desc_item[];





/* Be careful, 
   this enumeration order has to be the same
   as the order of *desc_field[]

   Some fake fields exist (EMPTYxx) to create groups of
   fields that are multiples of 5 (for aesthetic reasons
   in biblio.c).

   The first group of fields correspond to the fields described 
   in [BibTEXing, Oren Patashnik, 1988].

   The second group of fields correspond to others fields that 
   correspond to useful information.

   The third group of fields correspond to fields required by the 
   INRIA activity report. 
   x-editorial-board = {yes/no} : article with or without reviewing process
   x-proceedings = {yes/no} : conference with or without proceedings
   x-international-audience = {yes/no} : self-explaining
   x-invited-conference = {yes} : self-explaining
   x-scientific-popularization = {yes} : self-explaining
   x-pays : list of countries of co-authors according to the ISO standard 
            http://en.wikipedia.org/wiki/ISO_3166-1
            e.g. x-pays={US,GB,PT}. 

   The fourth group correspond to fields concerning the HAL repository.

*/
typedef enum {
  ADDRESS,      /*  1 */
  ALT,   
  ANNOTE,
  AUTHOR,
  BOOKTITLE,
  CHAPTER,      /*  6 */
  CROSSREF,
  EDITION, 
  EDITOR,
  HOWPUBLISHED,
  INSTITUTION,  /* 11 */
  JOURNAL,
  KEY,
  MONTH,        
  NOTE,
  NUMBER,       /* 16 */
  OPT,
  ORGANIZATION,
  PAGES,       
  PUBLISHER,
  SCHOOL,       /* 21 */
  SERIES,
  TITLE,
  TYPE,         
  VOLUME,
  YEAR,         /* 26 */
  EMPTYxAA,
  EMPTYxAB,
  EMPTYxAC,
  EMPTYxAD,

  ABSTRACT,     /* 31 */
  CITEKEY,
  COMMENTS,
  DOI,   
  ISBN,
  ISSN,         /* 36 */
  KEYWORD,
  KEYWORDS,
  URL,
  URLPUBLISHER,
  PDF,          /* 41 */
  POSTSCRIPT,
  PS,
  EMPTYxBA,
  EMPTYxBB,

#if (defined(_INRIA_) || defined(_HALINRIA_))
  XEDITORIALBOARD,           /* 46 */
  XINTERNATIONALAUDIENCE,
  XINVITEDCONFERENCE,
  XPAYS,
  XPROCEEDINGS,
  XSCIENTIFICPOPULARIZATION, /* 51 */
  EMPTYxCA,
  EMPTYxCB,
  EMPTYxCC,
  EMPTYxCD,

  HALDATEDEPOT, /* 56 */
  HALURL,
  HALIDENTIFIANT,
  HALVERSION,
  XLANGUAGE,
#endif
#if defined(_HALINRIA_) 
  FILES,
  WRITING_DATE,
  EMPTYxEA,
  EMPTYxEB,
  EMPTYxEC,
#endif

  UNKNOWN_TYPE_FIELD         /* 61 */
} enumTypeField;

extern const char *desc_field[];
extern const int desc_field_lgth[];





/* I need this for item structure declaration
   Do not count UNKNOWN_TYPE_FIELD
 */
#if (!defined( _INRIA_ ) &&  !defined( _HALINRIA_ ))
#define DESC_FIELD_NB 45
#elif defined( _HALINRIA_ )
#define DESC_FIELD_NB 65
#else
#define DESC_FIELD_NB 60
#endif



extern enumTypeBibtexField desc_fields_bibtex_item[][DESC_FIELD_NB];



/* some additional declarations
 */

extern const char *other_desc_field[];
extern const char *recognized_file_extensions[];
extern const char *month_names_abbr[];
extern const char *month_names_eng[];
extern const char *name_special_separator[];
extern const char *latex_to_8bits[][2];
extern const char *upper_8bits_to_7bits[][2];
extern const char *lower_8bits_to_7bits[][2];

extern const char *key_unknown_name;


typedef enum {
  PAGE_TO_BE_GENERATED = 2,
  TO_BE_INDEXED = 1,
  TO_BE_IGNORED = -1,
  INDIFFERENT = 0
} enumIndexValue;

extern enumIndexValue default_index_status;


typedef struct {
  char firstName[SHORT_STR_LENGTH];
  char lastName[SHORT_STR_LENGTH];
  char keyName[SHORT_STR_LENGTH];
  char equivKeyName[SHORT_STR_LENGTH];
  int  nreferences;
  enumIndexValue status;
} typeName;  


#define MAX_NAMES 50

typedef struct {
  typeName *name;
  int n;
  int n_alloc;
  int PACK_OF_NAMES;
} typeListOfNames;






typedef struct {

  char *bibtex_file;

  char *bibtex_key;
  enumTypeItem type_item;
  enumTypeCategory type_cat;

  int nb_authors;
  typeName *authors_names;

  int nb_editors;
  typeName *editors_names;

  int nb_keywords;
  typeName *keywords;

  int year;

  char *field[DESC_FIELD_NB];

  char *bibtex_entry;

} typeItem;



#define SORT_CRITERIA_NB 5

typedef struct {
  typeItem **item;
  int n;
  int n_alloc;
  enumSortCriterium sort[SORT_CRITERIA_NB];
} typeListOfItems;

#define PACK_OF_ITEMS 1000




typedef struct {
  int length;
  char key[SHORT_STR_LENGTH];
  char str[STD_STR_LENGTH];
} typeString;


typedef struct {
  typeString *string;
  int n;
  int n_alloc;
} typeListOfStrings;


#define PACK_OF_STRINGS 25

#define IS_SPACE( C ) ( C == ' ' || C == '\t' || C == -96 )
#define IS_SPACE_OR_RETURN( C ) ( C == ' ' || C == '\t' || C == -96 || C == '\n' || C == '\r' )
#define IS_NOT_SPACE( C ) ( C != ' ' && C != '\t' && C != -96 )
#define IS_NOT_SPACE_OR_RETURN( C ) ( C != ' ' && C != '\t' && C != -96 && C != '\n' && C != '\r' )




extern void print_bibtex_fields( FILE *fd );
extern void print_bibtex_all_fields( FILE *fd );

extern void print_bibtex_templates( FILE *fd );
extern void print_bibtex_full_templates( FILE *fd );



#ifdef __cplusplus
}
#endif

#endif

/*************************************************************************
 * common.c - from bibtex2html distribution
 *
 * $Id: common.c,v 2.1 2004/05/06 12:50:42 greg Exp $
 *
 * Copyright � INRIA 2004, Gregoire Malandain
 *
 * The purpose of this software is to automatically produce html pages from 
 * bibtex files, and to provide access to the bibtex entries by several 
 * criteria: year of publication, category of publication and author name, 
 * from an index page. 
 * see http://www-sop.inria.fr/epidaure/personnel/malandain/codes/bibtex2html.html
 *
 * AUTHOR:
 * Gregoire Malandain (greg@sophia.inria.fr)
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * CREATION DATE: 
 * 
 *
 * ADDITIONS, CHANGES
 *
 */

#include <common.h>

/* Some memory leaks occured, that motivates the _malloc() and _free()
 * functions, designed to monitor memory allocation and freeing.
 *
 * They should not be used in practice. They should only be called
 * through MALLOC and FREE, that are macros protected by flag
 * '__memory_leaks'.
 *   
 */





#ifdef __memory_leaks
static int _memory_leaks_ = 1;
#else
static int _memory_leaks_ = 0;
#endif

static int _debug_ = 0;
static int _warning_ = 0;


void *_malloc( size_t size, 
	       char *proc )
{
  void *ret =(void*)NULL;
  if ( size <= 0 ) {
    if ( _debug_ ) {
      fprintf( stderr, "_malloc: negative or null size." );
      if ( proc != NULL ) 
	fprintf( stderr, " Call from '%s'.\n", proc );
      else
	fprintf( stderr, "\n" );
	
    }
    return( (void*)NULL );
  }
  
  ret = (void*)malloc( size );

  if (ret == (void*)NULL ) {
    if ( _debug_ ) {
      fprintf( stderr, "_malloc: allocation failed. Requested size was %d", (int)size );
      if ( proc != NULL ) 
	fprintf( stderr, " Call from '%s'.\n", proc );
      else
	fprintf( stderr, "\n" );
    }
    return( (void*)NULL );
  }

  if ( _memory_leaks_ ) {
    fprintf( stderr, "%p (size of %d) allocated in _malloc.", ret, (int)size );
    if ( proc != NULL ) 
      fprintf( stderr, " Call from '%s'.\n", proc );
    else
      fprintf( stderr, "\n" );
  }
  
  return( ret );
}



void _free( void *ptr, char *proc )
{
  if ( ptr == (void*)NULL ) {
    if ( _warning_ )
      fprintf( stderr, "_free: pointer already NULL\n" );
    return;
  }
  if ( _memory_leaks_ ) {
    fprintf( stderr, "%p freed in _free.", ptr );
    if ( proc != NULL ) 
      fprintf( stderr, " Call from '%s'.\n", proc );
    else
      fprintf( stderr, "\n" );
  }
  free( ptr );
}

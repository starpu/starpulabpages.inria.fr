/*************************************************************************
 * sort.c - from bibtex2html distribution
 *
 * $Id: sort.c,v 2.10 2004/07/20 15:19:35 greg Exp $
 *
 * Copyright � INRIA 2001, Gregoire Malandain
 *
 * The purpose of this software is to automatically produce html pages from 
 * bibtex files, and to provide access to the bibtex entries by several 
 * criteria: year of publication, category of publication and author name, 
 * from an index page. 
 * see http://www-sop.inria.fr/epidaure/personnel/malandain/codes/bibtex2html.html
 *
 * AUTHOR:
 * Gregoire Malandain (greg@sophia.inria.fr)
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * CREATION DATE: 
 * 
 *
 * ADDITIONS, CHANGES
 *
 */

#include <sort.h>











static int _complete_equality_of_names_ = 1;


int compare_key_names( char *key1, char *key2 )
{
  int l1, l2;

  if ( _complete_equality_of_names_ ) 
    return( strcmp( key1, key2 ) );
  l1 = strlen( key1 );
  l2 = strlen( key2 );
  if ( l1 < l2 ) 
    return( strncmp( key1, key2, l1 ) );
  return( strncmp( key1, key2, l2 ) );
}

void set_complete_equality_of_names()
{
  _complete_equality_of_names_ = 1;
}
void set_partial_equality_of_names()
{
  _complete_equality_of_names_ = 0;
}



/* update items keywords status
   with the ones of the indexed keywords (found in files)
*/
void update_keywords( typeListOfItems *li, typeListOfNames *ln, 
		      typeListOfNames *list_of_indexed_names )
{
  int a, i, n;
  int last, d, lo;
  int *offset = NULL;
  int update_is_done;
  
  if ( ln->n == 0 ) return;

  lo = 'Z'-'0'+1;
  offset = (int*)malloc( lo * sizeof(int) );
  for ( i=0; i<lo; i++ ) offset[i] = 0;

  last = 0;
  d = ln->name[last].keyName[0] - '0';
  for ( i=0; i<=d; i++ ) offset[i] = 0;
  
  for ( n=1; n<ln->n; n++ ) {
    if ( ln->name[n].keyName[0] == ln->name[last].keyName[0] ) continue;
    d = ln->name[n].keyName[0] - '0';

    if ( d >= lo ) {
      fprintf( stderr, "WARNING: strange key '%s' (built from [%s][%s])\n", 
	       ln->name[n].keyName, ln->name[n].firstName, ln->name[n].lastName );
      d = lo-1;
    }

    for ( ; i <= d ; i++ ) offset[i] = n;
    last = n;
  }
  for ( ; i < lo ; i++ ) offset[i] = ln->n - 1;
  
  for ( i=0; i<li->n; i++ ) {
    
    /* keywords
     */
    for ( a=0; a<li->item[i]->nb_keywords; a++ ) {

      update_is_done = 0;
      d = offset[ li->item[i]->keywords[a].keyName[0] - '0' ];
      while ( update_is_done == 0 &&
	      ln->name[d].keyName[0] == li->item[i]->keywords[a].keyName[0] ) {
	if ( strcmp( ln->name[d].keyName, 
		     li->item[i]->keywords[a].keyName ) == 0 ) {
	  strcpy( li->item[i]->keywords[a].equivKeyName, ln->name[d].equivKeyName );
	  li->item[i]->keywords[a].status = ln->name[d].status;
	  update_is_done = 1;
	}
	d++;
      }

    }

   }
  free( offset );
}



/* update items name status
   with the ones of the indexed names (found in files)
*/
void update_names( typeListOfItems *li, typeListOfNames *ln, 
		   typeListOfNames *list_of_indexed_names )
{
  int a, i, n;
  int last, d;
  int offset[26];
  int update_is_done;


  if ( ln->n == 0 ) return;

  /* offset[i]: first position of keys beginning wih 'A'+i
   */

  for ( i=0; i<26; i++ ) offset[i] = 0;

  last = 0;
  d = ln->name[last].keyName[0] - 'A';
  for ( i=0; i<=d; i++ ) offset[i] = 0;
  
  for ( n=1; n<ln->n; n++ ) {
    if ( ln->name[n].keyName[0] == ln->name[last].keyName[0] ) continue;
    d = ln->name[n].keyName[0] - 'A';

    if ( d >= 26 ) {
      fprintf( stderr, "WARNING: strange key '%s' (built from [%s][%s])\n", 
	       ln->name[n].keyName, ln->name[n].firstName, ln->name[n].lastName );
      d = 25;
    }

    for ( ; i <= d ; i++ ) offset[i] = n;
    last = n;
  }
  for ( ; i < 26 ; i++ ) offset[i] = ln->n - 1;
  
  for ( i=0; i<li->n; i++ ) {

    /* authors
     */
    for ( a=0; a<li->item[i]->nb_authors; a++ ) {

      update_is_done = 0;
      d = offset[ li->item[i]->authors_names[a].keyName[0] - 'A' ];
      while ( update_is_done == 0 &&
	      ln->name[d].keyName[0] == li->item[i]->authors_names[a].keyName[0] ) {
	if ( strcmp( ln->name[d].keyName, 
		     li->item[i]->authors_names[a].keyName ) == 0 ) {
	  strcpy( li->item[i]->authors_names[a].equivKeyName, ln->name[d].equivKeyName );
	  li->item[i]->authors_names[a].status = ln->name[d].status;
	  update_is_done = 1;
	}
	d++;
      }

    }


    /* editors
     */
    switch ( li->item[i]->type_item ) {
    case BOOK :
    case INBOOK :
      if ( li->item[i]->nb_authors > 0 ) break;
    case PROCEEDINGS :
      for ( a=0; a<li->item[i]->nb_editors; a++ ) {
	update_is_done = 0;
	d = offset[ li->item[i]->editors_names[a].keyName[0] - 'A' ];
	while ( update_is_done == 0 &&
		ln->name[d].keyName[0] == li->item[i]->editors_names[a].keyName[0] ) {
	  if ( strcmp( ln->name[d].keyName, 
		       li->item[i]->editors_names[a].keyName ) == 0 ) {
	    strcpy( li->item[i]->editors_names[a].equivKeyName, ln->name[d].equivKeyName );
	    li->item[i]->editors_names[a].status = ln->name[d].status;
	    update_is_done = 1;
	  }
	  d++;
	}
      }
    default :
      break;
    }


    

  }
  
}












const char *prefix[] = { "van den ", "van der ", "van de ", "van ",
			 "von der ", "von ",
			 "de la ", "de ",
			 "XXX" };

char *nametocmp( char *name )
{
  int i;
  unsigned int l;

  return( name );

  /* pour le moment, ce n'est pas compatible avec l'ecriture de 
     tous les auteurs, puisqu'on utilise le keyname pour determiner
     la premiere lettre.
     A revoir plus tard.
  */

  if ( name == NULL ) return( name );
  if ( name[0] < 'a' || 'z' < name[0] ) return( name );

  for ( i=0; strncmp( prefix[i], "XXX", 3 ) != 0; i++ ) {
    l = strlen( prefix[i] );
    if ( strlen( name ) <= l ) continue;
    if ( strncmp( name, prefix[i], l ) == 0 ) {
      return( (name+l) );
    }
  }
  return( name );
}



void swap_names( typeName *name1, typeName *name2 )
{
  typeName name;
  if ( name1 != name2 ) {
    (void)memcpy( &name, name1, sizeof(typeName) );
    (void)memcpy( name1, name2, sizeof(typeName) );
    (void)memcpy( name2, &name, sizeof(typeName) );
  }
}



int compare_names( typeName *name1, typeName *name2 )
{
  int cmp;

  if ( 0 ) {
    /* ca, ca fait un effet de bord avec les mots-cles,
       qui peuvent commencer par une majscule ou une minuscule
     */
    if ( name1->lastName != NULL && name1->lastName[0] != '\0' 
	 && name2->lastName != NULL && name2->lastName[0] != '\0' ) {
      cmp =  strcmp( nametocmp( name1->lastName ), 
		     nametocmp( name2->lastName ) );
      if ( cmp != 0 ) return( cmp );
    }
  }

  cmp = strcmp( name1->keyName, name2->keyName );
  if ( cmp != 0 ) return( cmp );
  
  cmp = strcasecmp( name1->firstName, name2->firstName );
  if ( cmp != 0 ) return( cmp );

  return( 1 );
}



void sort_names( typeName *name, int left, int right ) 
{
  int i, last;

  if ( left >= right ) return;

  /*--- swap left et (left+right)/2 ---*/
  swap_names( &(name[left]), &(name[(left+right)/2]) );
  
  last = left;
  for ( i=left+1; i<=right; i++ ) {
    if ( compare_names( &(name[i]), &(name[left]) ) < 0 ) {
      last ++;
      swap_names( &(name[i]), &(name[last]) );
    }
  }
  swap_names( &(name[left]), &(name[last]) );
  sort_names( name, left, last-1 );
  sort_names( name, last+1, right );
}










int compare_items_by_name( typeItem *item1, typeItem *item2 )
{
  int n, n1=0, n2=0;
  int cmp;
  typeName *names1=NULL, *names2=NULL;

  n1 = item1->nb_authors;
  if ( n1 > 0 ) {
    names1 = item1->authors_names;
  } else {
    switch ( item1->type_item ) {
    default :
      break;
    case BOOK :
    case INBOOK :
    case PROCEEDINGS :
      n1 = item1->nb_editors;
      if ( n1 > 0 )
	names1 = item1->editors_names;
    }
  }

  n2 = item2->nb_authors;
  if ( n2 > 0 ) {
    names2 = item2->authors_names;
  } else {
    switch ( item2->type_item ) {
    default :
      break;
    case BOOK :
    case INBOOK :
    case PROCEEDINGS :
      n2 = item2->nb_editors;
      if ( n2 > 0 )
	names2 = item2->editors_names;
    }
  }

  
  if ( n1 > 0 && n2 == 0 ) {
    return( 1 );
  }
  if ( n1 == 0 && n2 > 0 ) {
    return( -11 );
  }
  

  for ( n=0; n<n1 && n<n2; n++ ) {
    cmp =  strcmp( nametocmp( names1[n].lastName ), 
		   nametocmp( names2[n].lastName ) );
    if ( cmp != 0 ) return( cmp );
  }
  
  if ( n1 > n2 ) return(  1 );
  if ( n1 < n2 ) return( -1 );

  if ( item1->year > item2->year ) return( -1 );
  if ( item1->year < item2->year ) return(  1 );

  if ( item1->type_cat < item2->type_cat ) return( -1 );
  if ( item1->type_cat > item2->type_cat ) return(  1 );

  if ( item1->field[TITLE] != NULL && item2->field[TITLE] != NULL )
    return( strcmp( item1->field[TITLE], item2->field[TITLE] ) );

  return( -1 );
}



void sort_items_by_name( typeItem **item, int left, int right ) 
{
  int i, last;
  typeItem *tmp;

  if ( left >= right ) return;

  /*--- swap left et (left+right)/2 ---*/
  tmp = item[left]; item[left] = item[(left+right)/2]; item[(left+right)/2] = tmp;
  
  last = left;
  for ( i=left+1; i<=right; i++ ) {
    if ( compare_items_by_name( item[i], item[left] ) < 0 ) {
      last ++;
      tmp = item[i]; item[i] = item[last]; item[last] = tmp;
    }
  }
  tmp = item[left]; item[left] = item[last]; item[last] = tmp;
  sort_items_by_name( item, left, last-1 );
  sort_items_by_name( item, last+1, right );
}









int compare_items_by_type( typeItem *item1, typeItem *item2 )
{
  int nb, n, n1, n2;
  int c;
 
  if ( item1->type_cat < item2->type_cat ) return( -1 );
  if ( item1->type_cat > item2->type_cat ) return(  1 );

  if ( item1->type_item < item2->type_item ) return( -1 );
  if ( item1->type_item > item2->type_item ) return(  1 );

  if ( item1->year > item2->year ) return( -1 );
  if ( item1->year < item2->year ) return(  1 );

  n1 = item1->nb_authors;
  n2 = item2->nb_authors;

  if ( n1 == 0 && n2 == 0 ) return( 0 );
  if ( n1 == 0 && n2 > 0 ) return(  1 );
  if ( n1 > 0 && n2 == 0 ) return( -1 );
  nb = ( n1 > n2 ) ? n2 : n1;
  for ( n = 0; n<nb; n++ ) {
    c = compare_key_names( item1->authors_names[n].keyName, item2->authors_names[n].keyName );
    if ( c < 0 ) return( -1 );
    if ( c > 0 ) return(  1 );
  }
  return( 0 );
}



void sort_items_by_type( typeItem **item, int left, int right ) 
{
  int i, last;
  typeItem *tmp;

  if ( left >= right ) return;

  /*--- swap left et (left+right)/2 ---*/
  tmp = item[left]; item[left] = item[(left+right)/2]; item[(left+right)/2] = tmp;
  
  last = left;
  for ( i=left+1; i<=right; i++ ) {
    if ( compare_items_by_type( item[i], item[left] ) < 0 ) {
      last ++;
      tmp = item[i]; item[i] = item[last]; item[last] = tmp;
    }
  }
  tmp = item[left]; item[left] = item[last]; item[last] = tmp;
  sort_items_by_type( item, left, last-1 );
  sort_items_by_type( item, last+1, right );
}









int compare_items_by_year( typeItem *item1, typeItem *item2 )
{
  int nb, n, n1, n2;

  if ( item1->year > item2->year ) return( -1 );
  if ( item1->year < item2->year ) return(  1 );

  if ( item1->type_item < item2->type_item ) return( -1 );
  if ( item1->type_item > item2->type_item ) return(  1 );

  n1 = item1->nb_authors;
  n2 = item2->nb_authors;

  if ( n1 == 0 && n2 == 0 ) return( 0 );
  if ( n1 == 0 && n2 > 0 ) return(  1 );
  if ( n1 > 0 && n2 == 0 ) return( -1 );
  nb = ( n1 > n2 ) ? n2 : n1;
  for ( n = 0; n<nb; n++ ) {

    if ( (item1->authors_names[n].lastName == NULL || item1->authors_names[n].lastName[0] == '\0') &&
	 (item2->authors_names[n].lastName == NULL || item1->authors_names[n].lastName[0] == '\0') )
      return( 0 );
    if ( (item1->authors_names[n].lastName == NULL || item1->authors_names[n].lastName[0] == '\0') &&
	 (item2->authors_names[n].lastName != NULL && item1->authors_names[n].lastName[0] != '\0') )
      return( 1 );
    if ( (item1->authors_names[n].lastName != NULL && item1->authors_names[n].lastName[0] != '\0') &&
	 (item2->authors_names[n].lastName == NULL || item1->authors_names[n].lastName[0] == '\0') )
      return( -1 );

    if ( strcasecmp( item1->authors_names[n].lastName, item2->authors_names[n].lastName ) < 0 )
      return( -1 );
    if ( strcasecmp( item1->authors_names[n].lastName, item2->authors_names[n].lastName ) > 0 )
      return( 1 );
  }
  return( 0 );
}



void sort_items_by_year( typeItem **item, int left, int right ) 
{
  int i, last;
  typeItem *tmp;

  if ( left >= right ) return;

  /*--- swap left et (left+right)/2 ---*/
  tmp = item[left]; item[left] = item[(left+right)/2]; item[(left+right)/2] = tmp;
  
  last = left;
  for ( i=left+1; i<=right; i++ ) {
    if ( compare_items_by_year( item[i], item[left] ) < 0 ) {
      last ++;
      tmp = item[i]; item[i] = item[last]; item[last] = tmp;
    }
  }
  tmp = item[left]; item[left] = item[last]; item[last] = tmp;
  sort_items_by_year( item, left, last-1 );
  sort_items_by_year( item, last+1, right );
}












void sort_list_of_items( typeListOfItems *l, enumSortCriterium c )
{
  enumSortCriterium newc[SORT_CRITERIA_NB];

  switch( c ) {
  default :
    return;
  case _NAME_ :
    newc[0] = _NAME_;
    newc[1] = _YEAR_;
    newc[2] = _CATEGORY_;
    newc[3] = _TYPE_;
    newc[4] = _TITLE_;   
    break;
  case _CATEGORY_ :
    newc[0] = _CATEGORY_;
    newc[1] = _YEAR_;
    newc[2] = _TYPE_;
    newc[3] = _NAME_;
    newc[4] = _TITLE_;   
    break;
  case _BIBKEY_ :
    newc[0] = _BIBKEY_;
    newc[1] = _NAME_;
    newc[2] = _YEAR_;
    newc[3] = _CATEGORY_;   
    newc[4] = _TYPE_;
    break;
  case _TYPE_ :
    newc[0] = _TYPE_;
    newc[1] = _JOURNAL_;
    newc[2] = _BOOKTITLE_;
    newc[3] = _YEAR_;
    newc[4] = _NAME_;   
    break;
  case _YEAR_ :
    newc[0] = _YEAR_;
    newc[1] = _CATEGORY_;
    newc[2] = _TYPE_;
    newc[3] = _NAME_;
    newc[4] = _TITLE_;   
    break;
  }

  sort_list_of_items_with_multiple_criteria( l, newc );
  return;

  if ( l->sort[0] == c ) return;
  switch( c ) {
  default :
    l->sort[0] = _NONE_;
    break;
  case _NAME_ :
    sort_items_by_name( l->item, 0, l->n-1 );
    l->sort[0] = _NAME_;
    break;
  case _CATEGORY_ :
    sort_items_by_type( l->item, 0, l->n-1 );
    l->sort[0] = _CATEGORY_;
    break;
  case _YEAR_ :
    sort_items_by_year( l->item, 0, l->n-1 );
    l->sort[0] = _YEAR_;
    break;
  }
}



int compare_items_by_title_only( typeItem *item1, typeItem *item2 ) 
{
  if ( item1->field[TITLE] == NULL && item2->field[TITLE] == NULL )
    return( 0 );
  if ( item1->field[TITLE] == NULL && item2->field[TITLE] != NULL )
    return( -1 );
  if ( item1->field[TITLE] != NULL && item2->field[TITLE] == NULL )
    return( 1 );
  return( strcmp( item1->field[TITLE], item2->field[TITLE] ) );
}



int compare_items_by_booktitle_only( typeItem *item1, typeItem *item2 ) 
{
  if ( item1->field[BOOKTITLE] == NULL && item2->field[BOOKTITLE] == NULL )
    return( 0 );
  if ( item1->field[BOOKTITLE] == NULL && item2->field[BOOKTITLE] != NULL )
    return( -1 );
  if ( item1->field[BOOKTITLE] != NULL && item2->field[BOOKTITLE] == NULL )
    return( 1 );
  return( strcmp( item1->field[BOOKTITLE], item2->field[BOOKTITLE] ) );
}



int compare_items_by_journal_only( typeItem *item1, typeItem *item2 ) 
{
  if ( item1->field[JOURNAL] == NULL && item2->field[JOURNAL] == NULL )
    return( 0 );
  if ( item1->field[JOURNAL] == NULL && item2->field[JOURNAL] != NULL )
    return( -1 );
  if ( item1->field[JOURNAL] != NULL && item2->field[JOURNAL] == NULL )
    return( 1 );
  return( strcmp( item1->field[JOURNAL], item2->field[JOURNAL] ) );
}



int compare_items_by_year_only( typeItem *item1, typeItem *item2 ) 
{
  if ( item1->year > item2->year ) return( -1 );
  if ( item1->year < item2->year ) return(  1 );
  return( 0 );
}



int compare_items_by_category_only( typeItem *item1, typeItem *item2 ) 
{
  if ( item1->type_cat < item2->type_cat ) return( -1 );
  if ( item1->type_cat > item2->type_cat ) return(  1 );
  return( 0 );
}



int compare_items_by_type_only( typeItem *item1, typeItem *item2 ) 
{
  if ( item1->type_item < item2->type_item ) return( -1 );
  if ( item1->type_item > item2->type_item ) return(  1 );
  return( 0 );
}



int compare_items_by_bibtex_key_only( typeItem *item1, typeItem *item2 ) 
{
  if ( item1->bibtex_key == NULL && item2->bibtex_key == NULL )
    return( 0 );
  if ( item1->bibtex_key == NULL && item2->bibtex_key != NULL )
    return( -1 );
  if ( item1->bibtex_key != NULL && item2->bibtex_key == NULL )
    return( 1 );
  return( strcmp( item1->bibtex_key, item2->bibtex_key ) );
}



int compare_items_by_names_only( typeItem *item1, typeItem *item2 ) 
{
  int n, n1=0, n2=0;
  int cmp;
  typeName *names1=NULL, *names2=NULL;

  n1 = item1->nb_authors;

  if ( n1 > 0 ) {
    names1 = item1->authors_names;
  } else {
    switch ( item1->type_item ) {
    default :
      break;
    case BOOK :
    case INBOOK :
    case PROCEEDINGS :
      n1 = item1->nb_editors;
      if ( n1 > 0 )
	names1 = item1->editors_names;
    }
  }

  n2 = item2->nb_authors;
  if ( n2 > 0 ) {
    names2 = item2->authors_names;
  } else {
    switch ( item2->type_item ) {
    default :
      break;
    case BOOK :
    case INBOOK :
    case PROCEEDINGS :
      n2 = item2->nb_editors;
      if ( n2 > 0 )
	names2 = item2->editors_names;
    }
  }

  
  if ( n1 > 0 && n2 == 0 ) {
    return( 1 );
  }
  if ( n1 == 0 && n2 > 0 ) {
    return( -1 );
  }
  if ( n1 == 0 && n2 == 0 ) {
    return( 0 );
  }

  /* it's a bit tricky
     comparison on the first name
     if equal, comparison on year
     if equal, comparison on other names
  */
  cmp =  strcmp( nametocmp( names1[0].lastName ), 
		 nametocmp( names2[0].lastName ) );
  if ( cmp != 0 ) return( cmp );
  cmp = compare_items_by_year_only( item1, item2 );
  if ( cmp != 0 ) return( cmp );

  for ( n=1; n<n1 && n<n2; n++ ) {
    cmp =  strcmp( nametocmp( names1[n].lastName ), 
		   nametocmp( names2[n].lastName ) );
    if ( cmp != 0 ) return( cmp );
  }
  
  if ( n1 > n2 ) return(  1 );
  if ( n1 < n2 ) return( -1 );

  return( 0 );
}


int compare_items_by_criteria( typeItem *item1, typeItem *item2,
			       enumSortCriterium c[SORT_CRITERIA_NB], int ic )
{
  int r = 0;

  if ( ic >= SORT_CRITERIA_NB ) return( 0 );
  if ( c[ic] == _NONE_ ) return( 0 );

  switch( c[ic] ) {
  default :
    return( 0 );
  case _YEAR_ :
    r = compare_items_by_year_only( item1, item2 );
    if ( r != 0 ) return( r );
    break;
  case _CATEGORY_ :
    r = compare_items_by_category_only( item1, item2 );
    if ( r != 0 ) return( r );
    break;
  case _TYPE_ :
    r = compare_items_by_type_only( item1, item2 );
    if ( r != 0 ) return( r );
    break;
  case _TITLE_ :
    r = compare_items_by_title_only( item1, item2 );
    if ( r != 0 ) return( r );
    break;
  case _BOOKTITLE_ :
    r = compare_items_by_booktitle_only( item1, item2 );
    if ( r != 0 ) return( r );
    break;
  case _JOURNAL_ :
    r = compare_items_by_journal_only( item1, item2 );
    if ( r != 0 ) return( r );
    break;
  case _NAME_ :
    r = compare_items_by_names_only( item1, item2 );
    if ( r != 0 ) return( r );
    break;
  case _BIBKEY_ :
    r = compare_items_by_bibtex_key_only( item1, item2 );
    if ( r != 0 ) return( r );
    break;
  }

  if ( r != 0 ) return( r );
  if ( ic == SORT_CRITERIA_NB - 1 ) return( 0 );

  return( compare_items_by_criteria( item1, item2, c, ic+1 ) );
}





void sort_items_by_criteria( typeItem **item, int left, int right,
			     enumSortCriterium c[SORT_CRITERIA_NB] ) 
{
  int i, last;
  typeItem *tmp;

  if ( left >= right ) return;

  /*--- swap left et (left+right)/2 ---*/
  tmp = item[left]; item[left] = item[(left+right)/2]; item[(left+right)/2] = tmp;
  
  last = left;
  for ( i=left+1; i<=right; i++ ) {
    if ( compare_items_by_criteria( item[i], item[left], c, 0 ) < 0 ) {
      last ++;
      tmp = item[i]; item[i] = item[last]; item[last] = tmp;
    }
  }
  tmp = item[left]; item[left] = item[last]; item[last] = tmp;
  sort_items_by_criteria( item, left, last-1, c );
  sort_items_by_criteria( item, last+1, right, c );
}


void sort_list_of_items_with_multiple_criteria( typeListOfItems *l, 
						enumSortCriterium c[SORT_CRITERIA_NB] )
{
  int i;
  int is_sorted = -1;
  
  for ( i=0; i < SORT_CRITERIA_NB && is_sorted == -1; i++ ) {
    if ( c[i] == _NONE_ ) {
      is_sorted = 1;
    }
    else if ( l->sort[i] != c[i] ) {
      is_sorted = 0;
    }
  }

  if ( is_sorted == 1 || is_sorted == -1 ) {
    return;
  }

  sort_items_by_criteria( l->item, 0, l->n-1, c );

  for ( i=0; i < SORT_CRITERIA_NB; i++ )
    l->sort[i] = c[i];

}

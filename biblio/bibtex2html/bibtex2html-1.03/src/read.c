/*************************************************************************
 * read.c - from bibtex2html distribution
 *
 * $Id: read.c,v 2.30 2005/07/21 08:20:08 greg Exp $
 *
 * Copyright � INRIA 2001, Gregoire Malandain
 *
 * The purpose of this software is to automatically produce html pages from 
 * bibtex files, and to provide access to the bibtex entries by several 
 * criteria: year of publication, category of publication and author name, 
 * from an index page. 
 * see http://www-sop.inria.fr/epidaure/personnel/malandain/codes/bibtex2html.html
 *
 * AUTHOR:
 * Gregoire Malandain (greg@sophia.inria.fr)
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * CREATION DATE: 
 * 
 *
 * ADDITIONS, CHANGES
 *
 */

#include <read.h>





static int _debug_ = 0;
static int _verbose_ = 1;
static int _warning_ = 1;





/************************************************************
 *
 * MISC #1
 *
 ************************************************************/





int alloc_and_copy_str( char ** dest, char *src )
{
  char *proc = "alloc_and_copy_str";
  char *tmp = NULL;
  int l;
  
  if ( src == NULL || src[0] == '\0' ) {
    *dest = NULL;
    return( 1 );
  }
  l = 1+strlen(src);
  tmp = (char *)malloc( l * sizeof(char) );
  if ( tmp == NULL ) {
    if ( _verbose_ ) 
      fprintf( stderr, "%s: allocation failed\n", proc );
    *dest = NULL;
    return( 0 );
  }
  (void)strncpy( tmp, src, l );
  *dest = tmp;
  return( 1 );
}


void remove_nested_braces( char *str )
{
  char *beg = str;
  char *end;
  char *aux;
  int nb_beg_braces = 0;
  int nb_end_braces = 0;
  int min_open_braces, nb_open_braces;

  if ( str == NULL || str[0] == '\0' ) return;


  /* count '{' at the beginning of string
   */
  while ( IS_SPACE( *beg ) || *beg == '{' ) {
    if ( *beg == '{' ) nb_beg_braces++;
    beg++;
  }
  if ( nb_beg_braces == 0 ) return;

  /* count '}' at the end of string
   */
  end = str;
  end +=strlen( str );
  end --;
  nb_end_braces = 0;
  while ( IS_SPACE( *end ) || *end == '}' ) {
    if ( *end == '}' ) nb_end_braces++;
    end --;
  }
  if ( nb_end_braces == 0 ) return;
  
  /* count minimum number of open braces
   */
  min_open_braces = nb_open_braces = nb_beg_braces;
  while ( beg < end ) {
    if ( *beg == '{' ) nb_open_braces++;
    if ( *beg == '}' ) {
      nb_open_braces--;
      if ( min_open_braces > nb_open_braces ) min_open_braces = nb_open_braces;
    }
    beg ++;
  }
  
  if ( min_open_braces == 0 ) return;
  /* we can remove #min_open_brace at the beginning and at the
     end of the string
  */
  
  beg = str;
  nb_beg_braces = nb_end_braces = min_open_braces;
  while ( IS_SPACE( *beg ) || (*beg == '{' && nb_beg_braces > 0) ) {
    if ( *beg == '{' && nb_beg_braces > 0 ) {
      *beg = ' ';
      nb_beg_braces --;
    }
    beg++;
  }

  aux = str;
  while ( *beg != '\0' ) *aux++ = *beg++;
  *aux = '\0';

  end = str;
  end +=strlen( str );
  end --;
  while ( IS_SPACE( *end ) || (*end == '}' && nb_end_braces > 0) ) {
    if ( *end == '}' && nb_end_braces > 0 ) {
      *end = '\0';
      /* the line below was missing
	 corrected Mon Mar  4 14:45:09 MET 2002 (GM)
      */
      nb_end_braces --;
    }
    end --;
  }
}


int count_words( char *str )
{
  char *tmp;
  int w = 0;
  
  if ( str == NULL || *str == '\0' ) return( 0 );
  tmp = str;

  do {

    /* skip the first separators 
     */
    while ( IS_SPACE( *tmp ) && *tmp != '\0' && *tmp != '\n' )
      tmp ++;
    
    if ( *tmp == '\0' || *tmp == '\n' ) return( w );

    /* skip the word
     */
    while ( IS_NOT_SPACE( *tmp ) && *tmp != '\0' && *tmp != '\n' )
      tmp ++;

    w++;
  
    if ( *tmp == '\0' || *tmp == '\n' ) return( w );

  } while ( 1 );
  
  /* should never occur
   */
  return( -1 );
}


char * strcpy_in_lower_case( char *dest, char *src )
{
  char *dst = dest;
  char *tmp = src;
  int i, t;
  int trs_is_done;

  if ( dst == NULL || tmp == NULL ) return ( NULL );
  
  for ( ; *tmp != '\0' && *tmp != '\n' ; tmp ++ ) {
    if ( 'a' <= *tmp && *tmp <= 'z' ) {
      *dst++ = *tmp;
      continue;
    }

    if ( 'A' <= *tmp && *tmp <= 'Z' ) {
      *dst++ = *tmp - 'A' + 'a';
      continue;
    }

    for ( trs_is_done=0, t=0; trs_is_done==0 && upper_8bits_to_7bits[t][1][0] !='X'; t++ ) {
      if ( strncmp( tmp, upper_8bits_to_7bits[t][0], strlen(upper_8bits_to_7bits[t][0]) ) == 0 ) {
	for ( i = 0; i < (int)strlen(upper_8bits_to_7bits[t][1]); i ++ ) {
	  if ( 'A' <= upper_8bits_to_7bits[t][1][i] && upper_8bits_to_7bits[t][1][i] <= 'Z' )
	    *dst++ = upper_8bits_to_7bits[t][1][i] - 'A' + 'a';
	}
	tmp += strlen( upper_8bits_to_7bits[t][0] );
	trs_is_done = 1;
      }
    }

    if ( trs_is_done == 1 ) continue;

    for ( trs_is_done=0, t=0; trs_is_done==0 && lower_8bits_to_7bits[t][1][0] !='X'; t++ ) {
      if ( strncmp( tmp, lower_8bits_to_7bits[t][0], strlen(lower_8bits_to_7bits[t][0]) ) == 0 ) {
	for ( i = 0; i < (int)strlen(lower_8bits_to_7bits[t][1]); i ++ ) {
	  if ( 'a' <= lower_8bits_to_7bits[t][1][i] && lower_8bits_to_7bits[t][1][i] <= 'z' )
	    *dst++ = lower_8bits_to_7bits[t][1][i];
	}
	tmp += strlen( lower_8bits_to_7bits[t][0] );
	trs_is_done = 1;
      }
    }
  }
  
  return( dst );
}


char * strcpy_first_letters_in_lower_case( char *dest, char *src )
{
  char *dst = dest;
  char *tmp = src;
  int t;
  int trs_is_done;

  if ( dst == NULL || tmp == NULL ) return ( dst );
  if ( *tmp == '\0' ) return ( dst );

    do {

    /* skip the first separators 
     */
    while ( IS_SPACE( *tmp ) && *tmp != '\0' && *tmp != '\n' )
      tmp ++;
    
    if ( *tmp == '\0' || *tmp == '\n' ) return( dst );

    /* write first letter
     */
    if ( 'a' <= *tmp && *tmp <= 'z' ) {
      *dst++ = *tmp;
    }
    else if ( 'A' <= *tmp && *tmp <= 'Z' ) {
      *dst++ = *tmp - 'A' + 'a';
    }
    else {
      for ( trs_is_done=0, t=0; trs_is_done==0 && upper_8bits_to_7bits[t][1][0] !='X'; t++ ) {
	if ( strncmp( tmp, upper_8bits_to_7bits[t][0], strlen(upper_8bits_to_7bits[t][0]) ) == 0 ) {
	  if ( 'A' <= upper_8bits_to_7bits[t][1][0] && upper_8bits_to_7bits[t][1][0] <= 'Z' )
	    *dst++ = upper_8bits_to_7bits[t][1][0] - 'A' + 'a';
	  trs_is_done = 1;
	}
      }
      
      if ( trs_is_done == 0 ) {
	for ( trs_is_done=0, t=0; trs_is_done==0 && lower_8bits_to_7bits[t][1][0] !='X'; t++ ) {
	  if ( strncmp( tmp, lower_8bits_to_7bits[t][0], strlen(lower_8bits_to_7bits[t][0]) ) == 0 ) {
	    if ( 'a' <= lower_8bits_to_7bits[t][1][0] && lower_8bits_to_7bits[t][1][0] <= 'z' )
	      *dst++ = lower_8bits_to_7bits[t][1][0];
	    trs_is_done = 1;
	  }
	}
      }
    }
    
    /* skip the word
     */
    while ( IS_NOT_SPACE( *tmp ) && *tmp != '\0' && *tmp != '\n' )
      tmp ++;
    
    if ( *tmp == '\0' || *tmp == '\n' ) return( dst );
    
    } while ( 1 );
  
  /* should never occur
   */
  return( NULL );
}





/************************************************************
 *
 * END OF MISC #1
 *
 * NAMES MANAGEMENT
 *
 ************************************************************/





void init_name( typeName *n ) 
{
  (void)memset( n->firstName, 0, SHORT_STR_LENGTH );
  (void)memset( n->lastName,  0, SHORT_STR_LENGTH );
  (void)memset( n->keyName,  0, SHORT_STR_LENGTH );
  (void)memset( n->equivKeyName,  0, SHORT_STR_LENGTH );
  n->nreferences = 0;
  n->status = default_index_status;
}


void copy_name( typeName *dest, const typeName *src )
{
  (void)memcpy( dest->firstName, src->firstName, SHORT_STR_LENGTH );
  (void)memcpy( dest->lastName,  src->lastName,  SHORT_STR_LENGTH );
  (void)memcpy( dest->keyName,   src->keyName,  SHORT_STR_LENGTH );
  (void)memcpy( dest->equivKeyName, src->equivKeyName,  SHORT_STR_LENGTH );
  dest->nreferences = src->nreferences;
  dest->status = src->status;
}


void print_name( FILE *file, typeName *n ) 
{
  FILE *f = file;

  if ( f == NULL ) f = stdout;
  fprintf( f, "firstname=[%s]   lastname=[%s]   keyname=[%s]",
	   n->firstName, n->lastName, n->keyName );
  if ( n->equivKeyName[0] != '\0' 
       && strcmp( n->keyName, n->equivKeyName ) != 0 )
    fprintf( f, "<->[%s]", n->equivKeyName );
  fprintf( f, "   status=[%2d] refs=[%2d]\n", n->status, n->nreferences );
}


void init_list_of_names( typeListOfNames *li )
{
  li->n = li->n_alloc = 0;
  li->PACK_OF_NAMES = 100;
  li->name = NULL;
}


void release_list_of_names( typeListOfNames *li )
{
  if ( li->name != NULL ) {
    free( li->name );
  }
  init_list_of_names( li );
}


void print_list_of_names( FILE *file, typeListOfNames *li )
{
  int i;
  FILE *f = file;

  if ( f == NULL ) f = stdout;
  fprintf( f, "#NAMES=%d\n", li->n );
  for (i=0; i<li->n; i++ ) {
    fprintf( f, " #%3d: ", i+1 );
    print_name( f, &(li->name[i]) );
  }
}


int add_name_to_list_of_names( typeListOfNames *ln, typeName *s )
{
  char *proc = "add_name_to_list_of_names";

  int nalloc;
  typeName *newname = NULL;
  int i;


  if ( ln->n == ln->n_alloc ) {
    /* allocation
     */
    nalloc = ln->n_alloc + ln->PACK_OF_NAMES;
    newname = (typeName *)malloc( nalloc * sizeof(typeName) );
    if ( newname == NULL ) {
      if ( _verbose_ ) 
	fprintf( stderr, "%s: unable to allocate list of names\n", proc );
      return( 0 );
    }
    /* init
     */
    for ( i=ln->n_alloc; i<ln->n_alloc + ln->PACK_OF_NAMES; i++ )
      init_name( &(newname[i]) );
    /* copy 
     */
    if ( ln->n_alloc > 0 ) {
      (void)memcpy( newname, ln->name, ln->n_alloc * sizeof(typeName) );
      free( ln->name );
    }
    ln->name = newname;
    ln->n_alloc = nalloc;
  }
  copy_name( &(ln->name[ln->n]), s );

  ln->n ++;
  return( 1 );
}


/* flag = 1 => accept [0-9] as first character
   that was added for the keywords

   A key is made of the upper "version" of the last name
   plus the upper letters of the first name
*/
static void build_keyname_from_last_and_first_names( typeName *theName, int flag )
{
  char *name, *aux;
  int trs_is_done, t, u;
  /* do the key
     translate into upper characters the name
     add the initial of the first name
     remove the non alphabetic characters
     skip to unknown if empty
  */
  
  name = theName->keyName;
  aux = theName->lastName;

  /* veryfing the name's length
   */
  t = 0;
  while ( *aux++ != '\0' ) {
    t ++;
    if ( t >= SHORT_STR_LENGTH ) {
      if ( _warning_ ) {
	fprintf( stderr, "WARNING: string '%s' too long, unable to build a key from it.\n", 
		 theName->lastName );
      }
      strcpy( theName->keyName, key_unknown_name );
      return;
    }
  }

  aux = theName->lastName;

  /* skip to the next alpha(-numerique) value
   */
  if ( flag ) {
    while( *aux != '\0' && (*aux < 'A' || *aux > 'Z') && (*aux < 'a' || *aux > 'z') && (*aux < '0' || *aux > '9') )
      aux++;
  }
  else {
    while( *aux != '\0' && (*aux < 'A' || *aux > 'Z') && (*aux < 'a' || *aux > 'z') )
      aux++;
  }


  /* "copy" the string theName->lastName into theName->keyName
   */
  while ( *aux != '\0' ) { 
    if ( (*aux >= 'A' && *aux <= 'Z') ||
	 (*aux >= '0' && *aux <= '9') || 
	 (*aux == '-') || (*aux == '_') ) {
      *name++ = *aux++;
      continue;
    } 
    if ( *aux >= 'a' && *aux <= 'z' ) {
      *name++ = *aux++ + 'A' - 'a';
      continue;
    } 
    if ( (*aux == ' ') ) {
      *name++ = '-';
      aux++;
      continue;
    } 
    
    for ( trs_is_done=0, t=0; trs_is_done==0 && upper_8bits_to_7bits[t][1][0] !='X'; t++ ) {
      if ( strncmp( aux, upper_8bits_to_7bits[t][0], strlen(upper_8bits_to_7bits[t][0]) ) == 0 ) {
	if ( strlen(upper_8bits_to_7bits[t][1]) > 0 ) {
	  strcpy( name, upper_8bits_to_7bits[t][1] );
	  name += strlen(upper_8bits_to_7bits[t][1]);
	  aux += strlen( upper_8bits_to_7bits[t][0] );
	  trs_is_done = 1;
	}
      }
    }
    if ( trs_is_done == 1 ) continue;
    for ( t=0; trs_is_done==0 && lower_8bits_to_7bits[t][1][0] !='X'; t++ ) {
      if ( strncmp( aux, lower_8bits_to_7bits[t][0], strlen(lower_8bits_to_7bits[t][0]) ) == 0 ) {
	if ( strlen(lower_8bits_to_7bits[t][1]) > 0 ) {
	  strcpy( name, lower_8bits_to_7bits[t][1] );
	  for ( u = 0; u < (int)strlen(lower_8bits_to_7bits[t][1]); u++, name++ ) {
	    if ( *name >= 'a' && *name <= 'z' ) 
	      *name += 'A' - 'a';
	  }
	  aux += strlen( lower_8bits_to_7bits[t][0] );
	  trs_is_done = 1;
	}
      }
    }
    if ( trs_is_done == 1 ) continue;
    aux ++;
  }


  /* the string theName->lastName has been copied into theName->keyName
     copy only upper letters from the first name
   */

  if ( strlen( theName->keyName ) > 0 && strlen( theName->firstName ) > 0) {
    *name++ = '-';
    aux = theName->firstName;
    while ( *aux != '\0' ) { 
      if ( *aux >= 'A' && *aux <= 'Z' ) {
	*name++ = *aux++;
	continue;
      }
      for ( trs_is_done=0, t=0; trs_is_done==0 && upper_8bits_to_7bits[t][1][0] !='X'; t++ ) {
	if ( strncmp( aux, upper_8bits_to_7bits[t][0], strlen(upper_8bits_to_7bits[t][0]) ) == 0 ) {
	  if ( strlen(upper_8bits_to_7bits[t][1]) > 0 ) {
	    strcpy( name, upper_8bits_to_7bits[t][1] );
	    name += strlen(upper_8bits_to_7bits[t][1]);
	    aux += strlen( upper_8bits_to_7bits[t][0] );
	    trs_is_done = 1;
	  }
	}
      }
      aux ++;
    }
  }

  if ( theName->keyName[0] == '\0' ) 
    (void)strcpy( theName->keyName, key_unknown_name );
}










int retrieve_names_to_be_indexed( typeListOfNames *li, 
				  typeListOfNames *list_of_indexed_names, 
				  typeListOfNames *list_of_equivalent_names )
{
  char *proc="retrieve_names_to_be_indexed";
  int i, j, k, l;
  int index_status = INDIFFERENT;
  int index_is_set;
  int key_is_found;

  if ( li->n == 0 ) return( 0 );


  /* what should we do ? -> get index_status
     
     if there is one TO_BE_INDEXED
     the default will be TO_BE_IGNORED
     else the default may be TO_BE_INDEXED
   */
  if ( list_of_indexed_names->n == 0 ) {
  
    index_status = TO_BE_INDEXED;

  } else {

    for ( i=0; index_status != TO_BE_IGNORED && i<list_of_indexed_names->n; i++ ) {
      if ( list_of_indexed_names->name[i].status == TO_BE_INDEXED ) {
	index_status = TO_BE_IGNORED;
      }
      else if ( list_of_indexed_names->name[i].status == TO_BE_IGNORED ) {
	index_status = TO_BE_INDEXED;
      }
    }

  }


  /* at this point
     index_status = TO_BE_IGNORED if there are names that are to be indexed
     or TO_BE_INDEXED else
  */


  /* first extraction 
     suppress multiple occurrences of the same name
   */
  for (j=0,i=1; i<li->n; i++ ) {
    if ( strcmp( li->name[i].keyName, li->name[j].keyName ) == 0 ) {
      if ( i != j ) {
	/* copy the name to get the full first name, if any
	   get the number of reference for each paper
	 */
	li->name[i].nreferences = li->name[j].nreferences;
	copy_name( &(li->name[j]), &(li->name[i]) );
	li->name[j].nreferences ++;
      }
      continue;
    }
    j++;
    if ( i != j ) copy_name( &(li->name[j]), &(li->name[i]) );
  }
  j++;
  li->n = j;



  /* add the names from the list of equivalent names 
     that are not already present in the list
   */
  for ( j=0; j<list_of_equivalent_names->n; j++ ) {
    for (i=0, key_is_found=0; i<li->n && key_is_found==0; i++ ) {
      if ( strcmp( li->name[i].keyName, list_of_equivalent_names->name[j].keyName ) == 0 ) {
	key_is_found = 1;
	(void)memcpy( li->name[i].equivKeyName, 
		      list_of_equivalent_names->name[j].equivKeyName,  SHORT_STR_LENGTH );
	if ( strlen( list_of_equivalent_names->name[j].firstName ) > strlen( li->name[i].firstName ) )
	  (void)memcpy( li->name[i].firstName, 
			list_of_equivalent_names->name[j].firstName,  SHORT_STR_LENGTH );
      }
    }
    if ( key_is_found == 0 ) {
      if ( add_name_to_list_of_names( li, &(list_of_equivalent_names->name[j]) ) != 1 ) {
	if ( _verbose_ )
	  fprintf( stderr, "%s: unable to add name to list\n", proc );
	return( 0 );
      }
    }
  }
  
  sort_names( li->name, 0, li->n-1 );



  /* propagation of the equivalent key names
   */
  for ( i = li->n-1; i > 0; i-- ) {
    for ( j = i-1; j >= 0; j-- ) {
      if ( strcmp( li->name[j].equivKeyName, li->name[i].keyName ) == 0 )
	(void)memcpy( li->name[j].equivKeyName, li->name[i].equivKeyName, 
		      SHORT_STR_LENGTH );
    }
  }

  /* count the references
   */
  for (i=0; i<li->n-1; i++ ) {
    if ( strcmp( li->name[i].equivKeyName, li->name[i].keyName ) != 0 ) {
      for ( key_is_found=0, j=i+1; j<li->n && key_is_found==0; j++ ) {
	if ( strcmp( li->name[i].equivKeyName, li->name[j].keyName ) == 0 ) {
	  key_is_found = 1;
	  li->name[j].nreferences += li->name[i].nreferences;
	  li->name[i].nreferences = 0;
	}
      }
      if ( key_is_found == 0 ) {
	if ( _verbose_ ) {
	  fprintf( stderr, "%s: STRANGE, found no equivalence for\n", proc );
	  print_name( stderr, &(li->name[i]) );
	}
      }
    }
  }


  /* comparison with the names to be indexed
   */
  for (i=0; i<li->n; i++ ) {
    for ( index_is_set=0, j=0; index_is_set == 0 && j<list_of_indexed_names->n; j++ ) {
      if ( strcmp( li->name[i].keyName, list_of_indexed_names->name[j].keyName ) == 0 ) {
	if ( strlen( list_of_indexed_names->name[j].firstName ) > strlen( li->name[i].firstName ) )
	  (void)memcpy( li->name[i].firstName, 
			list_of_indexed_names->name[j].firstName,  SHORT_STR_LENGTH );
	li->name[i].status = list_of_indexed_names->name[j].status;
	index_is_set = 1;
      }
    }
    if ( index_is_set == 0 ) {
      if ( li->name[i].status == INDIFFERENT ) {
	li->name[i].status = index_status;
      }
    }
    else {
      /* the representant gets the index status (forward propagation)
       */
      if ( strcmp( li->name[i].equivKeyName, li->name[i].keyName ) != 0 ) {
	for ( key_is_found=0, j=i+1; j<li->n && key_is_found==0; j++ ) {
	  if ( strcmp( li->name[i].equivKeyName, li->name[j].keyName ) == 0 ) {
	    key_is_found = 1;
	    li->name[j].status = li->name[i].status;
	  }
	}
      }
    }
  }


  
  /* the representant gives the index status (backward propagation)
   */
  for (i=0; i<li->n-1; i++ ) {
    if ( strcmp( li->name[i].equivKeyName, li->name[i].keyName ) != 0 ) {
      for ( key_is_found=0, j=i+1; j<li->n && key_is_found==0; j++ ) {
	if ( strcmp( li->name[i].equivKeyName, li->name[j].keyName ) == 0 ) {
	  key_is_found = 1;
	  li->name[i].status = li->name[j].status;
	}
      }
    }
  }





  /* second extraction
     partial equality of names
   */

  for (i=0; i<li->n-1; i++ ) {
    j = i;
    while ( j<li->n && compare_key_names( li->name[j].keyName, li->name[j+1].keyName ) == 0 ) {
      j++;
    }
    if ( j >= li->n ) j = li->n - 1;
    if ( j > i ) {
      if ( strcmp( li->name[j].equivKeyName, li->name[j].keyName ) != 0 ) {
	for ( key_is_found=0, k=j+1; k<li->n && key_is_found==0; k++ ) {
	  if ( strcmp( li->name[j].equivKeyName, li->name[k].keyName ) == 0 ) {
	    key_is_found = 1;
	    l = k;
	  }
	}
	if ( key_is_found == 0 ) {
	  l = j;
	  if ( _verbose_ ) {
	    fprintf( stderr, "%s: STRANGE, found no equivalence for\n", proc );
	    print_name( stderr, &(li->name[j]) );
	  }
	}
      }
      else {
	l = j;
      }
      for ( k=i; k<j; k++ ) {
	(void)memcpy( li->name[k].equivKeyName, li->name[l].keyName, SHORT_STR_LENGTH );
	li->name[l].nreferences += li->name[k].nreferences;
	li->name[k].nreferences = 0;
	li->name[k].status = li->name[l].status;
      }
      i = j;
    }
  }

  /* count names
   */
  j = 0;
  for (i=0; i<li->n; i++ ) {
    if ( li->name[i].nreferences > 0 ) j ++;
  }
  
  return( j );
}










/************************************************************
 *
 * END OF NAMES MANAGEMENT
 *
 * STRINGS MANAGEMENT
 * these are strings in the sense of bibtex (@string{})
 *
 ************************************************************/





void init_string( typeString *s )
{
  s->length = 0;
  (void)memset( s->key, 0, SHORT_STR_LENGTH );
  (void)memset( s->str, 0, STD_STR_LENGTH );
}


void copy_string( typeString *dest, const typeString *src )
{
  dest->length = src->length;
  (void)memcpy( dest->key, src->key, SHORT_STR_LENGTH );
  (void)memcpy( dest->str, src->str, STD_STR_LENGTH );
}


void print_string( typeString *s )
{
  printf( "KEY=%-10s (length=%3d) STR='%s'\n", s->key, s->length, s->str );
}


void init_list_of_strings( typeListOfStrings *ls )
{
  ls->n = ls->n_alloc = 0;
  ls->string = NULL;
}


void release_list_of_strings( typeListOfStrings *ls )
{
  if ( ls->string != NULL ) free( ls->string );
  init_list_of_strings( ls );
}


void print_list_of_strings( typeListOfStrings *ls )
{
  int i;
  printf( "#STRINGS=%d\n", ls->n );
  for (i=0; i<ls->n; i++ ) {
    printf( " #%3d: ", i+1 );
    print_string( &(ls->string[i]) );
  }
}


int add_string_to_list_of_strings( typeListOfStrings *ls, typeString *s )
{
  char *proc = "add_string_to_list_of_strings";

  int nalloc;
  typeString *newstr = NULL;
  int i;
  
  s->length = strlen( s->key );
  

  if ( ls->n == ls->n_alloc ) {
    /* allocation
     */
    nalloc = ls->n_alloc + PACK_OF_STRINGS;
    newstr = (typeString *)malloc( nalloc * sizeof(typeString) );
    if ( newstr == NULL ) {
      if ( _verbose_ ) 
	fprintf( stderr, "%s: unable to allocate list of strings\n", proc );
      return( 0 );
    }
    /* init
     */
    for ( i=ls->n_alloc; i<ls->n_alloc + PACK_OF_STRINGS; i++ )
      init_string( &(newstr[i]) );
    /* copy 
     */
    if ( ls->n_alloc > 0 ) {
      (void)memcpy( newstr, ls->string, ls->n_alloc * sizeof(typeString) );
      free( ls->string );
    }
    ls->string = newstr;
    ls->n_alloc = nalloc;
  }
  copy_string( &(ls->string[ls->n]), s );
  ls->n ++;
  return( 1 );
}










/************************************************************
 *
 * END OF STRINGS MANAGEMENT
 *
 * BIBLIOGRAPHIC ITEM MANAGEMENT
 *
 ************************************************************/




void init_item( typeItem *item )
{
  int i;
  
  item->bibtex_file = NULL;

  item->bibtex_key = NULL;
  item->type_item = UNKNOWN_TYPE_ITEM;
  item->type_cat  = CAT_UNKNOWN;

  item->nb_authors = 0;
  item->authors_names = NULL;

  item->nb_editors = 0;
  item->editors_names = NULL;

  item->nb_keywords = 0;  
  item->keywords = NULL;
  
  item->year = NO_YEAR;

  for ( i=0; strncmp( desc_field[i], "XXX", 3 ) != 0; i++ )
    item->field[i] = NULL;
  item->bibtex_entry = NULL;
}


void init_all_item( typeItem *item )
{
  int i;
  
  if ( item->bibtex_file != NULL )
    (void)memset( item->bibtex_file, 0, STD_STR_LENGTH );

  if ( item->bibtex_key != NULL )
    (void)memset( item->bibtex_key, 0, SHORT_STR_LENGTH );
  item->type_item = UNKNOWN_TYPE_ITEM;
  item->type_cat  = CAT_UNKNOWN;

  item->nb_authors = 0;
  if ( item->authors_names != NULL )
    (void)memset( item->authors_names, 0, MAX_NAMES * sizeof(typeName) );
  for ( i=0; i<MAX_NAMES; i++ )
    item->authors_names[i].status = default_index_status;

  item->nb_editors = 0;
  if ( item->editors_names != NULL )
    (void)memset( item->editors_names, 0, MAX_NAMES * sizeof(typeName) );
  for ( i=0; i<MAX_NAMES; i++ )
    item->editors_names[i].status = default_index_status;

  item->nb_keywords = 0;
  if ( item->keywords != NULL )
    (void)memset( item->keywords, 0, MAX_NAMES * sizeof(typeName) );
  for ( i=0; i<MAX_NAMES; i++ )
    item->keywords[i].status = default_index_status;
  

  item->year = NO_YEAR;

  for ( i=0; strncmp( desc_field[i], "XXX", 3 ) != 0; i++ )
    if ( desc_field_lgth[i] > 0 && item->field[i] != NULL ) {
      (void)memset( item->field[i], 0, desc_field_lgth[i] );
    }
  if( item->bibtex_entry != NULL )
    (void)memset( item->bibtex_entry, 0, LONG_STR_LENGTH );
  
}


void release_item( typeItem *item )
{
  int i;

  if ( item->bibtex_file != NULL )   free( item->bibtex_file );
  item->bibtex_file = NULL;

  if ( item->bibtex_key != NULL )   free( item->bibtex_key );
  item->bibtex_key = NULL;

  item->type_item = UNKNOWN_TYPE_ITEM;
  item->type_cat = CAT_UNKNOWN;
  
  item->nb_authors = 0;
  if ( item->authors_names != NULL )   free( item->authors_names );
  item->authors_names = NULL;
  
  item->nb_editors = 0;
  if ( item->editors_names != NULL )   free( item->editors_names );
  item->editors_names = NULL;
  
  item->nb_keywords = 0;
  if ( item->keywords != NULL )   free( item->keywords );
  item->keywords = NULL;
  
  item->year = NO_YEAR;

  for ( i=0; strncmp( desc_field[i], "XXX", 3 ) != 0; i++ ) {
    if ( item->field[i] != NULL )    free( item->field[i] );
    item->field[i] = NULL;
  }
  
  if ( item->bibtex_entry != NULL )   free( item->bibtex_entry );
  item->bibtex_entry = NULL;
  
  init_item( item );
}










int copy_keywords_in_item( typeItem *dest, const typeItem  *src )
{
  char *proc = "copy_keywords_in_item";
  int old, new;
  typeName *keywords;

  if ( src->nb_keywords <= 0 ) return( 1 );

  old = dest->nb_keywords;
  if ( old < 0 ) old = 0;
  new = src->nb_keywords;
  
  keywords = (typeName *)malloc( (new+old) * sizeof(typeName) );
  if ( keywords == NULL ) {
    if ( _verbose_ )
      fprintf( stderr, "%s: keywords allocation failed\n", proc );
    return( 0 );
  }
  if ( old > 0 && dest->keywords != NULL ) {
    (void)memcpy( keywords, dest->keywords, old*sizeof(typeName) );
    free( dest->keywords );
  }
  dest->keywords = keywords;
  (void)memcpy( &(keywords[old]), src->keywords, new*sizeof(typeName) );
  dest->nb_keywords = old + new;
  return( 1 );
}










int copy_editors_in_item( typeItem *dest, const typeItem  *src )
{
  char *proc = "copy_editors_in_item";

  if ( dest->nb_editors > 0 && dest->editors_names != NULL ) 
    free( dest->editors_names );
  dest->nb_editors = 0;

  if ( src->nb_editors > 0 ) {

    dest->editors_names = malloc( src->nb_editors * sizeof(typeName) );
    if ( dest->editors_names == NULL ) {
      if ( _verbose_ ) 
	fprintf( stderr, "%s: editors' name allocation failed\n", proc );
      return( 0 );
    }
    (void)memcpy( dest->editors_names, src->editors_names, src->nb_editors * sizeof(typeName) );
    dest->nb_editors = src->nb_editors;
  }
  return( 1 );
}










int copy_item( typeItem *dest, const typeItem  *src )
{
  char *proc = "copy_item";
  int i;

  init_item( dest );

  if ( alloc_and_copy_str( &(dest->bibtex_file), src->bibtex_file ) != 1 ) {
    if ( _verbose_ ) 
      fprintf( stderr, "%s: bibtex file allocation failed\n", proc );
    release_item( dest );
    return( 0 );
  }
  
  if ( alloc_and_copy_str( &(dest->bibtex_key), src->bibtex_key ) != 1 ) {
    if ( _verbose_ ) 
      fprintf( stderr, "%s: bibtex key allocation failed\n", proc );
    release_item( dest );
    return( 0 );
  }
  
  dest->type_item =   src->type_item;
  dest->type_cat  =   src->type_cat;

  dest->nb_authors = src->nb_authors;
  if ( src->nb_authors > 0 ) {
    dest->authors_names = malloc( src->nb_authors * sizeof(typeName) );
    if ( dest->authors_names == NULL ) {
      if ( _verbose_ ) 
	fprintf( stderr, "%s: authors' name allocation failed\n", proc );
      release_item( dest );
      return( 0 );
    }
    (void)memcpy( dest->authors_names, src->authors_names, src->nb_authors * sizeof(typeName) );
  }
  
  if ( copy_editors_in_item( dest, src ) != 1 ) {
    if ( _verbose_ ) 
      fprintf( stderr, "%s: unable to copy editors field\n", proc );
    release_item( dest );
    return( 0 );
  }
  
  dest->nb_keywords = src->nb_keywords;
  if ( src->nb_keywords > 0 ) {
    dest->keywords = malloc( src->nb_keywords * sizeof(typeName) );
    if ( dest->keywords == NULL ) {
      if ( _verbose_ ) 
	fprintf( stderr, "%s: keywords allocation failed\n", proc );
      release_item( dest );
      return( 0 );
    }
    (void)memcpy( dest->keywords, src->keywords, src->nb_keywords * sizeof(typeName) );
  }
  
  dest->year = src->year;

  for ( i=0; strncmp( desc_field[i], "XXX", 3 ) != 0; i++ ) {
    if ( src->field[i] == NULL || src->field[i][0] == '\0' )
      continue;
    if ( alloc_and_copy_str( &(dest->field[i]), src->field[i] ) != 1 ) {
      if ( _verbose_ ) 
	fprintf( stderr, "%s: item field #%d allocation failed\n", proc, i );
      release_item( dest );
      return( 0 );
    }
  }
  
  if ( alloc_and_copy_str( &(dest->bibtex_entry), src->bibtex_entry ) != 1 ) {
    if ( _verbose_ ) 
      fprintf( stderr, "%s: bibtex entry allocation failed\n", proc );
    release_item( dest );
    return( 0 );
  }
  
  return( 1 );
}










void print_item( FILE *file, typeItem *item )
{
  int i;
  FILE *f = file;

  if ( f == NULL ) f = stdout;

  fprintf( f, "\n" );
  if ( item->bibtex_file != NULL && item->bibtex_file[0] != '\0' )
    fprintf( f, " BIBTEX-FILE=[%s]\n", item->bibtex_file );

  if ( item->bibtex_key != NULL && item->bibtex_key[0] != '\0' )
    fprintf( f, " BIBTEX-KEY=[%s]\n", item->bibtex_key );

  fprintf( f, " ITEM-TYPE=" );
  fprintf( f, "[%s]\n", desc_item[ item->type_item ] );
  
  fprintf( f, " #AUTHORS=%d\n", item->nb_authors );
  for ( i=0; i<item->nb_authors; i++ ) {
    fprintf( f, "  #%3d: ", i+1 );
    print_name( f, &(item->authors_names[i]) );
  }

  fprintf( f, " #EDITORS=%d\n", item->nb_editors );
  for ( i=0; i<item->nb_editors; i++ ) {
    fprintf( f, "  #%3d: ", i+1 );
    print_name( f, &(item->editors_names[i]) );
  }
  
  fprintf( f, " YEAR=%d\n", item->year );

  for ( i=0; strncmp( desc_field[i], "XXX", 3 ) != 0; i++ ) {
    if ( item->field[i] != NULL && item->field[i][0] != '\0' )
      fprintf( f, " %s=[%s]\n", desc_field[i], item->field[i] );
  }

  if ( item->bibtex_entry != NULL && item->bibtex_entry[0] != '\0' )
    fprintf( f, " BIBTEX-ENTRY=[%s]\n", item->bibtex_entry );

  fprintf( f, " #KEYWORDS=%d\n", item->nb_keywords );
  for ( i=0; i<item->nb_keywords; i++ ) {
    fprintf( f, "  #%3d: ", i+1 );
    print_name( f, &(item->keywords[i]) );
  }


  fprintf( f, "\n" );
}










void init_list_of_items( typeListOfItems *li )
{
  int i;
  li->n = li->n_alloc = 0;
  li->item = NULL;
  for ( i=0; i<SORT_CRITERIA_NB; i++ )
    li->sort[i] = _NONE_;
}


void release_list_of_items( typeListOfItems *li )
{
  int i;
  if ( li->item != NULL ) {
    for (i=0; i<li->n; i++ ) {
      release_item( li->item[i] );
      free( li->item[i] );
    }
    free( li->item );
  }
  init_list_of_items( li );
}


void print_list_of_items( typeListOfItems *li )
{
  int i;
  printf( "#ITEMS=%d\n", li->n );
  for (i=0; i<li->n; i++ ) {
    printf( " #%3d: ", i+1 );
    print_item( stdout, li->item[i] );
  }
}


int alloc_all_item( typeItem *item )
{
  int i;

  item->bibtex_file = malloc( STD_STR_LENGTH * sizeof(char) );
  if ( item->bibtex_file == NULL ) {
    release_item( item );
    return( 0 );
  }

  item->bibtex_key = malloc( SHORT_STR_LENGTH * sizeof(char) );
  if ( item->bibtex_key == NULL ) {
    release_item( item );
    return( 0 );
  }
  item->type_item = UNKNOWN_TYPE_ITEM;
  item->type_cat  = CAT_UNKNOWN;

  item->nb_authors = 0;
  item->authors_names = malloc( MAX_NAMES * sizeof(typeName) );
  if ( item->authors_names == NULL ) {
    release_item( item );
    return( 0 );
  }

  item->nb_editors = 0;
  item->editors_names = malloc ( MAX_NAMES * sizeof(typeName) );
  if ( item->editors_names == NULL ) {
    release_item( item );
    return( 0 );
  }
  
  item->nb_keywords = 0;
  item->keywords = malloc ( MAX_NAMES * sizeof(typeName) );
  if ( item->keywords == NULL ) {
    release_item( item );
    return( 0 );
  }
  
  item->year = NO_YEAR;

  for ( i=0; strncmp( desc_field[i], "XXX", 3 ) != 0; i++ ) {
    if ( desc_field_lgth[i] > 0 ) {
      item->field[i] = malloc ( desc_field_lgth[i] * sizeof(char) );
      if ( item->field[i] == NULL ) {
	release_item( item );
	return( 0 );
      }
    } else {
      item->field[i] = NULL;
    }
  }

  item->bibtex_entry = malloc( LONG_STR_LENGTH * sizeof(char) );
  if ( item->bibtex_entry == NULL ) {
    release_item( item );
    return( 0 );
  }
  
  init_all_item( item );
  return( 1 );
}










int add_item_to_list_of_items( typeListOfItems *li, typeItem *myItem )
{
  char *proc = "add_item_to_list_of_items";
  int nalloc;
  typeItem **newlist = NULL;
  typeItem  *newitem = NULL;
  int i;

  if ( li->n == li->n_alloc ) {
    /* allocation
     */
    nalloc = li->n_alloc + PACK_OF_ITEMS;
    newlist = (typeItem **)malloc( nalloc * sizeof(typeItem*) );
    if ( newlist == NULL ) {
      if ( _verbose_ ) 
	fprintf( stderr, "%s: unable to allocate list of item\n", proc );
      return( 0 );
    }
    /* init and copy
     */
    for ( i=0; i<li->n_alloc; i++ )
      newlist[i] = li->item[i];
    for ( i=li->n_alloc; i<li->n_alloc + PACK_OF_ITEMS; i++ )
      newlist[i] = NULL;
    
    if ( li->item != NULL ) free ( li->item );
    li->item = newlist;
    li->n_alloc = nalloc;
  }

  newitem = (typeItem *)malloc( sizeof(typeItem) );
  if ( newitem == NULL ) {
    if ( _verbose_ ) 
      fprintf( stderr, "%s: unable to allocate item\n", proc );
    return( 0 );
  }
  
  li->item[li->n] = newitem;
  init_item( li->item[li->n] );

  if ( copy_item( li->item[ li->n ], myItem ) != 1 ) {
    if ( _verbose_ ) 
      fprintf( stderr, "%s: unable to copy item\n", proc );
    return( 0 );
  }
  li->n ++;
  
  return( 1 );
}


/* used in medline reading
 */
void add_name_to_authors( typeName *name, typeItem *item ) 
{
  int i;
  int add = 1;

  for ( i=0; i<item->nb_authors && add == 1; i++ ) {
    if ( strlen( item->authors_names[i].keyName ) == strlen( name->keyName ) && 
	 strncmp( item->authors_names[i].keyName, name->keyName, strlen( name->keyName ) ) == 0 ) {
      add = 0;
    }
  }

  if ( add == 1 && item->nb_authors < MAX_NAMES ) {
    name->status = item->authors_names[item->nb_authors].status;
    copy_name( &(item->authors_names[item->nb_authors]), name );
    item->nb_authors ++;
  }
}






/************************************************************
 *
 * END OF BIBLIOGRAPHIC ITEM MANAGEMENT
 *
 * BIBLIOGRAPHY MANAGEMENT
 *
 ************************************************************/





/* called from ReadBibtexInputs()
               ReadMedlineInputs()
*/
int add_bibliographic_item_to_list( typeListOfItems *li, typeItem *myItem,
				    char *filename ) 
{
  int trs_is_done, k;
  
  if ( filename != NULL && filename[0] != '\0' ) {
    (void)strncpy( myItem->bibtex_file, filename, STD_STR_LENGTH );
  }

  /* replace month by its value
     if necessary
  */
  if ( strlen( myItem->field[MONTH] ) == 3 ) {
    for ( trs_is_done=0, k=0; trs_is_done==0 && k<12; k++ ) {
      if ( strncasecmp( myItem->field[MONTH], month_names_abbr[k], 3 ) == 0 ) {

	(void)memcpy( myItem->field[MONTH], month_names_eng[k], strlen( month_names_eng[k] ) );

	trs_is_done = 1;
      }
    }
  }
  
  if ( add_item_to_list_of_items( li, myItem ) != 1 ) {
    return( 0 );
  }

  return( 1 );
}


int count_names( typeListOfItems *li )
{
  int i;
  int n=0;

  for (i=0; i<li->n; i++ ) {
    if ( li->item[i]->nb_authors > 0 ) n += li->item[i]->nb_authors;
    switch ( li->item[i]->type_item ) {
    case BOOK :
    case INBOOK :
      if ( li->item[i]->nb_authors > 0 ) break;
    case PROCEEDINGS :
      if ( li->item[i]->nb_editors > 0 ) n += li->item[i]->nb_editors;
      break;
    default :
      break;
    }
  }
  return( n );
}


int retrieve_all_names( typeListOfNames *ln, typeListOfItems *li )
{
  char *proc = "retrieve_all_names";

  int i, j;
  
  ln->PACK_OF_NAMES = count_names( li );

  for (i=0; i<li->n; i++ ) {
    if ( li->item[i]->nb_authors > 0 ) {
      for ( j=0; j<li->item[i]->nb_authors; j++ ) {
	(void)memcpy( li->item[i]->authors_names[j].equivKeyName, 
		      li->item[i]->authors_names[j].keyName,  SHORT_STR_LENGTH );
	li->item[i]->authors_names[j].nreferences = 1;
	if ( add_name_to_list_of_names( ln, &(li->item[i]->authors_names[j]) ) != 1 ) {
	  if ( _verbose_ )
	    fprintf( stderr, "%s: unable to add author name to list\n", proc );
	  release_list_of_names( ln );
	  return( 0 );
	}
      }
    }

    switch ( li->item[i]->type_item ) {
    case BOOK :
    case INBOOK :
      if ( li->item[i]->nb_authors > 0 ) break;
    case PROCEEDINGS :
      if ( li->item[i]->nb_editors > 0 ) {
	for ( j=0; j<li->item[i]->nb_editors; j++ ) {
	(void)memcpy( li->item[i]->editors_names[j].equivKeyName, 
		      li->item[i]->editors_names[j].keyName,  SHORT_STR_LENGTH );
	li->item[i]->editors_names[j].nreferences = 1;
	  if ( add_name_to_list_of_names( ln, &(li->item[i]->editors_names[j]) ) != 1 ) {
	    if ( _verbose_ )
	      fprintf( stderr, "%s: unable to add editor name to list\n", proc );
	    release_list_of_names( ln );
	    return( 0 );
	  }
	}
      }
      break;
    default :
      break;
    }
    
  }
  return( ln->n );
}


int count_keywords( typeListOfItems *li )
{
  int i;
  int n=0;

  for (i=0; i<li->n; i++ ) {
    n += li->item[i]->nb_keywords;
  }
  return( n );
}


int retrieve_all_keywords( typeListOfNames *ln, typeListOfItems *li )
{
  char *proc = "retrieve_all_keywords";

  int i, j;
  
  ln->PACK_OF_NAMES = count_keywords( li );

  for (i=0; i<li->n; i++ ) {

    if ( li->item[i]->nb_keywords > 0 ) {
      for ( j=0; j<li->item[i]->nb_keywords; j++ ) {
	(void)memcpy( li->item[i]->keywords[j].equivKeyName, 
		      li->item[i]->keywords[j].keyName,  SHORT_STR_LENGTH );
	li->item[i]->keywords[j].nreferences = 1;
	if ( add_name_to_list_of_names( ln, &(li->item[i]->keywords[j]) ) != 1 ) {
	  if ( _verbose_ )
	    fprintf( stderr, "%s: unable to add keyword to list\n", proc );
	  release_list_of_names( ln );
	  return( 0 );
	}
      }
    }

  }
  return( ln->n );
}

int build_bibtex_key( typeItem *item )
{
  char *key = item->bibtex_key;
  int i;
  char *tmp;
  enumTypeField typeField;

  if ( key == NULL ) return( 0 );

  /* copy first author's lastname
     translate in lowercase and 7bits
   */
  tmp = NULL;
  for ( i=0; i<item->nb_authors && tmp == NULL; i++ ) {
    tmp = item->authors_names[i].lastName;
    if ( tmp != NULL && *tmp == '\0' ) tmp = NULL;
  }
  if ( tmp != NULL ) {
    key = strcpy_in_lower_case( key, tmp );
    *key++ = ':';
  }

  typeField = UNKNOWN_TYPE_FIELD;

  switch( item->type_item ) {
  default :
    break;
  case ARTICLE :
    typeField = JOURNAL;
    break;
  case INPROCEEDINGS :
    typeField = BOOKTITLE;
    break;
  case BOOK :
    typeField = TITLE;
    break;
  }

  if ( typeField != UNKNOWN_TYPE_FIELD ) {
    
    if ( count_words( item->field[typeField] ) == 1 ) {
      key = strcpy_in_lower_case( key, item->field[typeField] );
      *key++ = ':';
    }
    else if ( count_words( item->field[typeField] ) > 1 ) {
      key = strcpy_first_letters_in_lower_case( key, item->field[typeField] );
      *key++ = ':';
    }

  }

  if ( item->year != NO_YEAR ) {
    sprintf( key, "%d", item->year );
  }

  return( 1 );
}



void update_bibtex_item_with_crossrefs( typeListOfItems *li, typeItem *item, 
				       int *nc, int update )
{
  char *proc = "update_bibtex_item_with_crossrefs";
  int max_updates = 10;
  int j, k, l;
  typeItem *tmp;
  enumTypeField field;
  
  if ( item->field[CROSSREF] == NULL || item->field[CROSSREF] == '\0' )
    return;
  if ( update > max_updates ) {
    if ( _warning_ ) {
      fprintf( stderr, "WARNING: recursive calls to '%s' exceeds %d\n",
	       proc, max_updates );
    }
    return;
  }

  /* look for the entry corresponding to the cross-reference
   */
  l = strlen( item->field[CROSSREF] );
  for ( j=-1, k=0; j == -1 && k<li->n; k++ ) {
    if ( li->item[k]->bibtex_key[0] != item->field[CROSSREF][0] )
      continue;
    if ( (int)strlen( li->item[k]->bibtex_key ) != l ) continue;
    if ( strcmp( item->field[CROSSREF], li->item[k]->bibtex_key ) == 0 )
      j = k;
  }
  if ( j == -1 ) {
    for ( k=0; j == -1 && k<li->n; k++ ) {
      if ( li->item[k]->field[CITEKEY] == NULL || li->item[k]->field[CITEKEY][0] ==  '\0' )
	continue;
      if ( li->item[k]->field[CITEKEY][0] != item->field[CROSSREF][0] )
	continue;
      if ( (int)strlen( li->item[k]->field[CITEKEY] ) != l ) continue;
      if ( strcmp( item->field[CROSSREF], li->item[k]->field[CITEKEY] ) == 0 )
	j = k;
    }
  }
   
  if ( j == -1 ) {
    if ( _warning_ ) {
      fprintf( stderr, "WARNING: do not find cross-reference\n\t '%s' for entry '%s'\n",
	       item->field[CROSSREF], item->bibtex_key );
    }
    return;
  }

  /* does this entry has a cross-reference ?
   */
  if ( li->item[j]->field[CROSSREF] != NULL && li->item[j]->field[CROSSREF] != '\0' )
    update_bibtex_item_with_crossrefs( li, li->item[j], nc, update+1 );
  
  /* put cross-references at the beginning of the list
     for more efficient future searches
   */
  if ( item != li->item[*nc] && j > *nc ) {
    tmp = li->item[*nc];
    li->item[*nc] = li->item[j];
    li->item[j]   = tmp;
    j = *nc;
    (*nc) ++;
  }
  
  switch( item->type_item ) {
  default :
    if ( _warning_ ) {
      fprintf( stderr, "%s:\n\t such article type (%s) not handled in switch\n",
	       proc, desc_item[ item->type_item ] );
    }
    break;
  case INBOOK :
    if ( li->item[j]->type_item != BOOK ) {
      if ( _warning_ ) {
	fprintf( stderr, "%s: cross-reference of entry '%s' is of type '%s'\n",
		 proc, item->bibtex_key, desc_item[ li->item[j]->type_item ] );
	fprintf( stderr, "\t type 'BOOK' was expected\n" );
      }
      break;
    }
    
    for ( field=0; strncmp( desc_field[field], "XXX", 3 ) != 0; field++ ) {
      
      switch( field ) {
      default :
	if ( li->item[j]->field[field] == NULL || li->item[j]->field[field][0] == '\0' )
	  break;
	if ( item->field[field] != NULL && item->field[field][0] != '\0' )
	  break;
	if ( alloc_and_copy_str( &(item->field[field]), li->item[j]->field[field] ) != 1 ) {
	  if ( _verbose_ ) 
	    fprintf( stderr, "%s: item field #%d allocation failed for '%s'\n", 
		     proc, field, item->bibtex_key );
	}
	break;

      case AUTHOR :
	break;
      case BOOKTITLE :
	/* it may not be used, one should use TITLE instead 
	   then verify that TITLE is not filled
	 */
	if ( li->item[j]->field[field] == NULL || li->item[j]->field[field][0] == '\0' )
	  break;
	if ( li->item[j]->field[TITLE] != NULL && li->item[j]->field[TITLE][0] != '\0' )
	  break;
	if ( item->field[field] != NULL && item->field[field][0] != '\0' )
	  break;
	if ( alloc_and_copy_str( &(item->field[field]), li->item[j]->field[field] ) != 1 ) {
	  if ( _verbose_ ) 
	    fprintf( stderr, "%s: item field #%d allocation failed for '%s'\n", 
		     proc, field, item->bibtex_key );
	}
	break;

      case EDITOR :
	if ( item->nb_editors > 0 || li->item[j]->nb_editors <= 0 )
	  break;
	if ( li->item[j]->nb_editors > 0 && item->nb_authors > 0 ) {
	  if ( _warning_ ) {
	    fprintf( stderr, "%s: WARNING\n", proc );
	    fprintf( stderr, "\t with cross-reference '%s', the entry\n",
		     li->item[j]->bibtex_key );
	    fprintf( stderr, "\t '%s' will have both authors *and* editors,\n",
		     item->bibtex_key );
	    fprintf( stderr, "\t which is *not* expected for type '%s'. Please\n",
		     desc_item[ item->type_item ] );
	    fprintf( stderr, "\t check whether INCOLLECTION will be more appropriate\n" );
	  }
	  break;
	}
	if ( copy_editors_in_item( item, li->item[j] ) != 1 ) {
	  if ( _verbose_ ) 
	    fprintf( stderr, "%s: unable to copy editors field for '%s'\n", 
		     proc, item->bibtex_key );
	}
  	break;
      case KEYWORDS :
      case KEYWORD :
	if ( li->item[j]->nb_keywords <= 0 ) break;
	if ( copy_keywords_in_item( item, li->item[j] ) != 1 ) {
	  if ( _verbose_ ) 
	    fprintf( stderr, "%s: unable to copy keywords for '%s'\n", 
		     proc, item->bibtex_key );
	}
	break;
      case TITLE :
	if ( li->item[j]->field[field] == NULL || li->item[j]->field[field][0] == '\0' )
	  break;
	if ( item->field[BOOKTITLE] != NULL && item->field[BOOKTITLE][0] != '\0' )
	  break;
	if ( alloc_and_copy_str( &(item->field[BOOKTITLE]), 
				 li->item[j]->field[field] ) != 1 ) {
	  if ( _verbose_ ) 
	    fprintf( stderr, "%s: item field #%d allocation failed for '%s'\n", 
		     proc, field, item->bibtex_key );
	}
	break;
      case YEAR :
	if ( li->item[j]->year != NO_YEAR && item->year == NO_YEAR )
	  item->year = li->item[j]->year;
	break;
      }
    }
    /* end of case INCOLLECTION : */
    break;
  case INCOLLECTION :
    if ( li->item[j]->type_item != BOOK ) {
      if ( _warning_ ) {
	fprintf( stderr, "%s: cross-reference of the entry\n", proc );
	fprintf( stderr, "\t '%s' is of type '%s', while type\n",
		 item->bibtex_key, desc_item[ li->item[j]->type_item ] );
	fprintf( stderr, "\t 'BOOK' was expected\n" );
      }
      break;
    }
    
    for ( field=0; strncmp( desc_field[field], "XXX", 3 ) != 0; field++ ) {
      
      switch( field ) {
      default :
	if ( li->item[j]->field[field] == NULL || li->item[j]->field[field][0] == '\0' )
	  break;
	if ( item->field[field] != NULL && item->field[field][0] != '\0' )
	  break;
	if ( alloc_and_copy_str( &(item->field[field]), li->item[j]->field[field] ) != 1 ) {
	  if ( _verbose_ ) 
	    fprintf( stderr, "%s: item field #%d allocation failed for '%s'\n", 
		     proc, field, item->bibtex_key );
	}
	break;

      case AUTHOR :
	break;
      case BOOKTITLE :
	/* it may not be used, one should use TITLE instead 
	   then verify that TITLE is not filled
	 */
	if ( li->item[j]->field[field] == NULL || li->item[j]->field[field][0] == '\0' )
	  break;
	if ( li->item[j]->field[TITLE] != NULL && li->item[j]->field[TITLE][0] != '\0' )
	  break;
	if ( item->field[field] != NULL && item->field[field][0] != '\0' )
	  break;
	if ( alloc_and_copy_str( &(item->field[field]), li->item[j]->field[field] ) != 1 ) {
	  if ( _verbose_ ) 
	    fprintf( stderr, "%s: item field #%d allocation failed for '%s'\n", 
		     proc, field, item->bibtex_key );
	}
	break;

      case EDITOR :
	if ( item->nb_editors > 0 || li->item[j]->nb_editors <= 0 )
	  break;
	if ( copy_editors_in_item( item, li->item[j] ) != 1 ) {
	  if ( _verbose_ ) 
	    fprintf( stderr, "%s: unable to copy editors field for '%s'\n", 
		     proc, item->bibtex_key );
	}
  	break;
      case KEYWORDS :
      case KEYWORD :
	if ( li->item[j]->nb_keywords <= 0 ) break;
	if ( copy_keywords_in_item( item, li->item[j] ) != 1 ) {
	  if ( _verbose_ ) 
	    fprintf( stderr, "%s: unable to copy keywords for '%s'\n", 
		     proc, item->bibtex_key );
	}
	break;
      case TITLE :
	if ( li->item[j]->field[field] == NULL || li->item[j]->field[field][0] == '\0' )
	  break;
	if ( item->field[BOOKTITLE] != NULL && item->field[BOOKTITLE][0] != '\0' )
	  break;
	if ( alloc_and_copy_str( &(item->field[BOOKTITLE]), 
				 li->item[j]->field[field] ) != 1 ) {
	  if ( _verbose_ ) 
	    fprintf( stderr, "%s: item field #%d allocation failed for '%s'\n", 
		     proc, field, item->bibtex_key );
	}
	break;
      case YEAR :
	if ( li->item[j]->year != NO_YEAR && item->year == NO_YEAR )
	  item->year = li->item[j]->year;
	break;
      }
    }
    /* end of case INCOLLECTION : */
    break;
  case INPROCEEDINGS :
    if ( li->item[j]->type_item != PROCEEDINGS ) {
      if ( _warning_ ) {
	fprintf( stderr, "%s: cross-reference of entry '%s' is of type '%s'\n",
		 proc, item->bibtex_key, desc_item[ li->item[j]->type_item ] );
	fprintf( stderr, "\t type 'PROCEEDINGS' was expected\n" );
      }
      break;
    }
    
    for ( field=0; strncmp( desc_field[field], "XXX", 3 ) != 0; field++ ) {
      
      switch( field ) {
      default :
	if ( li->item[j]->field[field] == NULL || li->item[j]->field[field][0] == '\0' )
	  break;
	if ( item->field[field] != NULL && item->field[field][0] != '\0' )
	  break;
	if ( alloc_and_copy_str( &(item->field[field]), li->item[j]->field[field] ) != 1 ) {
	  if ( _verbose_ ) 
	    fprintf( stderr, "%s: item field #%d allocation failed for '%s'\n", 
		     proc, field, item->bibtex_key );
	}
	break;

      case AUTHOR :
	break;
      case BOOKTITLE :
	/* it may not be used, one should use TITLE instead 
	   then verify that TITLE is not filled
	 */
	if ( li->item[j]->field[field] == NULL || li->item[j]->field[field][0] == '\0' )
	  break;
	if ( li->item[j]->field[TITLE] != NULL && li->item[j]->field[TITLE][0] != '\0' )
	  break;
	if ( item->field[field] != NULL && item->field[field][0] != '\0' )
	  break;
	if ( alloc_and_copy_str( &(item->field[field]), li->item[j]->field[field] ) != 1 ) {
	  if ( _verbose_ ) 
	    fprintf( stderr, "%s: item field #%d allocation failed for '%s'\n", 
		     proc, field, item->bibtex_key );
	}
	break;

      case EDITOR :
	if ( item->nb_editors > 0 || li->item[j]->nb_editors <= 0 )
	  break;
	if ( copy_editors_in_item( item, li->item[j] ) != 1 ) {
	  if ( _verbose_ ) 
	    fprintf( stderr, "%s: unable to copy editors field for '%s'\n", 
		     proc, item->bibtex_key );
	}
  	break;
      case KEYWORDS :
      case KEYWORD :
	if ( li->item[j]->nb_keywords <= 0 ) break;
	if ( copy_keywords_in_item( item, li->item[j] ) != 1 ) {
	  if ( _verbose_ ) 
	    fprintf( stderr, "%s: unable to copy keywords for '%s'\n", 
		     proc, item->bibtex_key );
	}
	break;
      case TITLE :
	if ( li->item[j]->field[field] == NULL || li->item[j]->field[field][0] == '\0' )
	  break;
	if ( item->field[BOOKTITLE] != NULL && item->field[BOOKTITLE][0] != '\0' )
	  break;
	if ( alloc_and_copy_str( &(item->field[BOOKTITLE]), 
				 li->item[j]->field[field] ) != 1 ) {
	  if ( _verbose_ ) 
	    fprintf( stderr, "%s: item field #%d allocation failed for '%s'\n", 
		     proc, field, item->bibtex_key );
	}
	break;
      case YEAR :
	if ( li->item[j]->field[field] == NULL || li->item[j]->field[field][0] == '\0' )
	  break;
	if ( item->field[field] != NULL && item->field[field][0] != '\0' )
	  break;
	if ( alloc_and_copy_str( &(item->field[field]), li->item[j]->field[field] ) != 1 ) {
	  if ( _verbose_ ) 
	    fprintf( stderr, "%s: item field #%d allocation failed for '%s'\n", 
		     proc, field, item->bibtex_key );
	}
	if ( li->item[j]->year != NO_YEAR && item->year == NO_YEAR )
	  item->year = li->item[j]->year;
	break;
      }
    }
    /* end of case INPROCEEDINGS : */
    break;
  }
}




void update_bibtex_items_with_crossrefs( typeListOfItems *li )
{
  int c = 0;
  int i;

  for (i=0; i<li->n; i++ ) {
    if ( li->item[i]->field[CROSSREF] == NULL || li->item[i]->field[CROSSREF] == '\0' )
      continue;
    update_bibtex_item_with_crossrefs( li, li->item[i], &c, 0 );
  }
}










/************************************************************
 *
 * END OF BIBLIOGRAPHY MANAGEMENT
 *
 * BIBTEX READING
 *
 ************************************************************/





int is_line_a_bibtex_string( char *str ) 
{
  char *tmp = str;
  
  while ( *tmp != '\0' && (IS_SPACE_OR_RETURN( *tmp )) ) tmp ++;
  if ( *tmp == '@' )
    if ( strncasecmp( &(tmp[1]), "STRING", 6 ) == 0 ) return( 1 );
  return( 0 );
}


int get_whole_bibtex_string( char *item, char *str, FILE *f )
{
  char *proc = "get_whole_string";
  int open_brace = -1;
  char *out = item;
  char *tmp = str;
  int separator=0;

  /* beginning
     copy of the first line
   */
  while ( IS_SPACE( *tmp ) ) tmp ++;
  
  while ( *tmp != '\0' && (open_brace != 0) ) {
    if ( IS_NOT_SPACE_OR_RETURN( *tmp ) ) {
      if ( *tmp == '{' ) {
	if ( open_brace < 0 ) open_brace = 1;
	else                  open_brace ++;
      }
      if ( *tmp == '}' ) open_brace --;
      *out++ = *tmp++;
      separator = 0;
    } else {
      if ( separator == 0 ) *out++ = ' ';
      separator = 1;
      tmp ++;
    }
  }
  

  while ( open_brace > 0 ) {

    /* get new line
     */
    if (  *tmp == '\0' ) {
      if ( fgets( str, STD_STR_LENGTH, f ) == NULL ) {
	if ( _verbose_ ) {
	  fprintf( stderr, "%s: EOF when parsing entry\n", proc );
	  fprintf( stderr, "\t %s\n", str );
	}
	return( 0 );
      }
      tmp = str;
    }


    /* copy line
     */
    
    while ( *tmp != '\0' && open_brace > 0 ) {
      if ( IS_NOT_SPACE( *tmp ) ) {
	if ( *tmp == '{' ) open_brace ++;
	if ( *tmp == '}' ) open_brace --;
	*out++ = *tmp++;
	separator = 0;
      } else {
	if ( separator == 0 ) *out++ = ' ';
	separator = 1;
	tmp ++;
      }
    }
  }

  *out ++ = '\0';
  return( 1 );
}










/* fill string structure from char *
   
   a string has the form
   @string{key="blablabla"}
*/
int get_string_from_bibtex_string( typeString *s, char *str,
				   typeListOfStrings *ls )
{
  char *proc = "get_string_from_bibtex_string";
  char *tmp = str;
  char *aux;
  char *out;
  int  trs_is_done, k, l;

  if ( strncasecmp( tmp, "@STRING{", 8 ) != 0 ) {
    if ( _verbose_ ) 
      fprintf( stderr, "%s: do not recognize string in '%s'\n", proc, str );
    return ( 0 );
  }
  tmp += 8;

  /* copy key
     all chars except ' ' and '\t'
     until '=' is reached
   */
  out = s->key;
  while ( *tmp != '=' ) {
    if ( IS_SPACE( *tmp ) ) {
      tmp ++;
      continue;
    }
    *out++ = *tmp++;
  }

  /* skip '=' 
   */
  tmp++;

  /* get value of the string
   */
  out = s->str;
  while ( IS_SPACE( *tmp ) ) {
    tmp ++;
  }

  /* remove spaces at the end
   */
  aux = tmp;
  aux += strlen( tmp );
  aux --;
  while ( IS_SPACE( *aux ) ) {
    *aux -- = '\0';
  }
  if ( *aux == '}' ) *aux -- = '\0';
  while ( IS_SPACE( *aux ) ) {
    *aux -- = '\0';
  }
 

  /* several cases are to be handled
     - real string enclosed by "..."
     - string to be replaced
   */


  do {

    if ( tmp[0] == '"' ) {
      
      tmp ++;

      while ( *tmp != '\0' && !(*tmp == '"' && tmp[-1] != '\\') ) {
	*out++ = *tmp++;
      } 

      if ( *tmp == '"' ) tmp ++;

    }
    else {

      trs_is_done = 0;
      if ( ls != NULL ) {
	l = 0;
	for ( aux = tmp; *aux != '\0' && !(IS_SPACE( *aux )); aux++, l++ )
	  ;
	for ( k=0; trs_is_done==0 && k<ls->n; k++ ) {
	  if ( l != ls->string[k].length ) continue;
	  if ( strncmp( tmp, ls->string[k].key, l ) == 0 ) {
	    (void)memcpy( out, ls->string[k].str,
			  strlen( ls->string[k].str ) );
	    tmp += l;
	    out += strlen( ls->string[k].str );
	    trs_is_done = 1;
	  }
	}
      }
      if ( trs_is_done == 0 ) {
	do {
	  *out++ = *tmp++;
	} while ( *tmp != '\0' && !(IS_SPACE( *tmp )) );
      }
    }

    while ( *tmp != '\0' && (IS_SPACE( *tmp ))  ) tmp++;
    if ( *tmp == '#' ) tmp ++;
    while ( *tmp != '\0' && (IS_SPACE( *tmp )) ) tmp++;    

  } while ( *tmp != '\0' );

  *out = '\0';
  
  /* translate \" into "
   */
  out = s->str;
  tmp = s->str;
  while ( *tmp != '\0' ) {
    if ( *tmp == '\\' && tmp[1] == '"' ) {
      tmp ++;
      tmp ++;
      *out++ = '"';
    }
    else {
      if ( tmp != out ) *out++ = *tmp++;
      else { tmp++; out++; }
    }
  }
  *out = '\0';

  return( 1 );
}










int get_string_from_predefined_string( typeString *s, char *str )
{
  char *proc = "get_string_from_predefined_string";
  char *tmp = str;
  char *aux;
  char *out;

  if ( strncasecmp( tmp, "@STRING{", 8 ) != 0 ) {
    if ( _verbose_ ) 
      fprintf( stderr, "%s: do not recognize string in '%s'\n", proc, str );
    return ( 0 );
  }
  tmp += 8;

  /* copy key
     all chars except ' ' and '\t'
     until '=' is reached
   */
  out = s->key;
  while ( *tmp != '=' ) {
    if ( IS_SPACE( *tmp ) ) {
      tmp ++;
      continue;
    }
    *out++ = *tmp++;
  }

  /* skip '=' 
   */
  tmp++;

  /* get value of the string
   */
  out = s->str;
  while ( IS_SPACE( *tmp ) ) {
    tmp ++;
  }

  /* remove spaces at the end
   */
  aux = tmp;
  aux += strlen( tmp );
  aux --;
  while ( IS_SPACE( *aux ) ) {
    *aux -- = '\0';
  }
  if ( *aux == '}' ) *aux -- = '\0';
  while ( IS_SPACE( *aux ) ) {
    *aux -- = '\0';
  }
 

  /* two cases
     either it's "blablabla" or simply blablabla (without quotes)
  */
  if ( tmp[0] == '"' ) {
    tmp ++;
    if ( *aux == '"' ) {
      *aux-- = '\0';
    }
    else {
      if ( _verbose_ ) {
	fprintf( stderr, "WARNING: value of (predefined) key '%s' begins by '\"'", s->key );
	fprintf( stderr, "\t but do not ended with the same character\n" );
      }
    }
    (void)memcpy( out, tmp, strlen( tmp ) );
    out += strlen( tmp );
    tmp += strlen( tmp );
  }
  else {
    do {
      *out++ = *tmp++;
    } while ( *tmp != '\0' && !(IS_SPACE( *tmp )) );
  }

  *out = '\0';
  
  return( 1 );
}










/* un input est une ligne commencant
   par des ' ' et des '\t'
   puis un @ et un mot inconnu
*/
int is_line_perhaps_a_new_input( char *str ) 
{
  char *tmp = str;
  
  while ( *tmp != '\0' && (IS_SPACE_OR_RETURN( *tmp )) ) tmp ++;
  if ( *tmp != '@' ) return 0;

  /* skip the '@' */
  tmp++;

  while ( *tmp != '\0' && (IS_SPACE_OR_RETURN( *tmp )) ) tmp ++;
  if ( (*tmp < 'a' || *tmp > 'z') && (*tmp < 'A' || *tmp > 'Z') ) return 0;

  if ( strncasecmp( tmp, "comment", 7 ) == 0 ) return 0;

  if ( _debug_ ) {
    fprintf( stderr, "is_line_perhaps_a_new_input: now parsing '%s'\n", str );
  }

  return( 1 );
}










/* un input est une ligne commencant
   par des ' ' et des '\t'
   puis un @ et un mot reconnu
*/
int is_line_a_new_input( char *str ) 
{
  char *tmp = str;
  int i;
  
  while ( *tmp != '\0' && (IS_SPACE_OR_RETURN( *tmp )) ) tmp ++;
    
  if ( *tmp == '@' ) 
    for (i=0; strncmp( desc_item[i], "XXX", 3 ) != 0; i++ ) 
      if ( strncasecmp( &(tmp[1]), desc_item[i], strlen(desc_item[i]) ) == 0 ) 
	return( 1 );
  return( 0 );
}










int get_whole_bibtex_item( char *item, char *str, FILE *f )
{
  char *proc = "get_whole_bibtex_item";
  int open_brace = 0;
  char *out = item;
  char *tmp = str;
  int empty_line = 0;
  int separator=0;

  if ( str == NULL || *str == '\0' ) return( 0 );

  /* beginning
     copy of the first line
   */
  while ( IS_SPACE( *tmp ) ) tmp ++;
  
  while ( *tmp != '\0' ) {
    if ( IS_NOT_SPACE_OR_RETURN( *tmp ) ) {
      if ( *tmp == '{' ) open_brace ++;
      *out++ = *tmp++;
      separator = 0;
    } else {
      if ( separator == 0 ) *out++ = ' ';
      separator = 1;
      tmp ++;
    }
  }

  while ( open_brace > 0 ) {

    /* get new line
     */
    if (  *tmp == '\0' ) {

      if ( fgets( str, STD_STR_LENGTH, f ) == NULL ) {
	if ( _verbose_ ) {
	  fprintf( stderr, "%s: EOF when parsing entry\n", proc );
	  fprintf( stderr, "\t %s\n", str );
	}
	return( 0 );
      }
      
      if ( strlen( item ) + strlen ( str ) >= LONG_STR_LENGTH ) {
	if ( _verbose_ ) {
	  fprintf( stderr, "%s: entry '", proc );
	  {
	    int i;
	    for (i=0; i<40 && item[i] !='\n'; i++ ) {
	      if ( item[i] != '\n' )
		fprintf( stderr, "%c", item[i] );
	    }
	  }
	  fprintf( stderr, "' seems too long.\n\t A closing brace may be missing.\n" );
	}
	return( 0 );
  
      }


      tmp = str;

      /* check if empty
       */
      empty_line = 1;
      while ( *tmp != '\0' && empty_line == 1 ) {
	if ( IS_NOT_SPACE_OR_RETURN( *tmp ) )
	  empty_line = 0;
	tmp ++;
      }

      tmp = str;
    }

    /* copy line
       empty line -> \n
     */
    if ( empty_line == 1 ) {
      *out++ = '\n';
      *out++ = '\n';
      *tmp = '\0';
    }
    while ( *tmp != '\0' && open_brace > 0 ) {
      if ( IS_NOT_SPACE_OR_RETURN( *tmp ) ) { 
	if ( *tmp == '{' ) open_brace ++;
	if ( *tmp == '}' ) open_brace --;
	*out++ = *tmp++;
	separator = 0;
      } else {
	if ( separator == 0 ) *out++ = ' ';
	separator = 1;
	tmp ++;
      }
    }
    
  } /* while ( open_brace > 0 ) */

  *out ++ = '\0';
  return( 1 );
}










void translate_latex_string_into_8bits_strings( char *from, char *to )
{
  char *f = from;
  char *t = to;
  int math_mode = 0;
  int trs_is_done = 0;
  int i;

  if ( from == NULL || to == NULL ) return;

  while ( *f != '\0' ) {

    if ( *f == '\\' && math_mode == 0 ) {
      for ( trs_is_done=0, i=0; trs_is_done==0 && latex_to_8bits[i][1][0] !='X'; i++ ) {
	if ( strncmp( f, latex_to_8bits[i][0], strlen(latex_to_8bits[i][0]) ) == 0 ) {
	  if ( strlen(latex_to_8bits[i][1]) > 0 ) {
	    strcpy( t, latex_to_8bits[i][1] );
	    t += strlen(latex_to_8bits[i][1]);
	  }
	  f += strlen(latex_to_8bits[i][0]);
	  trs_is_done = 1;
	}
      }
    }

    if ( *f == '$' ) {
      math_mode ++;
      if ( math_mode == 2 ) math_mode = 0;
      continue;
    }

    *t++ = *f++;
  }
  
  *t = '\0';
					
  return;
}
		








			       
void translate_bibtex_string_into_bibtex_entry( char *str,
						typeItem *item )
{
  char *tmp = str;
  char *out = item->bibtex_entry;
  int open_brace = 0;
  int open_double_quote = 0;
  
  if ( item->bibtex_entry == NULL ) return;

  while ( *tmp != '\0' ) {
    switch ( *tmp ) {
    case '{' :
      open_brace++;
      *out++ = *tmp++;
      break;
    case '}' :
      open_brace--;
      if ( open_brace == 0 ) *out++ = '\n';
      *out++ = *tmp++;
      break;
    case '"' :
      if ( tmp[-1] != '\\' ) {
	 open_double_quote ++;
	 if ( open_double_quote == 2 ) open_double_quote = 0;
      }
      *out++ = *tmp++;
      break;
    case ',' :
      if ( open_brace == 1 && open_double_quote == 0 ) {
	while ( out[-1] == ' ' ) out--;
      }
      *out++ = *tmp++;
      if ( open_brace == 1 && open_double_quote == 0 ) {
	*out++ = '\n';
	while ( *tmp == ' ' ) tmp++;
      }
      break;
    default :
      *out++ = *tmp++;
    }
  }
}










int translate_bibtex_field_string_into_names( char *str,
				 typeName *names )
{ 
  char *start, *tmp, *aux, *name;
  int i, stop, nb_names = 0;
  int nb_commas, nb_blanks, nb_minus, nb_special;
  int nb_open_braces;
  int offset;

  if ( str == NULL || *str == '\0' )
    return( 0 );
  if ( names == NULL ) return( 0 );

  remove_nested_braces( str );

  /* remove ending blanks
   */
  tmp = str;
  tmp += strlen( str );
  tmp --;
  while ( (IS_SPACE( *tmp )) && tmp >= str ) {
    *tmp = '\0';
    tmp--;
  }

  tmp = str;
  start = tmp;

  while ( *tmp != '\0' ) {
    
    /* after a name, we've got either 
       " and "
       ",and "
       '\0'
    */
    
    if ( (*tmp == ' ' && (strncasecmp( tmp, " and ", 5 ) == 0) ) 
	 || (*tmp == ',' && (strncasecmp( tmp, ",and ", 5 ) == 0) ) 
	 || tmp[1] == '\0' ) {

      /* we've got a name from start to here (*tmp)
	 we are looking for a first name and a last name
	 RULES : [last name], [first name]
	         in case of comma, first name is after the last comma
		 [first name] [last name]
		 the last name begins with a lower letter (de, von, van)
		 or is after the last blank
	 EXCEPTIONS
	       Jr. M. R. Garey
               Allen B. Tucker, Jr.
               A. B. Tucker Jr.
       */
      if ( *tmp == ',' && (strncasecmp( tmp, ",and ", 5 ) == 0) ) *tmp = ' ';
      if ( tmp[1] == '\0' ) tmp ++;
      offset = 0;


      /* suppress trailing comma, if any
       */
      if ( *(tmp-1) == ',' && ( IS_SPACE( *tmp ) || *tmp == '\0' ) ) {
	tmp --;
	*tmp = ' ';
	offset ++;
      }


      /* suppress Jr. at the end
       */
      if ( tmp - str > 3 && strncasecmp( &(tmp[-3]), "Jr.", 3 ) == 0 ) {
	tmp -= 4;
	offset += 4;
	while ( IS_SPACE( *tmp ) || *tmp == ',' ) {
	  tmp --;
	  offset ++;
	}
	/* tmp should be the character after the last character of the name
	 */
	tmp ++;
	offset --;
      }
      /* suppress Jr. at the beginning
       */
      if ( strncasecmp( start, "Jr.", 3 ) == 0 ) {
	start += 3;
	while ( IS_SPACE( *start ) )
	  start ++;
      }
      
      aux = start; 
      nb_commas = 0;
      nb_open_braces = 0;
      while ( aux != tmp ) {
	if ( *aux == '{' ) nb_open_braces++;
	if ( *aux == '}' ) nb_open_braces--;
	if ( nb_open_braces == 0 && *aux == ',' ) nb_commas ++;
	aux++;
      }
      
      if ( nb_commas >= 1 ) {
	/* find the last comma 
	 */
	while ( nb_open_braces != 0 || *aux != ',' ) {
	  if ( *aux == '{' ) nb_open_braces++;
	  if ( *aux == '}' ) nb_open_braces--;
	  aux--;
	}
	/* extract the first name
	 */
	aux++;
	while ( *aux == ' ' ) aux++;
	name = names[nb_names].firstName;
	while ( aux != tmp ) *name++ = *aux++ ;
	while ( name[-1] == ' ' && name > names[nb_names].firstName ) name --;
	*name = '\0';
	/* extract the last name
	 */
	while ( nb_open_braces != 0 || *aux != ',' ) {
	  if ( *aux == '{' ) nb_open_braces++;
	  if ( *aux == '}' ) nb_open_braces--;
	  aux--;
	}
	name = names[nb_names].lastName;
	while ( *start == ' ' ) start ++;
	while ( start != aux ) *name++ = *start ++;
	while ( name[-1] == ' ' && name > names[nb_names].lastName ) name --;
	*name = '\0';

      } else {

	/* no comma
	   count blanks and words beginning by minus letter
	 */

	aux = start; 
	nb_blanks = nb_minus = nb_special = 0;
	nb_open_braces = 0;
	while ( aux != tmp ) {
	  if ( *aux == '{' ) nb_open_braces++;
	  if ( *aux == '}' ) nb_open_braces--;
	  if ( nb_open_braces == 0 && *aux == ' ' ) nb_blanks ++;
	  if ( aux > start ) {
	    
	    if ( nb_open_braces == 0 && IS_SPACE( aux[-1] ) ) {
	      /* words beginning by a small letter
		 or a special separator
	      */
	      if ( *aux >= 'a' && *aux <= 'z' ) {
		nb_minus ++;
	      } 
	      else {
		for ( i=0; strncmp( name_special_separator[i], "XXX", 3 ) != 0; i++ )
		  if ( strncasecmp( aux, name_special_separator[i], strlen(name_special_separator[i]) ) == 0 && IS_SPACE( aux[strlen(name_special_separator[i])] ) ) {
		    nb_special ++;
		  }
		    
	      }
	    }

	  }
	  aux++;
	}

	if ( nb_blanks >= 1 ) {
	  /* is there a word beginning by a minus letter
	   */
	  if ( nb_minus > 0 ) {
	    aux = start; 
	    while ( *aux == ' ' ) aux++;
	    name = names[nb_names].firstName;
	    while ( *aux < 'a' || *aux > 'z' || aux[-1] != ' ' ) 
	      *name++ = *aux ++;
	    while ( name[-1] == ' ' && name > names[nb_names].firstName ) name --;
	    *name = '\0';
	    name = names[nb_names].lastName;
	    while ( aux != tmp ) *name++ = *aux++ ;
	    while ( name[-1] == ' ' && name > names[nb_names].lastName ) name --;
	    *name = '\0';
	  } 
	  else if ( nb_special > 0 ) {
	    aux = start; 
	    while ( *aux == ' ' ) aux++;
	    name = names[nb_names].firstName;
	    stop = 0;
	    while ( stop == 0  ) {
	      *name++ = *aux ++;
	      if (  IS_SPACE( aux[-1] ) ) {
		for ( i=0; strncmp( name_special_separator[i], "XXX", 3 ) != 0; i++ )
		  if ( strncasecmp( aux, name_special_separator[i], strlen(name_special_separator[i]) ) == 0 && IS_SPACE( aux[strlen(name_special_separator[i])] ) ) {
		    stop = 1;
		  }
	      }
	    }

	    while ( ( IS_SPACE( name[-1] ) ) && name > names[nb_names].firstName ) name --;
	    *name = '\0';

	    name = names[nb_names].lastName;
	    while ( aux != tmp ) *name++ = *aux++ ;
	    while ( name[-1] == ' ' && name > names[nb_names].lastName ) name --;
	    *name = '\0';
	  }
	  else {
	    /* deals with the blanks
	       just now aux == tmp, so *aux == ' '
	       to find the last blank decrease aux first
	     */
	    aux --;
	    while ( aux > start && (nb_open_braces != 0 || *aux != ' ') ) {
	      if ( *aux == '{' ) nb_open_braces++;
	      if ( *aux == '}' ) nb_open_braces--;
	      aux--;
	    }

	    if ( aux == start ) {
	      fprintf( stderr, "\n");
	      fprintf( stderr, "*** WARNING *** THIS should not occur ... can not find names in\n" );
	      fprintf( stderr, "-> '%s'\n", str );
	      fprintf( stderr, "*** I suggest to check the syntax of the corresponding entry\n" );
	      fprintf( stderr, "\n");
	      return( 0 );
	    }

	    name = names[nb_names].firstName;
	    while ( *start == ' ' ) start ++;
	    while ( start != aux ) *name++ = *start ++;
	    while ( name[-1] == ' ' && name > names[nb_names].firstName ) name --;
	    *name = '\0';
	    while ( *aux == ' ' ) aux++;
	    name = names[nb_names].lastName;
	    while ( aux != tmp ) *name++ = *aux++ ;
	    while ( name[-1] == ' ' && name > names[nb_names].lastName ) name --;
	    *name = '\0';
	  }
	    
	} else {
	  name = names[nb_names].lastName;
	  while ( start != tmp ) *name++ = *start ++;
	  *name = '\0';
	}
      }

      nb_names++;
      tmp += 5+offset;
      start = tmp;

      /* here, we've read a name
       */
      /* for ( i=0; i<nb_names; i++ ) print_name( &names[i] ); */
    }
    if ( *tmp != '\0' ) tmp ++;
  }
  

  /* build the keynames from the last and first names
   */
  for ( i=0; i<nb_names; i++ ) {

    /* remove the braces
    */
    name = tmp = names[i].firstName;
    while ( *tmp != '\0' ) {
      if ( *tmp == '{' || *tmp == '}' ) {
	tmp ++;
	continue;
      }
      *name++ = *tmp++;
    }
    *name = '\0';

    name = tmp = names[i].lastName;
    while ( *tmp != '\0' ) {
      if ( *tmp == '{' || *tmp == '}' ) {
	tmp ++;
	continue;
      }
      *name++ = *tmp++;
    }
    *name = '\0';

    build_keyname_from_last_and_first_names( &(names[i]), 0 ); 
  }
  return( nb_names );
}










int translate_bibtex_field_string_into_keywords( char *str,
				    typeName *names,
				    int nb )
{ 
  char *start, *tmp, *aux, *name;
  int i, nb_keywords = nb;

  if ( str == NULL || *str == '\0' )
    return( nb );
  if ( names == NULL ) return( 0 );
  if ( nb >= MAX_NAMES ) {
    fprintf( stderr, "translate_bibtex_field_string_into_keywords: already too many keywords\n" );
    return( nb );
  }


  remove_nested_braces( str );

  /* remove ending blanks
   */
  tmp = str;
  tmp += strlen( str );
  tmp --;
  while ( (IS_SPACE( *tmp ) ) && tmp >= str ) {
    *tmp = '\0';
    tmp--;
  }

  /* I found a keyword field beginning by ", ..."
     grrr!!
  */
  tmp = str;
  while ( (*tmp == ',' || *tmp == ';' || IS_SPACE( *tmp ) ) ) tmp ++;

  start = tmp;

  while ( *tmp != '\0' && nb_keywords < MAX_NAMES ) {

    if ( nb_keywords >= MAX_NAMES ) {
      if ( _verbose_ ) 
	fprintf( stderr, "translate_bibtex_field_string_into_keywords: too many keywords\n" );
    }
    else {
      if ( (*tmp == ',') || (*tmp == ';') || tmp[1] == '\0' ) {
	
	if ( tmp[1] == '\0' ) tmp ++;
	/* we've got a keyword from start to here (*tmp)
	   put it in last name
	*/
	while ( (*start == ',' || *start == ';' || IS_SPACE( *start ) ) ) start ++;
	aux = tmp;
	while ( (IS_SPACE( *aux ) || *aux == ',' || *aux == ';' ) ) aux--;
	aux++;
	
	name = names[nb_keywords].lastName;
	while ( start != aux ) *name++ = *start ++;
	*name = '\0';
	
	nb_keywords ++;
      }
      
      start = tmp;
      if ( *tmp != '\0' ) {
	tmp ++;
	while ( (*tmp != ',') && (*tmp != ';') && (*tmp != '\0') && tmp[1] != '\0' ) tmp++; 
      }
    }
  }

  
  /* build the keynames
   */
  for ( i=nb; i<nb_keywords; i++ ) {

    /* remove the braces
    */
    name = tmp = names[i].lastName;
    while ( *tmp != '\0' ) {
      if ( *tmp == '{' || *tmp == '}' ) {
	tmp ++;
	continue;
      }
      *name++ = *tmp++;
    }
    *name = '\0';

    build_keyname_from_last_and_first_names( &(names[i]), 1 );
  }  

  return( nb_keywords );
}










/*
 * called from get_item_from_bibtex_string()
 */
void translate_bibtex_field_string_into_field_value( char *str,
					typeItem *item,
					enumTypeField typefield ) 
{
  char *proc = "translate_bibtex_field_string_into_field_value";
  char *tmp = str;
  char *out;
  int l;
  int trs_is_done, t;
  int math_mode = 0;

  switch ( typefield ) {
  case ALT :
  case OPT :
  case UNKNOWN_TYPE_FIELD :
    break;
  case ABSTRACT :
  case ANNOTE :
  case COMMENTS :
    l = strlen( str );
    if ( l >= desc_field_lgth[ typefield ] ) {
      if ( _warning_ ) {
	fprintf( stderr, "WARNING: unable to copy string (too long) '%s' into field %s for entry %s\n",
		 str, desc_field[ typefield ], item->bibtex_key );
      }
      return;
    }
    remove_nested_braces( str );
    (void)strncpy( item->field[typefield], str, strlen( str ) );
    break;

  default :
    
    if ( item->field[typefield] == NULL ) return;

    l = strlen( str );
    if ( l >= desc_field_lgth[ typefield ] ) {
      if ( _warning_ ) {
	fprintf( stderr, "WARNING: unable to copy string (too long) '%s' into field %s for entry %s\n",
		 str, desc_field[ typefield ], item->bibtex_key );
      }
      return;
    }

    out = item->field[typefield];
    
    /* skip '{', '}', '"'
       if '\'=92 latex macro
       try to be smart 
     */
    while ( *tmp != '\0' ) {
      if ( *tmp == '"' ) {
	tmp ++;
	continue;
      }
      if ( typefield != AUTHOR && typefield != EDITOR && math_mode == 0 && (*tmp == '{' || *tmp == '}') ) {
	tmp ++;
	continue;
      }
      if ( typefield != URL && 
	   typefield != PS && 
	   typefield != POSTSCRIPT && 
	   typefield != PDF && 
	   typefield != DOI && 
	   *tmp == '~' ) {
	*out++ = ' ';
	tmp ++;
	continue;
      }
      if ( *tmp == '\\' && math_mode == 0 ) {

	for ( trs_is_done=0, t=0; trs_is_done==0 && latex_to_8bits[t][1][0] !='X'; t++ ) {
	  if ( strncmp( tmp, latex_to_8bits[t][0], strlen(latex_to_8bits[t][0]) ) == 0 ) {
	    if ( strlen(latex_to_8bits[t][1]) > 0 ) {
	      strcpy( out, latex_to_8bits[t][1] );
	      out += strlen(latex_to_8bits[t][1]);
	    }
	    tmp += strlen(latex_to_8bits[t][0]);
	    trs_is_done = 1;
	  }
	}

	
	if ( trs_is_done == 1 ) continue;
	fprintf( stderr, "%s: unknow latex macro in [%s]\n", proc, item->bibtex_key );
	fprintf( stderr, "\t [%s]\n", tmp );
	*out++ = *tmp++;
	continue;
      }
      /* skip ' ' and '\t' at the beginning 
       */
      if ( out == item->field[typefield] && (IS_SPACE( *tmp ) ) ) {
	tmp ++;
	continue;
      }
      /* math mode ?
       */
      if ( *tmp == '$' ) {
	math_mode ++;
	if ( math_mode == 2 ) math_mode = 0;
      }
      *out++ = *tmp++;
    }

    *out = '\0';

    /* remove ' ' and '\t'
       at the end
    */

    if ( out - item->field[typefield] > desc_field_lgth[typefield] + 10 ) {
      fprintf( stderr, "in %s, field %d is %d long (max is %d)\n",
	       item->bibtex_key, typefield, (int)(out - item->field[typefield]), desc_field_lgth[typefield] );
    }
    
    if ( out > item->field[typefield] )
      out --;
    while ( out >= item->field[typefield] && (IS_SPACE( *out ) ) ) {
      if ( out >= item->field[typefield] )
	*out = '\0';
      if ( out > item->field[typefield] )
	out--;
    }
  }

  switch ( typefield ) {
  default :
    break;
  case KEYWORD :
  case KEYWORDS :
    /* to do */
    if ( item->field[typefield] == NULL || item->field[typefield][0] == '\0' )
      break;
    item->nb_keywords = translate_bibtex_field_string_into_keywords( item->field[typefield],
							item->keywords, item->nb_keywords );
    break;
  case AUTHOR :
    if ( item->field[typefield] == NULL || item->field[typefield][0] == '\0' )
      break;
    item->nb_authors = translate_bibtex_field_string_into_names( item->field[typefield],
						    item->authors_names );
    break;
  case EDITOR :
    if ( item->field[typefield] == NULL || item->field[typefield][0] == '\0' )
      break;
    item->nb_editors = translate_bibtex_field_string_into_names( item->field[typefield],
						    item->editors_names );
    break;
  }
  
}










/* fill item structure from char *
   the char* begins with @something{
   and ends with the final }
*/
int get_item_from_bibtex_string( typeItem *item, char *str,
				   typeListOfStrings *ls ) 
{
  char *proc = "get_item_from_bibtex_string";
  char *tmp = str;
  char *aux = NULL;
  char *foo, *out;
  int i, year;

  int open_brace = 0;
  int open_double_quote = 0;

  enumTypeField type_field;
  int  next_field, trs_is_done, k, l;


  aux = (char*)malloc( LONG_STR_LENGTH * sizeof(char) );
  if ( aux == NULL ) {
    if ( _verbose_ ) 
      fprintf( stderr, "%s: unable to allocate auxiliary string\n", proc );
    return( 0 );
  }

  /* get the reformatted bibtex entry
   */
  translate_bibtex_string_into_bibtex_entry( str, item );

  /* get type
   */
  for ( i=0; item->type_item == UNKNOWN_TYPE_ITEM && strncmp( desc_item[i], "XXX", 3 ) != 0 ; i++ ) 
    if ( strncasecmp( &(tmp[1]), desc_item[i], strlen(desc_item[i]) ) == 0 )
      item->type_item = i;

  /* can be mismatched
   */
  if ( item->type_item == BOOK ) {
    if ( strncasecmp( &(tmp[1]), desc_item[BOOKLET], strlen(desc_item[BOOKLET]) ) == 0 )
      item->type_item = BOOKLET;
  }
  if ( item->type_item == CONFERENCE )
    item->type_item = INPROCEEDINGS;



  switch ( item->type_item ) {
  case BOOK :
  case PROCEEDINGS :
    item->type_cat = CAT_BOOK;
    break;
  case PHDTHESIS :
    item->type_cat = CAT_THESIS;
    break;
  case INBOOK :
  case INCOLLECTION :
  case ARTICLE :
    item->type_cat = CAT_ARTICLE;
    break;
  case INPROCEEDINGS :
  case CONFERENCE :
    item->type_cat = CAT_CONFERENCE;
    break;
  case TECHREPORT :
    item->type_cat = CAT_REPORTS;
    break;
  case BOOKLET :
  case MANUAL :
    item->type_cat = CAT_MANUALS_BOOKLETS;
    break;
  case PATENT :
  case STANDARD :
    item->type_cat = CAT_PATENT_STANDARD;
    break;
  case MASTERSTHESIS :
  case MISC :
  case UNPUBLISHED :
  /* case UNKNOWN_TYPE_ITEM : */
  default :
    item->type_cat = CAT_MISC;
    break;
  }

  /* go to '{'
     and skip it 
  */
  while ( *tmp != '{' ) tmp ++;
  open_brace = 1;
  tmp ++;
  
  /* bibtex key
     go to ',' while skipping ' '
     skip ','
  */
  out = item->bibtex_key;
  while ( *tmp != ',' ) {
    if ( !(IS_SPACE( *tmp )) ) *out++ = *tmp;
    tmp ++;
  }
  tmp++;

  if ( item->type_item == UNKNOWN_TYPE_ITEM ) {
    if ( _warning_ ) {
      fprintf( stderr, "Warning: unknown bibtex type for entry {%s}\n",
	       item->bibtex_key );
      fprintf( stderr, "         %s\n", str );
      fprintf( stderr, "         switch to @MISC\n" );
    }
    item->type_item = MISC;
  }

  
  if ( _debug_ ) {
    fprintf( stderr, "%s: decoding entry '%s'\n", proc, item->bibtex_key );
  }


  /* reading the item
     it ends with a closing brace
  */
  while ( open_brace > 0 ) {


    /* skip everything until 
       a letter or end
    */
    while ( open_brace > 0 &&
	    (*tmp < 'A' || *tmp > 'z' || (*tmp > 'Z' && *tmp < 'a')) ) {
      if ( *tmp == '}' ) open_brace --;
      tmp ++;
    }
    if ( open_brace == 0 ) continue;

    /* here we've got a letter
       get the field type
       so compare the field descriptor with the string
       either it matches OPT or ALT,
       or the following char is ' ', '\t', '\n', or '='
     */
    for( i=0, type_field = UNKNOWN_TYPE_FIELD; 
	 type_field == UNKNOWN_TYPE_FIELD && strncmp( desc_field[i], "XXX", 3 ) != 0; 
	 i++ ) {

      if ( strncasecmp( tmp, desc_field[i], strlen(desc_field[i]) ) == 0 ) {
	switch ( i ) {
	case OPT :
	case ALT :
	  type_field = i;
	  break;
	default :
	  if ( tmp[strlen(desc_field[i])] == ' ' 
	      || tmp[strlen(desc_field[i])] == '\t' 
	      || tmp[strlen(desc_field[i])] == '\n' 
	      || tmp[strlen(desc_field[i])] == '=' )
	    type_field = i;
	}
      }
    }
    
    if ( type_field == UNKNOWN_TYPE_FIELD ) {
      for( i=0; 
	   type_field == UNKNOWN_TYPE_FIELD && strncmp( other_desc_field[i], "XXX", 3 ) != 0;
	   i++ ) {
	if ( strncasecmp( tmp, other_desc_field[i], strlen(other_desc_field[i]) ) == 0 ) 
	  type_field = OPT;
      }
      if ( type_field == UNKNOWN_TYPE_FIELD ) {
	if ( _warning_ ) {
	  fprintf( stderr, "WARNING: unknown field type [%s] in entry {%s}\n",
		   tmp, item->bibtex_key );
	}
      }
    }

    /* go to '='
       and skip it
     */
    while ( open_brace > 0 && *tmp != '=' ) {
      if ( *tmp == '}' ) open_brace --;
      tmp ++;
    }
    if ( open_brace == 0 ) continue;
    tmp++;
    /* skip ' ' and '\t'
     */


    /* remove the blanks
     */
    while ( open_brace > 0 && ( IS_SPACE( *tmp ) ) ) {
      if ( *tmp == '}' ) open_brace --;
      tmp ++;
    }
    if ( open_brace == 0 ) continue;

    /* get the field value 
       until ',' or end
     */
    out = aux;
    next_field = 0;

    do {

      open_double_quote = 0;

      if ( tmp[0] == '{' ) {
	/* copy until open_brace == 1; */
	open_brace ++;
	tmp ++;
	while ( !( open_brace == 2 && *tmp == '}' && tmp[-1] != '\\' ) ) {
	  if ( *tmp == '\"' && tmp[-1] != '\\' ) open_double_quote ++;
	  if ( open_double_quote == 2 ) open_double_quote = 0;
	  if ( *tmp == '}' && tmp[-1] != '\\' ) open_brace --;
	  if ( *tmp == '{' && tmp[-1] != '\\' ) open_brace ++;
	  *out ++ = *tmp ++;
	}
	open_brace --;
	tmp ++;
	if ( open_double_quote > 0 ) {
	  if ( _warning_ ) {
	    fprintf( stderr, "\n" );
	    fprintf( stderr, "*** WARNING *** Extra double quote(s) in '%s'?\n", aux );
	    fprintf( stderr, "\t in entry '%s'\n", item->bibtex_key );
	    fprintf( stderr, "\n" );	
	  }
	}
      }
      else if ( tmp[0] == '"' ) {
	tmp ++;
	while ( open_brace > 0 
		&& !( *tmp == '"' && tmp[-1] != '\\' ) ) {
	  if ( *tmp == '}' && tmp[-1] != '\\' ) open_brace --;
	  if ( *tmp == '{' && tmp[-1] != '\\' ) open_brace ++;
	  *out ++ = *tmp ++;
	}
	tmp ++;
	if ( open_brace > 1 )  {
	  if ( _warning_ ) {
	    fprintf( stderr, "\n" );
	    fprintf( stderr, "*** WARNING *** Extra open brace(s) in '%s'?\n", aux );
	    fprintf( stderr, "\t in entry '%s'\n", item->bibtex_key );
	    fprintf( stderr, "\n" );
	  }
	}
      }
      else {
	trs_is_done = 0;
	while ( IS_SPACE( *tmp ) ) tmp++;
	for ( l = 0, foo = tmp; *foo != '\0' && *foo != ',' 
		&& *foo != '}' && !(IS_SPACE( *foo )); foo++, l++ )
	  ;
	for ( k=0; trs_is_done==0 && k<ls->n; k++ ) {
	  if ( l != ls->string[k].length ) continue;
	  if ( strncmp( tmp, ls->string[k].key, l ) == 0 ) {
	    (void)memcpy( out, ls->string[k].str,
			  strlen( ls->string[k].str ) );
	    tmp += l;
	    out += strlen( ls->string[k].str );
	    trs_is_done = 1;
	  }
	}
	if ( trs_is_done == 0 ) {
	  for( i=0, next_field = 0;
	       next_field == 0 && strncmp( desc_field[i], "XXX", 3 ) != 0; 
	       i++ ) {
	    if ( strncasecmp( tmp, desc_field[i], strlen(desc_field[i]) ) == 0 ) 
	      next_field = 1;
	  }
	  if ( next_field == 0 ) {
	    while ( *tmp != '\0' &&  *tmp != ',' 
		    &&  *tmp != '}' && !(IS_SPACE( *tmp )) ) {
	      *out ++ = *tmp ++;
	    }
	  }
	}
      }
      if ( next_field == 1 ) {
	if ( _warning_ ) {
	  fprintf( stderr, "WARNING: ',' seems to be missing at end of field\n" );
	  fprintf( stderr, "\t '%s' for entry '%s'\n", desc_field[type_field],
		   item->bibtex_key );
	}
	continue;
      }
      
      while ( *tmp != '\0' && (IS_SPACE( *tmp ))  ) tmp++;
      if ( *tmp == '#' ) tmp ++;
      while ( *tmp != '\0' && (IS_SPACE( *tmp )) ) tmp++;    

      if ( *tmp == '}' )  {
	open_brace --;
	tmp ++;
      }

    } while ( *tmp != '\0' && open_brace != 0 && *tmp != ',' && next_field != 1 );

    *out = '\0';
    
    if ( *tmp == ',' ) tmp ++;
    
    translate_bibtex_field_string_into_field_value( aux, item, type_field );
  }
  
  /* get the year
   */
  if ( item->field[YEAR] != NULL && item->field[YEAR][0] != '\0' ) {
    if ( sscanf( item->field[YEAR], "%d", &year ) == 1 ) {
      /* if ( i > 0 ) item->year = i; */
      item->year = year;
    }
  }

#if defined(_HALINRIA_)
  { 
    int i, e, tl, l, valid_extension;
    int fieldlinks[] = {URL, URLPUBLISHER, PDF, POSTSCRIPT, PS, HALURL };
    int nfieldlinks = 6;

    if ( item->field[WRITING_DATE] != NULL && item->field[YEAR] != NULL ) {
      
      l = desc_field_lgth[WRITING_DATE];
      if ( l > desc_field_lgth[YEAR] ) l = desc_field_lgth[YEAR];
      (void)strncpy( item->field[WRITING_DATE], item->field[YEAR], l );

    }

    if ( item->field[FILES] != NULL ) {

      for ( i=0; i<nfieldlinks; i ++ ) {
	tl = strlen( item->field[FILES] );
	if ( item->field[fieldlinks[i]] != NULL && item->field[fieldlinks[i]][0] != '\0' ) {
	  
	  for ( valid_extension=0, e=0; 
		strncmp( recognized_file_extensions[e], "XXX", 3 ) != 0 && valid_extension == 0; e++ ) {
	    if ( strncmp( &(item->field[fieldlinks[i]][ strlen(item->field[fieldlinks[i]]) - strlen(recognized_file_extensions[e]) ]),
			  recognized_file_extensions[e], strlen(recognized_file_extensions[e]) ) == 0 )
	      valid_extension = 1;
	  }

	  if ( valid_extension == 1 ) {
	    l = strlen( item->field[fieldlinks[i]] );
	    if ( tl + 1 + l + 1 < desc_field_lgth[FILES] ) {
	      if ( tl > 0 ) strcat( item->field[FILES], "," );
	      strcat( item->field[FILES], item->field[fieldlinks[i]] );
	    }
	    else {
	      if ( _verbose_ ) {
		fprintf( stderr, "%s: length of field #%d too long (=%d) to be copied into field 'FILE'\n", proc, fieldlinks[i], l );
		fprintf( stderr, "   whose length is already %d (max=%d)\n", tl, desc_field_lgth[FILES] );
	      }
	    }
	  }

	}

      }

    }
  }
#endif


  free( aux );
  return( 1 );
}










int ReadBibtexInputs( typeListOfItems *list_of_items, 
		      typeListOfStrings *list_of_strings,
		      typeListOfNames *list_of_indexed_names, 
		      typeListOfNames *list_of_equivalent_names, 
		      typeListOfNames *list_of_indexed_keywords,
		      typeListOfNames *list_of_equivalent_keywords,
		      typeArgDescription *customization_strings,
		      char *filename )
{
  char *proc = "ReadBibtexInputs";
  int n=0;
  char *str = NULL;
  char *item_str = NULL;
  FILE *f;
  
  typeString        myString;

  typeItem        myItem;
  int i, j, k, l, trs_is_done;
  int length;
  int int_to_be_read = 0;

  typeName *indexed_names = NULL;
  typeName *indexed_keywords = NULL;
  int nb_names, nb_keywords;

  f= fopen( filename, "r" );
  if ( f == NULL ) {
    if ( _verbose_ )
      fprintf( stderr, "%s: unable to open file %s\n", proc, filename );
    return( -1 );
  }
  
  if ( _debug_ ) {
    fprintf( stderr, "reading '%s' in '%s()'\n", filename, proc );
  }

  str = (char*)malloc( (STD_STR_LENGTH + LONG_STR_LENGTH ) * sizeof( char ) );
  if ( str == NULL ) {
    if ( _verbose_ )
      fprintf( stderr, "%s: unable to allocate string\n", proc );
    fclose( f );
    return( -1 );
  }
  item_str  = str;
  item_str += STD_STR_LENGTH;

  indexed_names = malloc( MAX_NAMES * sizeof(typeName) );
  if ( indexed_names == NULL ) {
    if ( _verbose_ )
      fprintf( stderr, "%s: unable to allocate indexed names\n", proc );
    free( str );
    fclose( f );
    return( -1 );
  }
  indexed_keywords = malloc( MAX_NAMES * sizeof(typeName) );
  if ( indexed_keywords == NULL ) {
    if ( _verbose_ )
      fprintf( stderr, "%s: unable to allocate indexed keywords\n", proc );
    free( indexed_names );
    free( str );
    fclose( f );
    return( -1 );
  }


  if ( alloc_all_item( &myItem ) != 1 ) {
    if ( _verbose_ ) 
      fprintf( stderr, "%s: unable to allocate auxiliary item\n", proc );
    free( indexed_keywords );
    free( indexed_names );
    free( str );
    fclose( f );
    return( -1 );
  }

  


  /*
       fgets()  reads  in  at  most one less than size characters
       from stream and stores them into the buffer pointed to  by
       s.  Reading stops after an EOF or a newline.  If a newline
       is read, it is stored into the buffer.  A '\0'  is  stored
       after the last character in the buffer.
  */
  while ( fgets( str, STD_STR_LENGTH, f ) != NULL ) {
    /* comment
     */
    if ( str[0] == '%' ) continue;
    (void)memset( item_str, 0, LONG_STR_LENGTH );
    
    

    
    if ( is_line_a_bibtex_string( str ) == 1 ) {

      /* here we have @STRING{ ...}
       */
      init_string( &myString );
      if ( get_whole_bibtex_string( item_str, str, f ) != 1
	   || get_string_from_bibtex_string( &myString, item_str, 
					     list_of_strings ) != 1
	   || add_string_to_list_of_strings( list_of_strings, &myString ) != 1 ) {
	if ( _verbose_ ) {
	  fprintf( stderr, "%s: unable to get or add string to list in '%s'\n", proc, filename );
	  fprintf( stderr, "\t see %s\n", str );
	}
	release_list_of_items( list_of_items );
	release_item( &myItem );
	free( indexed_keywords );
	free( indexed_names );
	free( str );
	fclose( f );
	return( -1 );
      }

    } /* if ( is_line_a_bibtex_string( str ) == 1 ) */

    else if ( is_line_perhaps_a_new_input( str ) == 1 ) {
    
      /* here we have @ITEM{ ...}
       */
      init_all_item( &myItem );
      if ( get_whole_bibtex_item( item_str, str, f ) != 1 
	   || get_item_from_bibtex_string( &myItem, item_str, 
					   list_of_strings ) != 1 
	   || add_bibliographic_item_to_list( list_of_items, &myItem, 
					      filename ) != 1 ) {
	if ( _verbose_ ) {
	  fprintf( stderr, "%s: unable to get or add item to list in '%s'\n", proc, filename );
	  if ( list_of_items->n >= 2 ) {
	    fprintf( stderr, "\t the two last read items were '%s' and '%s'\n",
		     list_of_items->item[ list_of_items->n-2 ]->bibtex_key,
		     list_of_items->item[ list_of_items->n-1 ]->bibtex_key );
	  }
	}
	release_list_of_items( list_of_items );
	release_item( &myItem );
	free( indexed_keywords );
	free( indexed_names );
	free( str );
	fclose( f );
	return( -1 );
      }

      n++;
      if ( _verbose_ )
	if ( n % 100 == 0 )
	  fprintf( stderr, "in %s, %5d entries already processed\r", filename, n );
      
    }

    /* unknown line
       skip to next
     */
  }

  if ( _verbose_ && n > 100 ) {
    fprintf( stderr, "\n" );
  }


  free( str );
  release_item( &myItem );
  fclose( f );

  /* We have finished reading the file
   */

  update_bibtex_items_with_crossrefs( list_of_items );
  

  /* PROCESS @STRING{}
     - get context elements
   */
  for ( i=0; i<list_of_strings->n; i++ ) {

    for ( trs_is_done=0, j=0; trs_is_done==0 && strncmp( customization_strings[j].key, "XXX", 3 ) != 0; j++ ) {
	
      
      /*
      if ( strncmp( list_of_strings->string[i].key,
		    customization_strings[j].key, 
		    strlen( customization_strings[j].key ) ) == 0 
	   && ( strcmp( list_of_strings->string[i].key 
			+ strlen( customization_strings[j].key ), ".start" ) == 0 
		|| strcmp( list_of_strings->string[i].key 
			   + strlen( customization_strings[j].key ), ".end" ) == 0 
		|| strncmp( customization_strings[j].key, 
			    list_of_strings->string[i].key, 
			    strlen( myString.key ) ) == 0 )) {
      */
      if ( strncmp( list_of_strings->string[i].key,
		    customization_strings[j].key, 
		    strlen( customization_strings[j].key ) ) == 0 ) {

	/* replace \n and \t by single character
	 */
	if ( list_of_strings->string[i].str != NULL
	     && list_of_strings->string[i].str[0] != '\0' 
	     && strcmp( list_of_strings->string[i].str, "NULL" ) != 0 ) {

	  for ( k = 0; list_of_strings->string[i].str[k] != '\0'; k++ ) {
	    /*
	      printf( "\n [%s][%s][%d] = [%s] \n", myString.key , 
	      myString.str, j , &(myString.str[j]) );
	    */
	    if ( list_of_strings->string[i].str[k] == '\\' ) {
	      if ( list_of_strings->string[i].str[k+1] == 'n' ) {
		list_of_strings->string[i].str[k] = '\n';
	      } 
	      else if ( list_of_strings->string[i].str[k+1] == 't' ) {
		list_of_strings->string[i].str[k] = '\t';
	      } 
	      else {
		continue;
	      }
	      for ( l = k+1; list_of_strings->string[i].str[l] != '\0'; l++ ) {
		list_of_strings->string[i].str[l] = list_of_strings->string[i].str[l+1];
	      }
	    }
	  }
	}

	switch ( customization_strings[j].type ) {
	  
	default :
	  if ( _warning_ )
	    fprintf( stderr, "%s: unhandled type for key '%s'?\n",
		     proc, customization_strings[j].key );
	  break;
	  
	case _INT_ :

	  if ( list_of_strings->string[i].str == NULL
	       || list_of_strings->string[i].str[0] == '\0' 
	       || strcmp( list_of_strings->string[i].str, "NULL" ) == 0 ) {
	    *((int*)customization_strings[j].arg) = 0 ;
	  }
	  else {
	    if ( sscanf( list_of_strings->string[i].str, "%d", &int_to_be_read ) != 1 ) {
	      if ( _warning_ ) {
		fprintf( stderr, "%s: unable to get new value for '%s'\n",
			 proc, customization_strings[j].key );
		fprintf( stderr, "         [%s]\n", list_of_strings->string[i].str );
	      }
	    }
	    else {
	      *((int*)customization_strings[j].arg) = int_to_be_read;
	    }
	  }
	  trs_is_done = 1 ;
	  break;
	  
	case _STRING_ :

	  l = 0;
	  if ( list_of_strings->string[i].str != NULL ) 
	    l = strlen( list_of_strings->string[i].str );
	  length = 0;
	  if ( *((char**)customization_strings[j].arg) != NULL ) 
	    length = strlen( *((char**)customization_strings[j].arg) );

	  if ( length == 0 || length < l ) {
	    if ( *((char**)customization_strings[j].arg) != NULL )
	      free( *((char**)customization_strings[j].arg) );
	    *((char**)customization_strings[j].arg) = (char*)malloc( l+1 );
	    if ( *((char**)customization_strings[j].arg) == NULL ) {
	      fprintf( stderr, "%s: unable to allocate string for key '%s'\n",
		       proc, customization_strings[j].key );
	      release_list_of_items( list_of_items );
	      free( indexed_keywords );
	      free( indexed_names );
	      return( -1 );
	    }
	  }

	  if ( list_of_strings->string[i].str == NULL 
	       || list_of_strings->string[i].str[0] == '\0' 
	       || strcmp( list_of_strings->string[i].str, "NULL" ) == 0 ) 
	    (*((char**)customization_strings[j].arg))[0] = '\0';
	  else 
	    (void)strcpy( *((char**)customization_strings[j].arg),
			  list_of_strings->string[i].str );

	  trs_is_done = 1 ;
	  break;
	  
	} /* switch ( customization_strings[j].type ) */
	
      }
    }
  }



  /* PROCESS @STRING{}
     - get authors to be indexed
   */
  for ( i=0; i<list_of_strings->n; i++ ) {
    if ( strcasecmp( list_of_strings->string[i].key, "author_not_to_be_indexed" ) == 0 ||
	 strcasecmp( list_of_strings->string[i].key, "authors_not_to_be_indexed" ) == 0 ) {
      (void)memset( indexed_names, 0, MAX_NAMES * sizeof(typeName) );
      nb_names = translate_bibtex_field_string_into_names( list_of_strings->string[i].str, 
							   indexed_names );
      for ( j=0; j<nb_names; j++ ) {
	(void)memcpy( indexed_names[j].equivKeyName, 
		      indexed_names[j].keyName,  SHORT_STR_LENGTH );
	indexed_names[j].nreferences = 0;
	indexed_names[j].status = TO_BE_IGNORED;
	if ( add_name_to_list_of_names( list_of_indexed_names, &(indexed_names[j]) ) != 1 ) {
	  if ( _warning_ )
	     fprintf( stderr, "%s: unable to add author name to indexed authors list\n", proc );
	}
      }
    }
  }
  for ( i=0; i<list_of_strings->n; i++ ) {
    if ( strcasecmp( list_of_strings->string[i].key, "author_to_be_indexed" ) == 0 ||
	 strcasecmp( list_of_strings->string[i].key, "authors_to_be_indexed" ) == 0 ) {
      (void)memset( indexed_names, 0, MAX_NAMES * sizeof(typeName) );
      nb_names = translate_bibtex_field_string_into_names( list_of_strings->string[i].str, 
						     indexed_names );
      for ( j=0; j<nb_names; j++ ) {
	(void)memcpy( indexed_names[j].equivKeyName, 
		      indexed_names[j].keyName,  SHORT_STR_LENGTH );
	indexed_names[j].nreferences = 0;
	indexed_names[j].status = TO_BE_INDEXED;
	if ( add_name_to_list_of_names( list_of_indexed_names, &(indexed_names[j]) ) != 1 ) {
	  if ( _warning_ )
	    fprintf( stderr, "%s: unable to add author name to indexed authors list\n", proc );
	}
      }
    }
  }
  for ( i=0; i<list_of_strings->n; i++ ) {
    if ( strcasecmp( list_of_strings->string[i].key, "same_author" ) == 0 
	 || strcasecmp( list_of_strings->string[i].key, "same_authors" ) == 0 ) {
      (void)memset( indexed_names, 0, MAX_NAMES * sizeof(typeName) );
      translate_latex_string_into_8bits_strings( list_of_strings->string[i].str, 
						 list_of_strings->string[i].str );
      nb_names = translate_bibtex_field_string_into_names( list_of_strings->string[i].str, 
							   indexed_names );
      if ( nb_names < 2 ) {
	if ( _warning_ ) {
	  fprintf( stderr, "%s: unable to extract multiple authors from\n", proc );
	  fprintf( stderr, "   '%s'\n", list_of_strings->string[i].str );
	}
      }
      else {
	sort_names( indexed_names, 0, nb_names-1 );
	for ( j=0; j<nb_names; j++ ) {
	  (void)memcpy( indexed_names[j].equivKeyName, 
			indexed_names[nb_names-1].keyName, SHORT_STR_LENGTH );
	  indexed_names[j].nreferences = 0;
	  if ( add_name_to_list_of_names( list_of_equivalent_names, &(indexed_names[j]) ) != 1 ) {
	    if ( _warning_ )
	      fprintf( stderr, "%s: unable to add author name to authors equivalence list\n", proc );
	  }
	}
      }
    }
  }


  /* PROCESS @STRING{}
     get keywords not to be indexed
   */
  for ( i=0; i<list_of_strings->n; i++ ) {
    if ( strcasecmp( list_of_strings->string[i].key, "keyword_not_to_be_indexed" ) == 0 ||
	 strcasecmp( list_of_strings->string[i].key, "keywords_not_to_be_indexed" ) == 0 ) {
      (void)memset( indexed_keywords, 0, MAX_NAMES * sizeof(typeName) );
      nb_keywords = translate_bibtex_field_string_into_keywords( list_of_strings->string[i].str, 
								 indexed_keywords, 0 );
      for ( j=0; j<nb_keywords; j++ ) {
	(void)memcpy( indexed_keywords[j].equivKeyName, 
		      indexed_keywords[j].keyName,  SHORT_STR_LENGTH );
	indexed_keywords[j].nreferences = 0;
	indexed_keywords[j].status = TO_BE_IGNORED;
	if ( add_name_to_list_of_names( list_of_indexed_keywords, &(indexed_keywords[j]) ) != 1 ) {
	  if ( _warning_ )
	    fprintf( stderr, "%s: unable to add keyword to indexed keywords list\n", proc );
	}
      }
    }
  }
  for ( i=0; i<list_of_strings->n; i++ ) {
    if ( strcasecmp( list_of_strings->string[i].key, "keyword_to_be_indexed" ) == 0 ||
	 strcasecmp( list_of_strings->string[i].key, "keywords_to_be_indexed" ) == 0 ) {
      (void)memset( indexed_keywords, 0, MAX_NAMES * sizeof(typeName) );
      nb_keywords = translate_bibtex_field_string_into_keywords( list_of_strings->string[i].str, 
								 indexed_keywords, 0 );
      for ( j=0; j<nb_keywords; j++ ) {
	(void)memcpy( indexed_keywords[j].equivKeyName, 
		      indexed_keywords[j].keyName,  SHORT_STR_LENGTH );
	indexed_keywords[j].nreferences = 0;
	indexed_keywords[j].status = TO_BE_INDEXED;
	if ( add_name_to_list_of_names( list_of_indexed_keywords, &(indexed_keywords[j]) ) != 1 ) {
	  if ( _warning_ )
	    fprintf( stderr, "%s: unable to add keyword to indexed keywords list\n", proc );
	}
      }
    }
  }
  for ( i=0; i<list_of_strings->n; i++ ) {
    if ( strcasecmp( list_of_strings->string[i].key, "same_keyword" ) == 0 
	 || strcasecmp( list_of_strings->string[i].key, "same_keywords" ) == 0 ) {
      (void)memset( indexed_keywords, 0, MAX_NAMES * sizeof(typeName) );
      translate_latex_string_into_8bits_strings( list_of_strings->string[i].str, 
						 list_of_strings->string[i].str );
       nb_keywords = translate_bibtex_field_string_into_keywords( list_of_strings->string[i].str, 
								 indexed_keywords, 0 );
      if ( nb_keywords < 2 ) {
	if ( _warning_ ) {
	  fprintf( stderr, "%s: unable to extract multiple keywords from\n", proc );
	  fprintf( stderr, "   '%s'\n", list_of_strings->string[i].str );
	}
      }
      else {
	sort_names( indexed_keywords, 0, nb_keywords-1 );
	for ( j=0; j<nb_keywords; j++ ) {
	  (void)memcpy( indexed_keywords[j].equivKeyName, 
			indexed_keywords[nb_keywords-1].keyName, SHORT_STR_LENGTH );
	  indexed_keywords[j].nreferences = 0;
	  if ( add_name_to_list_of_names( list_of_equivalent_keywords, &(indexed_keywords[j]) ) != 1 ) {
	    if ( _warning_ )
	      fprintf( stderr, "%s: unable to add keyword to keywords equivalence list\n", proc );
	  }
	}
      }
    }
  }


  free( indexed_keywords );
  free( indexed_names );

  return( n );
}


int CountBibtexInputs( char *filename )
{
  char *proc = "CountBibtexInputs";
  int n=0;
  char *str = NULL;
  FILE *f;


  f= fopen( filename, "r" );
  if ( f == NULL ) {
    if ( _verbose_ )
      fprintf( stderr, "%s: unable to open file %s\n", proc, filename );
    return( 0 );
  }
  
  str = (char*)malloc( STD_STR_LENGTH * sizeof( char ) );
  if ( str == NULL ) {
    if ( _verbose_ )
      fprintf( stderr, "%s: unable to allocate string\n", proc );
    fclose( f );
    return( 0 );
  }

  /*
       fgets()  reads  in  at  most one less than size characters
       from stream and stores them into the buffer pointed to  by
       s.  Reading stops after an EOF or a newline.  If a newline
       is read, it is stored into the buffer.  A '\0'  is  stored
       after the last character in the buffer.
  */
  while ( fgets( str, STD_STR_LENGTH, f ) != NULL ) {
    /* comment
     */
    if ( str[0] == '%' ) continue;
    if ( is_line_a_new_input( str ) == 1 ) {
      printf("%s", str );
      n++;
    }
  }

  fclose( f );
  return( n );
}





/************************************************************
 *
 * END OF BIBTEX READING
 *
 * MEDLINE READING
 *
 ************************************************************/





int medline_string_length( char *str ) 
{
  int l = 0;
  char *tmp = str;

  do {

    if ( *tmp == '\n' ) {
      tmp ++;
      if ( strncmp( tmp, "      ", 6 ) == 0 ) {
	tmp += 6;
	l++;
      }
      else {
	return( l );
      }
    }
    else {
      tmp++;
      l++;
    }
    
  } while( *tmp != '\0' );
  
  return( l );
}

char *next_medline_entry( char *str )
{
  char *tmp = str;

  do {

    if ( *tmp == '\n' ) {
      tmp ++;
      if ( strncmp( tmp, "      ", 6 ) == 0 ) {
	tmp += 6;
      }
      else {
	return( tmp );
      }
    }
    else {
      tmp++;
    }
    
  } while( *tmp != '\0' );
  return( tmp );
}

int get_name_from_medline_string( char *str, typeName *name )
{
  char sep = 0;
  char *tmp = str;
  char *end;
  char *n;

  init_name( name );

  if ( strncmp( str, "FAU - ", 6 ) == 0 ) {
    sep = ',';
  } 
  else if ( strncmp( str, "AU  - ", 6 ) == 0 ) {
    sep = ' ';
  }
  else {
    return( 0 );
  }

  /* go to end
   */
  tmp += 6;
  end = tmp;
  while ( *end != '\n' && *end != '\0' ) end ++;

  /* go back
   */
  while ( end > tmp && *end != sep ) end --;

  
  n = name->lastName;
  
  /* there is no first name ???
   */
  if ( end == tmp ) {
    while ( *tmp != '\n' && *tmp != '\0' ) 
      *n++ = *tmp++;
    return( 1 );
  }
    
  /* copy last name
   */
  while ( tmp != end ) {
    *n++ = *tmp++;
  }

  if ( sep == ',' ) tmp ++;
  while ( *tmp == ' ' ) tmp ++;
  
  n = name->firstName;
  
  while ( *tmp != '\n' && *tmp != '\0' )
    *n++ = *tmp++;

  return( 1 );
}




char *translate_medline_string_into_date( char *str,
					  typeItem *item )
{
  char *proc = "translate_medline_string_into_date";
  int year;
  char month[20];
  char *tmp = str;
  
  tmp += 6;
  if ( sscanf( tmp, "%d", &year ) == 1 ) {
    item->year = year;
    while ( *tmp >= '0' && *tmp <= '9') tmp++;
    while ( *tmp == ' ' ) tmp++;
    if ( *tmp == '\n' ) return( next_medline_entry( tmp ) );
  }
  else {
    fprintf( stderr, "%s: unable to get date from '", proc);
    while ( *tmp != '\n' && *tmp != '\0' ) fprintf( stderr, "%c", *tmp++ );
    fprintf( stderr, "\n" );
  }
  
  if ( sscanf( tmp, "%s", month ) == 1 ) {
    sprintf( item->field[MONTH], "%s", month );
    while ( (*tmp >= 'a' && *tmp <= 'z') || (*tmp >= 'A' && *tmp <= 'Z') ) tmp++;
    while ( *tmp == ' ' ) tmp++;
    if ( *tmp == '\n' ) return( next_medline_entry( tmp ) );
  }
  
  return( next_medline_entry( tmp ) );
}




char *translate_medline_string_into_doi( char *str,
					  typeItem *item )
{
  char *proc = "translate_medline_string_into_doi";
  char *tmp = str;
  char *t;

  tmp += 6;
  t = tmp;
  while ( *t != '\0' && *t != '\n' ) t++;
  t-=6;
  if ( strncmp( t, " [doi]", 6 ) == 0 ) {
    fprintf( stderr, "DOI=%s\n", tmp );
    strncpy( item->field[DOI], tmp, strlen(tmp)-strlen(t) );
  }
  
  return( next_medline_entry( tmp ) );
}



char *translate_medline_string_into_field_value( char *str,
						 typeItem *item,
					       enumTypeField typefield )
{
  char *proc = "translate_medline_string_into_field_value";
  char *tmp = str;
  char *out;
  int l;
  int copy;
  int nsep;
  char lang[4];

  tmp += 6;

  l = medline_string_length( tmp );

  switch ( typefield ) {

  case ALT :
  case OPT :
  case UNKNOWN_TYPE_FIELD :
    break;
    
  case KEYWORD :
  case KEYWORDS :

    if ( item->field[typefield] == NULL ) return( next_medline_entry( tmp ) );
    if ( l >= desc_field_lgth[ typefield ] ) {
      if ( _warning_ ) {
	fprintf( stderr, "WARNING: unable to copy string (too long) '%s' into field %s for entry %s\n",
		 str, desc_field[ typefield ], item->bibtex_key );
      }
      return( next_medline_entry( tmp ) );
    }
    
    nsep = 0;
    do { 
      
      out = item->field[typefield];
      do {
	if ( *tmp == '*') {
	  tmp ++;
	}
	else if ( *tmp == ',' ) {
	  tmp ++;
	}
	else {
	  *out++ = *tmp++;
	}
      } while( *tmp != '\0' && *tmp != '\n' && *tmp != '/' );
      
      *out++ = '\0';
      item->nb_keywords = translate_bibtex_field_string_into_keywords( item->field[typefield],
							  item->keywords, item->nb_keywords );
      
      if ( *tmp == '/' ) {
	tmp ++;
	nsep ++;
      }

    } while( *tmp != '\0' && *tmp != '\n' && nsep <= 1 );

    return( next_medline_entry( tmp ) );
    break;

  case NOTE :

    if ( item->field[typefield] == NULL ) return( next_medline_entry( tmp ) );
    if ( l >= desc_field_lgth[ typefield ] ) {
      if ( _warning_ ) {
	fprintf( stderr, "WARNING: unable to copy string (too long) '%s' into field %s for entry %s\n",
		 str, desc_field[ typefield ], item->bibtex_key );
      }
      return( next_medline_entry( tmp ) );
    }
    
    out = item->field[typefield];

    /* language
     */
    if ( strncasecmp( str, "LA  - ", 6 ) == 0 ) {
      if ( strncmp( tmp, "fre", 3 ) == 0 ) {
	sprintf( out, "[In French]" );
	out += strlen( "[In French]" );
      }
      else if ( strncmp( tmp, "ger", 3 ) == 0 ) {
	sprintf( out, "[In German]" );
	out += strlen( "[In German]" );
      }
      else if ( strncmp( tmp, "ita", 3 ) == 0 ) {
	sprintf( out, "[In Italian]" );
	out += strlen( "[In Italian]" );
      }
      else if ( strncasecmp( tmp, "eng", 3 ) == 0 ) {
	;
      }
      else {
	strncpy( lang, tmp, 3 ); lang[3] = '\0';
	fprintf( stderr, "WARNING: language (%s) not handled in %s\n",
		 lang, proc );
      }
      return( next_medline_entry( tmp ) );
      break;
    }
	
    /* erratum
     */
    
    if ( strncmp( str, "EIN - ", 6 ) == 0 ) {
      sprintf( out, "Erratum in " );
      out += strlen( "Erratum in " );
    }

    copy = 1;
    do {
      if ( *tmp == '\n' ) {
	tmp ++;
	if ( strncmp( tmp, "      ", 6 ) == 0 ) {
	  tmp += 6;
	  *out++ = ' ';
	}
	else {
	  copy = 0;
	}
      }
      else {
	*out++ = *tmp++;
      }
    } while( *tmp != '\0' && copy == 1);

    break;

  default :
    if ( item->field[typefield] == NULL ) return( next_medline_entry( tmp ) );
    if ( l >= desc_field_lgth[ typefield ] ) {
      if ( _warning_ ) {
	fprintf( stderr, "WARNING: unable to copy string (too long) '%s' into field %s for entry %s\n",
		 str, desc_field[ typefield ], item->bibtex_key );
      }
      return( next_medline_entry( tmp ) );
    }
    
    out = item->field[typefield];



    copy = 1;
    do {
      if ( *tmp == '\n' ) {
	tmp ++;
	if ( strncmp( tmp, "      ", 6 ) == 0 ) {
	  tmp += 6;
	  *out++ = ' ';
	}
	else {
	  copy = 0;
	}
      }
      else {
	*out++ = *tmp++;
      }
    } while( *tmp != '\0' && copy == 1);
    
    
  } /* end of switch ( typefield ) */


  /* remove ending point, or ending stuff
   */
  switch ( typefield ) {
  default :
    break;
  case PAGES :
    out = item->field[typefield];
    while ( ( *out >= '0' && *out <= '9' ) || *out == '-' ) out++;
    *out = '\0';
    break;
  case TITLE :
    out = item->field[typefield];
    if ( out[ strlen(out) - 1 ] == '.' ) out[ strlen(out) - 1 ] = '\0';
    break;
  }

  return( tmp );
}

/* http://www.nlm.nih.gov/search.html (with mnemonic)
   http://www.nlm.nih.gov/databases/license/medlars_elements2.html
*/
/*
   AD   ADDRESS
   CI   Copyright Information
   CIN  Copyright Information
   CY   (Country of Publication) field
   DA   Date Created
   DCOM Date Completed
   EDAT 
   EIN  Erratum in ?
   ID   ID NUMBER
   IS   ISSN INTERNATIONAL STANDARD SERIAL NUMBER
   JID  NLM Unique ID
   LA   Language Field (LA)
   LR   LAST REVISION DATE
   MHDA 
   PHST
   PMID PubMed Unique Identifier
   PST
   RF   NUMBER OF REFERENCES
        If an item is a review of the literature and has received
        REVIEW as a Publication Type, the number of bibliographic references
        listed in the item appears in this data element.
   RN   CAS REGISTRY/EC NUMBER (Chemical Abstracts Service)
   SB   JOURNAL SUBSET
   SO   Source field
        The DP field also appears as part of the Source (SO) field in ELHILL.
        The IP also appears as part of the Source (SO) field in ELHILL.
        The PG field also appears as part of the SO (Source) field in ELHILL.
   TT   TRANSLITERATED/VERNACULAR TITLE 
   UI   MEDLINE Unique Identifier (UI) */
char *medline_tag_to_be_ignored[] = { "AD  - ", 
				      "CI  - ", 
				      "CIN - ", 
				      "CN  - ", 
				      "CON - ", 
				      "CY  - ", 
				      "DA  - ", 
				      "DCOM- ", 
				      "DEP - ",
				      "EDAT- ",  
				      "GR  - ", 
				      "ID  - ", 
				      "IS  - ", 
				      "JID - ", 
				      "LR  - ", 
				      "MHDA- ",
				      "OID - ",
				      "OT  - ",
				      "OTO - ",
				      "OWN - ", 
				      "PHST- ", 
				      "PL  - ", 
				      "PMID- ", 
				      "PST - ", 
				      "PUBM- ", 
				      "RF  - ", 
				      "RN  - ", 
				      "SB  - ", 
				      "SO  - ", 
				      "STAT- ", 
				      "TT  - ", 
				      "UI  - ", 
				      "XXXXXX" };


/*
  AB   Abstract
  AU   AUTHOR
  DP   DATE OF PUBLICATION
  FAU  
  IP   ISSUE/PART/SUPPLEMENT
  MH   Medical Subject Headings (MeSH)
  PG   PAGINATION 
  PT   PUBLICATION TYPE 
  TA   TITLE ABBREVIATION 
  TI   TITLE
  VI   VOLUME ISSUE
 */

int get_item_from_medline_string( typeItem *item, char *str ) 
{
  char *proc = "get_item_from_medline_string";
  char *tmp = str;
  int i;
  int str_is_found;
  
  typeName name;

  while ( *tmp != '\0' && *tmp != ' ' && *tmp != '\n' ) {
    if ( strncmp( tmp, "AB  - ", 6 ) == 0 ) {
      tmp = translate_medline_string_into_field_value( tmp, item, ABSTRACT );
    }
    else if ( strncmp( tmp, "AID - ", 6 ) == 0 ) {
      tmp = translate_medline_string_into_doi( tmp, item );
    }
    else if ( strncmp( tmp, "AU  - ", 6 ) == 0 || strncmp( tmp, "FAU - ", 6 ) == 0 ) {
      if ( get_name_from_medline_string( tmp, &name ) == 1 ) {
	build_keyname_from_last_and_first_names( &name, 0 );
	add_name_to_authors( &name, item );
      }
      tmp = next_medline_entry( tmp+6 );
    }
    else if ( strncmp( tmp, "DP  - ", 6 ) == 0 ) {
      tmp = translate_medline_string_into_date( tmp, item );
    }
    else if ( strncmp( tmp, "EIN - ", 6 ) == 0 ) {
      tmp = translate_medline_string_into_field_value( tmp, item, NOTE );
    }
    else if ( strncmp( tmp, "IP  - ", 6 ) == 0 ) {
      tmp = translate_medline_string_into_field_value( tmp, item, NUMBER );
    }
    else if ( strncmp( tmp, "LA  - ", 6 ) == 0 ) {
      tmp = translate_medline_string_into_field_value( tmp, item, NOTE );
    }
    else if ( strncmp( tmp, "MH  - ", 6 ) == 0 ) {
      tmp = translate_medline_string_into_field_value( tmp, item, KEYWORDS );
    }
    else if ( strncmp( tmp, "PG  - ", 6 ) == 0 ) {
      tmp = translate_medline_string_into_field_value( tmp, item, PAGES );
    }
    else if ( strncmp( tmp, "PT  - ", 6 ) == 0 ) {
      tmp += 6;
      if ( strncasecmp( tmp, "Clinical Trial", 14 ) == 0 
	   || strncasecmp( tmp, "Randomized Controlled Trial", 27 ) == 0 
	   || strncasecmp( tmp, "Controlled Clinical Trial", 25 ) == 0 
	   || strncasecmp( tmp, "Clinical Trial, Phase III", 25 ) == 0 
	   || strncasecmp( tmp, "Comment", 7 ) == 0 
	   || strncasecmp( tmp, "Validation Studies", 18 ) == 0
	   || strncasecmp( tmp, "Evaluation Studies", 18 ) == 0 ) {
	;
      }
      else if ( strncasecmp( tmp, "Review", 6 ) == 0 
		|| strncasecmp( tmp, "Review, Tutorial", 16 ) == 0 ) {
	item->nb_keywords = translate_bibtex_field_string_into_keywords( "review",
									 item->keywords, 
									 item->nb_keywords );
	item->nb_keywords = translate_bibtex_field_string_into_keywords( "survey",
									 item->keywords, 
									 item->nb_keywords );
      }
      else if ( strncasecmp( tmp, "Journal Article", 15 ) == 0 
		|| strncasecmp( tmp, "Letter", 6 ) == 0 
		|| strncasecmp( tmp, "Editorial", 9 ) == 0 ) {
	item->type_item = ARTICLE;
	item->type_cat = CAT_ARTICLE;
      } 
      else {
	if ( item->type_item == UNKNOWN_TYPE_ITEM ) {
	  if ( _verbose_ || _warning_ ) {
	    fprintf( stderr, "%s: publication type '", proc );
	    while ( *tmp != '\n' && *tmp != '\0' ) fprintf( stderr, "%c", *tmp++ );
	    fprintf( stderr, "' not handled\n" );
	  }
	}
      }
      tmp = next_medline_entry( tmp );
    }
    else if ( strncmp( tmp, "TA  - ", 6 ) == 0 ) {
      tmp = translate_medline_string_into_field_value( tmp, item, JOURNAL );
    }
    else if ( strncmp( tmp, "TI  - ", 6 ) == 0 ) {
      tmp = translate_medline_string_into_field_value( tmp, item, TITLE );
    }
    else if ( strncmp( tmp, "VI  - ", 6 ) == 0 ) {
      tmp = translate_medline_string_into_field_value( tmp, item, VOLUME );
    }
    
    else {
      for ( str_is_found = 0, i = 0; 
	    str_is_found == 0 && strncmp( medline_tag_to_be_ignored[i], "XXXXXX", 6 ) != 0; i++ ) {
	if ( strncmp( tmp, medline_tag_to_be_ignored[i], 6 ) == 0 ) {
	  str_is_found = 1;
	}
      }
      if ( str_is_found == 1 ) {
	tmp += 6;
      } 
      else {
	if ( _verbose_ || _warning_ ) {
	  fprintf( stderr, "%s: unknowm medline tag '", proc );
	  for ( i=0; i<6;i++,tmp++ ) fprintf( stderr, "%c", *tmp );
	  fprintf( stderr, "'\n" );
	}
      }
      tmp = next_medline_entry( tmp );
    }
  }

  if ( build_bibtex_key( item ) != 1 ) {
    if ( _verbose_ || _warning_ ) {
      fprintf( stderr, "%s: unable to build bibtex key\n", proc );
    }
  }
  return( 1 );
}


int ReadMedlineInputs( typeListOfItems *list_of_items, 
		       typeListOfStrings *list_of_strings,
		       typeListOfNames *list_of_indexed_names, 
		       typeListOfNames *list_of_equivalent_names, 
		       typeListOfNames *list_of_indexed_keywords,
		       typeListOfNames *list_of_equivalent_keywords,
		       typeArgDescription *customization_strings,
		       char *filename )
{
  char *proc = "ReadMedlineInputs";
  FILE *f;
  int n = 0;
  char *str = NULL;
  char *item_str = NULL;
  char *tmp;
  char *item_tmp;
  
  typeItem        myItem;
  int is_reading_an_item;
  enumTypeField type_field;
  int empty_line;


  f = fopen( filename, "r" );
  if ( f == NULL ) {
    if ( _verbose_ )
      fprintf( stderr, "%s: unable to open file %s\n", proc, filename );
    return( 0 );
  }

  if ( _debug_ ) {
    fprintf( stderr, "reading '%s' in '%s()'\n", filename, proc );
  }

  str = (char*)malloc( (STD_STR_LENGTH + LONG_STR_LENGTH ) * sizeof( char ) );
  if ( str == NULL ) {
    if ( _verbose_ )
      fprintf( stderr, "%s: unable to allocate string\n", proc );
    fclose( f );
    return( 0 );
  }
  item_str  = str;
  item_str += STD_STR_LENGTH;

  if ( alloc_all_item( &myItem ) != 1 ) {
    if ( _verbose_ ) 
      fprintf( stderr, "%s: unable to allocate auxiliary item\n", proc );
    free( str );
    fclose( f );
    return( 0 );
  }

  init_all_item( &myItem );
  memset(item_str, 0, LONG_STR_LENGTH );
  item_tmp = item_str;
  
  is_reading_an_item = 0;
  empty_line = 1;
  type_field = UNKNOWN_TYPE_FIELD;

  /*
       fgets()  reads  in  at  most one less than size characters
       from stream and stores them into the buffer pointed to  by
       s.  Reading stops after an EOF or a newline.  If a newline
       is read, it is stored into the buffer.  A '\0'  is  stored
       after the last character in the buffer.
  */
  while ( fgets( str, STD_STR_LENGTH, f ) != NULL ) {
    
    /* items are separated by empty lines
     */
    tmp = str;
    empty_line = 1;
    while ( *tmp != '\0' && empty_line == 1 ) {
      if ( IS_NOT_SPACE_OR_RETURN( *tmp ) )
	empty_line = 0;
      tmp ++;
    }

    if ( empty_line == 1 ) {
      if ( is_reading_an_item == 1 ) {
	if ( get_item_from_medline_string( &myItem, item_str ) != 1 ) {
	  if ( _verbose_ )
	    fprintf( stderr, "%s: unable to get item in '%s'\n", proc, filename );
	  release_item( &myItem );
	  free( str );
	  fclose( f );
	  return( 0 );
	}
	if ( add_bibliographic_item_to_list( list_of_items, &myItem, 
					     filename ) != 1 ) {
	  if ( _verbose_ )
	    fprintf( stderr, "%s: unable to add item to list in '%s'\n", proc, filename );
	  release_item( &myItem );
	  free( str );
	  fclose( f );
	  return( 0 );
	}
	n++;
      }
      init_all_item( &myItem );
      memset(item_str, 0, LONG_STR_LENGTH );
      item_tmp = item_str;
      is_reading_an_item = 0;
      continue;
    }
    else {
      is_reading_an_item = 1;
      strcat( item_tmp, str );
    }
  }

  /* the file has been completely read,
     there must be an item not followed by an empty line
  */
  if ( is_reading_an_item == 1 ) {
    if ( get_item_from_medline_string( &myItem, item_str ) != 1 ) {
      if ( _verbose_ )
	fprintf( stderr, "%s: unable to get item in '%s'\n", proc, filename );
      release_item( &myItem );
      free( str );
      fclose( f );
      return( 0 );
    }
    if ( add_bibliographic_item_to_list( list_of_items, &myItem, 
					 filename ) != 1 ) {
      if ( _verbose_ )
	fprintf( stderr, "%s: unable to add item to list in '%s'\n", proc, filename );
      release_item( &myItem );
      free( str );
      fclose( f );
      return( 0 );
    }
    n++;
  }


  release_item( &myItem );
  free( str );
  fclose( f );
  return( n );
}





/********************************************************************************
 *
 * END OF MEDLINE READING
 *
 * CONTEXT STUFF
 *
 ********************************************************************************/



void print_predefined_strings( char **str ) 
{
  int i;
  char *t;

  for ( i=0; strncmp( str[i], "XXX", 3 ) != 0; i++ ) {
    /*
      fprintf( stdout, "%s\n", str[i] );
    */
    for ( t = str[i]; *t != '\0'; t++ ) {
      switch( *t ) {
      case '\n' : fprintf( stdout, "\\n" ); break;
      case '\t' : fprintf( stdout, "\\t" ); break;
      default :
	fprintf( stdout, "%c", *t );
      }
    }
    fprintf( stdout, "\n" );
  }
}


void load_predefined_strings( typeArgDescription *customization_strings, 
			      char **str ) 
{
  char *proc = "load_predefined_strings";
  int i, j;
  char *item_str = NULL;
  typeString  myString;
  int trs_is_done;
  int int_to_be_read = 0;
  int n_unrecognized = 0;
  
  item_str= (char*)malloc( ( LONG_STR_LENGTH ) * sizeof( char ) );
  if ( item_str == NULL ) {
    if ( _verbose_ )
      fprintf( stderr, "%s: unable to allocate string\n", proc );
    return;
  }

  for ( i=0; strncmp( str[i], "XXX", 3 ) != 0; i++ ) {

    if ( is_line_a_bibtex_string( str[i] ) != 1 ) {
      fprintf( stderr, "%s: bad string (#%d) = '%s'\n",
	       proc, i, str[i] );
      continue;
    }


    strncpy( item_str, str[i], LONG_STR_LENGTH );
    init_string( &myString );
    if ( get_string_from_predefined_string( &myString, item_str ) != 1 ) {
      fprintf( stderr, "%s: unable to extract string from '%s'\n",
	       proc, str[i] );
      continue;
    }

    myString.length = strlen( myString.key );

    for ( trs_is_done=0, j=0; trs_is_done==0 
	    && strncmp( customization_strings[j].key, "XXX", 3 ) != 0; j++ ) {

      if ( strncmp( myString.key,
		    customization_strings[j].key, 
		    strlen( customization_strings[j].key ) ) == 0 ) {

	switch ( customization_strings[j].type ) {
	  
	default :
	  if ( _warning_ )
	    fprintf( stderr, "%s: unhandled type for key '%s'?\n",
		     proc, customization_strings[j].key );
	  break;
	  
	case _INT_ :
	  
	  if ( sscanf( myString.str, "%d", &int_to_be_read ) != 1 ) {
	    if ( _warning_ ) {
	      fprintf( stderr, "%s: unable to get new value for '%s'\n",
		       proc, customization_strings[j].key );
	      fprintf( stderr, "         [%s]\n", myString.str );
	    }
	  }
	  else {
	    *((int*)customization_strings[j].arg) = int_to_be_read;
	  }
	  trs_is_done = 1 ;
	  break;
	  
	case _STRING_ :
	  if ( update_string( (char**)customization_strings[j].arg, myString.str, 0 ) == 1 )
	    trs_is_done = 1 ;
	  else {
	    fprintf( stderr, "%s: unable to allocate string for key '%s'\n",
		     proc, customization_strings[j].key );
	    continue;
	  }
	  break;
	  
	} /* switch ( customization_strings[j].type ) */
	
      }
    }
    if ( trs_is_done == 0 ) {
      if ( n_unrecognized == 0 ) fprintf( stderr, "\n" );
      fprintf( stderr, "%s: unrecognized key '%s' \n", proc, myString.key );
      n_unrecognized ++;
    }
  }

  if ( n_unrecognized != 0 ) fprintf( stderr, "\n" );
  free( item_str );
}



void load_predefined_style( typeArgDescription *customization_strings,
			    enumPredefinedStyle style ) 
{
  char *proc = "load_predefined_style";
  int i;
  char str[256];
  
  switch ( style ) {

  default :
  case _NO_STYLE_ :
    load_predefined_strings( customization_strings, 
			     default_environment_description_none );
    load_predefined_strings( customization_strings, 
			     default_generic_file_description_none );
    load_predefined_strings( customization_strings, 
			     default_specific_file_description_none );
    load_predefined_strings( customization_strings, 
			     default_bibitem_description_none );
    break;

  case _LAYOUT_ :
    for ( i=0; strncmp( customization_strings[i].key, "XXX", 3 ) != 0; i++ ) {

      switch ( customization_strings[i].type ) {
	  
      default :
	if ( _warning_ )
	  fprintf( stderr, "%s: unhandled type for key '%s'?\n",
		   proc, customization_strings[i].key );
	break;
	  
      case _INT_ :
	break;

      case _STRING_ :
	if ( strcmp( customization_strings[i].key, "filename_extension" ) == 0 ) {
	  if ( update_string( (char**)customization_strings[i].arg,
			   "layout", 0 ) == 0 ) {
	    fprintf( stderr, "%s: unable to allocate string for key '%s'\n",
		     proc, customization_strings[i].key );
	  }
	  continue;
	}
	else if ( strcmp( customization_strings[i].key, "directory_authors" ) == 0 
		  || strcmp( customization_strings[i].key, "directory_biblio" ) == 0 
		  || strcmp( customization_strings[i].key, "directory_categories" ) == 0 
		  || strcmp( customization_strings[i].key, "directory_keywords" ) == 0 
		  || strcmp( customization_strings[i].key, "directory_years" ) == 0 
		  || strcmp( customization_strings[i].key, "directory_icons" ) == 0 
		  || strcmp( customization_strings[i].key, "filename_complete_biblio" ) == 0 
		  || strcmp( customization_strings[i].key, "filename_index" ) == 0 
		  || strcmp( customization_strings[i].key, "filename_extension" ) == 0 ) {
	  if ( update_string( (char**)customization_strings[i].arg,
			   customization_strings[i].key, 0 ) == 0 ) {
	    fprintf( stderr, "%s: unable to allocate string for key '%s'\n",
		     proc, customization_strings[i].key );
	  }
	  continue;
	}
	else {
	  sprintf( str, "<%s>\n", customization_strings[i].key );
	  if ( update_string( (char**)customization_strings[i].arg, str, 0 ) == 0 ) {
	    fprintf( stderr, "%s: unable to allocate string for key '%s'\n",
		     proc, customization_strings[i].key );
	  }
	  continue;
	}
      }
    }
    break;

   case _BIBTEX_ :
    load_predefined_strings( customization_strings, 
			     default_environment_description_bibtex );
    break;
   case _LATEX_ :
    load_predefined_strings( customization_strings, 
			     default_environment_description_latex );
    load_predefined_strings( customization_strings, 
			     default_generic_file_description_latex );
    load_predefined_strings( customization_strings, 
			     default_specific_file_description_latex );
    load_predefined_strings( customization_strings, 
			     default_bibitem_description_latex );
    break;
   case _TXT_ :
    load_predefined_strings( customization_strings, 
			     default_environment_description_txt );
    load_predefined_strings( customization_strings, 
			     default_generic_file_description_txt );
    load_predefined_strings( customization_strings, 
			     default_specific_file_description_txt );
    load_predefined_strings( customization_strings, 
			     default_bibitem_description_txt );
    break;
  case _HTML_ :
    load_predefined_strings( customization_strings, 
			     default_environment_description_html );
    load_predefined_strings( customization_strings, 
			     default_generic_file_description_html );
    load_predefined_strings( customization_strings, 
			     default_specific_file_description_html );
    load_predefined_strings( customization_strings, 
			     default_bibitem_description_html );
    break;
  }
}






/************************************************************
 *
 * END OF CONTEXT STUFF
 *
 * MISC MANAGEMENT
 *
 ************************************************************/





void set_verbose_in_read()
{
  _verbose_ = 1;
}
void unset_verbose_in_read()
{
  _verbose_ = 0;
}

void set_warning_in_read()
{
  _warning_ = 1;
}
void unset_warning_in_read()
{
  _warning_ = 0;
}

void set_debug_in_read()
{
  _debug_ = 1;
}
void unset_debug_in_read()
{
  _debug_ = 0;
}

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# StarPU --- Runtime system for heterogeneous multicore architectures.
#
# Copyright (C) 2016-2024 Université de Bordeaux, CNRS (LaBRI UMR 5800), Inria
#
# StarPU is free software; you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation; either version 2.1 of the License, or (at
# your option) any later version.
#
# StarPU is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
#
# See the GNU Lesser General Public License in COPYING.LGPL for more details.
#

import sys
import re

def convert(fin, fout, filename, printall):
    tline = ""
    toprint=False
    for line in fin.readlines():
        if re.match(".*</title>", line):
            if printall:
                line = line.replace("</title>", "")
                fout.write("---\n")
                fout.write("layout: default\n")
                fout.write("title: " + line)
                fout.write("permalink: " + filename.replace("./", "publications/", 1) + "\n")
                fout.write("full-width: True\n")
                fout.write("---\n\n")
        elif line.startswith("<h4>"):
            toprint=True
            if printall:
                line = line.replace("<h4>", "## ").replace("</h4> ", "")
                if "&nbsp;" in line:
                    line = "## StarPU Publications\n"
                fout.write(line)
            else:
                name = line.replace("<h4>", "").replace("</h4> ", "").strip()
                fout.write("<a name=\"Publications" + name.replace(" ", "") + "\"></a>\n");
                fout.write("## " + name + "\n")
        elif re.match(".*<h3>.*", line):
            if printall:
                line = line.replace(" <h3>", "### ").replace("</h3>", "")
                fout.write(line)
        elif "<pre>" in line or "<hr" in line:
            toprint=False
        elif "</pre>" in line:
            toprint=True
        elif re.match(".*<a name.*", line):
            fout.write("- " + line.replace("</a>", "</a>\n"))
        elif "<table" in line or "</table" in line or "<tr" in line or "</.tr" in line or "<td" in line:
            if printall:
                fout.write(line)
        elif re.match(".*<ol>.*", line) or re.match(".*</ol>.*", line) or re.match(".*<li>.*", line) or re.match(".*</li>.*", line) or re.match("Keyword.*", line):
            True
        elif re.match(".*BACK TO INDEX.*", line) or "Disclaimer" in line:
            if toprint:
                raise StopIteration
        elif re.match(".*WWW.*", line):
            if toprint:
                url = line.replace('[<a href="', '').replace('">WWW</a>]\n', '')
                fout.write("<a title=\"Web link\" href=\"" + url + "\" target=\"_blank\" onclick=\"return trackOutboundLink('" + url + "');\"><i class=\"fa fa-globe\" aria-hidden=\"true\"></i></a>&nbsp;\n")
        elif re.match(".*PDF.*", line):
            if toprint:
                pdf = line.replace('[<a href="', '').replace('">PDF</a>]\n', '')
                fout.write("<a title=\"PDF\" href=\"" + pdf + "\" target=\"_blank\" onclick=\"return trackOutboundLink('" + pdf + "');\"><i class=\"fas fa-file-pdf\" aria-hidden=\"true\"></i></a>\n")
        elif re.match(".*POSTSCRIPT.*", line):
            if toprint:
                bib = line.replace('[<a href="', '').replace('">POSTSCRIPT</a>]\n', '')
                fout.write("<a title=\"BIB\" href=\"" + bib + "\" target=\"_blank\" onclick=\"return trackOutboundLink('" + bib + "');\"><i class=\"fa fa-book\" aria-hidden=\"true\"></i></a>\n")
        else:
            if toprint:
                line = line.replace("<strong>", "**").replace("</strong>", "**").replace("<em>", "*").replace("</em>", "*")
                fout.write(line)

def createMainPage():
    with open("starpu.md", "w") as fout:
        for html in ("out/Keyword/GENERAL-PRESENTATIONS.html",
                     "out/Keyword/ON-COMPOSABILITY.html",
                     "out/Keyword/ON-PARALLEL-TASKS.html",
                     "out/Keyword/ON-RECURSIVE-TASKS.html",
                     "out/Keyword/ON-SCHEDULING.html",
                     "out/Keyword/ON-PERFORMANCE-VISUALIZATION.html",
                     "out/Keyword/ON-THE-C-EXTENSIONS.html",
                     "out/Keyword/ON-OPENMP-SUPPORT-ON-TOP-OF-STARPU.html",
                     "out/Keyword/ON-MPI-SUPPORT.html",
                     "out/Keyword/ON-MEMORY-CONTROL.html",
                     "out/Keyword/ON-PERFORMANCE-MODEL-TUNING.html",
                     "out/Keyword/ON-THE-SIMULATION-SUPPORT-THROUGH-SIMGRID.html",
                     "out/Keyword/ON-THE-CELL-SUPPORT.html",
                     "out/Keyword/PAPERS-RELATED-TO-STARPU.html",
                     "out/Keyword/ON-APPLICATIONS.html",
        ):
            with open(html, "r") as fin:
                try:
                    convert(fin, fout, html, False)
                except Exception:
                    pass

if (len(sys.argv) >= 2):
    filename=sys.argv[1].replace(".html","")
    with open(filename+".md", "w") as fout:
        with open(filename+".html", "r") as fin:
            try:
                convert(fin, fout, filename+".html", True)
            except StopIteration:
                pass
else:
    createMainPage()


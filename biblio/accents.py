#!/usr/bin/env python3
# StarPU --- Runtime system for heterogeneous multicore architectures.
#
# Copyright (C) 2020-2021 Université de Bordeaux, CNRS (LaBRI UMR 5800), Inria
#
# StarPU is free software; you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation; either version 2.1 of the License, or (at
# your option) any later version.
#
# StarPU is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
#
# See the GNU Lesser General Public License in COPYING.LGPL for more details.
#
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import codecs
import os

html = []
for root, dirs, files in os.walk("out", topdown=False):
    html.extend([os.path.join(root, name) for name in files if name.endswith(".html")])

for f in html:
    with open("xx", "w") as fout:
        with codecs.open(f, 'r', 'iso-8859-1') as fin:
            for c in fin.read():
                fout.write(c)
    os.rename("xx", f)



#!/bin/bash

rootdir=$(realpath $(dirname $0))
publicdir=$rootdir/../contents/publications

mkdir -p $publicdir/$(dirname $1)

xbase=$(basename $1 .)
ishtml=$(basename $xbase html)
if test "$ishtml" == "$xbase"
then
    echo copying $1
    cp $1 $publicdir/$(dirname $1)/
else
    echo processing $1
    sed -i 's;<br.*/>;;' $1
    $rootdir/read.py $1
    mv $(dirname $1)/$(basename $1 html)md $publicdir/$(dirname $1)/$(basename $1 html)md
fi

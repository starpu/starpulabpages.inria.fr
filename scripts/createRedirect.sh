#!/bin/bash
# StarPU --- Runtime system for heterogeneous multicore architectures.
#
# Copyright (C) 2020-2024 Université de Bordeaux, CNRS (LaBRI UMR 5800), Inria
#
# StarPU is free software; you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation; either version 2.1 of the License, or (at
# your option) any later version.
#
# StarPU is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
#
# See the GNU Lesser General Public License in COPYING.LGPL for more details.
#

rootdir=$(realpath $(dirname $0))
publicdir=$(realpath $rootdir/../public)

createRedirect()
{
    cat $rootdir/redirect.txt | grep -v '#' | while read line
    do
	src=$(echo $line | awk '{print $1}')
	dst=$(echo $line | awk '{print $2}')
	echo create redirect from $src to $dst
	mkdir -p $publicdir/$(dirname $src)
	(cat <<EOF
<html>
<head>
<title>Page has moved!</title>
</head>
<body>
<a href="$dst">Page has moved</a>
<script>
let current_full_url = window.location
  if (String(current_full_url).indexOf("#") < 0) {
    window.location = "$dst"
  }
  else {
    window.location = "$dst" + String(current_full_url).substring(String(current_full_url).indexOf("#"));
  }
</script>
</body>
</html>
EOF
	) > $publicdir/$src
    done
}

createRedirect


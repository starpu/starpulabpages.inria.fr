#!/bin/bash
# StarPU --- Runtime system for heterogeneous multicore architectures.
#
# Copyright (C) 2020-2024 Université de Bordeaux, CNRS (LaBRI UMR 5800), Inria
#
# StarPU is free software; you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation; either version 2.1 of the License, or (at
# your option) any later version.
#
# StarPU is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
#
# See the GNU Lesser General Public License in COPYING.LGPL for more details.
#

rootdir=$(realpath $(dirname $0))
publicdir=$(realpath $rootdir/../public)

createRedirect()
{
    cat $rootdir/redirect.txt | grep -v '#' | while read line
    do
	src=$(echo $line | awk '{print $1}')
	dst=$(echo $line | awk '{print $2}')
	echo create redirect from $src to $dst
	mkdir -p $publicdir/$(dirname $src)
	(cat <<EOF
<html>
<head>
<title>Page has moved!</title>
</head>
<body>
<a href="$dst">Page has moved</a>
<script>
let current_full_url = window.location
  if (String(current_full_url).indexOf("#") < 0) {
    window.location = "$dst"
  }
  else {
    window.location = "$dst" + String(current_full_url).substring(String(current_full_url).indexOf("#"));
  }
</script>
</body>
</html>
EOF
	) > $publicdir/$src
    done
}

updateFileLink()
{
    dst=$(echo "https://files.inria.fr/starpu$1" | sed 's;file:///files;;')
    echo "update file link from $1 to $dst"
    dir=$(echo $1 | sed 's;file:///files/;;')
    case "$1" in
	*pdf) for f in $(grep -rsl $1 $publicdir) ; do sed -i 's;href="'$1'";href="'$dst'";g' $f ; done ;;
	*gz)  for f in $(grep -rsl $1 $publicdir) ; do sed -i 's;href="'$1'";href="'$dst'";g' $f ; done ;;
	*)    for f in $(grep -rsl $1 $publicdir) ; do sed -i 's;href="'$1'";href="'$dst'";g' $f ; done
    esac
}

updateFileLinks()
{
    for x in $(grep -rs 'href="file:///files/' $publicdir| tr '"' '\012' | grep file: | sort | uniq)
    do
	updateFileLink $x
    done
}

convertOrgFiles()
{
    echo processing org from $1
    dst=$1
    files=(*.org)
    if [ -e "${files[0]}" ];
    then
	touch *.org
	emacs --batch --no-init-file --load $rootdir/publish.el --funcall org-publish-all > /tmp/org.out 2>&1
	ret=$?
	error=$(grep "Unable to resolve link" /tmp/org.out)
	if test -n "$error" -o $ret != 0
	then
	    cat /tmp/org.out
	    echo error
	    exit 1
	fi
	for f in *org
	do
	    mv -f $(basename $f org)html $publicdir/$dst
	    if test "$2" != ""
	    then
		sed -e 's;\(<div id="text-table-of-contents.*">\);\1<ul><li><a href="'$2'">Back to main page</a></li></ul>;' -i $publicdir/$dst/$(basename $f org)html
	    fi
	done
    fi
    for f in *
    do
	noorg=$(basename $f .org)
	if test -f "$f" -a "$noorg" == "$f"
	then
	    cp -rp $f $publicdir/$dst
	fi
    done
}

processOrgFiles()
{
    cd contents
    convertOrgFiles ./
    cp -rp css/ $publicdir
    cp -rp images/ $publicdir

    cd help
    mkdir $publicdir/help
    convertOrgFiles help ../index.html

    cd ../tutorials
    mkdir $publicdir/tutorials
    for d in *
    do
	if test -d $d
	then
	    cd $d
	    mkdir $publicdir/tutorials/$d
	    convertOrgFiles tutorials/$d ../../tutorials.html
	    for f in *
	    do
		noorg=$(basename $f .org)
		if test "$noorg" == "$f"
		then
		    cp -rp $f $publicdir/tutorials/$d
		fi
	    done
	    cd ../
	else
	    cp $d $publicdir/tutorials
	fi
    done

    cd ../publications
    mkdir $publicdir/publications
    convertOrgFiles publications ../publications.html
    for x in Biblio Keyword Year
    do
	mkdir $publicdir/publications/$x
	cd $x
	convertOrgFiles publications/$x ../index.html
	cd ..
    done
    cd ..

    cd $publicdir
    for f in *html
    do
	if test "$f" == "index.html"
	then
	    grep "<li>.*LINK;" $f | while read line
	    do
		title=$(echo $line | awk -F';' '{print $2}')
		url=$(echo $line | awk -F';' '{print $3}' | sed 's;</a></li>;;')
		sed -e 's&<li><a href="#.*">\(.*\. \)LINK;'"$title"'.*&<li><a href="'$url'">\1'"$title"'</a></li>&' -i $f
		sed -e 's&\(<h. id=".*">\)\(.*\)LINK;'"$title"';\(.*\)\(</h.>\)&\1\2'"$title"'\4<a href="'$url'">'"$title"'</a>&' -i $f
	    done
	else
	    sed -e 's;\(<div id="text-table-of-contents.*">\);\1<ul><li><a href="./index.html">Back to main page</a></li></ul>;' -i $f
	fi

	sed -e 's/@@html:&lt;red&gt;@@/<red>/g' \
	    -e 's/@@html:&lt;\/red&gt;@@/<\/red>/g' \
	    -e 's/@@html:&lt;blue&gt;@@/<blue>/g' \
	    -e 's/@@html:&lt;\/blue&gt;@@/<\/blue>/g' \
	    -e 's/@@html:&lt;green&gt;@@/<green>/g' \
	    -e 's/@@html:&lt;\/green&gt;@@/<\/green>/g' \
	    -i $f
    done

    cd ..
}

rm -rf public
mkdir public
processOrgFiles
createRedirect
updateFileLinks

# needed by publications/
#cp contents/style.css public/
#cp -rp contents/publications public

cd contents
for f in *
do
    if test -f $f
    then
	noorg=$(basename $f .org)
	if test "$noorg" == "$f"
	then
	    cp $f ../public
	fi
    fi
done

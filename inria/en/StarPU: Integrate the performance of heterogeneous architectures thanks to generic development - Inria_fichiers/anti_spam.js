
var uncrypt_emails = new Array();

window.addEvent( 'domready', function() {

	uncrypt_links();
	
});


function uncrypt_links(){
	
	var links = $$('a.uncrypt');
	
	links.each(function(item,i){
		var key = item.get('href');
		var cryptedArray = uncrypt_emails[key];
		
		// Décryptage
		var ch = 'm' + String.fromCharCode(97) + 'ilt' + String.fromCharCode(111) + ':';
		
		for(i = cryptedArray.length - 1; i >= 0; i--){
			var tmp = cryptedArray[i].split(':');
			
			ch += String.fromCharCode(tmp[0]);
			
			if(tmp[1] != ''){
				ch += tmp[1];
			}
		}
		
		item.set('hr' + 'ef', ch);
	});
	
}
/*
*  Page recherche 
*/
function mr_ted(url_ted, type, page_suivante, page_precedente_cible, page_precedente_code, url_courante){

	//alert(url_ted+" ==type== "+type+" ==page_suivante== "+page_suivante+" ==page_precedente== "+page_precedente_cible+" ==url_courante== "+url_courante+" ====");
	var view = '/(view)';	
	
	// Traitements sur l'URL courante
	var currentURI = window.location;
	var currentURIObject 			= new URI(currentURI);
	var LOV2_get 						= currentURIObject.getData('LOV2');
	var LOV6_get 						= currentURIObject.getData('LOV6');
	var LOV5_get 						= currentURIObject.getData('LOV5');
	var ID_get	 						= currentURIObject.getData('ID');
	var id_get							= currentURIObject.getData('id');
	var nPostingTargetID_get	= currentURIObject.getData('nPostingTargetID');
	var ContractType_get	 		= currentURIObject.getData('ContractType');
	var sActivationToken 						= currentURIObject.getData('sActivationToken');
	
	//nouveau parametre du composant
	var LOV28_get 			= currentURIObject.getData('LOV28');
	var LOV3_get 				= currentURIObject.getData('LOV3');
	var LOV1_get 				= currentURIObject.getData('LOV1');
	var statlog_get	 				= currentURIObject.getData('statlog');
	var sort_get	 		= currentURIObject.getData('sort');
	var nDepartementID_get	 		= currentURIObject.getData('nDepartementID');
	var Resultsperpage_get	 		= currentURIObject.getData('Resultsperpage');


	
	// Traitements sur l'URL Mr Ted
	var tedURIObject = new URI(url_ted);
	if ((LOV2_get != null)  && (LOV2_get != '')) 						{   tedURIObject.setData('LOV2', 	dedoublonnage(LOV2_get));				}
	if ((LOV6_get != null) && (LOV6_get != '')) 						{   tedURIObject.setData('LOV6', 	dedoublonnage( LOV6_get));						}
	if ((LOV5_get != null) && (LOV5_get != '')) 						{   tedURIObject.setData('LOV5', 	dedoublonnage(LOV5_get));						}
	if ((ID_get != null) && (ID_get != '')) 								{   tedURIObject.setData('ID', 	dedoublonnage(ID_get));									}
	if ((id_get != null) && (id_get != '')) 								{   tedURIObject.setData('id', 	dedoublonnage(id_get));									}
	if ((nPostingTargetID_get != null) && (nPostingTargetID_get != '') )	{   tedURIObject.setData('nPostingTargetID_get', dedoublonnage(nPostingTargetID_get));	}
	if ((ContractType_get != null) && (ContractType_get != '') ) 				{   tedURIObject.setData('ContractType', dedoublonnage(ContractType_get));				}
	if ((sActivationToken != null) && (sActivationToken != '') ) 				{   tedURIObject.setData('sActivationToken', dedoublonnage(sActivationToken));				}
		
	// traitement des nouveaux parametres 
	if ((LOV28_get != null)  && (LOV28_get != '')) 						{   tedURIObject.setData('LOV28', 	dedoublonnage(LOV28_get));				}
	if ((LOV3_get != null) && (LOV3_get != '')) 							{   tedURIObject.setData('LOV3', 	dedoublonnage( LOV3_get));							}
	if ((LOV1_get != null) && (LOV1_get != '')) 							{   tedURIObject.setData('LOV1', 	dedoublonnage(LOV1_get));							}
	if ((statlog_get != null) && (statlog_get != '')) 								{   tedURIObject.setData('statlog', 	dedoublonnage(statlog_get));								}
	if ((nDepartementID_get != null) && (nDepartementID_get != '') )	{   tedURIObject.setData('nDepartementID', dedoublonnage(nDepartementID_get));	}
	if ((Resultsperpage_get != null) && (Resultsperpage_get != '') )	{   tedURIObject.setData('Resultsperpage', dedoublonnage(Resultsperpage_get));	}

	
	// on modifie le type de page sur laquel on est. (page courante ...)
	tedURIObject.setData('component', type);
		
	// Je construis la partie de l url pour la page suivante 
	if (page_suivante != '')
	{
		var url_page_suivante = "/"+url_courante+view+"/"+page_suivante;
		//conf de base 1
		//tedURIObject.setData('page', '(view)/'+page_suivante);
		//conf de base 2
		
		/*
		if (type != 'lay2012166_lst400a' ){
			tedURIObject.setData('page', url_page_suivante);
		}
		else{
			tedURIObject.setData('page', "/layout/set/ajax_html/content/view/mr_ted_login/2"+view+"/"+page_suivante);
		}
		*/
		tedURIObject.setData('page', url_page_suivante);
		
	}	
	
	if (page_precedente_cible != '')
	{
		var index = "/"+url_courante+view+"/"+page_precedente_cible;	
		var tedURIObject = new URI(tedURIObject);
		tedURIObject.setData(page_precedente_code,index);
	}	

	// oN reconstruit un objet URI pour obtenir le parametre pages
	//var url_long_ted = "https://v20.recruitmentplatform.com/syndicated/lay/laydisplay.cfm?component="+type+"&ID=PUQFK026203F3VBQB6G68LO2G&page="+directory+file+view+"/"+page_suivante+"&SUBDEPT1=2&ContractType=4548";
	//var test = "https://v20.recruitmentplatform.com/syndicated/lay/laydisplay.cfm?component=lay2012166_lst400a&ID=PGTFK026203F3VBQB6G68LONZ&page=details.html&pages=index.html&lg=FR&mask=campaign&LOV5=4510";
	//var test = "https://v20.recruitmentplatform.com/syndicated/lay/laydisplay.cfm?component=lay2012166_lst400a&ID=PUQFK026203F3VBQB6G68LO2G&page=details.html&pages=index.html&SUBDEPT1=2&LG=FR&statlog=1&ContractType=5033&LOV1=All&LOV2=All&LOV3=All&keywords";

	//alert(tedURIObject);
	
	
	document.write('<script type="text/javascript" src='+unescape(tedURIObject)+'></script>');
	//document.write('<script language="javascript" type="text/javascript" src="https://emea2.recruitmentplatform.com/syndicated/lay/laydisplay.cfm?id=Q9GFK026203F3VB8M79LOV4RP&component=lay2012166_jdesc100a&LG=FR&Resultsperpage=20&nPostingID=3764&nPostingTargetID=9473&option=52&sort=DESC&nDepartmentID=2"></script>');
	
	//document.write('<script language="javascript" type="text/javascript" src="https://emea2.recruitmentplatform.com/syndicated/lay/laydisplay.cfm?ID=Q9GFK026203F3VB8M79LOV4RP&component=lay2012166_src350a&LG=FR&browserchk=no&page=list.html"></script>');
}

/*
	Ouvre une popup sur la page de login
*/
function mr_ted_login(url_ted, url_open){

	var tedURIObject = new URI(url_ted);
	var id = tedURIObject.getData('ID');
	var lang = tedURIObject.getData('lg');
	var redirect = new URI(url_open + '/(view)/login.html');
	
	if(lang == null || lang == ''){
		lang = 'FR';
	}
	
	if(id == null || id == '')
		return false;
		
	redirect.setData('ID', id);
	redirect.setData('LG', lang);
	
	//window.open(redirect, "Login", "location=yes, menubar=no, resizable=yes, width=613, height=314");
	window.open(redirect, "Login");
	return false;
}

function get_component(url_ted){
	
	var myURI_ted = new URI(url_ted);
	// Recuperation parametre GET 
	var component = myURI_ted.getData('component');
	return component;
}


function dedoublonnage(value) {
	if (value instanceof Array) {
		return value[0];
	} else {
		value_temp = value.split(',');
		return value_temp[0];
	}
}



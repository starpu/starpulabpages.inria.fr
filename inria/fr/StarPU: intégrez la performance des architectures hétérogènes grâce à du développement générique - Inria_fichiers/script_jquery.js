jQuery(document).ready(function( $ ) {

    $(".btn_search").click(function(e){
        e.preventDefault();
        height = 250 + jQuery('#menu').height();
        if($('.sommaire_centre').length){
            height = $('.sommaire_centre').height() + jQuery('#menu').height();
        }
        else if($('.titre.no_background').length){
            height = $('.titre.no_background').height() + jQuery('#menu').height();
        }
        $('#search p').css('line-height',height+'px');
        $('#search').show().height(height);
        $('#search_form').focus();
    });

    $(".btn_search_close").click(function(e){
        e.preventDefault();
        $('#search').hide();
    });

    nav_mobile_search();
    if($(".visio").length>0){visionneuse2();}

    if(document.getElementById("amiando1") != null)
    {
        var iframe_height = document.getElementById("amiando1").getElementsByTagName("iframe")[0].attributes.getNamedItem("height").value;
        var iframe_width = document.getElementById("amiando1").getElementsByTagName("iframe")[0].attributes.getNamedItem("width").value;
        var iframe_ratio = (parseInt(iframe_height) + 10) / parseInt(iframe_width);

        resizeElem($(".amiando"),iframe_ratio);
    }

    resizeElem($("#diapo_slider"),0.67);
    setTimeout(function(){equal_block ($(".bando"),$(".bloc"))},500);

    $(window).bind("resize orientationchange", function(e) {
        nav_mobile_search();
        resizeElem($(".amiando"),iframe_ratio);
        resizeElem($("#diapo_slider"),0.67);
        equal_block ($(".bando"),$(".bloc"));
    });

    $('.flexslider').flexslider({
        animation: "slide",
        slideshow: false,
        selector: ".slides > .view",
        animationLoop: false,
        itemWidth: 166,
        itemMargin: 10,
        minItems: 1,
        maxItems: 5,
        move: 1,
        controlNav: false,
        start: function(){ equal_block ($(".bando"),$(".bloc"));}
    });

    $('#diapo_carousel').flexslider({
        animation: "slide",
        controlNav: false,
        animationLoop: false,
        itemWidth: 70,
        itemMargin: 5,
        asNavFor: '#diapo_slider'
    });

    $('#diapo_slider').flexslider({
        animation: "fade",
        controlNav: false,
        animationLoop: false,
        sync: "#diapo_carousel"
    });
    $('#visionneuse').flexslider({
        animation: "fade"
    });
    $('#visionneuse').hover(
        function(){
            $('.copyright').show();
        },
        function(){
            $('.copyright').hide();
        }
    );
    /*$('.vertical_carrousel').flexslider({
        animation: "slide",
        direction: "vertical",
        minItems: 4,
        slideshow: false,
        animationLoop: false,
        mousewheel: true
    });*/




    ////////////////////////////////////////////////////////////////////////////////////////////////////////////

    // Resize des elements aux dimensions fixes
    function resizeElem(elem, ratio) {
        if (elem.length>0) {
            var width = elem.width();
            // var ratio = elem.width() / elem.height();
            var height = width * ratio;
            elem.css('height', height);
        }
    }

      /* 
    Egaliseur de blocs
        @param zone = zone de référence dans laquelle les blocs se resizent
        @param bloc = bloc à resizer
    */
    function equal_block (zone, str_bloc){
        if (zone.length>0){
            zone.each( function(index, el) {
                var max_height = 0;
                var bloc = $(this).find(str_bloc);
                bloc.css('min-height', '0');
                bloc.each( function(index, el) {
                    var this_height = parseInt($(this).css("height"));
                    max_height = Math.max(max_height, this_height);
                });
                bloc.css('min-height', max_height);
            });
        }
    }


    // Navigation mobile > ouverture de la recherche au clic sur picto 
    function nav_mobile_search() {
        if($(window).width()<640){
            $('.btn_search').bind("touchstart click", function(e) {
                e.preventDefault();
                anchor=$(this).attr("href");
                $(anchor).toggleClass("open");
            })     
        }
    }

    // // Navigation mobile > sous-menu
    // function nav_mobile_subnav() {
    //     if($(window).width()<640){
    //         $('.ss_menu .col > ul > li').each( function(el) {
    //             if(el.find('ul').length > 0 ){
    //                 el.first('a').bind('click', function(e) {
    //                     e.preventDefault();
    //                     $(this).parent().toggleClass("open");
    //                 });
    //             }
    //         });
    //     }
    // }


    /** 
        visionneuse
    */

    function visionneuse2(){
        
        //variables de la visio
        var nav_view_table = [];
        var counter = 0;
        var max_counter = 0;
        var direction = 1;


        $(".visio .view").each(function(index, el) {
            nav_view_table[index] = $(this);
            max_counter=index;
        });
        //$('.visio_nav').append('<span class="counter"></span>');

        $( "<span class='counter'></span>" ).insertAfter( ".visio_nav .nav_prev" );

        $('.visio_nav .counter').html(counter+1+'/'+(max_counter+1));

        //timer  pour defilement automatique
        var timer=setInterval(function() {change_slide(direction);}, 5000);
        
        //changement de diapo
        function change_slide(direction){
            if(direction<0){
                if (counter==0) { 
                    counter=max_counter; 
                }else{
                    counter=counter+direction;
                }
            }else{
                if(counter==max_counter) {
                    counter=0; 
                }else{
                    counter=counter+direction;
                }
            }
            $(".visio .view").removeClass("active");
            nav_view_table[counter].addClass("active");
            $('.visio_nav .counter').html(counter+1+'/'+(max_counter+1));
        }

        //actions des boutons
        $(".visio .nav_prev").click(function() {
            direction=-1;
            change_slide(direction);
            return false;
        });
        $(".visio .nav_next").click(function() {
            direction=1;
            change_slide(direction);
            return false;
        });
        $(".visio").mouseover(function() {clearInterval(timer);});
        $(".visio").mouseout(function() {timer=setInterval(function() {change_slide(direction);}, 11000);});
        
    }

});








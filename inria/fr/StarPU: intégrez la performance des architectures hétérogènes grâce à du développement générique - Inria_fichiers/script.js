

window.addEvent( 'domready', function() {

    if($$('a[rel=external]').length>0){
        initExternalLinks();
    }
    if($$(".chapitre").length>0){
        chapitre();
    }
	if($('sommaire_ancre')){
		sommaire_auto_ancre();
	}
    $$('body').addClass("js_active");
	
	if ($$(".ss_menu").length>0){
		column_height ($$(".ss_menu"),".col");
	}
	
    if ($$(".bando").length>0){
		column_height ($$(".bando"),".bloc");
	}
	
	if ($("wrap_contact")){
		loadFormulaire();
	}

	if (jQuery(".prefooter a")){
		jQuery(".prefooter a").hover(function(){
			var svgDoc = jQuery(this).find('object')[0].contentDocument;
			jQuery('.cls-1',svgDoc).hover().css("fill","#384257");
		},function(){
			var svgDoc = jQuery(this).find('object')[0].contentDocument;
			jQuery('.cls-1',svgDoc).hover().css("fill","#E63312");
		});
	}
	
});


/* 
Sommaire automatique dans les articles.
	-> Permet de construire un sommaire automatique avec ancre
	-> a partir des h3 existant dans le bloc XML
*/
function sommaire_auto_ancre() {

	var sommaire_ul = $('sommaire_ancre');
	var all_header = $('repereArticle').getElements('h3');
	
	var tab_li = '';
	var tabValue = [];

	
	
	all_header.each(function(item,i) {
		
		// On construit le sommaire auto
		tab_li = tab_li + '<li id="titre'+i+'"><a href="#section'+[i]+'">'+item.get('text')+'</a></li>';
		// On ajoute les id aux h3
		item.set('id', 'section'+[i]);
		
		// On ajoute lancre top aux H3 
		item.addClass('section_top');
		var p = new Element('p', {"class": "top"});
		var myAnchor = new Element("a", {
			"href": "#sommaire_ancre",
			"html": "Top",
			"title": "Go on top"
	
		});
		
		myAnchor.inject(p);
		p.inject(item , 'after' );
	
	});

	sommaire_ul.set('html', tab_li);

}

/* 
Chapitres : accordeon :  forme des sections pour H3 
*/
function chapitre() {
	
	// On ajoute les classe section au H3
	var all_element = $('repereArticle').getChildren();
	
	var tabElt = [];
	var tabSection = [];
	var cpt = 0;
	var new_div_section = false;

	all_element.each(function(item,i) {
		if (item.tagName != 'H3') {
			if( $chk( new_div_section ) ) 	{
				item.inject( new_div_section );
			}
		} else {
				new_div_section = new Element('div', {
						'class': 'section'
                });
				new_div_section.inject( item, 'after' );
				cpt++;
			}
	});
		
	// On forme les accordeons
    var myAccordion = new Accordion($$('.chapitre #repereArticle h3'), $$('.chapitre .section'), {
        display:-1,
        alwaysHide:true,
        onActive: function(toggler) {toggler.addClass('moins');},
        onBackground: function(toggler) {toggler.removeClass('moins');} 
    });
}


/**
* Emulateur de target=_blank,
* Pour concilier en aparence des exigences contradictoires
* (validation XHTML stricte & ouverture des liens externes dans une nouvelle fenêtre).
*/
function initExternalLinks()
{
	$$( 'a[rel=external]' ).each( function( item ) {
        item.target = '_blank';
    });
}


/* 
Egaliseur de blocs
    @param zone = zone de référence dans laquelle les blocs se resizent
    @param bloc = bloc à resizer
*/
function column_height (zone, str_bloc){
    zone.each( function(el) {
        var max_height = 0;
        var bloc = el.getChildren(str_bloc);
        bloc.each( function(item) {
            max_height = Math.max(max_height, (item.getSize().y-12 ));
        });
        //#27408 - The +100 is because of the twitter widget
        if(bloc[0].hasClass("home"))
        {
        	bloc.setStyle('min-height', max_height + 100);
        }
    });
}


/*
* Formulaire de contact AJAX
*/
function loadFormulaire() {
	
	$$('input[type=button]').addEvent('click', function(e) {
			
		/** On modifie la valeur du input type hidden pour savoir quel bouton on a clique, et savoir du coup a quel etape on se trouve  **/
		$('etape').set('value', e.target.get('name'));

		/**
		 * Prevent the submit event
		 */
		e.stop();
		
		/**
		 * This empties the log and shows the spinning indicator
		 */
		var moduleUrl = '/layout/set/ajax/contact/formulaire';
		if($('loadFormulaire_moduleUrl')!==null){
			var moduleUrlText = $('loadFormulaire_moduleUrl').textContent;
			if(moduleUrlText!="") moduleUrl = moduleUrlText;
		}
		var log = $('wrap_contact').addClass('ajax-loading');
		$('monFormulaire').set('send', {
								url: moduleUrl, 
								method: 'post',
								evalScripts: true, 
								onComplete: function(response) {
										log.removeClass('ajax-loading');
										log.set('html', response);
										loadFormulaire();
								}
		});
		$('monFormulaire').send();
					
	});
	
}

// Reconstitue les liens des images sur les fiches EPI (activité)
function checkEpiActivityLink(prefixe){
	var links = $$('.l_replace');

	links.each(function(item,i) {
		
		var tmp = item.getProperty('src');
		item.setProperty('src', prefixe + tmp);
	});
	
	
}



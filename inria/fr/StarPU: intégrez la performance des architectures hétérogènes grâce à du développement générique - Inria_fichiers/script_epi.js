/*
	Fonctionnement javascript de la partie domaines/themes/equipes de recherche EPI
*/

// Index du bloc à ouvrir par défaut
var ThemeEpidisplayIndex = null;

// Variable indiquant si l'on se trouve dans l'arbo d'un centre pour la classe css
var is_epi_in_arbo_center = false;

// Variable stockant le nom du filtre courant sur les années de publications
var filter_year_publications = "";

// Variable indiquant si l'on désactive tout l'accordéon sur les EPI
var use_accordion_epi = true;

// Variable indiquant si l'on utilise l'accordeon pour les domaines ou non
var use_accordion_domaine = true;

var titrePublication = "";

window.addEvent( 'domready', function() {
	if($$(".epi").length>0){
        theme(ThemeEpidisplayIndex);
    }
	if($('search_epi') != null && $('search_responsable') != null){
		init_autocomplete();
	}
	if($('filter_publication_list') != null){
		buildPublicationFilter();
	}
});

/* 
	Thematiques depliables
*/
function theme(displayIndex) {
    
    var toggler = $$('.epi h3');
    var content = $$('.epi .section');
    var show_item = -1;
	
	if(use_accordion_epi){
    
		if(use_accordion_domaine){
			var myAccordion = new Accordion(toggler, content, {
				show:show_item,
				alwaysHide:true,
				onActive: function(toggler) {toggler.addClass('moins');},
				onBackground: function(toggler) {toggler.removeClass('moins');}
			});
			
			// Ouverture d'un onglet spécifique
			if(displayIndex != null)
				myAccordion.display(displayIndex);
		}
		
		if($$('.epi h4').length>0){
			
			var toggler_n2 = $$('.epi h4');
			var content_n2 = $$('.epi .section ul');
			
			var myAccordion_n2 = new Accordion(toggler_n2, content_n2, {
				show:-1,
				alwaysHide:true,
				onActive: function(toggler_n2) {
					toggler_n2.addClass('moins');
					var parent = toggler_n2.getParent(); 
					parent.setStyle('height', 'auto');
				},
				onBackground: function(toggler_n2) {
					toggler_n2.removeClass('moins');
				}
			});
		}
	}
}

/*
	Fonction d'autocompletion du moteur de recherche par équipe projet / responsable
*/
function init_autocomplete(){

	var search_epi_word = $('search_epi');
	
	var cn = 'autocompleter-choices';
	if(is_epi_in_arbo_center)
		cn = 'autocompleter-choices center';
	
	var a1 = new Autocompleter.Request.HTML(search_epi_word, '/searchepi/team', {
		'indicatorClass': 'autocompleter-loading',
		'postData': {
			'extended': '1' // send additional POST data, check the PHP code
		},
		'injectChoice': function(choice) {
			// choice is one <li> element
			var text = choice.getFirst();
			// the first element in this <li> is the <span> with the text
			var value = text.innerHTML;
			// inputValue saves value of the element for later selection
			choice.inputValue = value;
			// overrides the html with the marked query value (wrapped in a <span>)
			text.set('html', this.markQueryValue(value));
			// add the mouse events to the <li> element
			this.addChoiceEvents(choice);
		},
		className: cn
	});
	
	var search_resp_word = $('search_responsable');
	
	new Autocompleter.Request.HTML(search_resp_word, '/searchepi/responsable', {
		'indicatorClass': 'autocompleter-loading',
		'postData': {
			'extended': '1', // send additional POST data, check the PHP code
			'in_center': is_epi_in_arbo_center
		},
		'injectChoice': function(choice) {
			// choice is one <li> element
			var text = choice.getFirst();
			// the first element in this <li> is the <span> with the text
			var value = text.innerHTML;
			// inputValue saves value of the element for later selection
			choice.inputValue = value;
			// overrides the html with the marked query value (wrapped in a <span>)
			text.set('html', this.markQueryValue(value));
			// add the mouse events to the <li> element
			this.addChoiceEvents(choice);
		},
		className: cn
	});
};

/*
	Fonction permettant de générer le filtre par année des publications
	en colonne de droite sur une page équipe projet
*/
function buildPublicationFilter(){

	var filterContainer = $('filter_publication_list');
	var filterList = null;
	var items = $('res_script').getChildren();
	var firstFilter = "";
	
	if(items.length == 0){
		filterContainer.destroy();
		return false;
	}
	
	if($('title_publications') != null)
	{
		titrePublication = $('title_publications').get('html');
	}

	items.each(function(item,i) {
		// On ajout les année à la liste de filtres
		if(item.hasClass('Rubrique')){
		
			item.setStyle('display', 'none');
		
			if(firstFilter == "")
				firstFilter = item.get('html').trim();
			
			if(filterList == null){
				filterList = new Element("ul");
				filterContainer.adopt(filterList);
			}
			
			var li = new Element("li");
			var a = new Element("a", {
				"href": "#",
				"html": item.get('html'),
				"events": {
					"click": function(){
						displayHideFilter(item.get('html').trim());
						return false;
					}
				}
			});
			
			li.adopt(a);
			filterList.adopt(li);
		}
	});
	
	displayHideFilter(firstFilter);
}

function displayHideFilter(text){
	
	var items = $('res_script').getChildren();
	if(items.length == 0){
		return false;
	}
	
	$('title_publications').set('html', titrePublication + ' ' + text);
	
	var currentYear = "";
	items.each(function(item,i) {
	
		if(item.hasClass('Rubrique')){
			currentYear = item.get('html').trim();
		}
		
		if(currentYear == filter_year_publications || filter_year_publications == ""){
			item.setStyle('display', 'none');
		}
		
		if(currentYear == text.trim()){
			if(! item.hasClass('Rubrique'))
				item.setStyle('display', 'block');
		}
		
	});
	
	filter_year_publications = text.trim();
}

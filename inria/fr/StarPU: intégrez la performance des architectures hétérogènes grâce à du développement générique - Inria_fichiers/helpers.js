
function array_key_exists ( key, search ) {
	if (!search || (search.constructor !== Array && search.constructor !== Object)){
        return false;
    } 
    return key in search;
}

function sleep(time){
	var d = new Date();
    var start = d.getTime();
	
    while(d.getTime() < start+time){
		d = new Date();
	}
    return;
}

// prototype constructor
if(typeof Object.beget !== 'function'){
	Object.beget = function(o){
		var F = function(){};
		F.prototype = o;
		return new F();
	};
}

function in_array(p_val, tab) {
    for(var i = 0; i < tab.length; i++) {
        if(tab[i] == p_val) {
            return true;
        }
    }
    return false;
}

﻿
// Contient les nodes_id des menus principaux
var mainMenus = new Array();
var currentTabNode = new Array();

function InriaMenu(menuLinkClass, menuPanelClass, menuItemClass, closeLinkName) {

	// Classe CSS des liens composant le menu
	var menuLinkClass = menuLinkClass; // .open_menu
	// Classe CSS de la zone d'affichage qui apparait
	var menuPanelClass = menuPanelClass; // .bloc_menu
	// Classe CSS identifiant les menus à afficher dans le panel
	var menuItemClass = menuItemClass; // .ss_menu
	// Identifiant du lien du menu actif
	var activeMenu = null;
	// Hauteur actuelle du menu
	var currentHeight = 0;
	
	// Onglets
	var onglets = new Array();
	var liens = new Array();
	var ongletsfx = new Array();
	
	// création du bouton close pour chaque onglets
    var btn_close = new Element('p', {id: 'close_menu'});
    var lien_close = new Element('a',({href: 'javascript:void(0);',text: closeLinkName}));
    btn_close.grab(lien_close);
    $$(menuPanelClass).grab(btn_close);
	$$(lien_close).addEvent('click', function(){
		hideMenu(activeMenu, true);
	});
	
	// Ajout des events sur les liens du menu
	$$(menuLinkClass).each(function(element) {
        var id = element.get('href');
        id = id.substr(1);
		element.set('id', 'main_' + id);
        element.set('href','javascript:void(0)');
        element.addEvent("click", clickMenu);
		
		// On stocke le menu
		var m = $('main_' + id);
		liens[id] = m;
		// On stock le bloc associé au menu
		var o = $(id);
		onglets[id] = o;
		ongletsfx[id] = new Fx.Tween(onglets[id], {'link': 'cancel'});
		ongletsfx[id].start('opacity', 0);
    });
	
	// Gestion du menu sélectionné
	for(var i = 0; i < mainMenus.length; i++){
		if(in_array(mainMenus[i], currentTabNode)){
			$('main_menu' + i).addClass('selected');
		}
	}
	
	/**
	* Action quand on clique sur un menu
	*/
	function clickMenu(event){
		var tmp = this.id.split('_');
		var id = tmp[1];
		if(activeMenu == id){ // Cas ou l'on reclick sur un menu déja ouvert => on le ferme
			hideMenu(id, true);
		}
		else{
			if(activeMenu != null) // Si autre menu actif, on le ferme
				hideMenu(activeMenu, false);
			displayMenu(id);
			activeMenu = id;
		}
	}
	
	/**
	* Affiche un onglet du menu principal
	*/
	function displayMenu(id){
		if(activeMenu == null){
			$$('.nav .selected').toggleClass('selected_off');
			$$('.nav .selected').toggleClass('selected');
		}
		$('main_' + id).addClass('clicked');
		var height = $(id).getSize().y;
		//$(id).set( 'styles' , { 'color' : '#42A2D2'} ) ;
		ongletsfx[id].start('opacity', 1);
		$$(menuPanelClass).tween('height', [currentHeight, height]);
		$$(menuPanelClass).setStyle('border-bottom', '1px solid #6B6D80');
		currentHeight = height;
	}
	
	/**
	* Cache un onglet du menu principal, si closeContainer = true, cache également le paneau du menu
	*/
	function hideMenu(id, closeContainer){
		$('main_' + id).removeProperty('style');
		$('main_' + id).removeClass('clicked');
		ongletsfx[id].start('opacity', 0);
		
		if(closeContainer){
			$$(menuPanelClass).tween('height', [currentHeight, 0]);
			ongletsfx[id].start('opacity', 0);
			$$(menuPanelClass).setStyle('border-bottom', '0');
			currentHeight = 0;
			activeMenu = null;
			
			$$('.nav .selected_off').toggleClass('selected');
			$$('.nav .selected_off').toggleClass('selected_off');
		}
	}
}

